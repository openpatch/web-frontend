import { useMemo } from 'react';
import {
  createMuiTheme,
  ThemeProvider as MuiThemeProvider,
  ThemeProviderProps as MuiThemeProviderProps,
} from '@material-ui/core/styles';
import { getCookie } from '@utils/cookies';

export const defaultPrimary = '#006f95';
export const defaultSecondary = '#98ff98';

export type ThemeProviderViewProps = {
  darkMode?: boolean;
  primary?: string;
  secondary?: string;
} & Omit<MuiThemeProviderProps, "theme">;

export const ThemeProviderView = ({
  darkMode = false,
  primary = defaultPrimary,
  secondary = defaultSecondary,
  ...props
}: ThemeProviderViewProps) => {
  const theme = useMemo(
    () =>
      createMuiTheme({
        palette: {
          type: darkMode ? 'dark' : 'light',
          primary: {
            main: primary,
          },
          secondary: {
            main: secondary,
          },
        },
      }),
    [darkMode, primary, secondary]
  );
  return <MuiThemeProvider theme={theme} {...props} />;
};

export type ThemeProviderProps = Omit<ThemeProviderViewProps, 'darkMode'>;

export const ThemeProvider = (props: ThemeProviderProps) => {
  const darkMode = getCookie(null, 'darkMode') === 'true';

  return <ThemeProviderView darkMode={darkMode} {...props} />;
};
