declare module 'mui-undraw/lib/*' {
  import React from 'react';
  type UndrawProps = {
    color?: 'primary' | 'secondary';
  } & React.SVGProps<SVGElement>;
  export default function ({ color }: UndrawProps): React.ReactElement;
}

declare module 'ace-builds/src-noconflict/ace' {
  import { Ace } from 'ace-builds';
  class Document extends Ace.Document {
    constructor(source: string) {
      super(source);
    }
  }
  export function require(path: 'ace/document'): { Document };
}
