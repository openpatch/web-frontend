import DrawerLayout from '@components/DrawerLayout';
import ItemMetaForm, { ItemMetaFormProps } from '@components/ItemMetaForm';
import { t } from '@lingui/macro';
import {
  getClaimsFromContext,
  loginRedirect,
} from '@services/authentification/provider';
import { postItems } from '@services/itembank/items';
import { useNotifications } from '@utils/notification';
import { GetServerSideProps } from 'next';
import { useRouter } from 'next/router';

function NewItem() {
  const router = useRouter();
  const [add] = useNotifications();
  const handleSubmit: ItemMetaFormProps['onSubmit'] = async (values) => {
    return new Promise((resolve) => {
      postItems(null, values)
        .then(([itemId]) => {
          add({
            message: t`Item created`,
            severity: 'success',
          });
          router.push('/item/[id]/edit', `/item/${itemId}/edit`);
          resolve();
        })
        .catch((e) => {
          add({
            message: t`Something went wrong`,
            severity: 'error',
          });
          resolve(e.details);
        });
    });
  };
  return (
    <DrawerLayout
      breadcrumbs={[
        {
          href: '/dashboard',
          title: t`Dashboard`,
        },
        {
          href: `/items`,
          title: t`Items`,
        },
        {
          href: `/item/new`,
          title: t`New`,
          active: true,
        },
      ]}
    >
      <ItemMetaForm onSubmit={handleSubmit} submitVariant="fab" />
    </DrawerLayout>
  );
}

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const claims = getClaimsFromContext(ctx);
  if (!claims) {
    return loginRedirect(ctx);
  }

  return {
    props: {
      claims,
    },
  };
};

export default NewItem;
