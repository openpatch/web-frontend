import Comments from '@components/Comments';
import DrawerLayout from '@components/DrawerLayout';
import EditFab from '@components/EditFab';
import ItemMetaView from '@components/ItemMetaView';
import ItemVersionData from '@components/ItemVersionData';
import ItemVersionEvaluation from '@components/ItemVersionEvaluation';
import TabPanel from '@components/TabPanel';
import { t, Trans } from '@lingui/macro';
import Box from '@material-ui/core/Box';
import MenuItem from '@material-ui/core/MenuItem';
import Paper from '@material-ui/core/Paper';
import Select from '@material-ui/core/Select';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import Typography from '@material-ui/core/Typography';
import {
  getClaimsFromContext,
  useClaims,
} from '@services/authentification/provider';
import {
  getItem,
  GetItemResponse,
  getItemVersion,
  GetItemVersionResponse,
  useItem,
  useItemVersion,
} from '@services/itembank/items';
import { convertToText } from '@utils/editor';
import { AxiosResponse } from 'axios';
import { GetServerSideProps } from 'next';
import { NextSeo } from 'next-seo';
import Router from 'next/router';
import { ChangeEvent, useState } from 'react';

export type IdProps = {
  id: string;
  initialVersion: number | 'latest' | 'draft';
  initialData: AxiosResponse<GetItemResponse>;
  initialVersionData: AxiosResponse<GetItemVersionResponse>;
};

function Id({ id, initialVersion, initialData, initialVersionData }: IdProps) {
  const claims = useClaims();
  const [tab, setTab] = useState(0);
  const [version, setVersion] = useState(initialVersion);

  const { item, revalidate } = useItem(id, { initialData });
  const { itemVersion } = useItemVersion(id, version, {
    initialData: initialVersionData,
  });

  function handleTabChange(e: any, newValue: number) {
    setTab(newValue);
  }

  function handleVersionChange(
    e: ChangeEvent<{
      value: unknown;
    }>
  ) {
    setVersion(e.currentTarget.value as IdProps['initialVersion']);
  }

  const versions = item?.versions || [];

  return (
    <>
      <NextSeo
        title={item?.name}
        description={convertToText(item?.publicDescription)}
        openGraph={{
          title: item?.name,
          description: convertToText(item?.publicDescription),
          images: [
            {
              url: `https://og.openpatch.app/${item?.name}.png?theme=light&username=${item?.member?.username}&type=Task`,
            },
          ],
        }}
      />
      <DrawerLayout
        breadcrumbs={[
          {
            href: '/dashboard',
            title: t`Dashboard`,
          },
          {
            href: `/items`,
            title: t`Items`,
          },
          {
            href: `/item/[id]`,
            as: `/item/${id}`,
            title: item?.name || id,
            active: true,
          },
        ]}
        fab={
          claims?.id == item?.member?.id && (
            <EditFab
              title={t`Edit Item`}
              onClick={() => Router.push('/item/[id]/edit', `/item/${id}/edit`)}
            />
          )
        }
      >
        <Paper square style={{ marginBottom: 16 }}>
          <Box display="flex" alignItems="start">
            <Tabs
              value={tab}
              style={{ flex: 1 }}
              onChange={handleTabChange}
              indicatorColor="primary"
              textColor="primary"
              variant="scrollable"
              scrollButtons="auto"
            >
              <Tab label={<Trans>View</Trans>} />
              <Tab label={<Trans>Evaluation</Trans>} />
            </Tabs>
            {itemVersion?.version && versions.length > 0 && (
              <Select
                value={itemVersion?.version}
                onChange={handleVersionChange}
                style={{
                  width: 150,
                }}
              >
                {versions.map(({ status, version, versionMessage }) => (
                  <MenuItem key={version} value={version}>
                    <Box display="flex" flexDirection="column">
                      <Typography>
                        v{version} ({status})
                      </Typography>
                      <Typography variant="caption">
                        {versionMessage}
                      </Typography>
                    </Box>
                  </MenuItem>
                ))}
              </Select>
            )}
          </Box>
          <TabPanel value={tab} index={0}>
            <ItemVersionData {...itemVersion} />
          </TabPanel>
          <TabPanel value={tab} index={1}>
            <ItemVersionEvaluation {...itemVersion} />
          </TabPanel>
        </Paper>
        {item && (
          <Paper square style={{ marginBottom: 16, padding: 16 }}>
            <ItemMetaView {...item} revalidate={revalidate} />
          </Paper>
        )}
        <Paper square style={{ padding: 16 }}>
          {item?.id && <Comments id={item?.id} type="item" />}
        </Paper>
      </DrawerLayout>
    </>
  );
}

export const getServerSideProps: GetServerSideProps<IdProps> = async (ctx) => {
  const claims = getClaimsFromContext(ctx);
  const id = String(ctx.query.id);
  const version = Number(ctx.query.version);
  const [, initialData] = await getItem(ctx, id);
  const initialVersion = version || 'latest';
  const [, initialVersionData] = await getItemVersion(ctx, id, initialVersion);

  return {
    props: {
      claims,
      id,
      initialData,
      initialVersionData,
      initialVersion,
    },
  };
};

export default Id;
