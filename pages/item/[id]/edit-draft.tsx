import DrawerLayout from '@components/DrawerLayout';
import ItemVersionForm from '@components/ItemVersionForm';
import { t } from '@lingui/macro';
import {
  getClaimsFromContext,
  loginRedirect,
} from '@services/authentification/provider';
import {
  getItemVersion,
  GetItemVersionResponse,
  ItemVersion,
  putItemVersionResponse,
  useItem,
  useItemVersion,
} from '@services/itembank/items';
import { useNotifications } from '@utils/notification';
import { AxiosResponse } from 'axios';
import { GetServerSideProps } from 'next';

type EditDraftProps = {
  id: string;
  initialData: AxiosResponse<GetItemVersionResponse>;
};

function EditDraft({ id, initialData }: EditDraftProps) {
  const { item } = useItem(id);
  const { itemVersion, revalidate } = useItemVersion(id, 'draft', {
    initialData,
    revalidateOnFocus: false,
    revalidateOnReconnect: false,
  });
  const [add] = useNotifications();

  function handleSubmit(values: ItemVersion) {
    return new Promise((resolve, reject) => {
      putItemVersionResponse(null, id, values)
        .then(() => {
          add({
            message: t`Draft saved`,
            severity: 'success',
          });
          revalidate({});
          return resolve(true);
        })
        .catch((e) => {
          if (e?.response?.data?.code === 200) {
            add({
              message: t`A validation error occured. Please check, if you have filled
                  out all required fields.`,
              severity: 'error',
            });
            return resolve(e?.response?.data?.details);
          } else {
            add({
              message: t`Something went wrong`,
              severity: 'error',
            });
            return reject();
          }
        });
    });
  }

  return (
    <DrawerLayout
      breadcrumbs={[
        {
          href: '/dashboard',
          title: t`Dashboard`,
        },
        {
          href: `/items`,
          title: t`Items`,
        },
        {
          href: `/item/[id]`,
          as: `/item/${id}`,
          title: item?.name || id,
        },
        {
          href: `/item/[id]/edit`,
          as: `/item/${id}/edit`,
          title: t`Edit`,
        },
        {
          href: `/item/[id]/edit-draft`,
          as: `/item/${id}/edit-draft`,
          title: t`Draft`,
          active: true,
        },
      ]}
    >
      {itemVersion && (
        <ItemVersionForm
          submitVariant="fab"
          onSubmit={handleSubmit}
          initialValues={itemVersion}
        />
      )}
    </DrawerLayout>
  );
}

export const getServerSideProps: GetServerSideProps<EditDraftProps> = async (
  ctx
) => {
  const claims = getClaimsFromContext(ctx);
  if (!claims) {
    return loginRedirect(ctx);
  }
  const id = ctx.query.id as string;

  const [, initialData] = await getItemVersion(ctx, id, 'draft');

  return {
    props: {
      claims,
      id,
      initialData,
    },
  };
};

export default EditDraft;
