import DrawerLayout from '@components/DrawerLayout';
import ItemMetaForm, { ItemMetaFormProps } from '@components/ItemMetaForm';
import { t } from '@lingui/macro';
import {
  getClaimsFromContext,
  loginRedirect,
} from '@services/authentification/provider';
import {
  getItem,
  GetItemResponse,
  putItem,
  useItem,
} from '@services/itembank/items';
import { useNotifications } from '@utils/notification';
import { AxiosResponse } from 'axios';
import { GetServerSideProps } from 'next';

type EditProps = {
  id: string;
  initialData: AxiosResponse<GetItemResponse>;
};

function Edit({ id, initialData }: EditProps) {
  const { item, revalidate } = useItem(id, {
    revalidateOnFocus: false,
    revalidateOnReconnect: false,
    initialData,
  });
  const [add] = useNotifications();

  const handleSubmit: ItemMetaFormProps['onSubmit'] = async (values) => {
    return new Promise((resolve) => {
      putItem(null, id, values)
        .then(() => {
          add({
            message: t`Item saved`,
            severity: 'success',
          });
          resolve();
        })
        .catch((e) => {
          add({
            message: t`Something went wrong`,
            severity: 'error',
          });
          resolve(e.details);
        });
    });
  };

  return (
    <DrawerLayout
      breadcrumbs={[
        {
          href: '/dashboard',
          title: t`Dashboard`,
        },
        {
          href: `/items`,
          title: t`Items`,
        },
        {
          href: `/item/[id]`,
          as: `/item/${id}`,
          title: item?.name || id,
        },
        {
          href: `/item/[id]/edit`,
          as: `/item/${id}/edit`,
          title: t`Edit`,
          active: true,
        },
      ]}
    >
      {item && (
        <ItemMetaForm
          submitVariant="fab"
          onSubmit={handleSubmit}
          initialValues={item}
          revalidate={() => revalidate({})}
        />
      )}
    </DrawerLayout>
  );
}

export const getServerSideProps: GetServerSideProps<EditProps> = async (
  ctx
) => {
  const claims = getClaimsFromContext(ctx);
  if (!claims) {
    return loginRedirect(ctx);
  }
  const id = ctx.query.id as string;

  const [, initialData] = await getItem(ctx, id);

  return {
    props: {
      claims,
      id,
      initialData,
    },
  };
};

export default Edit;
