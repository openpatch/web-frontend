import BlankCenteredLayout from '@components/BlankCenteredLayout';
import Logo from '@components/Logo';
import { t, Trans } from '@lingui/macro';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import TextField from '@material-ui/core/TextField';
import { postResetPassword } from '@services/authentification/root';
import { useNotifications } from '@utils/notification';
import { useRouter } from 'next/router';
import { FormEvent, MouseEvent, useState } from 'react';

function resetPassword() {
  const router = useRouter();
  const [add] = useNotifications();
  const [password, setPassword] = useState('');
  const [passwordRepeat, setPasswordRepeat] = useState('');
  const token = router.query.token;

  function handleReset(e: MouseEvent | FormEvent) {
    e.preventDefault();

    if (
      password == passwordRepeat &&
      password.length >= 8 &&
      password.length <= 64 &&
      !Array.isArray(token)
    ) {
      postResetPassword(null, password, token)
        .then(() => {
          router.push('/login');
          add({
            message: t`New password is set.`,
            severity: 'success',
          });
        })
        .catch(() => {
          add({
            message: t`Something went wrong.`,
            severity: 'error',
          });
        });
    }
  }

  return (
    <BlankCenteredLayout>
      <Box width={400}>
        <Box display="flex" alignItems="center" justifyContent="center" mb={2}>
          <Logo size="large" />
        </Box>
        <Card component="form" onSubmit={handleReset}>
          <CardContent>
            <TextField
              label={<Trans>Password</Trans>}
              variant="outlined"
              required
              margin="dense"
              fullWidth
              type="password"
              value={password}
              helperText={
                <Trans>Needs to be between 8 and 64 characters long</Trans>
              }
              error={password.length < 8 || password.length >= 64}
              onChange={(e) => setPassword(e.target.value)}
            />
            <TextField
              label={<Trans>Password Repeat</Trans>}
              variant="outlined"
              margin="dense"
              fullWidth
              required
              type="password"
              value={passwordRepeat}
              error={passwordRepeat !== password}
              onChange={(e) => setPasswordRepeat(e.target.value)}
            />
          </CardContent>
          <CardActions>
            <Button
              type="submit"
              onClick={handleReset}
              color="primary"
              fullWidth
            >
              <Trans>Reset Password</Trans>
            </Button>
          </CardActions>
        </Card>
      </Box>
    </BlankCenteredLayout>
  );
}

export default resetPassword;
