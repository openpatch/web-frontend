import AppBarLayout from '@components/AppBarLayout';
import RichText from '@components/RichText';
import { getLatestPolicy, Policy } from '@services/authentification/policies';
import { convertFromRaw } from '@utils/editor';
import { Locale } from '@utils/i18n';
import { GetServerSideProps } from 'next';

type TypeProps = {
  policy: Policy;
};

function Type({ policy }: TypeProps) {
  return (
    <AppBarLayout>
      <RichText editorState={convertFromRaw(policy?.content)} />
    </AppBarLayout>
  );
}

export const getServerSideProps: GetServerSideProps<TypeProps> = async (
  ctx
) => {
  const t = ctx.query.type as string;
  const locale = ctx.locale as Locale;
  const [policy] = await getLatestPolicy(ctx, t, locale);

  return {
    props: {
      policy,
    },
  };
};

export default Type;
