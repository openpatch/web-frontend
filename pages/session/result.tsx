import ItemResultDialog from '@components/ItemResultDialog';
import SessionLayout from '@components/SessionLayout';
import SessionResult from '@components/SessionResult';
import { ItemVersionCache } from '@services/assessment/assessments';
import {
  getResult,
  GetResultResponse,
  useResult,
} from '@services/assessment/session/result';
import { AxiosResponse } from 'axios';
import { GetServerSideProps } from 'next';
import { useState } from 'react';

type ResultProps = {
  session: string;
  initialData: AxiosResponse<GetResultResponse>;
};

function Result({ session, initialData }: ResultProps) {
  const { itemResults, itemVersions, score, aborted } = useResult(session, {
    initialData,
  });
  const [open, setOpen] = useState(false);
  const [itemVersion, setItemVersion] = useState<ItemVersionCache | null>(null);
  const [solution, setSolution] = useState(null);

  function handleClose() {
    setOpen(false);
  }

  function handleShow(itemId: string, itemVersion: number, solution: any) {
    const itemVersionTmp = itemVersions.find(
      (iv) => iv.item == itemId && iv.version == itemVersion
    );
    if (itemVersionTmp) {
      setItemVersion(itemVersionTmp);
      setSolution(solution);
      setOpen(true);
    }
  }

  return (
    <SessionLayout>
      <ItemResultDialog
        open={open}
        itemVersion={itemVersion}
        onClose={handleClose}
        solution={solution}
      />
      <SessionResult
        itemResults={itemResults}
        itemVersions={itemVersions}
        score={score}
        aborted={aborted}
        onShow={handleShow}
      />
    </SessionLayout>
  );
}

export const getServerSideProps: GetServerSideProps<ResultProps> = async (
  ctx
) => {
  const session = ctx.query.session as string;
  const [, initialData] = await getResult(ctx, session);

  return {
    props: {
      session,
      initialData,
    },
  };
};

export default Result;
