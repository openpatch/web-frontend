import TextField from '@components/fields/TextField';
import SessionLayout from '@components/SessionLayout';
import { Trans } from '@lingui/macro';
import { Button } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import { postPrepare } from '@services/assessment/session/prepare';
import { GetServerSideProps } from 'next';
import Router from 'next/router';
import { FormEvent, useState } from 'react';

type PrepareState =
  | 'PREPARING'
  | 'NOT_FOUND'
  | 'EXPIRED'
  | 'INACTIVE'
  | 'PROTECTED'
  | 'ERROR';

type PrepareProps = {
  id: string;
  initialState: PrepareState;
};

function Prepare({ id, initialState }: PrepareProps) {
  const [state, setState] = useState<PrepareState>(initialState);
  const [password, setPassword] = useState('');

  function handlePassword(e: FormEvent<HTMLInputElement>) {
    setPassword(e.currentTarget.value);
  }

  function prepare() {
    setState('PREPARING');
    postPrepare(null, id, password)
      .then((session) => {
        Router.push(`/session/welcome?session=${session}`);
      })
      .catch((e) => {
        if (e?.response?.status === 404) {
          setState('NOT_FOUND');
        } else {
          if (e?.response?.data?.code == 4) {
            setState('EXPIRED');
          } else if (e?.response?.data?.code == 5) {
            setState('INACTIVE');
          } else if (e?.response?.data?.code == 7) {
            setState('PROTECTED');
          } else {
            setState('ERROR');
          }
        }
      });
  }

  return (
    <SessionLayout>
      {state === 'PROTECTED' && (
        <>
          <TextField
            input={{
              name: 'password',
              value: password,
              onChange: handlePassword,
              onBlur: () => null,
              onFocus: () => null,
            }}
            meta={{}}
            label={<Trans>Password</Trans>}
            type="password"
          />
          <Button onClick={prepare}>
            <Trans>Open Protected Assessment</Trans>
          </Button>
        </>
      )}
      <Box position="fixed" top="50%" left="50%">
        {state === 'PREPARING' && <Trans>Preparing the Test</Trans>}
        {state === 'NOT_FOUND' && <Trans>Test not found</Trans>}
        {state === 'ERROR' && <Trans>An error occurred</Trans>}
      </Box>
    </SessionLayout>
  );
}

export const getServerSideProps: GetServerSideProps<PrepareProps> = async (
  ctx
) => {
  const id = ctx.query.assessment as string;
  try {
    const { session } = await postPrepare(ctx, id, '');
    return {
      redirect: {
        destination: `/session/welcome?session=${encodeURIComponent(session)}`,
        permanent: false,
      },
    };
  } catch (e) {
    let state: PrepareState = 'ERROR';
    if (e?.response?.status === 404) {
      state = 'NOT_FOUND';
    } else {
      if (e?.response?.data?.code == 4) {
        state = 'EXPIRED';
      } else if (e?.response?.data?.code == 5) {
        state = 'INACTIVE';
      } else if (e?.response?.data?.code == 7) {
        state = 'PROTECTED';
      }
    }
    return {
      props: {
        id,
        initialState: state,
      },
    };
  }
};

export default Prepare;
