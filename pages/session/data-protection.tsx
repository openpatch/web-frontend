import SessionDataProtection from '@components/SessionDataProtection';
import SessionLayout from '@components/SessionLayout';
import { t, Trans } from '@lingui/macro';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import {
  getDataProtection,
  GetDataProtectionReponse,
  useDataProtection,
} from '@services/assessment/session/dataProtection';
import { Locale } from '@utils/i18n';
import { useNotifications } from '@utils/notification';
import { AxiosResponse } from 'axios';
import { GetServerSideProps } from 'next';
import Router from 'next/router';
import { useState } from 'react';

type DataProtectionProps = {
  session: string;
  locale: Locale;
  initialData: AxiosResponse<GetDataProtectionReponse>;
};

function DataProtection({ session, locale, initialData }: DataProtectionProps) {
  const [isAccepted, setIsAccepted] = useState(false);
  const { dataProtection, record, encrypt, accept } = useDataProtection(
    session,
    { initialData }
  );
  const [add] = useNotifications();

  function handleStart() {
    accept()
      .then(() => {
        Router.push(`/session/items?session=${session}`);
      })
      .catch(() => {
        add({
          message: t`Something went wrong. Please try again`,
          severity: 'error',
        });
      });
  }

  return (
    <SessionLayout
      locale={locale}
      toolbarBottom={{
        right: (
          <Button disabled={!accept} onClick={handleStart}>
            <Trans>Start</Trans>
          </Button>
        ),
      }}
    >
      <Box m={3}>
        <SessionDataProtection
          onAccept={() => setIsAccepted((a) => !a)}
          accept={isAccepted}
          record={record}
          encrypt={encrypt}
          dataProtection={dataProtection}
        />
      </Box>
    </SessionLayout>
  );
}

export const getServerSideProps: GetServerSideProps<DataProtectionProps> = async (
  ctx
) => {
  const session = ctx.query.session as string;
  const [{ locale }, initialData] = await getDataProtection(ctx, session);
  return {
    props: {
      session,
      initialData,
      locale,
    },
  };
};

export default DataProtection;
