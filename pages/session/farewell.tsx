import RichText from '@components/RichText';
import SessionLayout from '@components/SessionLayout';
import { Trans } from '@lingui/macro';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import {
  getFarewell,
  GetFarewellReponse,
  useFarewell,
} from '@services/assessment/session/farewell';
import { convertFromRaw } from '@utils/editor';
import { Locale } from '@utils/i18n';
import { AxiosResponse } from 'axios';
import { GetServerSideProps } from 'next';
import Link from 'next/link';

type FarewellProps = {
  session: string;
  initialData: AxiosResponse<GetFarewellReponse>;
  locale: Locale;
};

function Farewell({ locale, session, initialData }: FarewellProps) {
  const { farewell, showResult } = useFarewell(session, { initialData });

  return (
    <SessionLayout
      locale={locale}
      toolbarBottom={{
        right: showResult && (
          <Link href={`/session/result?session=${session}`}>
            <Button>
              <Trans>Show Result</Trans>
            </Button>
          </Link>
        ),
      }}
    >
      <Box m={2}>
        <RichText editorState={convertFromRaw(farewell)} />
      </Box>
    </SessionLayout>
  );
}

export const getServerSideProps: GetServerSideProps<FarewellProps> = async (
  ctx
) => {
  const session = ctx.query.session as string;
  const [{ locale }, initialData] = await getFarewell(ctx, session);
  return {
    props: {
      session,
      locale,
      initialData,
    },
  };
};

export default Farewell;
