import ItemsRenderer from '@components/ItemsRenderer';
import {
  SessionItemsToolbarCenter,
  SessionItemsToolbarLeft,
  SessionItemsToolbarRight,
} from '@components/SessionItemsToolbars';
import SessionLayout from '@components/SessionLayout';
import { SessionState, useSubmit } from '@services/assessment/session';
import { postAbort } from '@services/assessment/session/abort';
import { TimeAction } from '@services/assessment/session/actions';
import { useItems } from '@services/assessment/session/items';
import { SessionProvider } from '@services/assessment/session/provider';
import { useRecorder } from '@services/recorder/recorder';
import { initWorker } from '@utils/openpgp';
import { GetServerSideProps } from 'next';
import { useRouter } from 'next/router';

type SubmitButtonProps = {
  disabled: boolean;
};

function SubmitButton(props: SubmitButtonProps) {
  const handleSubmit = useSubmit();

  return <SessionItemsToolbarRight {...props} onSubmit={handleSubmit} />;
}

initWorker();

type ItemsProps = {
  session: string;
};

function Items({ session }: ItemsProps) {
  const router = useRouter();

  const { items, recordingId, submit, state, encryptionPublicKey } = useItems(
    session
  );
  const { isRecording, record, stop, start } = useRecorder(recordingId);

  function handleAbort(message: string) {
    postAbort(null, session, message)
      .then(() => {
        router.push(`/session/farewell?session=${session}`);
      })
      .catch(() => {
        router.push('/');
      });
  }

  function handleSubmit(state: Pick<SessionState, 'items'>) {
    stop();
    submit(state).catch(() => {
      start();
    });
  }

  function handleRecord(action: TimeAction) {
    record({
      ...action,
      type: `${action.scope}$${action.type}`,
    });
  }

  return (
    <SessionProvider recorder={handleRecord} onSubmit={handleSubmit}>
      <SessionLayout
        toolbarBottom={{
          left: <SessionItemsToolbarLeft onAbort={handleAbort} />,
          center: (
            <SessionItemsToolbarCenter
              record={isRecording}
              encrypt={encryptionPublicKey !== null}
              session={session}
            />
          ),
          right: (
            <SubmitButton
              disabled={state === 'SUBMITTING' || state === 'FETCHING'}
            />
          ),
        }}
      >
        <ItemsRenderer items={items} />
      </SessionLayout>
    </SessionProvider>
  );
}

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const session = ctx.query.session as string;

  return {
    props: {
      session,
    },
  };
};

export default Items;
