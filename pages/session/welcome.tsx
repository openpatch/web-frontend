import RichText from '@components/RichText';
import SessionLayout from '@components/SessionLayout';
import { Trans } from '@lingui/macro';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import { getWelcome } from '@services/assessment/session/welcome';
import { convertFromRaw } from '@utils/editor';
import { Locale } from '@utils/i18n';
import { RawDraftContentState } from 'draft-js';
import { GetServerSideProps } from 'next';
import Router from 'next/router';

type WelcomeProps = {
  session: string;
  welcome: RawDraftContentState;
  locale: Locale;
};

function Welcome({ session, welcome, locale }: WelcomeProps) {
  return (
    <SessionLayout
      locale={locale}
      toolbarBottom={{
        right: (
          <Button
            onClick={() =>
              Router.push(`/session/data-protection?session=${session}`)
            }
            disabled={!welcome}
          >
            <Trans>Continue</Trans>
          </Button>
        ),
      }}
    >
      <Box m={3}>
        <RichText editorState={convertFromRaw(welcome)} />
      </Box>
    </SessionLayout>
  );
}

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const session = ctx.query.session as string;
  const [{ welcome, locale }] = await getWelcome(ctx, session);

  // TODO what should happen if not welcome message could be fetched?

  return {
    props: {
      session,
      welcome,
      locale,
    },
  };
};

export default Welcome;
