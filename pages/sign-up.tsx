import BlankCenteredLayout from '@components/BlankCenteredLayout';
import PolicyDialog from '@components/PolicyDialog';
import SignUpForm from '@components/SignUpForm';
import { t } from '@lingui/macro';
import { castLocale } from '@services/assessment/session/utils';
import { useLatestPolicy } from '@services/authentification/policies';
import { postRegister } from '@services/authentification/root';
import { useNotifications } from '@utils/notification';
import { useRouter } from 'next/router';
import { useState } from 'react';

function signUp() {
  const router = useRouter();
  const [add] = useNotifications();
  const [checkMail, setCheckEmail] = useState(false);
  const [open, setOpen] = useState(false);
  const [posting, setPosting] = useState(false);
  const { policy } = useLatestPolicy('privacy', castLocale(router.locale));
  const [username, setUsername] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const invitationCodeId = !Array.isArray(router.query['invitation-code'])
    ? router.query['invitation-code']
    : undefined;

  function handleAgree() {
    setPosting(true);
    postRegister(null, {
      username,
      password,
      email,
      policyId: policy?.id,
      invitationCodeId,
    })
      .then(() => {
        setOpen(false);
        setCheckEmail(true);
      })
      .catch((e) => {
        setPosting(false);
        if (e.response?.data?.code === 115) {
          add({
            message: t`The registration of new accounts is currently disabled`,
            severity: 'error',
          });
        } else if (e.response?.data?.code === 102) {
          add({
            message: t`Some information is missing`,
            severity: 'error',
          });
        } else if (e.response?.data?.code === 103) {
          add({
            message: t`The password is invalid`,
            severity: 'error',
          });
        } else if (e.response?.data?.code === 104) {
          add({
            message: t`The username is already in use`,
            severity: 'error',
          });
        } else if (e.response?.data?.code === 105) {
          add({
            message: t`The email is already in use`,
            severity: 'error',
          });
        }
      });
  }

  function handleSignUp({
    username,
    password,
    email,
  }: {
    username: string;
    password: string;
    email: string;
  }) {
    setUsername(username);
    setPassword(password);
    setEmail(email);
    setOpen(true);
  }

  return (
    <BlankCenteredLayout>
      <SignUpForm
        loading={posting}
        onSignUp={handleSignUp}
        checkMail={checkMail}
        invitationCode={invitationCodeId}
      />
      {policy && (
        <PolicyDialog
          {...policy}
          open={open}
          loading={posting}
          onClose={() => setOpen(false)}
          onAgree={handleAgree}
        />
      )}
    </BlankCenteredLayout>
  );
}

export default signUp;
