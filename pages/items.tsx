import CreateFab from '@components/CreateFab';
import DrawerLayout from '@components/DrawerLayout';
import ItemCard from '@components/ItemCard';
import NoEntriesCreate from '@components/NoEntriesCreate';
import Pagination from '@components/Pagination';
import { t } from '@lingui/macro';
import { getClaimsFromContext } from '@services/authentification/provider';
import { urls } from '@services/itembank';
import { getItems, GetItemsResponse, Item } from '@services/itembank/items';
import { QueryFilter } from '@utils/api';
import { AxiosResponse } from 'axios';
import { GetServerSideProps } from 'next';
import Router from 'next/router';

type ItemsProps = {
  my: boolean;
  filter: QueryFilter;
  initialData: AxiosResponse<GetItemsResponse>[];
};

function Items({ my, filter, initialData }: ItemsProps) {
  const url = `${urls.browser}/v1/items`;
  const selector = (data: GetItemsResponse) => data.items;

  return (
    <DrawerLayout
      breadcrumbs={[
        {
          href: '/dashboard',
          title: t`Dashboard`,
        },
        {
          href: my ? '/items?my=true' : '/items',
          title: t`Items`,
          active: true,
        },
      ]}
      fab={
        <CreateFab
          title={t`Create Item`}
          onClick={() => Router.push('/item/new')}
        />
      }
    >
      <Pagination<GetItemsResponse, Item>
        initialData={initialData}
        url={url}
        filter={filter}
        orderBy="createdOn"
        order="desc"
        placeholder={<NoEntriesCreate />}
        selector={selector}
        Renderer={ItemCard}
      />
    </DrawerLayout>
  );
}

export const getServerSideProps: GetServerSideProps<ItemsProps> = async (
  ctx
) => {
  const claims = getClaimsFromContext(ctx);
  const my = ctx.query.my;
  let filter: QueryFilter = {};
  if (my && claims?.id) {
    filter = {
      memberId: {
        equals: claims?.id,
      },
    };
  }

  const [, data] = await getItems(ctx, { filter });

  return {
    props: {
      claims,
      my: my === 'true',
      filter,
      initialData: [data],
    },
  };
};

export default Items;
