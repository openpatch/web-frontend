import CollectionMetaForm, {
  CollectionMetaFormProps,
} from '@components/CollectionMetaForm';
import { t } from '@lingui/macro';
import {
  getClaimsFromContext,
  loginRedirect,
} from '@services/authentification/provider';
import { postCollections } from '@services/itembank/collections';
import { GetServerSideProps } from 'next';
import { useRouter } from 'next/router';
import DrawerLayout from '../../components/DrawerLayout';
import { useNotifications } from '../../utils/notification';

function NewCollection() {
  const router = useRouter();
  const [add] = useNotifications();

  const handleSubmit: CollectionMetaFormProps['onSubmit'] = async (values) => {
    return new Promise((resolve) => {
      postCollections(null, values)
        .then(() => {
          add({
            message: t`Collection created`,
            severity: 'success',
          });
          router.push(`/collections?my=true`);
          resolve();
        })
        .catch((e) => {
          add({
            message: t`Something went wrong`,
            severity: 'error',
          });
          resolve(e.details);
        });
    });
  };

  return (
    <DrawerLayout
      breadcrumbs={[
        {
          href: '/dashboard',
          title: t`Dashboard`,
        },
        {
          href: `/collections`,
          title: t`Collections`,
        },
        {
          href: `/collection/new`,
          as: `/collection/new`,
          title: t`New`,
          active: true,
        },
      ]}
    >
      <CollectionMetaForm onSubmit={handleSubmit} submitVariant="fab" />
    </DrawerLayout>
  );
}

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const claims = getClaimsFromContext(ctx);
  if (!claims) {
    return loginRedirect(ctx);
  }

  return {
    props: {
      claims,
    },
  };
};

export default NewCollection;
