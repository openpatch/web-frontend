import CollectionMetaView from '@components/CollectionMetaView';
import Comments from '@components/Comments';
import DrawerLayout from '@components/DrawerLayout';
import EditFab from '@components/EditFab';
import ItemCard from '@components/ItemCard';
import NoEntries from '@components/NoEntries';
import TabPanel from '@components/TabPanel';
import TestCard from '@components/TestCard';
import { t, Trans } from '@lingui/macro';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import {
  getClaimsFromContext,
  useClaims,
} from '@services/authentification/provider';
import {
  getCollection,
  GetCollectionResponse,
  useCollection,
} from '@services/itembank/collections';
import { convertToText } from '@utils/editor';
import { AxiosResponse } from 'axios';
import { GetServerSideProps } from 'next';
import { NextSeo } from 'next-seo';
import Router from 'next/router';
import { useState } from 'react';

type IdProps = {
  id: string;
  initialData: AxiosResponse<GetCollectionResponse>;
};

function Id({ id, initialData }: IdProps) {
  const claims = useClaims();
  const { collection, items, tests } = useCollection(id, {
    initialData,
  });
  const [tab, setTab] = useState(0);

  return (
    <>
      <NextSeo
        title={collection?.name}
        description={convertToText(collection?.publicDescription)}
        openGraph={{
          title: collection?.name,
          description: convertToText(collection?.publicDescription),
          images: [
            {
              url: `https://og.openpatch.app/${collection?.name}.png?theme=light&username=${collection?.member?.username}&type=Task`,
            },
          ],
        }}
      />
      <DrawerLayout
        breadcrumbs={[
          {
            href: '/dashboard',
            title: t`Dashboard`,
          },
          {
            href: '/collections',
            title: t`Collections`,
          },
          {
            href: `/collection/[id]`,
            as: `/collection/${id}`,
            title: collection?.name || id,
            active: true,
          },
        ]}
        fab={
          claims?.id == collection?.member?.id && (
            <EditFab
              title={t`Edit Collection`}
              onClick={() =>
                Router.push(
                  '/collection/[id]/edit',
                  `/collection/${collection?.id}/edit`
                )
              }
            />
          )
        }
      >
        <Tabs
          value={tab}
          style={{ flex: 1 }}
          onChange={(e, newValue) => setTab(newValue)}
          indicatorColor="primary"
          textColor="primary"
          variant="scrollable"
          scrollButtons="auto"
        >
          <Tab label={<Trans>Items</Trans>} />
          <Tab label={<Trans>Tests</Trans>} />
        </Tabs>
        <TabPanel value={tab} index={0}>
          {items && items.length > 0 ? (
            <Grid container spacing={3} justify="center" alignItems="stretch">
              {items.map((item) => (
                <Grid item key={item.id} xs={12}>
                  <ItemCard {...item} />
                </Grid>
              ))}
            </Grid>
          ) : (
            <NoEntries />
          )}
        </TabPanel>
        <TabPanel value={tab} index={1}>
          {tests && tests.length > 0 ? (
            <Grid container spacing={3} justify="center" alignItems="stretch">
              {tests.map((test) => (
                <Grid item key={test.id} xs={12}>
                  <TestCard {...test} />
                </Grid>
              ))}
            </Grid>
          ) : (
            <NoEntries />
          )}
        </TabPanel>
        {collection && (
          <Paper square style={{ marginBottom: 16, padding: 16 }}>
            <CollectionMetaView {...collection} />
          </Paper>
        )}
        <Paper square style={{ padding: 16 }}>
          {collection?.id && <Comments id={collection?.id} type="collection" />}
        </Paper>
      </DrawerLayout>
    </>
  );
}

export const getServerSideProps: GetServerSideProps<IdProps> = async (ctx) => {
  const claims = getClaimsFromContext(ctx);
  const id = String(ctx.query.id);
  const [, initialData] = await getCollection(ctx, id);

  return {
    props: {
      claims,
      id,
      initialData,
    },
  };
};

export default Id;
