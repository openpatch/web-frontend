import CollectionMetaForm, {
  CollectionMetaFormProps,
} from '@components/CollectionMetaForm';
import DnDList from '@components/DnDList';
import DrawerLayout from '@components/DrawerLayout';
import ItemCard from '@components/ItemCard';
import TabPanel from '@components/TabPanel';
import TestCard from '@components/TestCard';
import { t, Trans } from '@lingui/macro';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import {
  getClaimsFromContext,
  loginRedirect,
} from '@services/authentification/provider';
import {
  getCollection,
  GetCollectionResponse,
  putCollection,
  useCollection,
} from '@services/itembank/collections';
import { Item } from '@services/itembank/items';
import { Test } from '@services/itembank/tests';
import { useNotifications } from '@utils/notification';
import { AxiosResponse } from 'axios';
import { GetServerSideProps } from 'next';
import { useEffect, useState } from 'react';

type EditProps = {
  id: string;
  initialData: AxiosResponse<GetCollectionResponse>;
};

function Edit({ id, initialData }: EditProps) {
  const { collection, items, tests } = useCollection(id, {
    initialData,
    revalidateOnFocus: false,
    revalidateOnReconnect: false,
  });
  const [tab, setTab] = useState(0);
  const [add] = useNotifications();

  const [formTests, setTests] = useState<Test[]>([]);
  const [formItems, setItems] = useState<Item[]>([]);

  useEffect(() => {
    if (tests) {
      setTests(tests);
    }
  }, [tests]);

  useEffect(() => {
    if (items) {
      setItems(items);
    }
  }, [items]);

  const handleSubmit: CollectionMetaFormProps['onSubmit'] = async (values) => {
    return new Promise((resolve, reject) => {
      putCollection(null, id, {
        ...values,
        items: formItems.map((item) => item.id),
        tests: formTests.map((test) => test.id),
      })
        .then(() => {
          add({
            message: t`Collection saved`,
            severity: 'success',
          });
          resolve();
        })
        .catch(() => {
          add({
            message: t`Something went wrong`,
            severity: 'error',
          });
          reject();
        });
    });
  };

  return (
    <DrawerLayout
      breadcrumbs={[
        {
          href: '/dashboard',
          title: t`Dashboard`,
        },
        {
          href: `/collections`,
          title: t`Collections`,
        },
        {
          href: `/collection/[id]`,
          as: `/collection/${id}`,
          title: collection?.name || id,
        },
        {
          href: `/collection/[id]/edit`,
          as: `/collection/${id}/edit`,
          title: t`Edit`,
          active: true,
        },
      ]}
    >
      <CollectionMetaForm onSubmit={handleSubmit} initialValues={collection} />
      <Tabs
        value={tab}
        style={{ flex: 1 }}
        onChange={(e, newValue) => setTab(newValue)}
        indicatorColor="primary"
        textColor="primary"
        variant="scrollable"
        scrollButtons="auto"
      >
        <Tab label={<Trans>Items</Trans>} />
        <Tab label={<Trans>Tests</Trans>} />
      </Tabs>
      <TabPanel value={tab} index={0}>
        {formItems && (
          <DnDList value={formItems} Renderer={ItemCard} onChange={setItems} />
        )}
      </TabPanel>
      <TabPanel value={tab} index={1}>
        {formTests && (
          <DnDList value={formTests} Renderer={TestCard} onChange={setTests} />
        )}
      </TabPanel>
    </DrawerLayout>
  );
}

export const getServerSideProps: GetServerSideProps<EditProps> = async (
  ctx
) => {
  const claims = getClaimsFromContext(ctx);
  if (!claims) {
    return loginRedirect(ctx);
  }
  const id = ctx.query.id as string;

  const [, initialData] = await getCollection(ctx, id);

  return {
    props: {
      claims,
      id,
      initialData,
    },
  };
};

export default Edit;
