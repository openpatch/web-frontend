import AppBarLayout from '@components/AppBarLayout';
import Link from '@components/Link';
import Typography from '@material-ui/core/Typography';
import { getClaimsFromContext } from '@services/authentification/provider';
import { GetServerSideProps } from 'next';

function Contact() {
  return (
    <AppBarLayout>
      <Typography style={{ marginBottom: 16 }} variant="h4">
        Contact
      </Typography>
      <Typography>
        Mike Barkmin
        <br />
        Hans-Thoma-Straße 31
        <br />
        45147 Essen
        <br />
      </Typography>
      <Link href="mailto:contact@openpatch.org">contact@openpatch.org</Link>
    </AppBarLayout>
  );
}

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const claims = getClaimsFromContext(ctx);
  return {
    props: {
      claims,
    },
  };
};

export default Contact;
