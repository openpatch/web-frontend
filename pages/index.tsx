import AppBarLayout from '@components/AppBarLayout';
import VideoContainer from '@components/VideoContainer';
import { t, Trans } from '@lingui/macro';
import Box, { BoxProps } from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import { useTheme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import {
  getClaimsFromContext,
  useClaims,
} from '@services/authentification/provider';
import { getLogout } from '@services/authentification/root';
import { GetServerSideProps } from 'next';
import Link from 'next/link';

const Feature = (props: BoxProps) => (
  <Box mt={5} style={{ position: 'relative' }} mb={25} {...props} />
);

function Index() {
  const theme = useTheme();
  const claims = useClaims();

  return (
    <AppBarLayout
      right={
        claims?.username ? (
          <>
            <Link href="/dashboard" as="/dashboard">
              <Button variant="contained" color="secondary">
                <Trans>Dashboard</Trans>
              </Button>
            </Link>
            <Button
              style={{ marginLeft: theme.spacing(2) }}
              variant="outlined"
              color="secondary"
              onClick={() => getLogout(null)}
            >
              <Trans>Logout</Trans>
            </Button>
          </>
        ) : (
          <>
            <Link href="/sign-up" as="/sign-up">
              <Button variant="contained" color="secondary">
                <Trans>Sign Up</Trans>
              </Button>
            </Link>
            <Link href="/login" as="/login">
              <Button
                style={{ marginLeft: theme.spacing(2) }}
                variant="outlined"
                color="secondary"
              >
                <Trans>Login</Trans>
              </Button>
            </Link>
          </>
        )
      }
    >
      <Container
        style={{
          textAlign: 'center',
          marginTop: 50,
          marginBottom: 100,
        }}
      >
        <Typography variant="h1">OpenPatch</Typography>
        <Typography variant="h2" component="span">
          {t`Your social network for online assessments`}
        </Typography>
      </Container>
      <Container maxWidth="md">
        <Feature>
          <VideoContainer
            src="https://f002.backblazeb2.com/file/openpatch-static/interactive.mp4"
            title={t`Interactive Tasks`}
            description={t`Highlighting, Puzzles and Coding to name a few. Interactive tasks are a great way to make use of the digital format. Each tasks can automatically be evaluated.`}
            maxWidth={500}
          />
        </Feature>
        <Feature>
          <VideoContainer
            src="https://f002.backblazeb2.com/file/openpatch-static/test.mp4"
            title={t`Adaptive Tests`}
            reverse
            description={t`Define multiple paths in your tests by using our easy drag and drop test creation tool. This allows you to assess to a more detailed level.`}
            maxWidth={500}
          />
        </Feature>
        <Feature>
          <VideoContainer
            src="https://f002.backblazeb2.com/file/openpatch-static/social.mp4"
            title={t`Social Interactions`}
            description={t`Follow your favorite creators. Like, share and give feedback. Remix tasks or tests you like and want to adapt for your needs.`}
            maxWidth={500}
          />
        </Feature>
        <Feature>
          <VideoContainer
            src="https://f002.backblazeb2.com/file/openpatch-static/assessment.mp4"
            title={t`Assessments`}
            reverse
            description={t`Conduct assessments based on tests you or other have created. Get results and visualization in nearly realtime. Download the results for further analysis.`}
            maxWidth={500}
          />
        </Feature>
        <Feature>
          <VideoContainer
            src="https://f002.backblazeb2.com/file/openpatch-static/player.mp4"
            title={t`Recordings`}
            description={t`See every step in the process of solving one of your tasks, by reviewing the recording. Thus, you can get better insights on difficulties.`}
            maxWidth={500}
          />
        </Feature>
      </Container>
    </AppBarLayout>
  );
}

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const claims = getClaimsFromContext(ctx);

  return {
    props: { claims },
  };
};

export default Index;
