import AppBarLayout from '@components/AppBarLayout';
import PolicyForm, { PolicyFormProps } from '@components/PolicyForm';
import {
  getPolicy,
  Policy,
  putPolicy,
} from '@services/authentification/policies';
import {
  getClaimsFromContext,
  loginRedirect,
} from '@services/authentification/provider';
import { useNotifications } from '@utils/notification';
import { GetServerSideProps } from 'next';

type EditPolicy = {
  id: string;
  policy: Policy;
};

const EditPolicy = ({ id, policy }: EditPolicy) => {
  const [add] = useNotifications();

  const handleSubmit: PolicyFormProps['onSubmit'] = (values) => {
    return new Promise((resolve) => {
      putPolicy(null, id, values)
        .then(() => {
          add({
            message: 'Policy created',
            severity: 'success',
          });
          resolve(undefined);
        })
        .catch((e) => {
          add({
            message: 'Something went wrong',
            severity: 'error',
          });
          resolve(e.details);
        });
    });
  };
  return (
    <AppBarLayout inverted>
      <PolicyForm onSubmit={handleSubmit} initialValues={policy} />
    </AppBarLayout>
  );
};

export const getServerSideProps: GetServerSideProps<EditPolicy> = async (
  ctx
) => {
  const claims = getClaimsFromContext(ctx);
  if (!claims) {
    return loginRedirect(ctx);
  }

  if (claims.role !== 'admin') {
    return {
      redirect: {
        destination: '/access-not-allowed',
        permanent: false,
      },
    };
  }
  const id = ctx.query.id as string;
  const [policy] = await getPolicy(ctx, id);

  return {
    props: {
      claims,
      id,
      policy,
    },
  };
};

export default EditPolicy;
