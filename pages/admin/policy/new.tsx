import AppBarLayout from '@components/AppBarLayout';
import PolicyForm, { PolicyFormProps } from '@components/PolicyForm';
import { postPolicies } from '@services/authentification/policies';
import {
  getClaimsFromContext,
  loginRedirect,
} from '@services/authentification/provider';
import { useNotifications } from '@utils/notification';
import { GetServerSideProps } from 'next';
import Router from 'next/router';

const NewPolicy = () => {
  const [add] = useNotifications();

  const handleSubmit: PolicyFormProps['onSubmit'] = (values) => {
    return new Promise((resolve) => {
      postPolicies(null, values)
        .then(([policyId]) => {
          add({
            message: 'Policy created',
            severity: 'success',
          });
          Router.push(
            `/admin/policy/[id]/edit`,
            `/admin/policy/${policyId}/edit`
          );
          resolve(undefined);
        })
        .catch((e) => {
          add({
            message: 'Something went wrong',
            severity: 'error',
          });
          resolve(e.details);
        });
    });
  };
  return (
    <AppBarLayout inverted>
      <PolicyForm onSubmit={handleSubmit} />
    </AppBarLayout>
  );
};

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const claims = getClaimsFromContext(ctx);
  if (!claims) {
    return loginRedirect(ctx);
  }

  if (claims.role !== 'admin') {
    return {
      redirect: {
        destination: '/access-not-allowed',
        permanent: false,
      },
    };
  }

  return {
    props: {
      claims,
    },
  };
};

export default NewPolicy;
