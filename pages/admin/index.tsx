import AppBarLayout from '@components/AppBarLayout';
import Link from '@components/Link';
import {
  getClaimsFromContext,
  loginRedirect,
} from '@services/authentification/provider';
import { GetServerSideProps } from 'next';

function index() {
  return (
    <AppBarLayout>
      <Link href="/admin/policies">Policies</Link>
    </AppBarLayout>
  );
}

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const claims = getClaimsFromContext(ctx);
  if (!claims) {
    return loginRedirect(ctx);
  }

  if (claims.role !== 'admin') {
    return {
      redirect: {
        destination: '/access-not-allowed',
        permanent: false,
      },
    };
  }

  return {
    props: {
      claims,
    },
  };
};

export default index;
