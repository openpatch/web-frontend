import AppBarLayout from '@components/AppBarLayout';
import CreateFab from '@components/CreateFab';
import Pagination from '@components/Pagination';
import PolicyCard from '@components/PolicyCard';
import {
  GetPoliciesResponse,
  Policy,
} from '@services/authentification/policies';
import {
  getClaimsFromContext,
  loginRedirect,
} from '@services/authentification/provider';
import { GetServerSideProps } from 'next';
import { useRouter } from 'next/router';

function policies() {
  const router = useRouter();

  function handleNew() {
    router.push('/admin/policy/new');
  }

  return (
    <AppBarLayout
      inverted
      fab={<CreateFab title="Create Policy" onClick={handleNew} />}
    >
      <Pagination<GetPoliciesResponse, Policy>
        variant="list"
        url={`${process.env.NEXT_API_AUTHENTIFICATION_BROWSER}/v1/policies`}
        selector={(d) => d.policies}
        Renderer={PolicyCard}
      />
    </AppBarLayout>
  );
}

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const claims = getClaimsFromContext(ctx);
  if (!claims) {
    return loginRedirect(ctx);
  }

  if (claims.role !== 'admin') {
    return {
      redirect: {
        destination: '/access-not-allowed',
        permanent: false,
      },
    };
  }

  return {
    props: {
      claims,
    },
  };
};

export default policies;
