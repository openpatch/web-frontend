import '@draft-js-plugins/emoji/lib/plugin.css';
import CssBaseline from '@material-ui/core/CssBaseline';
import { castLocale } from '@services/assessment/session/utils';
import { AuthentificationProvider } from '@services/authentification/provider';
import { DefaultSeo } from 'next-seo';
import { AppProps } from 'next/dist/next-server/lib/router/router';
import { useRouter } from 'next/router';
import { Fragment, useEffect } from 'react';
import 'vis-network/styles/vis-network.css';
import I18nProvider from '../components/I18nProvider';
import SEO from '../next-seo.config';
import { ThemeProvider } from '../theme';
import { NotificationProvider } from '../utils/notification';
import { init } from '../utils/sentry';

init();

function MyApp({ Component, pageProps, err }: AppProps) {
  const router = useRouter();
  useEffect(() => {
    const jssStyles = document.querySelector('#jss-server-side');
    jssStyles?.parentElement?.removeChild(jssStyles);
  }, []);

  return (
    <Fragment>
      <DefaultSeo {...SEO} />
      <AuthentificationProvider claims={pageProps.claims}>
        <ThemeProvider>
          {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
          <CssBaseline />
          <I18nProvider locale={castLocale(router.locale)}>
            <NotificationProvider>
              <Component {...pageProps} err={err} />
            </NotificationProvider>
          </I18nProvider>
        </ThemeProvider>
      </AuthentificationProvider>
    </Fragment>
  );
}

export default MyApp;
