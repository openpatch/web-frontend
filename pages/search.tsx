import CollectionCard from '@components/CollectionCard';
import DrawerLayout from '@components/DrawerLayout';
import ItemCard from '@components/ItemCard';
import NoSearchResults from '@components/NoSearchResults';
import Pagination from '@components/Pagination';
import TabPanel from '@components/TabPanel';
import TestCard from '@components/TestCard';
import { t, Trans } from '@lingui/macro';
import Divider from '@material-ui/core/Divider';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import {
  Collection,
  GetCollectionsResponse,
} from '@services/itembank/collections';
import { GetItemsResponse, Item } from '@services/itembank/items';
import { GetTestsResponse, Test } from '@services/itembank/tests';
import { makeFilter } from '@utils/api';
import { GetServerSideProps } from 'next';
import { useState } from 'react';

type SearchProps = {
  query: string;
};

function Search({ query }: SearchProps) {
  const [tab, setTab] = useState(0);

  function handleTabChange(event: any, newValue: number) {
    setTab(newValue);
  }

  return (
    <DrawerLayout
      breadcrumbs={[
        {
          href: '/dashboard',
          title: t`Dashboard`,
        },
        {
          href: '/search',
          title: t`Search`,
          active: true,
        },
      ]}
    >
      <Tabs
        value={tab}
        onChange={handleTabChange}
        indicatorColor="primary"
        textColor="primary"
        variant="scrollable"
        scrollButtons="auto"
      >
        <Tab label={<Trans>Tests</Trans>} />
        <Tab label={<Trans>Collections</Trans>} />
        <Tab label={<Trans>Items</Trans>} />
      </Tabs>
      <Divider />
      <TabPanel value={tab} index={0}>
        <Pagination<GetTestsResponse, Test>
          url={`${process.env.NEXT_API_ITEMBANK_BROWSER}/v1/tests`}
          filter={makeFilter(query, ['name', 'publicDescriptionText'])}
          selector={(data) => data.tests}
          placeholder={<NoSearchResults />}
          Renderer={TestCard}
        />
      </TabPanel>
      <TabPanel value={tab} index={1}>
        <Pagination<GetCollectionsResponse, Collection>
          url={`${process.env.NEXT_API_ITEMBANK_BROWSER}/v1/collections`}
          filter={makeFilter(query, ['name', 'publicDescriptionText'])}
          selector={(data) => data.collections}
          placeholder={<NoSearchResults />}
          Renderer={CollectionCard}
        />
      </TabPanel>
      <TabPanel value={tab} index={2}>
        <Pagination<GetItemsResponse, Item>
          url={`${process.env.NEXT_API_ITEMBANK_BROWSER}/v1/items`}
          filter={makeFilter(query, ['name', 'publicDescriptionText'])}
          selector={(data) => data.items}
          placeholder={<NoSearchResults />}
          Renderer={ItemCard}
        />
      </TabPanel>
    </DrawerLayout>
  );
}

export const getServerSideProps: GetServerSideProps<SearchProps> = async (
  ctx
) => {
  const query = ctx.query.query;
  return {
    props: {
      query: !Array.isArray(query) ? query : '',
    },
  };
};

export default Search;
