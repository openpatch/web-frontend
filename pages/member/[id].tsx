import AssessmentCard from '@components/AssessmentCard';
import CollectionCard from '@components/CollectionCard';
import ConfirmButton from '@components/ConfirmButton';
import DrawerLayout from '@components/DrawerLayout';
import FormSection from '@components/FormSection';
import FormSectionContent from '@components/FormSectionContent';
import FormSectionHeader from '@components/FormSectionHeader';
import ItemCard from '@components/ItemCard';
import Link from '@components/Link';
import MemberHeader from '@components/MemberHeader';
import Pagination from '@components/Pagination';
import TabPanel from '@components/TabPanel';
import TestCard from '@components/TestCard';
import { t, Trans } from '@lingui/macro';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import {
  Assessment,
  GetAssessmentsResponse,
} from '@services/assessment/assessments';
import {
  GetInvationCodesResponse,
  getMember,
  GetMemberResponse,
  InvationCode,
  MemberClaims,
  postInvationCodes,
  useMember,
} from '@services/authentification/members';
import { getClaimsFromContext } from '@services/authentification/provider';
import {
  Collection,
  GetCollectionsResponse,
} from '@services/itembank/collections';
import { GetItemsResponse, Item } from '@services/itembank/items';
import { GetTestsResponse, Test } from '@services/itembank/tests';
import { QueryFilter } from '@utils/api';
import { useNotifications } from '@utils/notification';
import { AxiosResponse } from 'axios';
import { GetServerSideProps } from 'next';
import { useRouter } from 'next/router';
import { useState } from 'react';

type IdProps = {
  my: boolean;
  id: string;
  claims: MemberClaims | null;
  initialData: AxiosResponse<GetMemberResponse>;
  filter: QueryFilter;
};

function Id({ my, id, filter, claims, initialData }: IdProps) {
  const router = useRouter();
  const [add] = useNotifications();
  const [tab, setTab] = useState(0);
  const { member } = useMember(id, { initialData });

  function handleTabChange(event: any, newValue: number) {
    setTab(newValue);
  }

  function handleGiftInvitation() {
    postInvationCodes(null, id)
      .then(() => {
        add({
          message: t`You gifted a invitation code`,
          severity: 'success',
        });
      })
      .catch(() => {
        add({
          message: t`Something went wrong`,
          severity: 'error',
        });
      });
  }

  function handleRequestMoreInvitations() {
    window.location.href = `mailto:support@openpatch.org?subject=Request Invitation Codes for ${router.query.id}&body=Please describe for what you need the inviation codes and how many you would like to receive.`;
  }

  return (
    <DrawerLayout
      breadcrumbs={[
        {
          href: '/dashboard',
          title: t`Dashboard`,
        },
        {
          href: `/member/${id}`,
          title: t`Members`,
          active: true,
        },
        {
          href: `/member/${id}`,
          title: member?.username || id,
          active: true,
        },
      ]}
    >
      <MemberHeader
        username={member?.username || initialData.data.member.username}
        id={member?.id || id}
        variant="large"
        avatarId={member?.avatarId || initialData.data.member.avatarId}
      />
      <Tabs
        value={tab}
        onChange={handleTabChange}
        indicatorColor="primary"
        textColor="primary"
        variant="scrollable"
        scrollButtons="auto"
      >
        <Tab label={<Trans>Items</Trans>} />
        <Tab label={<Trans>Tests</Trans>} />
        <Tab label={<Trans>Collections</Trans>} />
        {(my || claims?.role === 'admin') && (
          <Tab label={<Trans>Assessments</Trans>} />
        )}
        {(my || claims?.role === 'admin') && (
          <Tab label={<Trans>Account</Trans>} />
        )}
      </Tabs>
      <Divider />
      <TabPanel value={tab} index={0}>
        <Pagination<GetItemsResponse, Item>
          url={`${process.env.NEXT_API_ITEMBANK_BROWSER}/v1/items`}
          filter={filter}
          order="desc"
          orderBy="createdOn"
          selector={(data) => data?.items}
          Renderer={ItemCard}
        />
      </TabPanel>
      <TabPanel value={tab} index={1}>
        <Pagination<GetTestsResponse, Test>
          url={`${process.env.NEXT_API_ITEMBANK_BROWSER}/v1/tests`}
          filter={filter}
          selector={(data) => data?.tests}
          Renderer={TestCard}
        />
      </TabPanel>
      <TabPanel value={tab} index={2}>
        <Pagination<GetCollectionsResponse, Collection>
          url={`${process.env.NEXT_API_ITEMBANK_BROWSER}/v1/collections`}
          filter={filter}
          selector={(data) => data?.collections}
          Renderer={CollectionCard}
        />
      </TabPanel>
      {(my || claims?.role === 'admin') && (
        <>
          <TabPanel value={tab} index={3}>
            <Pagination<GetAssessmentsResponse, Assessment>
              url={`${process.env.NEXT_API_ASSESSMENT_BROWSER}/v1/assessments`}
              filter={filter}
              selector={(data) => data?.assessments}
              Renderer={AssessmentCard}
            />
          </TabPanel>
          <TabPanel value={tab} index={4}>
            <FormSection>
              <FormSectionHeader>Data</FormSectionHeader>
              <FormSectionContent>
                <Button disabled fullWidth variant="outlined" color="primary">
                  <Trans>Download everything about your account</Trans>
                </Button>
                <ConfirmButton
                  disabled
                  title=""
                  text=""
                  onOK={() => null}
                  onCancel={() => null}
                  style={{ marginTop: 8 }}
                  fullWidth
                  variant="outlined"
                  color="secondary"
                >
                  <Trans>Delete your account</Trans>
                </ConfirmButton>
                <Trans>
                  We are working on an automatic solution. In the mean time
                  write us an e-mail (support@openpatch.org) and we will do it
                  manually.
                </Trans>
              </FormSectionContent>
            </FormSection>
            <FormSection>
              <FormSectionHeader>
                <Trans>Invitation Codes</Trans>
                {'  '}
                {claims?.role === 'admin' ? (
                  <Button onClick={handleGiftInvitation} size="small">
                    <Trans>Gift</Trans>
                  </Button>
                ) : (
                  <Button onClick={handleRequestMoreInvitations} size="small">
                    <Trans>Request More</Trans>
                  </Button>
                )}
              </FormSectionHeader>
              <FormSectionContent style={{ alignItems: 'center' }}>
                <Pagination<GetInvationCodesResponse, InvationCode>
                  url={`${process.env.NEXT_API_AUTHENTIFICATION_BROWSER}/v1/members/${router.query.id}/invitation-codes`}
                  variant="list"
                  spacing={1}
                  selector={(data) => data?.invitationCodes}
                  Renderer={({ used, id }) => (
                    <Box textAlign="center">
                      <Link
                        style={{
                          fontFamily: 'monospace',
                          textDecoration: used ? 'line-through' : undefined,
                        }}
                        href={`/sign-up?invitation-code=${id}`}
                      >
                        {id}
                      </Link>
                    </Box>
                  )}
                />
              </FormSectionContent>
            </FormSection>
          </TabPanel>
        </>
      )}
    </DrawerLayout>
  );
}

export const getServerSideProps: GetServerSideProps<IdProps> = async (ctx) => {
  const id = ctx.query.id as string;
  const claims = getClaimsFromContext(ctx);
  const [, initialData] = await getMember(ctx, id);
  const filter: QueryFilter = {
    memberId: {
      equals: ctx.query.id as string,
    },
  };

  return {
    props: {
      my: id === claims?.id,
      id,
      claims,
      initialData,
      filter,
    },
  };
};

export default Id;
