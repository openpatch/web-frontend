import CollectionCard from '@components/CollectionCard';
import CreateFab from '@components/CreateFab';
import DrawerLayout from '@components/DrawerLayout';
import NoEntriesCreate from '@components/NoEntriesCreate';
import Pagination from '@components/Pagination';
import { t } from '@lingui/macro';
import { getClaimsFromContext } from '@services/authentification/provider';
import { urls } from '@services/itembank';
import {
  Collection,
  getCollections,
  GetCollectionsResponse,
} from '@services/itembank/collections';
import { QueryFilter } from '@utils/api';
import { AxiosResponse } from 'axios';
import { GetServerSideProps } from 'next';
import { useRouter } from 'next/router';

type CollectionProps = {
  my: boolean;
  filter: QueryFilter;
  initialData: AxiosResponse<GetCollectionsResponse>[];
};

function Collections({ my, filter, initialData }: CollectionProps) {
  const router = useRouter();
  const url = `${urls.browser}/v1/collections`;
  const selector = (data: GetCollectionsResponse) => data.collections;

  return (
    <DrawerLayout
      breadcrumbs={[
        {
          href: '/dashboard',
          title: t`Dashboard`,
        },
        {
          href: my ? '/collections?my=true' : '/collections',
          title: t`Collections`,
          active: true,
        },
      ]}
      fab={
        <CreateFab
          title={t`Create Collection`}
          onClick={() => router.push('/collection/new')}
        />
      }
    >
      <Pagination<GetCollectionsResponse, Collection>
        url={url}
        filter={filter}
        orderBy="createdOn"
        order="desc"
        initialData={initialData}
        placeholder={<NoEntriesCreate />}
        selector={selector}
        Renderer={CollectionCard}
      />
    </DrawerLayout>
  );
}

export const getServerSideProps: GetServerSideProps<CollectionProps> = async (
  ctx
) => {
  const claims = getClaimsFromContext(ctx);
  const my = ctx.query.my;
  let filter: QueryFilter = {};
  if (my && claims?.id) {
    filter = {
      memberId: {
        equals: claims?.id,
      },
    };
  }

  const [, data] = await getCollections(ctx, { filter });

  return {
    props: {
      claims,
      my: my === 'true',
      filter,
      initialData: [data],
    },
  };
};

export default Collections;
