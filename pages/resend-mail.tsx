import BlankCenteredLayout from '@components/BlankCenteredLayout';
import Logo from '@components/Logo';
import { t, Trans } from '@lingui/macro';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { postResendEmail } from '@services/authentification/root';
import { useNotifications } from '@utils/notification';
import Mail from 'mui-undraw/lib/Mail';
import { FormEvent, MouseEvent, useState } from 'react';

function resendEmail() {
  const [add] = useNotifications();
  const [email, setEmail] = useState('');
  const [checkMail, setCheckEmail] = useState(false);

  function handleResend(e: MouseEvent | FormEvent) {
    e.preventDefault();

    postResendEmail(null, email)
      .then(() => {
        setCheckEmail(true);
      })
      .catch((e) => {
        if (e.response) {
          if (e.response?.data?.code === 110) {
            add({
              message: t`The E-Mail was not found`,
              severity: 'error',
            });
          }
        }
        add({
          message: t`Something went wrong`,
          severity: 'error',
        });
      });
  }

  return (
    <BlankCenteredLayout>
      <Box width={400}>
        <Box mb={2} display="flex" alignItems="center" justifyContent="center">
          <Logo size="large" />
        </Box>
        <Card component="form" onSubmit={handleResend}>
          {!checkMail ? (
            <>
              <CardContent>
                <TextField
                  label={<Trans>E-Mail</Trans>}
                  variant="outlined"
                  margin="dense"
                  required
                  fullWidth
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
              </CardContent>
              <CardActions>
                <Button
                  type="submit"
                  onClick={handleResend}
                  color="primary"
                  fullWidth
                >
                  <Trans>Resend E-Mail</Trans>
                </Button>
              </CardActions>
            </>
          ) : (
            <CardContent>
              <Mail />
              <Typography variant="subtitle1" align="center">
                <Trans>
                  Check your E-Mail and follow the link. We wish you much joy in
                  our community.
                </Trans>
              </Typography>
            </CardContent>
          )}
        </Card>
      </Box>
    </BlankCenteredLayout>
  );
}

export default resendEmail;
