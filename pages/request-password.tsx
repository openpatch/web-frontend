import BlankCenteredLayout from '@components/BlankCenteredLayout';
import Link from '@components/Link';
import Logo from '@components/Logo';
import { t, Trans } from '@lingui/macro';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { getResetPassword } from '@services/authentification/root';
import Mail from 'mui-undraw/lib/Mail';
import { FormEvent, MouseEvent, useState } from 'react';

function request() {
  const [email, setEmail] = useState('');
  const [checkMail, setCheckEmail] = useState(false);

  function handleReset(e: MouseEvent | FormEvent) {
    e.preventDefault();

    if (email) {
      getResetPassword(null, email).then(() => {
        setCheckEmail(true);
      });
    }
  }

  return (
    <BlankCenteredLayout>
      <Box width={400}>
        <Box display="flex" alignItems="center" justifyContent="center" mb={2}>
          <Logo size="large" />
        </Box>
        <Card component="form" onSubmit={handleReset}>
          {!checkMail ? (
            <>
              <CardContent>
                <TextField
                  label={<Trans>E-Mail</Trans>}
                  variant="outlined"
                  margin="dense"
                  required
                  fullWidth
                  error={!email}
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
              </CardContent>
              <CardActions>
                <Button
                  type="submit"
                  onClick={handleReset}
                  color="primary"
                  fullWidth
                >
                  <Trans>Request Password Reset</Trans>
                </Button>
              </CardActions>
            </>
          ) : (
            <CardContent>
              <Mail />
              <Typography variant="subtitle1" align="center">
                <Trans>
                  Check your E-Mail and follow the link to reset your password.
                </Trans>
              </Typography>
            </CardContent>
          )}
        </Card>
        {!checkMail && (
          <Box mt={2}>
            <Link
              href="/login"
              style={{ color: 'white', paddingTop: 4, cursor: 'pointer' }}
              align="center"
            >
              {t`Remembered your password? Sign In`}
            </Link>
          </Box>
        )}
      </Box>
    </BlankCenteredLayout>
  );
}

export default request;
