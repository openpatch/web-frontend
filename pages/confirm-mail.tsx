import BlankCenteredLayout from '@components/BlankCenteredLayout';
import Logo from '@components/Logo';
import { t, Trans } from '@lingui/macro';
import Box from '@material-ui/core/Box';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { postConfirmEmail } from '@services/authentification/root';
import { useNotifications } from '@utils/notification';
import Router, { useRouter } from 'next/router';
import { useEffect } from 'react';

function ConfirmEmail() {
  const router = useRouter();
  const token = router.query.token;
  const [add] = useNotifications();

  useEffect(() => {
    if (token) {
      if (!Array.isArray(token)) {
        postConfirmEmail(null, token)
          .then(() => {
            add({
              message: t`You can now log in.`,
              severity: 'success',
            });
            Router.push(`/login`);
          })
          .catch(() => {
            add({
              message: t`Something went wrong`,
              severity: 'error',
            });
            Router.push(`/`);
          });
      } else {
        add({
          message: t`Something went wrong`,
          severity: 'error',
        });
        Router.push(`/`);
      }
    }
  }, [token]);
  return (
    <BlankCenteredLayout>
      <Box width={400}>
        <Box display="flex" alignItems="center" justifyContent="center" mb={2}>
          <Logo size="large" />
        </Box>
        <Card>
          <CardContent>
            <Trans>Checking...</Trans>
          </CardContent>
        </Card>
      </Box>
    </BlankCenteredLayout>
  );
}

export default ConfirmEmail;
