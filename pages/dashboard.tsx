import DrawerLayout from '@components/DrawerLayout';
import Link from '@components/Link';
import { t, Trans } from '@lingui/macro';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import {
  getClaimsFromContext,
  loginRedirect,
} from '@services/authentification/provider';
import DataTrends from 'mui-undraw/lib/DataTrends';
import { GetServerSideProps } from 'next';

function Dashboard() {
  return (
    <DrawerLayout
      joyride={true}
      breadcrumbs={[
        {
          href: '/dashboard',
          title: t`Dashboard`,
          active: true,
        },
      ]}
    >
      <Box display="flex" flexDirection="column" alignItems="center">
        <DataTrends style={{ maxWidth: 600 }} />
        <Typography variant="subtitle1">
          <Trans>
            We are working on creating a dashboard for you. It will be here
            soon. In the meantime you can <Link href="/item/new">create</Link>{' '}
            or <Link href="/discover">discover</Link> some items.
          </Trans>
        </Typography>
      </Box>
    </DrawerLayout>
  );
}

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const claims = getClaimsFromContext(ctx);
  if (!claims) {
    return loginRedirect(ctx);
  }

  return {
    props: { claims },
  };
};

export default Dashboard;
