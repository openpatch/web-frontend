import AssessmentCard from '@components/AssessmentCard';
import CreateFab from '@components/CreateFab';
import DrawerLayout from '@components/DrawerLayout';
import Pagination from '@components/Pagination';
import { t } from '@lingui/macro';
import { urls } from '@services/assessment';
import {
  Assessment,
  getAssessments,
  GetAssessmentsResponse,
} from '@services/assessment/assessments';
import { getClaimsFromContext } from '@services/authentification/provider';
import { QueryFilter } from '@utils/api';
import { AxiosResponse } from 'axios';
import { GetServerSideProps } from 'next';
import { useRouter } from 'next/router';

type AssessmentsProps = {
  my: boolean;
  filter: QueryFilter;
  initialData: AxiosResponse<GetAssessmentsResponse>[];
};

function Assessments({ my, filter, initialData }: AssessmentsProps) {
  const router = useRouter();

  const url = `${urls.browser}/v1/assessments`;
  const selector = (data: GetAssessmentsResponse) => data.assessments;
  return (
    <DrawerLayout
      breadcrumbs={[
        {
          href: '/dashboard',
          title: t`Dashboard`,
        },
        {
          href: my ? '/assessments?my=true' : '/assessments',
          title: t`Assessments`,
          active: true,
        },
      ]}
      fab={
        <CreateFab
          title={t`Create Assessment`}
          onClick={() => router.push('/assessment/new')}
        />
      }
    >
      <Pagination<GetAssessmentsResponse, Assessment>
        url={url}
        orderBy="createdOn"
        order="desc"
        filter={filter}
        selector={selector}
        initialData={initialData}
        Renderer={AssessmentCard}
      />
    </DrawerLayout>
  );
}

export const getServerSideProps: GetServerSideProps<AssessmentsProps> = async (
  ctx
) => {
  const claims = getClaimsFromContext(ctx);
  const my = ctx.query.my;
  let filter: QueryFilter = {};
  if (my && claims?.id) {
    filter = {
      memberId: {
        equals: claims?.id,
      },
    };
  }

  const [, data] = await getAssessments(ctx, { filter });

  return {
    props: {
      claims,
      my: my === 'true',
      filter,
      initialData: [data],
    },
  };
};

export default Assessments;
