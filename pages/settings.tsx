import DrawerLayout from '@components/DrawerLayout';
import LanguageSelect from '@components/LanguageSelect';
import { Trans } from '@lingui/macro';
import Box from '@material-ui/core/Box';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Typography from '@material-ui/core/Typography';
import {
  getClaimsFromContext,
  loginRedirect,
} from '@services/authentification/provider';
import { setCookie } from '@utils/cookies';
import { GetServerSideProps } from 'next';
import { useState } from 'react';

type SettingsProps = {
  darkMode: boolean;
};

function Settings({ darkMode }: SettingsProps) {
  const [tmpDarkMode, setTmpDarkMode] = useState(darkMode);

  function handleDarkModeChange() {
    const newTmpDarkMode = !tmpDarkMode;
    setCookie(null, 'darkMode', newTmpDarkMode.toString());
    setTmpDarkMode(newTmpDarkMode);
  }

  return (
    <DrawerLayout>
      <Box display="flex" flexDirection="column">
        <Typography variant="h5" style={{ marginBottom: 8 }}>
          <Trans>Settings (only local)</Trans>
        </Typography>
        <LanguageSelect />
        <FormControlLabel
          style={{ marginBottom: 16 }}
          control={
            <Checkbox checked={tmpDarkMode} onChange={handleDarkModeChange} />
          }
          label={<Trans>Dark Mode</Trans>}
        />
      </Box>
    </DrawerLayout>
  );
}

export const getServerSideProps: GetServerSideProps<SettingsProps> = async (
  ctx
) => {
  const claims = getClaimsFromContext(ctx);
  if (!claims) {
    return loginRedirect(ctx);
  }
  const darkMode = ctx?.req?.cookies?.['darkMode'] === 'true' || false;
  return {
    props: {
      darkMode,
    },
  };
};

export default Settings;
