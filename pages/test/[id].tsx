import Comments from '@components/Comments';
import DrawerLayout from '@components/DrawerLayout';
import EditFab from '@components/EditFab';
import TabPanel from '@components/TabPanel';
import TestMetaView from '@components/TestMetaView';
import TestVersionGraph from '@components/TestVersionGraph';
import TestVersionNodes from '@components/TestVersionNodes';
import { t, Trans } from '@lingui/macro';
import Box from '@material-ui/core/Box';
import MenuItem from '@material-ui/core/MenuItem';
import Paper from '@material-ui/core/Paper';
import Select from '@material-ui/core/Select';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import Typography from '@material-ui/core/Typography';
import {
  getClaimsFromContext,
  loginRedirect,
  useClaims,
} from '@services/authentification/provider';
import {
  getTest,
  GetTestResponse,
  getTestVersion,
  GetTestVersionResponse,
  useTest,
  useTestVersion,
} from '@services/itembank/tests';
import { convertToText } from '@utils/editor';
import { AxiosResponse } from 'axios';
import { GetServerSideProps } from 'next';
import { NextSeo } from 'next-seo';
import Router from 'next/router';
import { ChangeEvent, useState } from 'react';

type IdProps = {
  id: string;
  initialVersion: number | 'latest';
  initialTestData: AxiosResponse<GetTestResponse>;
  initialTestVersionData: AxiosResponse<GetTestVersionResponse>;
};

function Id({
  id,
  initialVersion,
  initialTestData,
  initialTestVersionData,
}: IdProps) {
  const claims = useClaims();

  const [tab, setTab] = useState(0);
  const [version, setVersion] = useState(initialVersion);

  const { test, revalidate } = useTest(id, { initialData: initialTestData });
  const { testVersion } = useTestVersion(id, version, {
    initialData: initialTestVersionData,
  });

  function handleTabChange(e: ChangeEvent<any>, newValue: number) {
    setTab(newValue);
  }

  function handleVersionChange(e: ChangeEvent<{ value: unknown }>) {
    setVersion(Number(e.target.value));
  }

  const versions = test?.versions || [];

  return (
    <>
      <NextSeo
        title={test?.name}
        description={convertToText(test?.publicDescription)}
        openGraph={{
          title: test?.name,
          description: convertToText(test?.publicDescription),
          images: [
            {
              url: `https://og.openpatch.app/${test?.name}.png?theme=light&username=${test?.member?.username}&type=Task`,
            },
          ],
        }}
      />
      <DrawerLayout
        breadcrumbs={[
          {
            href: '/dashboard',
            title: t`Dashboard`,
          },
          {
            href: `/tests`,
            title: t`Tests`,
          },
          {
            href: `/test/[id]`,
            as: `/test/${id}`,
            title: test?.name || id,
            active: true,
          },
        ]}
        fab={
          claims?.id == test?.member?.id && (
            <EditFab
              title={t`Edit Test`}
              onClick={() =>
                Router.push('/test/[id]/edit', `/test/${test?.id}/edit`)
              }
            />
          )
        }
      >
        <Paper square style={{ marginBottom: 16 }}>
          <Box display="flex" alignItems="start">
            <Tabs
              value={tab}
              style={{ flex: 1 }}
              onChange={handleTabChange}
              indicatorColor="primary"
              textColor="primary"
              variant="scrollable"
              scrollButtons="auto"
            >
              <Tab label={<Trans>Nodes</Trans>} />
              <Tab label={<Trans>Graph</Trans>} />
            </Tabs>
            {testVersion?.version && versions.length > 0 && (
              <Select
                value={testVersion?.version}
                onChange={handleVersionChange}
              >
                {versions.map(({ status, version, versionMessage }) => (
                  <MenuItem key={version} value={version}>
                    <Box display="flex" flexDirection="column">
                      <Typography>
                        v{version} ({status})
                      </Typography>
                      <Typography variant="caption">
                        {versionMessage}
                      </Typography>
                    </Box>
                  </MenuItem>
                ))}
              </Select>
            )}
          </Box>
          <TabPanel value={tab} index={0} style={{ overflowX: 'auto' }}>
            <TestVersionNodes nodes={testVersion?.nodes} />
          </TabPanel>
          <TabPanel value={tab} index={1}>
            <TestVersionGraph nodes={testVersion?.nodes} />
          </TabPanel>
        </Paper>
        {test && (
          <Paper square style={{ marginBottom: 16, padding: 16 }}>
            <TestMetaView {...test} revalidate={revalidate} />
          </Paper>
        )}
        <Paper square style={{ padding: 16 }}>
          {test?.id && <Comments id={test?.id} type="test" />}
        </Paper>
      </DrawerLayout>
    </>
  );
}

export const getServerSideProps: GetServerSideProps<IdProps> = async (ctx) => {
  const claims = getClaimsFromContext(ctx);
  if (!claims) {
    return loginRedirect(ctx);
  }

  const id = ctx.query.id as string;
  const [, initialTestData] = await getTest(ctx, id);
  const version = ctx.query.version as string;
  let initialVersion: IdProps['initialVersion'] = Number(version);
  if (!initialVersion) {
    initialVersion = 'latest';
  }
  const [, initialTestVersionData] = await getTestVersion(
    ctx,
    id,
    initialVersion
  );

  return {
    props: {
      claims,
      id,
      initialTestData,
      initialTestVersionData,
      initialVersion,
    },
  };
};

export default Id;
