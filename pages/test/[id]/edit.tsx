import DrawerLayout from '@components/DrawerLayout';
import TestMetaForm, { TestMetaFormProps } from '@components/TestMetaForm';
import { t } from '@lingui/macro';
import {
  getClaimsFromContext,
  loginRedirect,
} from '@services/authentification/provider';
import {
  getTest,
  GetTestResponse,
  putTest,
  useTest,
} from '@services/itembank/tests';
import { useNotifications } from '@utils/notification';
import { AxiosResponse } from 'axios';
import { GetServerSideProps } from 'next';

type EditProps = {
  id: string;
  initialData: AxiosResponse<GetTestResponse>;
};

function Edit({ id, initialData }: EditProps) {
  const { test, revalidate } = useTest(id, {
    initialData,
  });
  const [add] = useNotifications();

  const handleSubmit: TestMetaFormProps['onSubmit'] = async (values) => {
    return new Promise((resolve) => {
      putTest(null, id, values)
        .then(() => {
          add({
            message: t`Test saved`,
            severity: 'success',
          });
          resolve();
        })
        .catch((e) => {
          add({
            message: t`Something went wrong`,
            severity: 'error',
          });
          resolve(e.details);
        });
    });
  };

  return (
    <DrawerLayout
      breadcrumbs={[
        {
          href: '/dashboard',
          title: t`Dashboard`,
        },
        {
          href: `/tests`,
          title: t`Tests`,
        },
        {
          href: `/test/[id]`,
          as: `/test/${id}`,
          title: test?.name || id,
        },
        {
          href: `/test/[id]/edit`,
          as: `/test/${id}/edit`,
          title: t`Edit`,
          active: true,
        },
      ]}
    >
      {test && (
        <TestMetaForm
          submitVariant="fab"
          onSubmit={handleSubmit}
          initialValues={test}
          revalidate={() => revalidate({})}
        />
      )}
    </DrawerLayout>
  );
}

export const getServerSideProps: GetServerSideProps<EditProps> = async (
  ctx
) => {
  const claims = getClaimsFromContext(ctx);
  if (!claims) {
    return loginRedirect(ctx);
  }
  const id = ctx.query.id as string;

  const [, initialData] = await getTest(ctx, id);

  return {
    props: {
      claims,
      id,
      initialData,
    },
  };
};

export default Edit;
