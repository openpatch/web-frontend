import DrawerLayout from '@components/DrawerLayout';
import TestVersionForm from '@components/TestVersionForm';
import { t } from '@lingui/macro';
import {
  getClaimsFromContext,
  loginRedirect,
} from '@services/authentification/provider';
import {
  getTestVersion,
  GetTestVersionResponse,
  putTestVersionResponse,
  TestVersion,
  useTest,
  useTestVersion,
} from '@services/itembank/tests';
import { useNotifications } from '@utils/notification';
import { AxiosResponse } from 'axios';
import { GetServerSideProps } from 'next';

type EditDraftProps = {
  id: string;
  initialData: AxiosResponse<GetTestVersionResponse>;
};

function EditDraft({ id, initialData }: EditDraftProps) {
  const { test } = useTest(id);
  const { testVersion, revalidate } = useTestVersion(id, 'draft', {
    initialData,
    revalidateOnFocus: false,
    revalidateOnReconnect: false,
  });
  const [add] = useNotifications();

  function handleSubmit(values: TestVersion) {
    return new Promise((resolve, reject) => {
      putTestVersionResponse(null, id, values)
        .then(() => {
          add({
            message: t`Draft saved`,
            severity: 'success',
          });
          revalidate({});
          return resolve(true);
        })
        .catch((e) => {
          if (e?.response?.data?.code === 200) {
            add({
              message: t`
                  A validation error occured. Please check, if you have filled
                  out all required fields.
                  `,
              severity: 'error',
            });
            return resolve(e?.response?.data?.details);
          } else {
            add({
              message: t`Something went wrong`,
              severity: 'error',
            });
            return reject();
          }
        });
    });
  }

  return (
    <DrawerLayout
      breadcrumbs={[
        {
          href: '/dashboard',
          title: t`Dashboard`,
        },
        {
          href: `/tests`,
          title: t`Tests`,
        },
        {
          href: `/test/[id]`,
          as: `/test/${id}`,
          title: test?.name || id,
        },
        {
          href: `/test/[id]/edit`,
          as: `/test/${id}/edit`,
          title: t`Edit`,
        },
        {
          href: `/test/[id]/edit-draft`,
          as: `/test/${id}/edit-draft`,
          title: t`Draft`,
          active: true,
        },
      ]}
    >
      {testVersion && (
        <TestVersionForm
          onSubmit={handleSubmit}
          initialValues={testVersion}
          submitVariant="fab"
        />
      )}
    </DrawerLayout>
  );
}

export const getServerSideProps: GetServerSideProps<EditDraftProps> = async (
  ctx
) => {
  const claims = getClaimsFromContext(ctx);
  if (!claims) {
    return loginRedirect(ctx);
  }
  const id = ctx.query.id as string;

  const [, initialData] = await getTestVersion(ctx, id, 'draft');

  return {
    props: {
      claims,
      id,
      initialData,
    },
  };
};

export default EditDraft;
