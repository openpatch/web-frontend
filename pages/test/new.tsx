import DrawerLayout from '@components/DrawerLayout';
import TestMetaForm, { TestMetaFormProps } from '@components/TestMetaForm';
import { t } from '@lingui/macro';
import {
  getClaimsFromContext,
  loginRedirect,
} from '@services/authentification/provider';
import { postTest } from '@services/itembank/tests';
import { useNotifications } from '@utils/notification';
import { GetServerSideProps } from 'next';
import Router from 'next/router';

function NewTest() {
  const [add] = useNotifications();

  const handleSubmit: TestMetaFormProps['onSubmit'] = async (values) => {
    return new Promise((resolve) => {
      postTest(null, values)
        .then(([testId]) => {
          add({
            message: t`Test created`,
            severity: 'success',
          });
          resolve();
          Router.push('/test/[id]/edit', `/test/${testId}/edit`);
        })
        .catch((e) => {
          add({
            message: t`Something went wrong`,
            severity: 'error',
          });
          resolve(e.details);
        });
    });
  };

  return (
    <DrawerLayout
      breadcrumbs={[
        {
          href: '/dashboard',
          title: t`Dashboard`,
        },
        {
          href: `/tests`,
          title: t`Tests`,
        },
        {
          href: `/test/new`,
          title: t`New`,
          active: true,
        },
      ]}
    >
      <TestMetaForm onSubmit={handleSubmit} submitVariant="fab" />
    </DrawerLayout>
  );
}

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const claims = getClaimsFromContext(ctx);
  if (!claims) {
    return loginRedirect(ctx);
  }

  return {
    props: {
      claims,
    },
  };
};

export default NewTest;
