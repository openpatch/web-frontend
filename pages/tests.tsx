import CreateFab from '@components/CreateFab';
import DrawerLayout from '@components/DrawerLayout';
import NoEntriesCreate from '@components/NoEntriesCreate';
import Pagination from '@components/Pagination';
import TestCard from '@components/TestCard';
import { t } from '@lingui/macro';
import { getClaimsFromContext } from '@services/authentification/provider';
import { getTests, GetTestsResponse, Test } from '@services/itembank/tests';
import { QueryFilter } from '@utils/api';
import { AxiosResponse } from 'axios';
import { GetServerSideProps } from 'next';
import { useRouter } from 'next/router';

type TestsProps = {
  my: boolean;
  filter: QueryFilter;
  initialData: AxiosResponse<GetTestsResponse>[];
};

function Tests({ my, filter, initialData }: TestsProps) {
  const router = useRouter();
  const url = `${process.env.NEXT_API_ITEMBANK_BROWSER}/v1/tests`;
  const selector = (data: GetTestsResponse) => data.tests;

  return (
    <DrawerLayout
      breadcrumbs={[
        {
          href: '/dashboard',
          title: t`Dashboard`,
        },
        {
          href: my ? '/tests?my=true' : '/tests',
          title: t`Tests`,
          active: true,
        },
      ]}
      fab={
        <CreateFab
          title={t`Create Item`}
          onClick={() => router.push('/test/new')}
        />
      }
    >
      <Pagination<GetTestsResponse, Test>
        url={url}
        orderBy="createdOn"
        order="desc"
        filter={filter}
        initialData={initialData}
        placeholder={<NoEntriesCreate />}
        selector={selector}
        Renderer={TestCard}
      />
    </DrawerLayout>
  );
}

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const claims = getClaimsFromContext(ctx);
  const my = ctx.query.my;
  let filter: QueryFilter = {};
  if (my && claims?.id) {
    filter = {
      memberId: {
        equals: claims?.id,
      },
    };
  }

  const [, data] = await getTests(ctx, { filter });

  return {
    props: {
      claims,
      my: my === 'true',
      filter,
      initialData: [data],
    },
  };
};

export default Tests;
