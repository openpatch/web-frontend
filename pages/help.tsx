import DrawerLayout from '@components/DrawerLayout';
import { Trans } from '@lingui/macro';
import Box from '@material-ui/core/Box';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import { getClaimsFromContext } from '@services/authentification/provider';
import HelpfulSign from 'mui-undraw/lib/HelpfulSign';
import { GetServerSideProps } from 'next';

function Help() {
  return (
    <DrawerLayout>
      <Box display="flex" flexDirection="column" alignItems="center">
        <HelpfulSign style={{ maxWidth: 600 }} />
        <Typography variant="subtitle1">
          <Trans>
            Need some help? Soon there will be a help section to assist you by
            creating awesome content. For the moment visit our{' '}
            <Link href="https://discord.gg/bdHtrz8">Discord-Group</Link> and ask
            for help there.
          </Trans>
        </Typography>
      </Box>
    </DrawerLayout>
  );
}

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const claims = getClaimsFromContext(ctx);
  return {
    props: {
      claims,
    },
  };
};

export default Help;
