import { getClaimsFromContext } from '@services/authentification/provider';
import { GetServerSideProps } from 'next';
import ANA from '../components/AccessNotAllowed';
import DrawerLayout from '../components/DrawerLayout';

function AccessNotAllowed() {
  return (
    <>
      <DrawerLayout>
        <ANA />
      </DrawerLayout>
    </>
  );
}

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const claims = getClaimsFromContext(ctx);
  return {
    props: {
      claims,
    },
  };
};

export default AccessNotAllowed;
