import BlankCenteredLayout from '@components/BlankCenteredLayout';
import LogInForm from '@components/LogInForm';
import { t, Trans } from '@lingui/macro';
import Button from '@material-ui/core/Button';
import { postLogin } from '@services/authentification/root';
import { useNotifications } from '@utils/notification';
import { useRouter } from 'next/router';

function Login() {
  const [add] = useNotifications();
  const router = useRouter();
  const redirect = router.query.redirect;

  function handleResend() {
    router.push('/resend-mail');
  }

  function handleLogin(username: string, password: string) {
    postLogin(null, username, password)
      .then(() => {
        if (redirect && !Array.isArray(redirect)) {
          router.push(redirect);
        } else {
          // if redirect is not provided or is in an incorrect format default to
          // the dashboard
          router.push('/dashboard');
        }
      })
      .catch((e) => {
        if (e.response) {
          if (e.response?.data?.code === 100) {
            add({
              message: t`Bad username or password`,
              severity: 'error',
            });
          } else if (e.response?.data?.code === 101) {
            add({
              message: t`E-Mail not confirmed`,
              severity: 'error',
              action: (
                <Button onClick={handleResend}>
                  <Trans>Resend</Trans>
                </Button>
              ),
            });
          }
        } else {
          add({
            message: t`Something went wrong`,
            severity: 'error',
          });
        }
      });
  }
  return (
    <BlankCenteredLayout>
      <LogInForm onLogin={handleLogin} />
    </BlankCenteredLayout>
  );
}

export default Login;
