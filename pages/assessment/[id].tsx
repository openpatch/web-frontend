import Comments from '@components/Comments';
import ConfirmButton from '@components/ConfirmButton';
import DrawerLayout from '@components/DrawerLayout';
import EditFab from '@components/EditFab';
import Link from '@components/Link';
import Pagination from '@components/Pagination';
import RichText from '@components/RichText';
import TabPanel from '@components/TabPanel';
import TestCard from '@components/TestCard';
import TestResultEntry from '@components/TestResultEntry';
import { t, Trans } from '@lingui/macro';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import Typography from '@material-ui/core/Typography';
import {
  deleteAssessmentResults,
  getAssessment,
  GetAssessmentResponse,
  getAssessmentResultsExport,
  GetAssessmentResultsResponse,
  getAssessmentStatistic,
  GetAssessmentStatisticResponse,
  useAssessment,
  useAssessmentStatistic,
} from '@services/assessment/assessments';
import { TestResult } from '@services/assessment/testResults';
import {
  getClaimsFromContext,
  loginRedirect,
  useClaims,
} from '@services/authentification/provider';
import { getTest, GetTestResponse, useTest } from '@services/itembank/tests';
import { convertFromRaw } from '@utils/editor';
import { round } from '@utils/math';
import { useNotifications } from '@utils/notification';
import { AxiosResponse } from 'axios';
import FileSaver from 'file-saver';
import { GetServerSideProps } from 'next';
import Router from 'next/router';
import { useState } from 'react';

type IdProps = {
  id: string;
  testId: string;
  initialData: AxiosResponse<GetAssessmentResponse>;
  initialTestData: AxiosResponse<GetTestResponse>;
  initialStatisticData: AxiosResponse<GetAssessmentStatisticResponse>;
};

function Id({
  id,
  testId,
  initialData,
  initialTestData,
  initialStatisticData,
}: IdProps) {
  const [tab, setTab] = useState(0);
  const [add] = useNotifications();
  const claims = useClaims();
  const { assessment, revalidate } = useAssessment(id, { initialData });
  const {
    assessmentStatistic,
    revalidate: revalidateStatistic,
  } = useAssessmentStatistic(id, { initialData: initialStatisticData });
  const { test } = useTest(testId, {
    initialData: initialTestData,
  });

  function handleTabChange(e: any, newValue: number) {
    setTab(newValue);
  }

  function handleDownload() {
    getAssessmentResultsExport(null, id)
      .then(([data]) => {
        const blob = new Blob([JSON.stringify(data, null, 2)], {
          type: 'application/json',
        });
        FileSaver.saveAs(blob, `${assessment?.name}.json`);
      })
      .catch(() => {
        add({
          message: t`Something went wrong.`,
          severity: 'error',
        });
      });
  }

  function handleDeleteAll() {
    deleteAssessmentResults(null, id)
      .then(() => {
        add({
          message: t`All results were deleted`,
          severity: 'success',
        });
        revalidate({});
        revalidateStatistic({});
      })
      .catch(() => {
        add({
          message: t`Something went wrong.`,
          severity: 'error',
        });
      });
  }

  return (
    <DrawerLayout
      breadcrumbs={[
        {
          href: '/dashboard',
          title: t`Dashboard`,
        },
        {
          href: `/assessments?my=true`,
          title: t`Assessments`,
        },
        {
          href: `/assessment/[id]`,
          as: `/assessment/${id}`,
          title: assessment?.name || id,
          active: true,
        },
      ]}
      fab={
        claims?.id == assessment?.member && (
          <EditFab
            title={t`Edit Assessment`}
            onClick={() =>
              Router.push('/assessment/[id]/edit', `/assessment/${id}/edit`)
            }
          />
        )
      }
    >
      <Grid container style={{ marginBottom: 16 }} spacing={3} justify="center">
        <Grid item xs={12} sm={6} lg={3}>
          <Paper square style={{ padding: 16 }}>
            <Typography align="center" variant="h3">
              {round(assessmentStatistic?.avgScore || 0)}
            </Typography>
            <Typography align="center">
              <Trans>Ø Score</Trans>
            </Typography>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={6} lg={3}>
          <Paper square style={{ padding: 16 }}>
            <Typography align="center" variant="h3">
              {assessmentStatistic?.avgTime
                ? new Date(1000 * assessmentStatistic.avgTime)
                    .toISOString()
                    .substr(11, 8)
                : 0}
            </Typography>
            <Typography align="center">
              <Trans>Ø Time</Trans>
            </Typography>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={6} lg={3}>
          <Paper square style={{ padding: 16 }}>
            <Typography align="center" variant="h3">
              {assessmentStatistic?.finished || 0}
            </Typography>
            <Typography align="center">
              <Trans>Finished</Trans>
            </Typography>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={6} lg={3}>
          <Paper square style={{ padding: 16 }}>
            <Typography align="center" variant="h3">
              {assessmentStatistic?.aborted || 0}
            </Typography>
            <Typography align="center">
              <Trans>Aborted</Trans>
            </Typography>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={6} lg={3}>
          <Paper square style={{ padding: 16 }}>
            <Link
              href={`/assessment/[id]/report`}
              as={`/assessment/${id}/report`}
            >
              <Typography align="center" variant="h3">
                <Trans>Report</Trans>
              </Typography>
              <Typography align="center">
                <Trans>Live</Trans>
              </Typography>
            </Link>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={6} lg={3}>
          <Paper square style={{ padding: 16 }}>
            <Link
              as={`/session/prepare?assessment=${id}`}
              href={`/session/prepare?assessment=${id}`}
            >
              <Typography align="center" variant="h3">
                <Trans>Public</Trans>
              </Typography>
              <Typography align="center">
                <Trans>Sharable URL</Trans>
              </Typography>
            </Link>
          </Paper>
        </Grid>
      </Grid>
      <Paper square style={{ marginBottom: 16 }}>
        <Box display="flex" alignItems="start">
          <Tabs
            value={tab}
            style={{ flex: 1 }}
            onChange={handleTabChange}
            indicatorColor="primary"
            textColor="primary"
            variant="scrollable"
            scrollButtons="auto"
          >
            <Tab label={<Trans>Greeting</Trans>} />
            <Tab label={<Trans>Farewell</Trans>} />
            <Tab label={<Trans>Data Protection</Trans>} />
            <Tab label={<Trans>Settings</Trans>} />
          </Tabs>
        </Box>
        <TabPanel value={tab} index={0}>
          <RichText editorState={convertFromRaw(assessment?.greeting)} />
        </TabPanel>
        <TabPanel value={tab} index={1}>
          <RichText editorState={convertFromRaw(assessment?.farewell)} />
        </TabPanel>
        <TabPanel value={tab} index={2}>
          <RichText
            editorState={convertFromRaw(
              assessment?.dataProtection || undefined
            )}
          />
        </TabPanel>
        <TabPanel value={tab} index={3}>
          {assessment?.startsOn}
          {assessment?.endsOn}
          {assessment?.passwordProtected}
        </TabPanel>
      </Paper>
      {test && <TestCard {...test} />}
      <Paper square style={{ marginTop: 16, marginBottom: 16, padding: 16 }}>
        <Box display="flex" alignItems="center">
          <Typography variant="body1" style={{ flex: 1 }}>
            <Trans>Results</Trans>
          </Typography>
          <Button onClick={handleDownload}>Download</Button>
          <ConfirmButton
            title={t`Delete all results`}
            text={t`Are you sure you want to delete all results? This is
                irreversible.`}
            onOK={handleDeleteAll}
            onCancel={() => null}
          >
            <Trans>Delete all</Trans>
          </ConfirmButton>
        </Box>
        <Pagination<GetAssessmentResultsResponse, TestResult>
          variant="list"
          url={`${process.env.NEXT_API_ASSESSMENT_BROWSER}/v1/assessments/${id}/results`}
          orderBy="createdOn"
          order="desc"
          selector={(data) => data.testResults}
          Renderer={(data) => <TestResultEntry {...data} />}
        />
      </Paper>
      <Paper square style={{ padding: 16 }}>
        <Comments id={id} type="assessment" />
      </Paper>
    </DrawerLayout>
  );
}

export const getServerSideProps: GetServerSideProps<IdProps> = async (ctx) => {
  const claims = getClaimsFromContext(ctx);
  if (!claims) {
    return loginRedirect(ctx);
  }
  const id = ctx.query.id as string;
  const [assessment, initialData] = await getAssessment(ctx, id);
  const [, initialTestData] = await getTest(ctx, assessment.testId);
  const [, initialStatisticData] = await getAssessmentStatistic(ctx, id);

  return {
    props: {
      claims,
      id,
      testId: assessment.testId,
      initialData,
      initialTestData,
      initialStatisticData,
    },
  };
};

export default Id;
