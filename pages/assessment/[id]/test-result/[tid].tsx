import DrawerLayout from '@components/DrawerLayout';
import NodeResultEntry from '@components/NodeResultEntry';
import TestVersionGraph from '@components/TestVersionGraph';
import { t, Trans } from '@lingui/macro';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Alert from '@material-ui/lab/Alert';
import {
  Assessment,
  getAssessment,
  getTestVersion,
} from '@services/assessment/assessments';
import {
  getTestResult,
  GetTestResultResponse,
  useTestResult,
} from '@services/assessment/testResults';
import {
  getClaimsFromContext,
  loginRedirect,
} from '@services/authentification/provider';
import { TestVersion } from '@services/itembank/tests';
import { AxiosResponse } from 'axios';
import { GetServerSideProps } from 'next';
import { useEffect, useState } from 'react';
import { Edge } from 'vis-network';

type IdProps = {
  id: string;
  initialData: AxiosResponse<GetTestResultResponse>;
  assessment: Assessment;
  testVersion: TestVersion;
};

function Id({ id, initialData, assessment, testVersion }: IdProps) {
  const { testResult } = useTestResult(id, {
    initialData,
  });
  const [path, setPath] = useState<Edge[]>([]);

  useEffect(() => {
    const tmpPath: Edge[] = [];
    const nodeResults = testResult?.nodeResults || [];
    let previousNodeId: string;
    let index = 1;
    nodeResults.forEach((nodeResult) => {
      const nextNodeId = nodeResult.testNodeId;
      if (previousNodeId) {
        tmpPath.push({
          from: previousNodeId,
          to: nextNodeId,
          label: t`Step: ${index++}`,
        });
      }
      previousNodeId = nextNodeId;
    });
    setPath(tmpPath);
  }, [testResult]);

  // aborted -> aborted message
  // graph with user path
  // score
  // list of item results: item/version (link), correct, time, solution (view), recording (if available)
  return (
    <DrawerLayout
      breadcrumbs={[
        {
          href: '/dashboard',
          title: t`Dashboard`,
        },
        {
          href: '/assessment/[id]',
          as: `/assessment/${assessment.id}`,
          title: assessment.name,
        },
        {
          href: '/assessment/[id]/test-result/[tid]',
          as: `/assessment/${assessment.id}/test-result/${id}`,
          title: testResult?.sessionHash || id,
          active: true,
        },
      ]}
    >
      {testResult?.aborted && (
        <Alert severity="warning" elevation={1} style={{ marginBottom: 16 }}>
          {testResult?.abortedReason || <Trans>Aborted</Trans>}
        </Alert>
      )}
      {!testResult?.end && (
        <Alert severity="warning" elevation={1} style={{ marginBottom: 16 }}>
          <Trans>Not finished</Trans>
        </Alert>
      )}
      <Paper style={{ padding: 16 }}>
        <Typography>
          <Trans>Path</Trans>
        </Typography>
        <TestVersionGraph nodes={testVersion?.nodes} path={path} />
      </Paper>
      {testResult &&
        testVersion &&
        testResult.nodeResults.map((nodeResult) => (
          <NodeResultEntry
            key={nodeResult.id}
            node={testVersion.nodes.find((n) => n.id == nodeResult.testNodeId)}
            {...nodeResult}
            assessmentId={assessment.id}
          />
        ))}
    </DrawerLayout>
  );
}

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const claims = getClaimsFromContext(ctx);
  if (!claims) {
    return loginRedirect(ctx);
  }

  const assessmentId = ctx.query.id as string;
  const testResultId = ctx.query.tid as string;

  const [assessment] = await getAssessment(ctx, assessmentId);
  const [, initialData] = await getTestResult(ctx, testResultId);
  const [testVersion] = await getTestVersion(ctx, assessmentId, false);

  return {
    props: {
      id: testResultId,
      assessment,
      testVersion,
      initialData,
    },
  };
};

export default Id;
