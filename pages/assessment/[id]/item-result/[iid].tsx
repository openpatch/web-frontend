import Comments from '@components/Comments';
import DrawerLayout from '@components/DrawerLayout';
import TextField from '@components/fields/TextField';
import ItemVersionRenderer from '@components/ItemVersionRenderer';
import { t, Trans } from '@lingui/macro';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import {
  getItemVersion,
  ItemVersionCache,
} from '@services/assessment/assessments';
import { getItemResult, ItemResult } from '@services/assessment/itemResults';
import { TaskState } from '@services/assessment/session';
import { SessionProvider } from '@services/assessment/session/provider';
import {
  getClaimsFromContext,
  loginRedirect,
} from '@services/authentification/provider';
import { useNotifications } from '@utils/notification';
import { decrypt, initWorker } from '@utils/openpgp';
import { GetServerSideProps } from 'next';
import { FormEvent, useEffect, useState } from 'react';

initWorker();

type IdProps = {
  id: string;
  assessmentId: string;
  itemResult: ItemResult;
  itemVersion: ItemVersionCache;
};

function Id({ id, assessmentId, itemResult, itemVersion }: IdProps) {
  const [add] = useNotifications();
  const [solution, setSolution] = useState<Record<string, TaskState> | null>(
    null
  );
  const [encrypted, setEncrypted] = useState(false);
  const [password, setPassword] = useState('');

  useEffect(() => {
    if (itemResult.encrypted) {
      setEncrypted(true);
    } else {
      setSolution(itemResult.solution);
    }
  }, [itemResult]);

  function handleDecrypt(e: FormEvent) {
    e.preventDefault();
    if (!itemResult?.solutionEncrypted) {
      return;
    }
    decrypt(
      itemResult?.encryptionPrivateKey,
      password,
      itemResult?.solutionEncrypted
    )
      .then((s) => {
        setSolution(JSON.parse(s.data));
      })
      .catch(() => {
        add({
          message: t`Incorrect password`,
          severity: 'error',
        });
      });
  }

  return (
    <DrawerLayout
      breadcrumbs={[
        {
          href: '/dashboard',
          title: t`Dashboard`,
        },
        {
          href: `/assessment/[id]`,
          as: `/assessment/${assessmentId}`,
          title: t`Assessment`,
        },
        {
          href: '/item-result/[id]',
          as: `/item-result/${id}`,
          title: t`Item Result`,
          active: true,
        },
      ]}
    >
      {!solution && encrypted && (
        <Box
          component="form"
          onSubmit={handleDecrypt}
          display="flex"
          justifyContent="center"
          alignItems="center"
        >
          <TextField
            meta={{}}
            label={<Trans>Encryption Password</Trans>}
            type="password"
            input={{
              name: 'password',
              value: password,
              onChange: (e) => setPassword(e.target.value),
              onBlur: () => null,
              onFocus: () => null,
            }}
          />
          <Button onClick={handleDecrypt}>Decrypt</Button>
        </Box>
      )}
      {solution && (
        <SessionProvider
          initialState={{
            currentItem: 0,
            items: {
              0: {
                tasks: solution,
              },
            },
          }}
          readOnly
        >
          {itemVersion && (
            <ItemVersionRenderer
              tasks={itemVersion?.tasks}
              index={0}
              variant="result"
            />
          )}
        </SessionProvider>
      )}
      <Paper style={{ marginTop: 16, padding: 16 }}>
        <Comments id={id} type="item-result" />
      </Paper>
    </DrawerLayout>
  );
}

export const getServerSideProps: GetServerSideProps<IdProps> = async (ctx) => {
  const claims = getClaimsFromContext(ctx);
  if (!claims) {
    return loginRedirect(ctx);
  }

  const assessmentId = ctx.query.id as string;
  const itemResultId = ctx.query.iid as string;

  const [itemResult] = await getItemResult(ctx, itemResultId);
  const [itemVersion] = await getItemVersion(
    ctx,
    assessmentId,
    itemResult.itemId,
    itemResult.itemVersion
  );

  return {
    props: {
      claims,
      id: itemResultId,
      assessmentId,
      itemResult,
      itemVersion,
    },
  };
};

export default Id;
