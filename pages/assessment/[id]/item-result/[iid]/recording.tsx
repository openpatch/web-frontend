import Comments from '@components/Comments';
import DrawerLayout from '@components/DrawerLayout';
import RecordingPlayer from '@components/RecordingPlayer';
import RecordingTranscript from '@components/RecordingTranscript';
import TabPanel from '@components/TabPanel';
import { t, Trans } from '@lingui/macro';
import Paper from '@material-ui/core/Paper';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import { getItemVersion, ItemVersionCache } from '@services/assessment/assessments';
import { getItemResult } from '@services/assessment/itemResults';
import { SessionProvider } from '@services/assessment/session/provider';
import { getClaimsFromContext, loginRedirect } from '@services/authentification/provider';
import { getRecording, Recording } from '@services/recorder/recordings';
import { GetServerSideProps } from 'next';
import { useState } from 'react';

type IdProps = {
  id: string;
  assessmentId: string;
  recording: Recording;
  itemVersion: ItemVersionCache;
};

function Id({ id, assessmentId, recording, itemVersion }: IdProps) {
  const [tab, setTab] = useState(0);

  function handleTabChange(e: any, newValue: number) {
    setTab(newValue);
  }

  return (
    <DrawerLayout
      breadcrumbs={[
        {
          href: '/dashboard',
          title: t`Dashboard`,
        },
        {
          href: `/assessment/[id]`,
          as: `/assessment/${assessmentId}`,
          title: t`Assessment`,
        },
        {
          href: `/assessment/[id]/item-result/[iid]`,
          as: `/assessment/${assessmentId}/item-result/${id}`,
          title: t`Item Result`,
        },
        {
          href: `/assessment/[id]/item-result/[iid]/recording`,
          as: `/assessment/${assessmentId}/item-result/${id}/recording`,
          title: t`Recording`,
          active: true,
        },
      ]}
    >
      <SessionProvider readOnly>
        <RecordingPlayer items={[itemVersion]} recording={recording} />
      </SessionProvider>
      <Paper style={{ marginTop: 16 }}>
        <Tabs
          value={tab}
          textColor="primary"
          variant="scrollable"
          scrollButtons="auto"
          indicatorColor="primary"
          onChange={handleTabChange}
        >
          <Tab label={<Trans>Comments</Trans>} />
          <Tab label={<Trans>Transcript</Trans>} />
        </Tabs>
        <TabPanel value={tab} index={0}>
          <Comments id={id} type="item-recording" />
        </TabPanel>
        <TabPanel value={tab} index={1}>
          <RecordingTranscript frames={recording.frames} />
        </TabPanel>
      </Paper>
    </DrawerLayout>
  );
}

export const getServerSideProps: GetServerSideProps<IdProps> = async (ctx) => {
  const claims = getClaimsFromContext(ctx);
  if(!claims) {
    return loginRedirect(ctx);
  }
  const assessmentId = ctx.query.id as string;
  const id = ctx.query.iid as string;

  const [recording] = await getRecording(ctx, id);
  const [itemResult] = await getItemResult(ctx, id);
  const [itemVersion] = await getItemVersion(ctx, assessmentId, itemResult.itemId, itemResult.itemVersion)

  return {
    props: {
      assessmentId,
      id,
      recording,
      itemVersion
    }
  }
}

export default Id;
