import Comments from '@components/Comments';
import DrawerLayout from '@components/DrawerLayout';
import RecordingPlayer from '@components/RecordingPlayer';
import RecordingTranscript from '@components/RecordingTranscript';
import TabPanel from '@components/TabPanel';
import { t, Trans } from '@lingui/macro';
import Paper from '@material-ui/core/Paper';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import {
  getNodeItemVersions,
  ItemVersionCache,
} from '@services/assessment/assessments';
import { getNodeResult } from '@services/assessment/nodeResults';
import { SessionProvider } from '@services/assessment/session/provider';
import {
  getClaimsFromContext,
  loginRedirect,
} from '@services/authentification/provider';
import { getRecording, Recording } from '@services/recorder/recordings';
import { GetServerSideProps } from 'next';
import { useState } from 'react';

type IdProps = {
  id: string;
  assessmentId: string;
  recording: Recording;
  nodeItemVersions: ItemVersionCache[];
};

function Id({ id, assessmentId, recording, nodeItemVersions }: IdProps) {
  const [tab, setTab] = useState(0);

  function handleTabChange(e: any, newValue: number) {
    setTab(newValue);
  }

  return (
    <DrawerLayout
      breadcrumbs={[
        {
          href: '/dashboard',
          title: t`Dashboard`,
        },
        {
          href: `/assessment/[id]`,
          as: `/assessment/${assessmentId}`,
          title: t`Assessment`,
        },
        {
          href: `/assessment/[id]/node-result/[nid]`,
          as: `/assessment/${assessmentId}/node-result/${id}`,
          title: t`Node Result`,
        },
        {
          href: `/assessment/[id]/item-result/[nid]/recording`,
          as: `/assessment/${assessmentId}/node-result/${id}/recording`,
          title: t`Recording`,
          active: true,
        },
      ]}
    >
      <SessionProvider readOnly>
        <RecordingPlayer items={nodeItemVersions} recording={recording} />
      </SessionProvider>
      <Paper style={{ marginTop: 16 }}>
        <Tabs
          value={tab}
          textColor="primary"
          variant="scrollable"
          scrollButtons="auto"
          indicatorColor="primary"
          onChange={handleTabChange}
        >
          <Tab label={<Trans>Comments</Trans>} />
          <Tab label={<Trans>Transcript</Trans>} />
        </Tabs>
        <TabPanel value={tab} index={0}>
          <Comments id={id} type="node-result" />
        </TabPanel>
        <TabPanel value={tab} index={1}>
          <RecordingTranscript frames={recording?.frames} />
        </TabPanel>
      </Paper>
    </DrawerLayout>
  );
}

export const getServerSideProps: GetServerSideProps<IdProps> = async (ctx) => {
  const claims = getClaimsFromContext(ctx);
  if (!claims) {
    return loginRedirect(ctx);
  }

  const assessmentId = ctx.query.id as string;
  const nodeResultId = ctx.query.nid as string;
  const [nodeResult] = await getNodeResult(ctx, nodeResultId);
  const [recording] = await getRecording(ctx, nodeResultId);
  const [nodeItemVersions] = await getNodeItemVersions(
    ctx,
    assessmentId,
    nodeResult.testNodeId
  );

  return {
    props: {
      claims,
      id: nodeResultId,
      assessmentId,
      recording,
      nodeItemVersions,
    },
  };
};

export default Id;
