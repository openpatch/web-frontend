import AssessmentForm, {
  AssessmentFormProps,
} from '@components/AssessmentForm';
import DrawerLayout from '@components/DrawerLayout';
import { t } from '@lingui/macro';
import {
  getAssessment,
  GetAssessmentResponse,
  putAssessment,
  useAssessment,
} from '@services/assessment/assessments';
import {
  getClaimsFromContext,
  loginRedirect,
} from '@services/authentification/provider';
import { useNotifications } from '@utils/notification';
import { AxiosResponse } from 'axios';
import { GetServerSideProps } from 'next';

type EditProps = {
  id: string;
  initialData: AxiosResponse<GetAssessmentResponse>;
};

function Edit({ id, initialData }: EditProps) {
  const { assessment } = useAssessment(id, { initialData });
  const [add] = useNotifications();

  const handleSubmit: AssessmentFormProps['onSubmit'] = (values) => {
    return new Promise((resolve) => {
      putAssessment(null, id, values)
        .then(() => {
          add({
            message: t`Assessment saved`,
            severity: 'success',
          });
          resolve(undefined);
        })
        .catch((e) => {
          add({
            message: t`Something went wrong`,
            severity: 'error',
          });
          resolve(e.details);
        });
    });
  };
  return (
    <DrawerLayout
      breadcrumbs={[
        {
          href: '/dashboard',
          title: t`Dashboard`,
        },
        {
          href: `/assessments`,
          title: t`Assessments`,
        },
        {
          href: `/assessment/[id]`,
          as: `/assessment/${id}`,
          title: assessment?.name || id,
        },
        {
          href: `/assessment/[id]/edit`,
          as: `/assessment/${id}/edit`,
          title: t`Edit`,
          active: true,
        },
      ]}
    >
      {assessment && (
        <AssessmentForm
          onSubmit={handleSubmit}
          initialValues={assessment}
          submitVariant="fab"
          edit
        />
      )}
    </DrawerLayout>
  );
}

export const getServerSideProps: GetServerSideProps<EditProps> = async (
  ctx
) => {
  const claims = getClaimsFromContext(ctx);
  if (!claims) {
    return loginRedirect(ctx);
  }
  const id = ctx.query.id as string;
  const [, initialData] = await getAssessment(ctx, id);

  return {
    props: {
      id,
      claims,
      initialData,
    },
  };
};

export default Edit;
