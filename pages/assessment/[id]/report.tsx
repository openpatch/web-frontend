import DrawerLayout from '@components/DrawerLayout';
import TextField from '@components/fields/TextField';
import TestVersionStatistic from '@components/TestVersionStatistic';
import { t, Trans } from '@lingui/macro';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import {
  Assessment,
  getAssessment,
  useAssessmentPassword,
} from '@services/assessment/assessments';
import { useAssessmentReport } from '@services/assessment/report';
import {
  getClaimsFromContext,
  loginRedirect,
} from '@services/authentification/provider';
import { GetServerSideProps } from 'next';
import { useState } from 'react';

type ReportProps = {
  id: string;
  assessment: Assessment;
};

function Report({ id, assessment }: ReportProps) {
  /**
   * Get raw results calculate statistics client side.
   * Store results in localStorage fetch only results after last fetch.
   * 1) Path for each result => Display TestGraph
   * 2) Like test result list all nodes and items
   *  - For each node show flow
   *  - For each node show avg time
   *  - For each node show avg score
   *  /assessment/[id]/node-results
   *  - For each item show avg item
   *  - For each item show avg score
   *  /assessment/[id]/item-results
   *  - Distinguish between linear and jumpable
   * 3) Click on Item links to item/[id]/report
   */
  const [password, setPassword] = useState('');
  const [encryptionPassword, setEncryptionPassword] = useAssessmentPassword(id);
  const [report, revalidate] = useAssessmentReport(
    id,
    !assessment?.encryptionPrivateKey || encryptionPassword !== null,
    assessment?.encryptionPrivateKey || null
  );

  function handleDecrypt() {
    setEncryptionPassword(password);
  }

  function reset() {
    revalidate();
    window.location.reload();
  }

  return (
    <DrawerLayout
      breadcrumbs={[
        {
          href: '/dashboard',
          title: t`Dashboard`,
        },
        {
          href: `/assessments?my=true`,
          title: t`Assessments`,
        },
        {
          href: `/assessment/[id]`,
          as: `/assessment/${id}`,
          title: assessment?.name || id,
        },
        {
          href: '/assessment/[id]/report',
          as: `/assessment/${id}/report`,
          title: t`Report`,
          active: true,
        },
      ]}
    >
      <Box display="flex" flexDirection="column">
        {assessment?.encryptionPrivateKey && !encryptionPassword ? (
          <Box display="flex" justifyContent="center" alignItems="center">
            <TextField
              meta={{}}
              label={<Trans>Encryption Password</Trans>}
              type="password"
              input={{
                name: 'password',
                value: password,
                onChange: (e) => setPassword(e.target.value),
                onBlur: () => null,
                onFocus: () => null,
              }}
            />
            <Button onClick={handleDecrypt}>Decrypt</Button>
          </Box>
        ) : (
          <>
            <Button onClick={reset}>Reset Report</Button>
            <TestVersionStatistic {...report} assessmentId={id} />
          </>
        )}
      </Box>
    </DrawerLayout>
  );
}

export const getServerSideProps: GetServerSideProps<ReportProps> = async (
  ctx
) => {
  const claims = getClaimsFromContext(ctx);
  if (!claims) {
    return loginRedirect(ctx);
  }
  const id = ctx.query.id as string;
  const [assessment] = await getAssessment(ctx, id);

  return {
    props: {
      claims,
      id,
      assessment,
    },
  };
};

export default Report;
