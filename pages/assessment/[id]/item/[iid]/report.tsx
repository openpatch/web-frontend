import DrawerLayout from '@components/DrawerLayout';
import TextField from '@components/fields/TextField';
import ItemVersionStatistic from '@components/ItemVersionStatistic';
import { t, Trans } from '@lingui/macro';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import {
  Assessment,
  getAssessment,
  useAssessmentPassword,
} from '@services/assessment/assessments';
import {
  TestItemStatistic,
  useAssessmentReport,
} from '@services/assessment/report';
import {
  getClaimsFromContext,
  loginRedirect,
} from '@services/authentification/provider';
import { GetServerSideProps } from 'next';
import { useEffect, useState } from 'react';

type ReportProps = {
  id: string;
  assessmentId: string;
  assessment: Assessment;
};

function Report({ id, assessmentId, assessment }: ReportProps) {
  /**
   * if encrypted require password for decryption
   *
   * number of correct
   * avg time
   * build visualisation based on solution
   * /assessment/[id]/node/[nid]/item/[iid]/results
   */
  const [autoUpdate, setAutoUpdate] = useState(false);
  const [password, setPassword] = useState('');
  const [itemReport, setItemReport] = useState<TestItemStatistic | null>(null);
  const [encryptionPassword, setEncryptionPassword] = useAssessmentPassword(
    assessmentId
  );
  const [report] = useAssessmentReport(
    assessmentId,
    autoUpdate,
    assessment.encryptionPrivateKey
  );

  useEffect(() => {
    const newItemReport = report?.items?.[id] || null;
    setItemReport({
      ...newItemReport,
      statistic: {
        avgTime: 0,
        count: 0,
        avgScore: 0,
        ...newItemReport?.statistic,
      },
    });
  }, [report]);

  useEffect(() => {
    if (itemReport) {
      setAutoUpdate(!itemReport.encrypted);
    }
  }, [itemReport]);

  function handleDecrypt() {
    setAutoUpdate(true);
    setEncryptionPassword(password);
  }

  return (
    <DrawerLayout
      breadcrumbs={[
        {
          href: '/dashboard',
          title: t`Dashboard`,
        },
        {
          href: `/assessments?my=true`,
          title: t`Assessments`,
        },
        {
          href: `/assessment/[id]`,
          as: `/assessment/${assessmentId}`,
          title: assessment.name,
        },
        {
          href: `/assessment/[id]/report`,
          as: `/assessment/${assessmentId}/report`,
          title: t`Report`,
        },
        {
          href: '/assessment/[id]/item/[iid]/report',
          as: `/assessment/${assessmentId}/item/${id}/report`,
          title: id,
          active: true,
        },
      ]}
    >
      <Box display="flex" flexDirection="column">
        {assessment?.encryptionPrivateKey && !encryptionPassword ? (
          <Box display="flex" justifyContent="center" alignItems="center">
            <TextField
              meta={{}}
              label={<Trans>Encryption Password</Trans>}
              type="password"
              input={{
                name: 'password',
                value: password,
                onChange: (e) => setPassword(e.target.value),
                onBlur: () => null,
                onFocus: () => null,
              }}
            />
            <Button onClick={handleDecrypt}>
              <Trans>Decrypt</Trans>
            </Button>
          </Box>
        ) : (
          <ItemVersionStatistic {...itemReport} />
        )}
      </Box>
    </DrawerLayout>
  );
}

export const getServerSideProps: GetServerSideProps<ReportProps> = async (
  ctx
) => {
  const claims = getClaimsFromContext(ctx);
  if (!claims) {
    return loginRedirect(ctx);
  }
  const assessmentId = ctx.query.id as string;
  const id = ctx.query.iid as string;
  const [assessment] = await getAssessment(ctx, assessmentId);

  return {
    props: {
      id,
      assessmentId,
      assessment,
    },
  };
};

export default Report;
