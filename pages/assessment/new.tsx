import AssessmentForm, {
  AssessmentFormProps,
} from '@components/AssessmentForm';
import DrawerLayout from '@components/DrawerLayout';
import { t } from '@lingui/macro';
import { postAssessments } from '@services/assessment/assessments';
import {
  getClaimsFromContext,
  loginRedirect,
} from '@services/authentification/provider';
import { useNotifications } from '@utils/notification';
import { generateKeyPair, initWorker } from '@utils/openpgp';
import { GetServerSideProps } from 'next';
import { useRouter } from 'next/router';

initWorker();

function NewAssessment() {
  const router = useRouter();
  const [add] = useNotifications();

  const post: AssessmentFormProps['onSubmit'] = (values) => {
    return new Promise((resolve) => {
      postAssessments(null, values)
        .then(([assessmentId]) => {
          add({
            message: t`Assessment created`,
            severity: 'success',
          });
          router.push('/assessment/[id]', `/assessment/${assessmentId}`);
          resolve(undefined);
        })
        .catch((e) => {
          add({
            message: t`Something went wrong`,
            severity: 'error',
          });
          resolve(e.details);
        });
    });
  };

  const handleSubmit: AssessmentFormProps['onSubmit'] = (values, ...args) => {
    if (values.encryptionPassword && values.encryptionPassword !== '') {
      return generateKeyPair(values.encryptionPassword).then((key) => {
        values.encryptionPublicKey = key.publicKeyArmored;
        values.encryptionPrivateKey = key.privateKeyArmored;
        values.encryptionPassword = undefined;
        return post(values, ...args);
      });
    } else {
      return post(values, ...args);
    }
  };

  return (
    <DrawerLayout
      breadcrumbs={[
        {
          href: '/dashboard',
          title: t`Dashboard`,
        },
        {
          href: `/assessments`,
          title: t`Assessments`,
        },
        {
          href: `/assessment/new`,
          title: t`New`,
          active: true,
        },
      ]}
    >
      <AssessmentForm onSubmit={handleSubmit} submitVariant="fab" />
    </DrawerLayout>
  );
}

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const claims = getClaimsFromContext(ctx);
  if (!claims) {
    return loginRedirect(ctx);
  }

  return {
    props: {
      claims,
    },
  };
};

export default NewAssessment;
