import CollectionCard from '@components/CollectionCard';
import DrawerLayout from '@components/DrawerLayout';
import ItemCard from '@components/ItemCard';
import Pagination from '@components/Pagination';
import TabPanel from '@components/TabPanel';
import TestCard from '@components/TestCard';
import { Trans } from '@lingui/macro';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import {
  getClaimsFromContext,
  loginRedirect,
} from '@services/authentification/provider';
import {
  Collection,
  GetCollectionsResponse,
} from '@services/itembank/collections';
import { GetItemsResponse, Item } from '@services/itembank/items';
import { GetTestsResponse, Test } from '@services/itembank/tests';
import { GetServerSideProps } from 'next';
import { ChangeEvent, useState } from 'react';

function Discover() {
  const [tab, setTab] = useState(0);

  function handleTabChange(e: ChangeEvent<any>, newValue: number) {
    setTab(newValue);
  }
  return (
    <DrawerLayout
      breadcrumbs={[
        {
          href: '/dashboard',
          title: 'Dashboard',
        },
        {
          href: '/discover',
          title: 'Discover',
          active: true,
        },
      ]}
    >
      <Tabs
        value={tab}
        onChange={handleTabChange}
        indicatorColor="primary"
        textColor="primary"
        variant="scrollable"
        scrollButtons="auto"
      >
        <Tab label={<Trans>Items</Trans>} />
        <Tab label={<Trans>Tests</Trans>} />
        <Tab label={<Trans>Collections</Trans>} />
      </Tabs>
      <TabPanel value={tab} index={0}>
        <Pagination<GetItemsResponse, Item>
          url={`${process.env.NEXT_API_ITEMBANK_BROWSER}/v1/items`}
          orderBy="createdOn"
          order="desc"
          selector={(data) => data.items}
          Renderer={ItemCard}
        />
      </TabPanel>
      <TabPanel value={tab} index={1}>
        <Pagination<GetTestsResponse, Test>
          url={`${process.env.NEXT_API_ITEMBANK_BROWSER}/v1/tests`}
          orderBy="createdOn"
          order="desc"
          selector={(data) => data.tests}
          Renderer={TestCard}
        />
      </TabPanel>
      <TabPanel value={tab} index={2}>
        <Pagination<GetCollectionsResponse, Collection>
          url={`${process.env.NEXT_API_ITEMBANK_BROWSER}/v1/collections`}
          orderBy="createdOn"
          order="desc"
          selector={(data) => data.collections}
          Renderer={CollectionCard}
        />
      </TabPanel>
    </DrawerLayout>
  );
}

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const claims = getClaimsFromContext(ctx);
  if (!claims) {
    return loginRedirect(ctx);
  }

  return {
    props: { claims },
  };
};

export default Discover;
