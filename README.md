# Setup

You need to have [node](https://nodejs.org/en/) and [yarn 1.x.x](https://classic.yarnpkg.com/lang/en/) installed on your system.

```
yarn install
```

For running the whole next.js application:

```
yarn dev
```

For running storybook to build individual components:

```
yarn storybook
```

# Running Backend Services

To run all backend services, just run this command:

```
docker network create openpatch-runner-net
docker-compose up
```

This will also populate all services with some mock data. Be careful it will
initialize the mock data on each run, thus changes will not be persisted.

## Which user accounts can I use?

You can use the following user accounts:

| user | password | email |
| ---- | -------- | ----- |
| admin | admin | admin@openpatch.org |
| user | user | user@user.de |

You can also create a new user. You will not receive an email, but the outgoing
email will be printed to the logs of the mail service. You can inspect them by
running:

```
docker-compose logs --follow mail
```

## How to persist data?

If you want to not overwrite data on each start, delete or comment-out the *-mock entries from
the `docker-compose.yml` after the first start. These are responsible to populate the database.

## How to inspect the databases?

If you want to inspect the database of each service, you can open the sqlite
databases `db/<service>/database/database.db` for example with
[beekeeper](https://www.beekeeperstudio.io/).

## How much RAM will this be using?

In idle mode after starting the services consume so much RAM:

| NAME                        |      MEM USAGE |
| --- | --- |
| mail               | 17.52MiB |
| activity           | 85.93MiB |
| assessment         | 86.16MiB |
| runner             | 466.8MiB |
| recorder           | 78.18MiB |
| authentification   | 85.21MiB |
| format             | 49.96MiB |
| comment            | 83.9MiB |
| media              | 76.95MiB |
| itembank           | 91.5MiB  |
| rabbitmq           | 95.8MiB  |

## Do I need to ran all services?

No, you do not need to ran all services. You should be running at least the
authentification-serivce. The other are optional. Be also sure to run services,
which are a dependency of the one you want to use.

## How do I clean the data of all services?

You just need to delete the folder `.openpatch`.

# Testing

```
yarn test
```

# Coverage

```
yarn test:ci --coverage
```
