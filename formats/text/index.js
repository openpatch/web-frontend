import { Trans, t } from '@lingui/macro';
import PropTypes from 'prop-types';
import produce from 'immer';
import { Field } from 'react-final-form';

import TextField from '../../components/fields/TextField';
import CheckboxField from '../../components/fields/Checkbox';

export const CHANGE_VALUE = 'text/CHANGE_VALUE';

export function changeValue(value) {
  return {
    type: CHANGE_VALUE,
    payload: {
      value,
    },
  };
}

const initialState = {
  value: '',
};

export const reducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case CHANGE_VALUE:
        draft.value = action.payload.value;
        break;
    }
  });

export function Renderer({ state, dispatch, multiline, rows }) {
  function handleChange(e) {
    dispatch(changeValue(e.target.value));
  }

  return (
    <TextField
      meta={{}}
      multiline={multiline}
      rows={rows}
      fullWidth
      input={{
        value: state.value,
        onChange: handleChange,
      }}
    />
  );
}

Renderer.propTypes = {
  state: PropTypes.shape({
    value: PropTypes.string,
  }),
  dispatch: PropTypes.func.isRequired,
  multiline: PropTypes.bool,
  rows: PropTypes.number,
};

Renderer.defaultProps = {
  state: initialState,
};

function Form({ field }) {
  return (
    <>
      <Field
        name={`${field}.multiline`}
        label={<Trans>Multiline</Trans>}
        component={CheckboxField}
        type="checkbox"
      />
      <Field
        name={`${field}.rows`}
        fullWidth
        label={<Trans>Rows</Trans>}
        component={TextField}
        type="number"
      />
    </>
  );
}

Form.propTypes = {
  field: PropTypes.string.isRequired,
};

function validateRegex(regex) {
  try {
    new RegExp(regex);
  } catch (e) {
    return t`Not valid: ${e.message}`;
  }
}

function Evaluation({ field }) {
  return (
    <Field
      name={`${field}.regex`}
      label={<Trans>Regex</Trans>}
      fullWidth
      validate={validateRegex}
      component={TextField}
    />
  );
}

Evaluation.propTypes = {
  field: PropTypes.string.isRequired,
};

export default {
  Renderer,
  Evaluation,
  Form,
  reducer,
};
