import PropTypes from 'prop-types';
import { Draggable } from 'react-beautiful-dnd';
import { useTheme } from '@material-ui/core/styles';

import RichText from '../../components/RichText';
import { convertFromRaw } from '../../utils/editor';

const grid = 8;

const colors = ['red', 'green', 'blue', 'yellow', 'orange'];

const getItemStyle = (draggableStyle, isDragging, locked, linked, theme) => ({
  // some basic styles to make the items look a bit nicer
  userSelect: 'none',
  margin: `0 0 ${grid}px 0`,
  background: theme.palette.background.default,
  opacity: locked ? 0.5 : 1,
  borderColor: colors[linked],
  borderSize: 1,
  borderStyle: 'solid',
  textAlign: 'left',
  padding: grid * 2,
  ...draggableStyle,
});

const Item = ({ item, index }) => {
  const theme = useTheme();
  return (
    <Draggable
      key={item.id}
      isDragDisabled={item.locked || item.lockLinked}
      draggableId={item.id}
      index={index}
    >
      {(provided, snapshot) => (
        <div
          ref={provided.innerRef}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
          style={getItemStyle(
            provided.draggableProps.style,
            snapshot.isDragging,
            item.locked || item.lockLinked,
            item.linked,
            theme
          )}
        >
          <RichText editorState={convertFromRaw(item.text)} />
        </div>
      )}
    </Draggable>
  );
};

Item.propTypes = {
  index: PropTypes.number,
  item: PropTypes.shape({
    id: PropTypes.string,
    locked: PropTypes.bool,
    lockLinked: PropTypes.bool,
    linked: PropTypes.number,
    text: PropTypes.object,
  }),
};

export default Item;
