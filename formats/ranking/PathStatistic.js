import { useEffect, useRef, useState, memo } from 'react';
import PropTypes from 'prop-types';
import { Network } from 'vis-network';
import { useTheme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import _isEqual from 'deep-equal';

function PathStatistic({ nodes: pNodes }) {
  const theme = useTheme();
  const network = useRef();
  const container = useRef();
  const [nodes, setNodes] = useState([]);
  const [edges, setEdges] = useState([]);
  useEffect(() => {
    create();
  }, [nodes, edges]);

  useEffect(() => {
    const tmpNodes = [];
    const tmpEdges = [];
    Object.values(pNodes).forEach((n) => {
      tmpNodes.push({
        id: n.id,
        title: n.state,
        value: n.count,
        font: {
          color: n.start ? theme.palette.primary.dark : undefined,
        },
        shape: n.start ? 'star' : null,
        color: {
          background: n.correct ? 'green' : null,
        },
      });
      Object.values(n.edges).forEach((e) => {
        tmpEdges.push({
          label: e.count + '',
          from: e.from,
          to: e.to,
          value: e.count,
        });
      });
    });
    setNodes(tmpNodes);
    setEdges(tmpEdges);
  }, [pNodes]);

  function create() {
    let defaultOptions = {
      autoResize: true,
      interaction: {
        navigationButtons: true,
      },
      layout: {
        randomSeed: 1,
      },
      manipulation: {
        enabled: false,
      },
      nodes: {
        scaling: {
          min: 14,
          max: 30,
          label: {
            min: 14,
            max: 30,
            enabled: true,
          },
        },
        shape: 'box',
        color: {
          border: theme.palette.primary.dark,
          background: theme.palette.primary.main,
          highlight: {
            border: theme.palette.primary.main,
            background: theme.palette.primary.light,
          },
        },
        font: {
          face: 'Roboto',
          color: theme.palette.primary.contrastText,
        },
      },
      edges: {
        smooth: {
          enabled: true,
          forceDirection: 'none',
        },
        color: theme.palette.secondary.main,
        font: {
          face: 'Roboto',
          color: theme.palette.primary.dark,
        },
        width: 0.5,
        arrows: {
          to: {
            enabled: true,
            scaleFactor: 0.5,
          },
        },
      },
    };
    network.current?.destroy();
    network.current = new Network(container.current, {
      nodes,
      edges,
      options: {
        ...defaultOptions,
      },
    });
  }

  return <Box ref={container} height={400} />;
}

PathStatistic.propTypes = {
  nodes: PropTypes.array,
};

PathStatistic.defaultProps = {
  nodes: [],
};

export default memo(PathStatistic, _isEqual);
