import { useReducer, useEffect } from 'react';
import PropTypes from 'prop-types';
import produce from 'immer';
import { useField } from 'react-final-form';
import { FieldArray } from 'react-final-form-arrays';

import List from './List';
import BlockFieldArray from './BlockFieldArray';
import PathStatistic from './PathStatistic';

export const INIT_POSITIONS = 'ranking/INIT_POSITIONS';
export const MOVE_WITHIN_USER = 'ranking/MOVE_WITHIN_USER';
export const MOVE_WITHIN_SOURCE = 'ranking/MOVE_WITHIN_SOURCE';
export const MOVE_FROM_USER_TO_SOURCE = 'ranking/MOVE_FROM_USER_TO_SOURCE';
export const MOVE_FROM_SOURCE_TO_USER = 'ranking/MOVE_FROM_SOURCE_TO_USER';

export function initPositions(user, source) {
  return {
    type: INIT_POSITIONS,
    payload: {
      user,
      source,
    },
  };
}

export function moveWithinUser(from, to) {
  return {
    type: MOVE_WITHIN_USER,
    payload: {
      from,
      to,
    },
  };
}

export function moveWithinSource(from, to) {
  return {
    type: MOVE_WITHIN_SOURCE,
    payload: {
      from,
      to,
    },
  };
}

export function moveFromUserToSource(from, to) {
  return {
    type: MOVE_FROM_USER_TO_SOURCE,
    payload: {
      from,
      to,
    },
  };
}

export function moveFromSourceToUser(from, to) {
  return {
    type: MOVE_FROM_SOURCE_TO_USER,
    payload: {
      from,
      to,
    },
  };
}

const initialState = {
  sourcePositions: [],
  positions: [],
  path: [],
};

const reorder = (list, startIndex, endIndex) => {
  const result = [...list];
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

/**
 * Moves an item from one list to another list.
 */
const move = (source, destination, from, to) => {
  const sourceClone = [...source];
  const destClone = [...destination];
  const [removed] = sourceClone.splice(from, 1);

  destClone.splice(to, 0, removed);

  return { source: sourceClone, destination: destClone };
};

export const reducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case INIT_POSITIONS:
        draft.sourcePositions = action.payload.source;
        draft.positions = action.payload.user;
        draft.path = [...draft.path, draft.positions];
        break;
      case MOVE_WITHIN_USER:
        draft.positions = reorder(
          draft.positions,
          action.payload.from,
          action.payload.to
        );
        draft.path = [...draft.path, draft.positions];
        break;
      case MOVE_WITHIN_SOURCE:
        draft.sourcePositions = reorder(
          draft.sourcePositions,
          action.payload.from,
          action.payload.to
        );
        break;
      case MOVE_FROM_SOURCE_TO_USER: {
        let result = move(
          draft.sourcePositions,
          draft.positions,
          action.payload.from,
          action.payload.to
        );
        draft.sourcePositions = result.source;
        draft.positions = result.destination;
        draft.path = [...draft.path, draft.positions];
        break;
      }
      case MOVE_FROM_USER_TO_SOURCE: {
        let result = move(
          draft.positions,
          draft.sourcePositions,
          action.payload.from,
          action.payload.to
        );
        draft.positions = result.source;
        draft.sourcePositions = result.destination;
        draft.path = [...draft.path, draft.positions];
        break;
      }
    }
  });

export function Renderer({ state, dispatch, blocks, variant }) {
  useEffect(() => {
    if (blocks && variant === 'normal') {
      const positions = [];
      const sourcePositions = [];

      blocks.forEach((block, index) => {
        if (block.locked) {
          positions.push(index);
        } else {
          sourcePositions.push(index);
        }
      });
      setTimeout(() => {
        dispatch(initPositions(positions, sourcePositions));
      }, 400);
    }
  }, [blocks]);

  const { positions, sourcePositions } = state;
  const user = [];
  const source = [];

  if (positions && sourcePositions) {
    positions.forEach((pos) => {
      user.push(blocks[pos]);
    });

    sourcePositions.forEach((pos) => {
      const block = blocks[pos];
      let lockLinked = false;
      if (block.linked !== null && block.linked !== undefined) {
        lockLinked = user.find((b) => b.linked == block.linked) !== undefined;
      }
      source.push({ ...block, lockLinked });
    });
  }

  function handleDragEnd(res) {
    if (variant === 'normal') {
      const { source, destination } = res;

      // dropped outside the list
      if (!destination) {
        return;
      }

      if (source.droppableId === destination.droppableId) {
        if (source.droppableId === 'source') {
          dispatch(moveWithinSource(source.index, destination.index));
        } else {
          dispatch(moveWithinUser(source.index, destination.index));
        }
      } else {
        if (source.droppableId === 'source') {
          dispatch(moveFromSourceToUser(source.index, destination.index));
        } else {
          dispatch(moveFromUserToSource(source.index, destination.index));
        }
      }
    }
  }

  return <List user={user} source={source} onDragEnd={handleDragEnd} />;
}

Renderer.propTypes = {
  state: PropTypes.shape({
    positions: PropTypes.arrayOf(PropTypes.number),
    sourcePositions: PropTypes.arrayOf(PropTypes.number),
    path: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.number)),
  }),
  dispatch: PropTypes.func.isRequired,
  blocks: PropTypes.array,
  variant: PropTypes.oneOf(['normal', 'player', 'result']),
};

Renderer.defaultProps = {
  state: initialState,
  blocks: [],
};

/**
 * LOCK? LINKED[number] TextField
 */
function Form({ field }) {
  return (
    <FieldArray
      name={`${field}.blocks`}
      fullWidth
      component={BlockFieldArray}
    />
  );
}

Form.propTypes = {
  field: PropTypes.string.isRequired,
};

function Evaluation({ field, data }) {
  const {
    input: { value, onChange },
  } = useField(`${field}.positions`);

  const [state, dispatch] = useReducer(reducer, {
    positions: value || [],
    sourcePositions: [],
    path: [],
  });

  useEffect(() => {
    if (state) {
      onChange(state.positions);
    }
  }, [state]);

  useEffect(() => {
    if (data.blocks) {
      let clean = true;
      if (value) {
        value.forEach((p) => {
          if (p >= data.blocks.length) {
            clean = false;
          }
        });
        data.blocks.forEach((block, index) => {
          if (!value.includes(index) && block.locked) {
            clean = false;
          }
        });
      } else {
        clean = false;
      }

      if (clean) {
        const sourcePositions = [];
        data.blocks.forEach((_, index) => {
          if (!value.includes(index)) {
            sourcePositions.push(index);
          }
        });
        dispatch(initPositions(value, sourcePositions));
      } else {
        const positions = [];
        const sourcePositions = [];
        data.blocks.forEach((block, index) => {
          if (block.locked) {
            positions.push(index);
          } else {
            sourcePositions.push(index);
          }
        });
        dispatch(initPositions(positions, sourcePositions));
      }
    }
  }, [data.blocks]);

  return (
    <Renderer
      state={{
        positions: value,
        sourcePositions: state.sourcePositions,
      }}
      variant="normal"
      dispatch={dispatch}
      blocks={data.blocks}
    />
  );
}

Evaluation.propTypes = {
  field: PropTypes.string.isRequired,
  data: PropTypes.shape({
    blocks: PropTypes.arrayOf(PropTypes.object),
  }),
};

function Statistic({ statistic }) {
  return (
    <PathStatistic nodes={{ ...statistic.nodes }} max={statistic?.count} />
  );
}

Statistic.propTypes = {
  statistic: PropTypes.shape({
    count: PropTypes.number,
    nodes: PropTypes.array,
  }),
};

Statistic.defaultProps = {
  statistic: {
    nodes: {},
  },
};

function getText(block) {
  let text = '';
  if (block.text.blocks) {
    block.text.blocks.forEach((block) => {
      text += '<p>' + block.text + '</p>';
    });
  }
  return text;
}

function getTextForPath(path, blocks) {
  let text = '';
  path.forEach((p) => {
    const block = blocks[p];
    text += getText(block);
  });
  return text;
}

function updateStatistic(statistic, solution, _, task) {
  if (!statistic) {
    statistic = {
      count: 0,
      nodes: {},
    };
  }

  if (solution?.path) {
    let lastNode = null;

    let startKey = '[]';
    task.blocks.forEach((b, index) => {
      const positions = [];
      if (b.locked) {
        positions.push(index);
      }
      startKey = JSON.stringify(positions);
    });
    solution.path.forEach((p, index) => {
      const key = JSON.stringify(p);
      const node = statistic.nodes[key] || {
        id: key,
        state: getTextForPath(p, task.blocks),
        count: 0,
        edges: {},
      };

      if (key === startKey) {
        node.start = true;
      }

      if (index == solution.path.length - 1) {
        node.end = true;
      }
      node.count += 1;
      statistic.nodes[key] = node;

      if (lastNode) {
        const edge = lastNode.edges[key] || {
          from: lastNode.id,
          to: key,
          count: 0,
        };
        edge.count += 1;
        lastNode.edges[key] = edge;
      }
      lastNode = node;
      statistic.nodes[lastNode.id] = lastNode;
    });
  }
  statistic.count = statistic.count + 1;
  return statistic;
}

export default {
  Renderer,
  Evaluation,
  Form,
  reducer,
  Statistic,
  updateStatistic,
};
