import PropTypes from 'prop-types';
import { Droppable } from 'react-beautiful-dnd';
import { useTheme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

import Item from './Item';

const grid = 8;

const getListStyle = (isDraggingOver, theme) => ({
  background: isDraggingOver
    ? theme.palette.primary.light
    : theme.palette.background.paper,
  padding: grid,
  width: '100%',
});

const Column = ({ droppableId, data, title, ...props }) => {
  const theme = useTheme();
  return (
    <Droppable droppableId={droppableId}>
      {(provided, snapshot) => (
        <div
          ref={provided.innerRef}
          style={getListStyle(snapshot.isDraggingOver, theme)}
        >
          <div>
            <Typography style={{ marginBottom: 8 }}>{title}</Typography>
            {data.map((item, index) => (
              <Item item={item} index={index} key={item.id} {...props} />
            ))}
          </div>
          {provided.placeholder}
        </div>
      )}
    </Droppable>
  );
};

Column.propTypes = {
  droppableId: PropTypes.string.isRequired,
  data: PropTypes.object,
  title: PropTypes.string,
};

export default Column;
