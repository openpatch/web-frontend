import { Trans } from '@lingui/macro';
import PropTypes from 'prop-types';
import { DragDropContext } from 'react-beautiful-dnd';
import Column from './Column';

function List({ onDragEnd, user, source, ...props }) {
  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'space-between',
        width: '100%',
      }}
    >
      <DragDropContext onDragEnd={onDragEnd}>
        <Column
          droppableId="source"
          title={<Trans>Drag from here</Trans>}
          data={source}
          {...props}
        />
        <div
          style={{
            marginLeft: 24,
            marginRight: 24,
            width: 4,
            background: 'black',
          }}
        />
        <Column
          droppableId="user"
          title={<Trans>Drop blocks here</Trans>}
          data={user}
          {...props}
        />
      </DragDropContext>
    </div>
  );
}

List.propTypes = {
  onDragEnd: PropTypes.func,
  user: PropTypes.array,
  source: PropTypes.array,
};

List.defaultProps = {
  onDragEnd: () => null,
  source: [],
  user: [],
};

export default List;
