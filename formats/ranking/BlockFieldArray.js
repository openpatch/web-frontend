import { Trans } from '@lingui/macro';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import { Field } from 'react-final-form';

import RichTextField from '../../components/fields/RichTextField';
import Checkbox from '../../components/fields/Checkbox';
import TextField from '../../components/fields/TextField';
import { uuidv4 } from '../../utils/uuid';

const BlockFieldArray = ({
  fields,
  label,
  helperText,
  fullWidth,
  ...props
}) => {
  <FormControl fullWidth={fullWidth} margin="dense" {...props}>
    {label && <FormLabel>{label}</FormLabel>}
    {fields.map((field, index) => (
      <div key={index} style={{ display: 'flex', alignItems: 'center' }}>
        <div style={{ marginRight: 8, flex: 2 }}>
          <ButtonGroup variant="outlined" fullWidth>
            <Button onClick={() => fields.remove(index)}>Remove</Button>
          </ButtonGroup>
          <Field
            name={`${field}.locked`}
            component={Checkbox}
            type="checkbox"
            label={<Trans>Locked</Trans>}
          />
          <Field
            name={`${field}.linked`}
            component={TextField}
            fullWidth
            type="number"
            label={<Trans>Linked</Trans>}
          />
        </div>
        <Field
          name={`${field}.text`}
          fullWidth={fullWidth}
          component={RichTextField}
          label={<Trans>Text</Trans>}
        />
      </div>
    ))}
    <Button
      onClick={() =>
        fields.push({
          id: uuidv4(),
        })
      }
    >
      Add Element
    </Button>
    {helperText && <FormHelperText>{helperText}</FormHelperText>}
  </FormControl>;
};

BlockFieldArray.propTypes = {
  fields: PropTypes.object.isRequired,
  label: PropTypes.node,
  helperText: PropTypes.node,
  fullWidth: PropTypes.bool,
};

BlockFieldArray.defaultProps = {};

export default BlockFieldArray;
