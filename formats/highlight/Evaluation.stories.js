import { action } from '@storybook/addon-actions';
import arrayMutators from 'final-form-arrays';

import { Evaluation } from './';
import MForm from '../../components/Form';
import { getLoremIpsum } from '../../utils/editor';
import highlightPlugin from './highlight-plugin';

const loremIpsum = getLoremIpsum();

export default {
  title: 'Format/Highlight/Evaluation',
  component: Evaluation,
};

export const Empty = () => (
  <MForm
    onSubmit={(v) => {
      action('submit')(v);
    }}
    mutators={{ ...arrayMutators }}
    render={() => (
      <Evaluation
        field="evaluation"
        data={{
          text: loremIpsum,
          colors: highlightPlugin.colors.map((color) => color.value),
        }}
      />
    )}
  />
);

export const PreFilled = () => (
  <MForm
    onSubmit={action('submit')}
    initialValues={{
      evaluation: {
        cutoff: 0.8,
        highlightedText: {
          blocks: [
            {
              key: 'biakn',
              text:
                'Lorem ipsum dolor sit amet, consetetur sadipscingelitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magnaaliquyam erat, sed diam voluptua. At vero eos et accusam et justo duodolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctusest Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetursadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore etdolore magna aliquyam erat, sed diam voluptua. At vero eos et accusamet justo duo dolores et ea rebum. Stet clita kasd gubergren, no seatakimata sanctus est Lorem ipsum dolor sit amet. Lorem ip',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [
                {
                  offset: 0,
                  length: 5,
                  style: '#4e79a7',
                },
                {
                  offset: 264,
                  length: 5,
                  style: '#4e79a7',
                },
                {
                  offset: 292,
                  length: 5,
                  style: '#4e79a7',
                },
                {
                  offset: 584,
                  length: 5,
                  style: '#4e79a7',
                },
                {
                  offset: 6,
                  length: 5,
                  style: '#e15759',
                },
                {
                  offset: 270,
                  length: 5,
                  style: '#e15759',
                },
                {
                  offset: 298,
                  length: 5,
                  style: '#e15759',
                },
              ],
              entityRanges: [],
              data: {},
            },
          ],
          entityMap: {},
        },
      },
    }}
    mutators={{ ...arrayMutators }}
    render={() => (
      <Evaluation
        field="evaluation"
        data={{
          text: loremIpsum,
          colors: highlightPlugin.colors.map((color) => color.value),
        }}
      />
    )}
  />
);
