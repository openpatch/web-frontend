import { Statistic } from './';
import highlightPlugin from './highlight-plugin';

export default {
  title: 'Format/Highlight/Statistic',
  component: Statistic,
};

export const Default = () => (
  <Statistic
    statistic={{
      avgKappa: 0.7,
      highlights: {
        [highlightPlugin.colors[0].value]: {
          biakn: {
            '0': 20,
            '1': 19,
            '2': 18,
            '3': 19,
            '4': 20,
            '5': 18,
          },
        },
      },
      count: 20,
    }}
    data={{
      colors: highlightPlugin.colors.map((color) => color.value),
      text: {
        blocks: [
          {
            key: 'biakn',
            text:
              'Lorem ipsum dolor sit amet, consetetur sadipscingelitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magnaaliquyam erat, sed diam voluptua. At vero eos et accusam et justo duodolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctusest Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetursadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore etdolore magna aliquyam erat, sed diam voluptua. At vero eos et accusamet justo duo dolores et ea rebum. Stet clita kasd gubergren, no seatakimata sanctus est Lorem ipsum dolor sit amet. Lorem ip',
            type: 'unstyled',
            depth: 0,
            inlineStyleRanges: [],
            entityRanges: [],
            data: {},
          },
        ],
        entityMap: {},
      },
    }}
    evaluation={{
      blocks: [
        {
          key: 'biakn',
          text:
            'Lorem ipsum dolor sit amet, consetetur sadipscingelitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magnaaliquyam erat, sed diam voluptua. At vero eos et accusam et justo duodolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctusest Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetursadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore etdolore magna aliquyam erat, sed diam voluptua. At vero eos et accusamet justo duo dolores et ea rebum. Stet clita kasd gubergren, no seatakimata sanctus est Lorem ipsum dolor sit amet. Lorem ip',
          type: 'unstyled',
          depth: 0,
          inlineStyleRanges: [
            {
              offset: 0,
              length: 5,
              style: '#4e79a7',
            },
            {
              offset: 264,
              length: 5,
              style: '#4e79a7',
            },
            {
              offset: 292,
              length: 5,
              style: '#4e79a7',
            },
            {
              offset: 584,
              length: 5,
              style: '#4e79a7',
            },
            {
              offset: 6,
              length: 5,
              style: '#e15759',
            },
            {
              offset: 270,
              length: 5,
              style: '#e15759',
            },
            {
              offset: 298,
              length: 5,
              style: '#e15759',
            },
          ],
          entityRanges: [],
          data: {},
        },
      ],
      entityMap: {},
    }}
  />
);
