import { useReducer } from 'react';
import { action } from '@storybook/addon-actions';
import { Renderer, reducer } from './';
import { getLoremIpsum } from '../../utils/editor';
import highlightPlugin from './highlight-plugin';

export default {
  title: 'Format/Highlight/Renderer',
  component: Renderer,
};

const loremIpsum = getLoremIpsum();

export const Empty = () => (
  <Renderer
    dispatch={action('dispatch')}
    state={{ choices: {} }}
    text={loremIpsum}
    colors={highlightPlugin.colors.map((color) => color.value)}
  />
);

export const WithDispatch = () => {
  const [state, dispatch] = useReducer(reducer);

  const handleDispatch = (a) => {
    action('dispatch')(a);
    dispatch(a);
  };

  return (
    <Renderer
      dispatch={handleDispatch}
      state={state}
      text={loremIpsum}
      colors={highlightPlugin.colors.map((color) => color.value)}
    />
  );
};

export const WithDispatchRich = () => {
  const [state, dispatch] = useReducer(reducer);

  const handleDispatch = (a) => {
    action('dispatch')(a);
    dispatch(a);
  };

  return (
    <Renderer
      dispatch={handleDispatch}
      state={state}
      text={loremIpsum}
      colors={highlightPlugin.colors.map((color) => color.value)}
    />
  );
};
