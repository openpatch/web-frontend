import { useEffect, useState, useReducer } from 'react';
import { Trans, t } from '@lingui/macro';
import PropTypes from 'prop-types';
import produce from 'immer';
import { Field, useField } from 'react-final-form';
import { FieldArray } from 'react-final-form-arrays';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';
import { EditorState, Modifier, SelectionState } from 'draft-js';

import highlightPlugin, {
  createHandleKeyCommand,
  clear,
} from './highlight-plugin';
import Editor from '../../components/RichTextEditor';
import RichTextField from '../../components/fields/RichTextField';
import TextField from '../../components/fields/TextField';
import SelectFieldArray from '../../components/fields/SelectFieldArray';
import { convertFromRaw, convertToRaw } from '../../utils/editor';
import { updateAvg } from '../../utils/math';

export const INIT_TEXT = 'highlight/INIT_TEXT';
export const INIT_COLORS = 'highlight/INIT_COLORS';
export const SELECT = 'highlight/SELECT';
export const HIGHLIGHT_SELECTION = 'highlight/HIGHLIGHT_SELECTION';
export const CLEAR_HIGHLIGHT = 'highlight/CLEAR_HIGHLIGHT';
export const CLEAR_ALL = 'highlight/CLEAR_ALL';

const format = 'highlight';

export function initText(text) {
  return {
    type: INIT_TEXT,
    format,
    payload: {
      text,
    },
  };
}

export function initColors(colors) {
  return {
    type: INIT_COLORS,
    format,
    payload: {
      colors,
    },
  };
}

export function select(selection) {
  return {
    type: SELECT,
    format,
    payload: {
      ...selection,
    },
  };
}

export function highlightSelection(color) {
  return {
    type: HIGHLIGHT_SELECTION,
    format,
    payload: {
      color,
    },
  };
}

export function clearHighlight() {
  return {
    type: CLEAR_HIGHLIGHT,
    format,
    payload: {},
  };
}

export function clearAll() {
  return {
    format,
    type: CLEAR_ALL,
  };
}

const initialState = {
  text: {},
  editorState: EditorState.createEmpty(),
  highlightedText: {},
  selection: null,
  colors: [],
};

export const reducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case INIT_COLORS:
        draft.colors = action.payload.colors;
        break;
      case INIT_TEXT:
        draft.text = action.payload.text;
        draft.editorState = convertFromRaw(action.payload.text);
        draft.highlightedText = action.payload.text;
        draft.selection = SelectionState.createEmpty();
        draft.editorState = EditorState.forceSelection(
          draft.editorState,
          draft.selection
        );
        break;
      case SELECT:
        draft.selection = new SelectionState(action.payload);
        draft.editorState = EditorState.forceSelection(
          draft.editorState,
          draft.selection
        );
        break;
      case HIGHLIGHT_SELECTION:
        {
          let editorState = clear(state.editorState, state.selection);
          draft.editorState = EditorState.push(
            state.editorState,
            Modifier.applyInlineStyle(
              editorState.getCurrentContent(),
              state.selection,
              action.payload.color
            ),
            'change-inline-style'
          );
          draft.editorState = EditorState.forceSelection(
            draft.editorState,
            draft.selection
          );
          draft.highlightedText = convertToRaw(
            draft.editorState.getCurrentContent()
          );
        }
        break;
      case CLEAR_ALL:
        draft.editorState = convertFromRaw(draft.text);
        draft.highlightedText = convertToRaw(
          draft.editorState.getCurrentContent()
        );
        break;
      case CLEAR_HIGHLIGHT:
        draft.editorState = clear(state.editorState, state.selection);
        draft.editorState = EditorState.forceSelection(
          draft.editorState,
          draft.selection
        );
        draft.highlightedText = convertToRaw(
          draft.editorState.getCurrentContent()
        );
        break;
    }
  });

export function Renderer({ state, dispatch, text, colors, variant }) {
  function handleHighlight(color) {
    if (variant === 'normal' && colors.includes(color)) {
      dispatch(highlightSelection(color));
    }
  }

  function handleSelect(e) {
    if (variant === 'normal' && state.editorState) {
      const selection = e.getSelection();
      const oldSelection = state.selection || SelectionState.createEmpty();
      if (oldSelection.serialize() != selection.serialize()) {
        dispatch(
          select({
            anchorKey: selection.anchorKey,
            anchorOffset: selection.anchorOffset,
            focusKey: selection.focusKey,
            focusOffset: selection.focusOffset,
            isBackward: selection.isBackward,
          })
        );
      }
    }
  }

  function handleClearHighlight() {
    if (variant === 'normal') {
      dispatch(clearHighlight());
    }
  }

  function handleClearAll() {
    if (variant === 'normal') {
      dispatch(clearAll());
    }
  }

  useEffect(() => {
    if (text && variant === 'normal') {
      // something is up with dispatch and the session provider that a timeout is needed
      setTimeout(() => {
        dispatch(initText(text));
      }, 400);
    }
  }, [text]);

  useEffect(() => {
    if (variant === 'result') {
      setTimeout(() => {
        dispatch(initText(state.highlightedText));
      }, 400);
    }
  }, [state.highlightedText]);

  return (
    <Box width="100%">
      <Box display="flex">
        {colors.map((color, i) => (
          <Tooltip key={color} title={`Shortcut: ${i + 1}`}>
            <Button
              variant="outlined"
              style={{ marginRight: 8 }}
              onClick={() => handleHighlight(color)}
            >
              <div
                style={{
                  borderRadius: 2,
                  width: '100%',
                  height: 20,
                  backgroundColor: color,
                }}
              ></div>
            </Button>
          </Tooltip>
        ))}
        <Tooltip title={t`Shortcut: DEL`}>
          <Button onClick={() => handleClearHighlight()}>
            <Trans>Clear</Trans>
          </Button>
        </Tooltip>
        <Box flex={1}></Box>
        <Button onClick={() => handleClearAll()}>
          <Trans>Clear All</Trans>
        </Button>
      </Box>
      <Box p={1}>
        <Editor
          handleKeyCommand={createHandleKeyCommand({
            onClear: handleClearHighlight,
            onClearAll: handleClearAll,
            onHighlight: handleHighlight,
          })}
          editorState={state.editorState || EditorState.createEmpty()}
          onChange={handleSelect}
          customPlugins={[highlightPlugin]}
        />
      </Box>
    </Box>
  );
}

Renderer.propTypes = {
  state: PropTypes.shape({
    highlightedText: PropTypes.object,
    editorState: PropTypes.object,
    selection: PropTypes.object,
  }),
  dispatch: PropTypes.func.isRequired,
  text: PropTypes.object,
  colors: PropTypes.arrayOf(PropTypes.string),
  variant: PropTypes.oneOf(['normal', 'player', 'result']),
};

Renderer.defaultProps = {
  colors: [],
  text: null,
  state: initialState,
  variant: 'normal',
};

export function Form({ field }) {
  return (
    <>
      <Field
        name={`${field}.text`}
        label={<Trans>Text to highlight</Trans>}
        component={RichTextField}
        deactivateFromToolbar={[
          'format-color-text',
          'format-color-fill',
          'image',
          'link',
          'embed',
        ]}
      />
      <FieldArray
        name={`${field}.colors`}
        label={<Trans>Highlight Colors</Trans>}
        fullWidth
        options={highlightPlugin.colors}
        getText={(o) => (
          <Box display="flex" alignItems="center">
            <Box
              style={{
                backgroundColor: o.value,
                width: 10,
                height: 10,
                marginRight: 8,
              }}
            ></Box>
            {o.label}
          </Box>
        )}
        getValue={(o) => o.value}
        component={SelectFieldArray}
      />
    </>
  );
}

Form.propTypes = {
  field: PropTypes.string.isRequired,
};

export function Evaluation({ field, data }) {
  const {
    input: { value, onChange },
  } = useField(`${field}.highlightedText`, {
    initialValue: data?.text,
  });
  const [state, dispatch] = useReducer(reducer, {
    ...initialState,
    text: data?.text,
    editorState: convertFromRaw(value),
    highlightedText: value,
  });

  useEffect(() => {
    if (state?.highlightedText) {
      onChange(state?.highlightedText);
    }
  }, [state?.highlightedText]);

  return (
    <>
      <Field
        name={`${field}.cutoff`}
        label={<Trans>Cutoff</Trans>}
        fullWidth
        component={TextField}
        type="number"
      />
      <Button
        onClick={() => {
          dispatch(clearAll());
        }}
      >
        Reset Text and Highlights
      </Button>
      <Renderer
        state={{
          ...state,
          editorState: convertFromRaw(value),
          highlightedText: value,
        }}
        variant="normal"
        dispatch={dispatch}
        {...data}
        text={null}
      />
    </>
  );
}

Evaluation.propTypes = {
  field: PropTypes.string.isRequired,
  data: PropTypes.shape({
    text: PropTypes.object,
    colors: PropTypes.array,
  }),
};

export function Legend({ buckets, colors }) {
  const entries = [];
  colors.forEach((color) => {
    buckets.forEach((bucket) => {
      let colorRGB = hexToRgb(color);
      entries.push(
        <Box
          key={`${color}-${bucket}`}
          display="inline-flex"
          alignItems="center"
          mr={2}
        >
          <Box
            width={10}
            height={6}
            mr={1}
            style={{
              backgroundColor: `rgba(${colorRGB.r}, ${colorRGB.g}, ${colorRGB.b}, ${bucket})`,
            }}
          />
          <Typography variant="caption">{bucket * 100} %</Typography>
        </Box>
      );
    });
    entries.push(<br key={color} />);
  });
  return <Box>{entries}</Box>;
}

Legend.propTypes = {
  buckets: PropTypes.arrayOf(PropTypes.number),
  colors: PropTypes.arrayOf(PropTypes.string),
};

function hexToRgb(hex) {
  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result
    ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16),
      }
    : null;
}

export function Statistic({ statistic, data, evaluation }) {
  const [editorState, setEditorState] = useState(EditorState.createEmpty());
  const { colors, text } = data;
  const { highlights, count } = statistic;
  const { highlightedText } = evaluation;

  let styleMap = null;
  const buckets = [0, 0.2, 0.4, 0.6, 0.8, 1];
  if (colors) {
    styleMap = {};
    colors.forEach((color) => {
      let colorRGB = hexToRgb(color);
      styleMap[`${color}-correct`] = {
        backgroundColor: `rgba(${colorRGB.r}, ${colorRGB.g}, ${colorRGB.b}, ${buckets[1]})`,
      };
    });
    buckets.forEach((bucket) => {
      colors.forEach((color) => {
        let colorRGB = hexToRgb(color);
        styleMap[`${color}-${bucket}`] = {
          borderBottomColor: `rgba(${colorRGB.r}, ${colorRGB.g}, ${colorRGB.b}, ${bucket})`,
          borderBottomSize: 1,
          borderBottomStyle: 'solid',
        };
      });
    });
  }

  useEffect(() => {
    if (text) {
      let highlightedEditorState = convertFromRaw(data.text);

      if (highlightedText) {
        highlightedText.blocks.forEach((block) => {
          block.inlineStyleRanges.forEach((inlineStyleRange) => {
            const anchorOffset = inlineStyleRange.offset;
            const focusOffset =
              inlineStyleRange.offset + inlineStyleRange.length;
            const color = inlineStyleRange.style;

            if (colors.includes(color)) {
              const selectionState = new SelectionState({
                anchorKey: block.key,
                anchorOffset: anchorOffset,
                focusKey: block.key,
                focusOffset: focusOffset,
                isBackward: false,
              });
              const newContentState = Modifier.applyInlineStyle(
                highlightedEditorState.getCurrentContent(),
                selectionState,
                `${color}-correct`
              );
              highlightedEditorState = EditorState.push(
                highlightedEditorState,
                newContentState,
                'change-inline-style'
              );
            }
          });
        });
      }

      if (highlights) {
        Object.keys(highlights).forEach((color) => {
          const colorHighlights = highlights[color];
          Object.keys(colorHighlights).forEach((key) => {
            const indexes = colorHighlights[key];
            Object.keys(indexes).forEach((index) => {
              const value = indexes[index];
              const selectionState = new SelectionState({
                anchorKey: key,
                anchorOffset: index,
                focusKey: key,
                focusOffset: Number(index) + 1,
                isBackward: false,
              });
              const percentage = value / count;
              const bucket = buckets.find((b) => b >= percentage);
              const newContentState = Modifier.applyInlineStyle(
                highlightedEditorState.getCurrentContent(),
                selectionState,
                `${color}-${bucket}`
              );
              highlightedEditorState = EditorState.push(
                highlightedEditorState,
                newContentState,
                'change-inline-style'
              );
            });
          });
        });
      }

      setEditorState(highlightedEditorState);
    }
  }, [text, highlights, count]);

  return (
    <div>
      {styleMap && (
        <>
          <Editor
            readOnly
            onChange={() => null}
            editorState={editorState}
            customStyleMap={styleMap}
          />
          <Legend buckets={buckets} colors={colors} />
        </>
      )}
    </div>
  );
}

Statistic.propTypes = {
  statistic: PropTypes.shape({
    avgKappa: PropTypes.number,
    highlights: PropTypes.object,
    count: PropTypes.number,
  }),
  data: PropTypes.shape({
    text: PropTypes.object,
    colors: PropTypes.arrayOf(PropTypes.string),
  }),
  evaluation: PropTypes.shape({
    highlightedText: PropTypes.object,
  }),
};

Statistic.defaultProps = {
  statistic: {},
  data: {},
};

export function updateStatistic(statistic, solution, evaluation, data) {
  if (!statistic) {
    statistic = {
      count: 0,
      avgKappa: {},
      highlights: {},
    };
  }

  statistic.count = statistic.count + 1;
  const colors = data.colors;
  colors.forEach((color) => {
    if (!statistic.highlights[color]) {
      statistic.highlights[color] = {};
    }
  });

  if (solution && solution.highlightedText) {
    const blocks = solution.highlightedText.blocks;
    const highlights = statistic.highlights;

    blocks.forEach((block) => {
      const key = block.key;

      colors.forEach((color) => {
        if (!highlights[color][key]) {
          highlights[color][key] = {};
        }
      });

      block.inlineStyleRanges.forEach((inlineStyleRange) => {
        const offset = inlineStyleRange.offset;
        const length = inlineStyleRange.length;
        const color = inlineStyleRange.style;

        const blockHighlight = highlights[color][key];

        for (let i = offset; i < offset + length; i++) {
          blockHighlight[i] = (blockHighlight[i] || 0) + 1;
        }
      });
    });
  }

  if (evaluation.details) {
    Object.keys(evaluation.details).forEach((color) => {
      if (!statistic.avgKappa[color]) {
        statistic.avgKappa[color] = 0;
      }

      statistic.avgKappa[color] = updateAvg(
        statistic.avgKappa[color],
        evaluation.details[color].kappa,
        statistic.count
      );
    });
  }

  return statistic;
}

export function clean(state) {
  return {
    highlightedText: state.highlightedText,
  };
}

export default {
  Renderer,
  Evaluation,
  Statistic,
  Form,
  reducer,
  clean,
  updateStatistic,
};
