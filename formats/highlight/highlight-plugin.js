import { getDefaultKeyBinding, KeyBindingUtil } from 'draft-js';
import colorPlugin from '../../utils/editor/color-plugin';
import { removeStylesFromSelection } from '../../utils/editor';

const colors = colorPlugin.colors;

const customStyleMap = {};
colorPlugin.colors.forEach((color) => {
  customStyleMap[color.value] = {
    borderBottomColor: color.value,
    borderBottomSize: 1,
    borderBottomStyle: 'solid',
  };
});

const keyBindingFn = (e) => {
  const { keyCode } = e;
  const colorIndex = keyCode - 49;
  if (colors[colorIndex] && colorIndex < 10) {
    return `highlight-${colorIndex}`;
  } else if (keyCode == 46 && KeyBindingUtil.hasCommandModifier(e)) {
    return `highlight-clear-all`;
  } else if (keyCode == 46) {
    return `highlight-clear`;
  }
  return getDefaultKeyBinding(e);
};

export const clear = (editorState, selection) => {
  return removeStylesFromSelection(
    editorState,
    selection,
    colorPlugin.colors.map((color) => color.value)
  );
};

export const createHandleKeyCommand = ({
  onClear,
  onClearAll,
  onHighlight,
}) => (command) => {
  if (command.match(/^highlight-\d+$/g)) {
    const colorIndex = command.split('-')[1];
    onHighlight(colorPlugin.colors[colorIndex].value);
    return 'handled';
  }
  if (command === 'highlight-clear-all') {
    onClearAll();
    return 'handled';
  }
  if (command === 'highlight-clear') {
    onClear();
    return 'handled';
  }
  return 'not-handled';
};

export default {
  keyBindingFn,
  customStyleMap,
  colors,
};
