import { action } from '@storybook/addon-actions';
import arrayMutators from 'final-form-arrays';
import { Form } from './';
import MForm from '../../components/Form';
import { getLoremIpsum } from '../../utils/editor';
import highlightPlugin from './highlight-plugin';

const loremIpsum = getLoremIpsum();

export default {
  title: 'Format/Highlight/Form',
  component: Form,
};

export const Empty = () => (
  <MForm
    onSubmit={action('submit')}
    mutators={{ ...arrayMutators }}
    render={() => <Form field="form" />}
  />
);

export const PreFilled = () => (
  <MForm
    onSubmit={action('submit')}
    initialValues={{
      form: {
        text: loremIpsum,
        colors: highlightPlugin.colors.map((color) => color.value),
      },
    }}
    mutators={{ ...arrayMutators }}
    render={() => <Form field="form" />}
  />
);
