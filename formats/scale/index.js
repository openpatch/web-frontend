import { Trans, t } from '@lingui/macro';
import PropTypes from 'prop-types';
import produce from 'immer';
import { Field, useField } from 'react-final-form';
import Slider from '@material-ui/core/Slider';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import { ResponsiveBar } from '@nivo/bar';

import TextField from '../../components/fields/TextField';
import OperatorField from '../../components/fields/OperatorField';

export const CHANGE_VALUE = 'text/CHANGE_VALUE';

export function changeValue(value) {
  return {
    type: CHANGE_VALUE,
    payload: {
      value,
    },
  };
}

const initialState = {
  value: 0,
};

export const reducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case CHANGE_VALUE:
        draft.value = action.payload.value;
        break;
    }
  });

const MySlider = withStyles((theme) => ({
  track: {
    height: 4,
    backgroundColor: theme.palette.primary.light,
  },
  rail: {
    backgroundColor: theme.palette.primary.main,
  },
  valueLabel: {
    '& *': {
      backgroundColor: theme.palette.secondary.main,
      color: theme.palette.primary.main,
      fontWeight: 'bold',
    },
  },
  mark: {
    width: 2,
    height: 8,
    marginTop: -4,
  },
  markActive: {
    backgroundColor: theme.palette.primary.light,
  },
}))(Slider);

export function Renderer({
  state,
  dispatch,
  min,
  max,
  steps,
  lowerLabel,
  upperLabel,
  variant,
}) {
  function handleChange(e, value) {
    dispatch(changeValue(value));
  }

  return (
    <Box display="flex" alignItems="center">
      {lowerLabel && (
        <Typography style={{ marginRight: 16, flex: 1, textAlign: 'center' }}>
          {lowerLabel}
        </Typography>
      )}
      <MySlider
        style={{ flex: 2 }}
        min={min ? Number(min) : undefined}
        max={max ? Number(max) : undefined}
        valueLabelDisplay="auto"
        step={steps ? Number(steps) : undefined}
        marks={steps ? true : undefined}
        value={state?.value}
        onChange={variant === 'normal' ? handleChange : () => null}
      />
      {upperLabel && (
        <Typography style={{ marginLeft: 16, flex: 1, textAlign: 'center' }}>
          {upperLabel}
        </Typography>
      )}
    </Box>
  );
}

Renderer.propTypes = {
  state: PropTypes.shape({
    value: PropTypes.number,
  }),
  dispatch: PropTypes.func.isRequired,
  min: PropTypes.number,
  max: PropTypes.number,
  steps: PropTypes.number,
  lowerLabel: PropTypes.string,
  upperLabel: PropTypes.string,
  variant: PropTypes.oneOf(['normal', 'player', 'result']),
};

Renderer.defaultProps = {
  state: initialState,
};

function Form({ field }) {
  const {
    input: { value: min },
  } = useField(`${field}.min`);
  const {
    input: { value: max },
  } = useField(`${field}.max`);
  return (
    <>
      <Field
        name={`${field}.lowerLabel`}
        fullWidth
        label={<Trans>Lower Label</Trans>}
        component={TextField}
      />
      <Field
        name={`${field}.upperLabel`}
        fullWidth
        label={<Trans>Upper Label</Trans>}
        component={TextField}
      />
      <Field
        name={`${field}.min`}
        fullWidth
        label={<Trans>Min</Trans>}
        required={true}
        max={max}
        component={TextField}
        type="number"
      />
      <Field
        name={`${field}.max`}
        fullWidth
        label={<Trans>Max</Trans>}
        required={true}
        min={min}
        component={TextField}
        type="number"
      />
      <Field
        name={`${field}.steps`}
        fullWidth
        label={<Trans>Steps</Trans>}
        min={min}
        max={max}
        required={true}
        component={TextField}
        type="number"
      />
    </>
  );
}

Form.propTypes = {
  field: PropTypes.string.isRequired,
};

function Evaluation({ field }) {
  return (
    <>
      <Field
        name={`${field}.operator`}
        label={<Trans>Operator</Trans>}
        fullWidth
        component={OperatorField}
      />
      <Field
        name={`${field}.value`}
        fullWidth
        label={<Trans>Value</Trans>}
        component={TextField}
        type="number"
      />
    </>
  );
}

Evaluation.propTypes = {
  field: PropTypes.string.isRequired,
};

function updateStatistic(statistic, solution) {
  if (!statistic) {
    statistic = {
      count: 1,
      frequencies: {
        [solution.value]: 1,
      },
    };
  } else if (solution && solution.value) {
    statistic.count = statistic.count + 1;
    statistic.frequencies[solution.value] =
      (statistic.frequencies[solution.value] || 0) + 1;
  }
  return statistic;
}

export function Statistic({ statistic }) {
  const data = [];
  if (statistic?.frequencies) {
    Object.keys(statistic.frequencies).forEach((key) => {
      const value = statistic.frequencies[key];
      data.push({ id: key, value });
    });
  }

  return statistic?.frequencies ? (
    <Box height={250}>
      <ResponsiveBar
        data={data}
        animate={true}
        colors={{ scheme: 'nivo' }}
        margin={{ top: 20, right: 30, bottom: 50, left: 50 }}
        padding={0.3}
        axisBottom={{
          tickSize: 5,
          tickPadding: 5,
          legend: t`Choice`,
          legendPosition: 'middle',
          legendOffset: 32,
        }}
        axisLeft={{
          tickSize: 5,
          tickPadding: 5,
          tickRotation: 0,
          legend: t`Frequency`,
          legendPosition: 'middle',
          legendOffset: -40,
        }}
      />
    </Box>
  ) : null;
}

Statistic.propTypes = {
  statistic: PropTypes.shape({
    frequencies: PropTypes.object,
  }),
};

export default {
  Renderer,
  Evaluation,
  Form,
  reducer,
  Statistic,
  updateStatistic,
};
