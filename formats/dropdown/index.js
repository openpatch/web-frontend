import { t } from '@lingui/macro';
import PropTypes from 'prop-types';
import produce from 'immer';
import { useField } from 'react-final-form';
import { FieldArray } from 'react-final-form-arrays';
import { ResponsiveBar } from '@nivo/bar';
import Box from '@material-ui/core/Box';

import SelectField from '../../components/fields/SelectField';
import TextFieldArray from '../../components/fields/TextFieldArray';

export const SELECT_CHOICE = 'choice/SELECT_CHOICE';

export function selectChoice(choice) {
  return {
    type: SELECT_CHOICE,
    payload: {
      choice,
    },
  };
}

const initialState = {
  choices: {},
};

export const reducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case SELECT_CHOICE:
        draft.choices = {
          [action.payload.choice]: true,
        };
        break;
    }
  });

export function Renderer({ state, choices, dispatch }) {
  function handleSingleChoice(choice) {
    const index = choices.indexOf(choice);
    dispatch(selectChoice(index));
  }

  let selectedValue = '';
  Object.keys(state.choices).forEach((c) => {
    if (state.choices[c]) {
      selectedValue = choices[c];
    }
  });

  return (
    <SelectField
      input={{
        value: selectedValue,
        onChange: (e) => handleSingleChoice(e.target.value),
      }}
      meta={{}}
      fullWidth
      getText={(o) => o}
      getValue={(o) => o}
      options={choices}
    />
  );
}

Renderer.propTypes = {
  state: PropTypes.shape({
    choices: PropTypes.object,
  }),
  choices: PropTypes.arrayOf(PropTypes.string),
  dispatch: PropTypes.func.isRequired,
};

Renderer.defaultProps = {
  choices: [],
  state: initialState,
};

function Form({ field }) {
  return (
    <>
      <FieldArray
        fullWidth
        name={`${field}.choices`}
        component={TextFieldArray}
      />
    </>
  );
}

Form.propTypes = {
  field: PropTypes.string.isRequired,
};

function Evaluation({ field, data }) {
  const {
    input: { value, onChange },
  } = useField(`${field}.choices`);

  const choices = data?.choices.map((c, i) => ({
    value: i,
    text: c,
  }));

  return (
    <SelectField
      input={{
        value: Object.keys(value || {}).find((o) => value[o] == true) || '',
        onChange: (e) => onChange({ [e.target.value]: true }),
      }}
      meta={{}}
      fullWidth
      getText={(o) => o.text}
      getValue={(o) => o.value}
      options={choices}
    />
  );
}

Evaluation.propTypes = {
  field: PropTypes.string.isRequired,
  data: PropTypes.shape({
    choices: PropTypes.arrayOf(PropTypes.string),
  }),
};

function updateStatistic(statistic, solution) {
  if (!statistic) {
    statistic = {
      count: 0,
      frequencies: {},
    };
  }

  statistic.count = statistic.count + 1;
  Object.keys(solution?.choices || {}).forEach((key) => {
    let frequency = statistic.frequencies[key] || 0;
    if (solution.choices[key]) {
      frequency += 1;
    }
    statistic.frequencies[key] = frequency;
  });

  return statistic;
}

function Statistic({ statistic, data, evaluation }) {
  const barData = [];

  if (statistic?.frequencies) {
    Object.keys(statistic.frequencies).forEach((key) => {
      const value = statistic.frequencies[key];
      barData.push({ id: key, value });
    });
  }

  const legend = [];
  if (data.choices) {
    data.choices.forEach((choice, index) => {
      let correct = false;
      if (evaluation && evaluation.choices) {
        correct = evaluation.choices[index];
      }
      legend.push(
        <Box key={choice} color={correct ? 'success.main' : null}>
          {index}: {choice}
        </Box>
      );
    });
  }

  return statistic?.frequencies ? (
    <>
      <Box height={250}>
        <ResponsiveBar
          data={barData}
          animate={true}
          colors={{ scheme: 'nivo' }}
          margin={{ top: 20, right: 30, bottom: 50, left: 50 }}
          padding={0.3}
          axisBottom={{
            tickSize: 5,
            tickPadding: 5,
            legend: t`Choice`,
            legendPosition: 'middle',
            legendOffset: 32,
          }}
          axisLeft={{
            tickSize: 5,
            tickPadding: 5,
            tickRotation: 0,
            legend: t`Frequency`,
            legendPosition: 'middle',
            legendOffset: -40,
          }}
        />
      </Box>
      {legend}
    </>
  ) : null;
}

Statistic.propTypes = {
  statistic: PropTypes.shape({
    frequencies: PropTypes.objectOf(PropTypes.number),
  }),
  data: PropTypes.shape({
    choices: PropTypes.arrayOf(PropTypes.string),
  }),
  evaluation: PropTypes.shape({
    choices: PropTypes.objectOf(PropTypes.bool),
  }),
};

export default {
  Renderer,
  Evaluation,
  Form,
  reducer,
  updateStatistic,
  Statistic,
};
