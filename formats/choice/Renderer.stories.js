import { useReducer } from 'react';
import { action } from '@storybook/addon-actions';
import { boolean } from '@storybook/addon-knobs';
import { Renderer, reducer } from './';
import { getLoremIpsum } from '../../utils/editor';

export default {
  title: 'Format/Choice/Renderer',
  component: Renderer,
};

export const Empty = () => (
  <Renderer
    dispatch={action('dispatch')}
    state={{ choices: {} }}
    choices={['Hallo']}
    allowMultiple={boolean('allowMultiple')}
  />
);

export const WithDispatch = () => {
  const [state, dispatch] = useReducer(reducer);

  const handleDispatch = (a) => {
    action('dispatch')(a);
    dispatch(a);
  };

  return (
    <Renderer
      dispatch={handleDispatch}
      state={state}
      choices={['Choice 1', 'Choice 2', 'Choice 3']}
      allowMultiple={boolean('allowMultiple')}
    />
  );
};

export const WithDispatchRich = () => {
  const [state, dispatch] = useReducer(reducer);

  const handleDispatch = (a) => {
    action('dispatch')(a);
    dispatch(a);
  };

  return (
    <Renderer
      dispatch={handleDispatch}
      state={state}
      choices={[getLoremIpsum(), 'Choice 2', 'Choice 3']}
      allowMultiple={boolean('allowMultiple')}
    />
  );
};
