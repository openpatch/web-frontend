import { action } from '@storybook/addon-actions';
import arrayMutators from 'final-form-arrays';

import { Evaluation } from './';
import MForm from '../../components/Form';
import { getLoremIpsum } from '../../utils/editor';

export default {
  title: 'Format/Choice/Evaluation',
  component: Evaluation,
};

export const Empty = () => (
  <MForm
    onSubmit={action('submit')}
    mutators={{ ...arrayMutators }}
    render={() => (
      <Evaluation
        field="evaluation"
        data={{ choices: ['Choice 1', 'Choice 2'] }}
      />
    )}
  />
);

export const PreFilled = () => (
  <MForm
    onSubmit={action('submit')}
    initialValues={{
      evaluation: {
        choices: {
          '0': true,
        },
      },
    }}
    mutators={{ ...arrayMutators }}
    render={() => (
      <Evaluation
        field="evaluation"
        data={{ choices: ['Choice 1', 'Choice 2'], allowMultiple: true }}
      />
    )}
  />
);

export const PreFilledRich = () => (
  <MForm
    onSubmit={action('submit')}
    initialValues={{
      evaluation: {
        choices: {
          '0': true,
        },
      },
    }}
    mutators={{ ...arrayMutators }}
    render={() => (
      <Evaluation
        field="evaluation"
        data={{ choices: [getLoremIpsum(), 'Choice 2'], allowMultiple: true }}
      />
    )}
  />
);
