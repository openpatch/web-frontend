import { Statistic } from './';
import { getLoremIpsum } from '../../utils/editor';

export default {
  title: 'Format/Choice/Statistic',
  component: Statistic,
};

export const PreFilled = () => (
  <Statistic
    statistic={{
      frequencies: {
        '0': 200,
        '1': 129,
        '2': 180,
      },
    }}
    data={{ choices: ['Choice 1', 'Choice 2', 'Choice 2'] }}
    evaluation={{ choices: { '0': true } }}
  />
);

export const PreFilledRich = () => (
  <Statistic
    statistic={{
      frequencies: {
        '0': 200,
        '1': 129,
        '2': 180,
      },
    }}
    data={{ choices: [getLoremIpsum(), 'Choice 2', 'Choice 2'] }}
    evaluation={{ choices: { '0': true } }}
  />
);
