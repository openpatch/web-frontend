import { reducer, updateStatistic, deselectChoice, selectChoice } from './';

describe('reducer', () => {
  test('initial state', () => {
    expect(reducer(undefined, {})).toEqual({ choices: {} });
  });

  test('selectChoice without multiple', () => {
    expect(reducer(undefined, selectChoice(1, false))).toEqual({
      choices: { 1: true },
    });

    expect(reducer({ choices: { 0: true } }, selectChoice(1, false))).toEqual({
      choices: { 1: true },
    });
  });

  test('selectChoice with multiple', () => {
    expect(reducer({ choices: { 0: true } }, selectChoice(1, true))).toEqual({
      choices: { 1: true, 0: true },
    });
  });

  test('deselectChoice without multiple', () => {
    expect(reducer(undefined, deselectChoice(1, false)).choices).toEqual({
      1: false,
    });

    expect(
      reducer({ choices: { 1: true } }, deselectChoice(1, false)).choices
    ).toEqual({ 1: false });

    expect(
      reducer({ choices: { 2: true } }, deselectChoice(1, false)).choices
    ).toEqual({ 1: false });
  });

  test('deselectchoice with multiple', () => {
    expect(
      reducer({ choices: { 2: true } }, deselectChoice(1, true)).choices
    ).toEqual({ 1: false, 2: true });

    expect(
      reducer({ choices: { 2: true, 1: true } }, deselectChoice(1, true))
        .choices
    ).toEqual({ 1: false, 2: true });
  });
});

describe('updateStatistic', () => {
  test('empty solution', () => {
    const statistic = updateStatistic(null, {});
    expect(statistic).toEqual({
      count: 1,
      frequencies: {},
    });
  });

  test('many solutions single', () => {
    const solutions = [
      {
        choices: { 1: true },
      },
      {
        choices: { 2: true },
      },
      {
        choices: { 1: true },
      },
      {
        choices: {},
      },
    ];

    const statistic = solutions.reduce(updateStatistic, undefined);
    expect(statistic).toEqual({
      count: 4,
      frequencies: {
        1: 2,
        2: 1,
      },
    });
  });
});
