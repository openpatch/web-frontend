import { Trans, t } from '@lingui/macro';
import PropTypes from 'prop-types';
import produce from 'immer';
import { Field, useField } from 'react-final-form';
import { useTheme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Radio from '@material-ui/core/Radio';
import Checkbox from '@material-ui/core/Checkbox';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Typography from '@material-ui/core/Typography';
import { ResponsiveBar } from '@nivo/bar';

import CheckboxField from '../../components/fields/Checkbox';
import RichText from '../../components/RichText';
import RichTextField from '../../components/fields/RichTextField';
import DnDFieldArray from '../../components/fields/DnDFieldArray';
import { convertFromRaw } from '../../utils/editor';

export const SELECT_CHOICE = 'choice/SELECT_CHOICE';
export const DESELECT_CHOICE = 'choice/DESELECT_CHOICE';

export function deselectChoice(choice, allowMultiple) {
  return {
    type: DESELECT_CHOICE,
    payload: {
      choice,
      allowMultiple,
    },
  };
}

export function selectChoice(choice, allowMultiple) {
  return {
    type: SELECT_CHOICE,
    payload: {
      choice,
      allowMultiple,
    },
  };
}

const initialState = {
  choices: {},
};

export const reducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case SELECT_CHOICE:
        if (!action.payload.allowMultiple) {
          draft.choices = {};
        }
        draft.choices[action.payload.choice] = true;
        break;
      case DESELECT_CHOICE:
        if (!action.payload.allowMultiple) {
          draft.choices = {};
        }
        draft.choices[action.payload.choice] = false;
        break;
    }
  });

export function Renderer({ allowMultiple, state, choices, dispatch }) {
  const theme = useTheme();

  function handleSingleChoice(choice) {
    dispatch(selectChoice(choice, false));
  }

  function handleMultipleChoice(choice) {
    const oldValue = state.choices[choice];

    if (oldValue === true) {
      dispatch(deselectChoice(choice, true));
    } else {
      dispatch(selectChoice(choice, true));
    }
  }

  return (
    <FormControl fullWidth component="fieldset">
      <FormGroup>
        {choices.map((choice, i) => (
          <FormControlLabel
            key={i}
            style={{
              marginBottom: theme.spacing(2),
              padding: theme.spacing(1),
              borderRadius: theme.shape.borderRadius,
              background:
                i % 2 === 0
                  ? theme.palette.background.default
                  : theme.palette.background.paper,
            }}
            control={
              allowMultiple ? (
                <Checkbox
                  checked={state.choices[i] || false}
                  aria-label={choice}
                  onChange={() => handleMultipleChoice(i)}
                  color="primary"
                />
              ) : (
                <Radio
                  checked={state.choices[i] || false}
                  aria-label={choice}
                  onChange={() => handleSingleChoice(i)}
                  color="primary"
                />
              )
            }
            label={<RichText editorState={convertFromRaw(choice)} />}
          />
        ))}
      </FormGroup>
    </FormControl>
  );
}

Renderer.propTypes = {
  allowMultiple: PropTypes.bool,
  state: PropTypes.shape({
    choices: PropTypes.object,
  }),
  choices: PropTypes.arrayOf(
    PropTypes.oneOfType([PropTypes.string, PropTypes.object])
  ),
  dispatch: PropTypes.func.isRequired,
};

Renderer.defaultProps = {
  allowMultiple: false,
  choices: [],
  state: initialState,
};

export function Form({ field }) {
  return (
    <>
      <Field
        name={`${field}.allowMultiple`}
        label={<Trans>Multiple answers can be checked</Trans>}
        type="checkbox"
        component={CheckboxField}
      />
      <DnDFieldArray
        fullWidth
        name={`${field}.choices`}
        elementComponent={RichTextField}
        elementComponentProps={{
          fullWidth: true,
          deactivateFromToolbar: [
            'link',
            'image',
            'embed',
            'ordered-list',
            'unordered-list',
            'blockquote',
          ],
        }}
        locales={{
          add: t`Add Answer Option`,
          remove: (id) => t`Remove Answer Option ${id}`,
          element: (id) => t`Answer Option ${id}`,
        }}
      />
    </>
  );
}

Form.propTypes = {
  field: PropTypes.string.isRequired,
};

export function Evaluation({ field, data }) {
  const {
    input: { value, onChange },
  } = useField(`${field}.choices`);
  const { choices = [], allowMultiple } = data;

  function handleSingleChoice(choice) {
    onChange({
      [choice]: true,
    });
  }

  function handleMultipleChoice(choice) {
    const oldValue = value[choice];

    if (oldValue === true) {
      onChange({ ...value, [choice]: false });
    } else {
      onChange({ ...value, [choice]: true });
    }
  }
  return (
    <FormControl component="fieldset">
      <FormGroup>
        {choices.map((choice, i) => (
          <FormControlLabel
            key={i}
            control={
              allowMultiple ? (
                <Checkbox
                  checked={value[i] || false}
                  aria-label={choice}
                  onChange={() => handleMultipleChoice(i)}
                  color="primary"
                />
              ) : (
                <Radio
                  checked={value[i] || false}
                  aria-label={choice}
                  onChange={() => handleSingleChoice(i)}
                  color="primary"
                />
              )
            }
            label={<RichText editorState={convertFromRaw(choice)} />}
          />
        ))}
      </FormGroup>
    </FormControl>
  );
}

Evaluation.propTypes = {
  field: PropTypes.string.isRequired,
  data: PropTypes.shape({
    choices: PropTypes.arrayOf(
      PropTypes.oneOfType([PropTypes.string, PropTypes.object])
    ),
    allowMultiple: PropTypes.bool,
  }),
};

export function Statistic({ statistic, data, evaluation }) {
  const barData = [];

  if (statistic?.frequencies) {
    Object.keys(statistic.frequencies).forEach((key) => {
      const value = statistic.frequencies[key];
      barData.push({ id: key, value });
    });
  }

  const legend = [];
  if (data.choices) {
    data.choices.forEach((choice, index) => {
      let correct = false;
      if (evaluation && evaluation.choices) {
        correct = evaluation.choices[index];
      }
      legend.push(
        <div key={index} style={{ display: 'flex' }}>
          <Typography
            key={index}
            color={correct ? 'primary' : 'initial'}
            style={{ marginRight: 8, fontWeight: correct ? 'bold' : null }}
          >
            {index}:
          </Typography>
          <RichText editorState={convertFromRaw(choice)} />
        </div>
      );
    });
  }

  return statistic?.frequencies ? (
    <>
      <Box height={250}>
        <ResponsiveBar
          data={barData}
          animate={true}
          colors={{ scheme: 'nivo' }}
          margin={{ top: 20, right: 30, bottom: 50, left: 50 }}
          padding={0.3}
          axisBottom={{
            tickSize: 5,
            tickPadding: 5,
            legend: 'Choice',
            legendPosition: 'middle',
            legendOffset: 32,
          }}
          axisLeft={{
            tickSize: 5,
            tickPadding: 5,
            tickRotation: 0,
            legend: 'Frequency',
            legendPosition: 'middle',
            legendOffset: -40,
          }}
        />
      </Box>
      {legend}
    </>
  ) : null;
}

Statistic.propTypes = {
  statistic: PropTypes.shape({
    frequencies: PropTypes.objectOf(PropTypes.number),
  }),
  data: PropTypes.shape({
    choices: PropTypes.arrayOf(PropTypes.string),
  }),
  evaluation: PropTypes.shape({
    choices: PropTypes.objectOf(PropTypes.bool),
  }),
};

export function updateStatistic(statistic, solution) {
  if (!statistic) {
    statistic = {
      count: 0,
      frequencies: {},
    };
  }
  statistic.count = statistic.count + 1;
  Object.keys(solution?.choices || {}).forEach((key) => {
    let frequency = statistic.frequencies[key] || 0;
    if (solution.choices[key]) {
      frequency += 1;
    }
    statistic.frequencies[key] = frequency;
  });

  return statistic;
}

export default {
  Renderer,
  Evaluation,
  Form,
  Statistic,
  reducer,
  updateStatistic,
};
