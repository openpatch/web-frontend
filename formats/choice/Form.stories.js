import { action } from '@storybook/addon-actions';
import arrayMutators from 'final-form-arrays';
import { Form } from './';
import MForm from '../../components/Form';
import { getLoremIpsum } from '../../utils/editor';

export default {
  title: 'Format/Choice/Form',
  component: Form,
};

export const Empty = () => (
  <MForm
    onSubmit={action('submit')}
    mutators={{ ...arrayMutators }}
    render={() => <Form field="form" />}
  />
);

export const PreFilled = () => (
  <MForm
    onSubmit={action('submit')}
    initialValues={{
      form: {
        choices: ['Choice 1', 'Choice 2'],
        allowMultiple: true,
      },
    }}
    mutators={{ ...arrayMutators }}
    render={() => <Form field="form" />}
  />
);

export const PreFilledRich = () => (
  <MForm
    onSubmit={action('submit')}
    initialValues={{
      form: {
        choices: [getLoremIpsum(), 'Choice 2'],
        allowMultiple: true,
      },
    }}
    mutators={{ ...arrayMutators }}
    render={() => <Form field="form" />}
  />
);
