import { Trans, t } from '@lingui/macro';
import PropTypes from 'prop-types';
import produce from 'immer';
import { Field, useField } from 'react-final-form';
import { ResponsiveBar } from '@nivo/bar';
import Box from '@material-ui/core/Box';

import TextField from '../../components/fields/TextField';
import OperatorField from '../../components/fields/OperatorField';

export const CHANGE_VALUE = 'text/CHANGE_VALUE';

export function changeValue(value) {
  return {
    type: CHANGE_VALUE,
    payload: {
      value,
    },
  };
}

const initialState = {
  value: 0,
};

export const reducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case CHANGE_VALUE:
        draft.value = action.payload.value;
        break;
    }
  });

export function Renderer({ state, dispatch, min, max, steps }) {
  function handleChange(e) {
    dispatch(changeValue(e.target.value));
  }

  return (
    <TextField
      meta={{}}
      fullWidth
      input={{
        value: state.value,
        onChange: handleChange,
      }}
      inputProps={{
        max,
        min,
        step: steps,
      }}
      type="number"
    />
  );
}

Renderer.propTypes = {
  state: PropTypes.shape({
    value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  }),
  dispatch: PropTypes.func.isRequired,
  min: PropTypes.number,
  max: PropTypes.number,
  steps: PropTypes.number,
};

Renderer.defaultProps = {
  state: initialState,
};

function Form({ field }) {
  const {
    input: { value: min },
  } = useField(`${field}.min`);
  const {
    input: { value: max },
  } = useField(`${field}.max`);
  return (
    <>
      <Field
        name={`${field}.min`}
        fullWidth
        label={<Trans>Min</Trans>}
        required={true}
        inputProps={{
          max,
        }}
        parse={(value) => Number(value)}
        component={TextField}
        type="number"
      />
      <Field
        name={`${field}.max`}
        fullWidth
        label={<Trans>Max</Trans>}
        required={true}
        inputProps={{
          min,
        }}
        parse={(value) => Number(value)}
        component={TextField}
        type="number"
      />
      <Field
        name={`${field}.steps`}
        fullWidth
        label={<Trans>Steps</Trans>}
        required={true}
        parse={(value) => Number(value)}
        component={TextField}
        type="number"
      />
    </>
  );
}

Form.propTypes = {
  field: PropTypes.string.isRequired,
};

function Evaluation({ field }) {
  return (
    <>
      <Field
        name={`${field}.operator`}
        label={<Trans>Operator</Trans>}
        fullWidth
        component={OperatorField}
      />
      <Field
        name={`${field}.value`}
        fullWidth
        label={<Trans>Value</Trans>}
        parse={(value) => {
          return Number(value);
        }}
        component={TextField}
        type="number"
      />
    </>
  );
}

Evaluation.propTypes = {
  field: PropTypes.string.isRequired,
};

function updateStatistic(statistic, solution) {
  if (!statistic) {
    statistic = {
      count: 1,
      frequencies: {
        [solution.value]: 1,
      },
    };
  } else if (solution && solution.value) {
    statistic.count = statistic.count + 1;
    statistic.frequencies[solution.value] =
      (statistic.frequencies[solution.value] || 0) + 1;
  }
  return statistic;
}

export function Statistic({ statistic }) {
  const data = [];
  if (statistic?.frequencies) {
    Object.keys(statistic.frequencies).forEach((key) => {
      const value = statistic.frequencies[key];
      data.push({ id: key, value });
    });
  }

  return statistic?.frequencies ? (
    <Box height={250}>
      <ResponsiveBar
        data={data}
        animate={true}
        colors={{ scheme: 'nivo' }}
        margin={{ top: 20, right: 30, bottom: 50, left: 50 }}
        padding={0.3}
        axisBottom={{
          tickSize: 5,
          tickPadding: 5,
          legend: t`Number`,
          legendPosition: 'middle',
          legendOffset: 32,
        }}
        axisLeft={{
          tickSize: 5,
          tickPadding: 5,
          tickRotation: 0,
          legend: t`Frequency`,
          legendPosition: 'middle',
          legendOffset: -40,
        }}
      />
    </Box>
  ) : null;
}

Statistic.propTypes = {
  statistic: PropTypes.shape({
    frequencies: PropTypes.array,
  }),
};

export default {
  Renderer,
  Evaluation,
  Form,
  reducer,
  updateStatistic,
  Statistic,
};
