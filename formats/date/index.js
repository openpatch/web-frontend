import { Trans } from '@lingui/macro';
import PropTypes from 'prop-types';
import produce from 'immer';
import { Field } from 'react-final-form';

import DateField from '../../components/fields/DateField';
import OperatorField from '../../components/fields/OperatorField';

export const CHANGE_VALUE = 'text/CHANGE_VALUE';

export function changeValue(value) {
  return {
    type: CHANGE_VALUE,
    payload: {
      value: value || '',
    },
  };
}

const initialState = {
  value: '',
};

export const reducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case CHANGE_VALUE:
        draft.value = action.payload.value;
        break;
    }
  });

export function Renderer({ state, dispatch }) {
  function handleChange(value) {
    dispatch(changeValue(value));
  }

  return (
    <DateField
      meta={{}}
      fullWidth
      input={{
        value: state.value,
        onChange: handleChange,
      }}
    />
  );
}

Renderer.propTypes = {
  state: PropTypes.shape({
    value: PropTypes.any,
  }),
  dispatch: PropTypes.func.isRequired,
};

Renderer.defaultProps = {
  state: initialState,
};

function Form() {
  return <></>;
}

function Evaluation({ field }) {
  return (
    <>
      <Field
        name={`${field}.min`}
        fullWidth
        label={<Trans>Min</Trans>}
        component={DateField}
      />
      <Field
        name={`${field}.max`}
        fullWidth
        label={<Trans>Max</Trans>}
        component={DateField}
      />
      <Field
        name={`${field}.operator`}
        label={<Trans>Operator</Trans>}
        fullWidth
        component={OperatorField}
      />
      <Field
        name={`${field}.value`}
        label={<Trans>Compare To</Trans>}
        fullWidth
        component={DateField}
      />
    </>
  );
}

Evaluation.propTypes = {
  field: PropTypes.string.isRequired,
};

export default {
  Renderer,
  Evaluation,
  Form,
  reducer,
};
