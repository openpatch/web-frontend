import { t, Trans } from '@lingui/macro';
import PropTypes from 'prop-types';
import choice from './choice';
import yesNo from './yes-no';
import text from './text';
import time from './time';
import date from './date';
import scale from './scale';
import number from './number';
import highlight from './highlight';
import fillin from './fill-in';
import dropdown from './dropdown';
import display from './display';
import code from './code';
import ranking from './ranking';

const formats = {
  choice: {
    id: 'choice',
    label: t`Choice`,
    module: choice,
  },
  'yes-no': {
    id: 'yes-no',
    label: t`Yes/No`,
    module: yesNo,
  },
  text: {
    id: 'text',
    label: t`Text`,
    module: text,
  },
  time: {
    id: 'time',
    label: t`Time`,
    module: time,
  },
  scale: {
    id: 'scale',
    label: t`Scale`,
    module: scale,
  },
  number: {
    id: 'number',
    label: t`Number`,
    module: number,
  },
  highlight: {
    id: 'highlight',
    label: t`Highlight`,
    module: highlight,
  },
  'fill-in': {
    id: 'fill-in',
    label: t`Fill In`,
    module: fillin,
  },
  date: {
    id: 'date',
    label: t`Date`,
    module: date,
  },
  dropdown: {
    id: 'dropdown',
    label: t`Dropdown`,
    module: dropdown,
  },
  display: {
    id: 'display',
    label: t`Display`,
    module: display,
  },
  code: {
    id: 'code',
    label: t`Code`,
    module: code,
  },
  ranking: {
    id: 'ranking',
    label: t`Ranking`,
    module: ranking,
  },
};

const formatGroups = [
  {
    id: 'common',
    label: t`Common`,
    formats: [
      formats.choice,
      formats['yes-no'],
      formats.text,
      formats.time,
      formats.scale,
      formats.number,
      formats.highlight,
      formats['fill-in'],
      formats.date,
      formats.dropdown,
      formats.display,
      formats.ranking,
    ],
  },
  {
    id: 'programming',
    label: t`Programming`,
    formats: [formats.code],
  },
];

export default formatGroups;

export function Renderer({ type, ...props }) {
  const format = formats[type];

  if (!format) {
    return <Trans>Not supported</Trans>;
  }

  return <format.module.Renderer {...props} />;
}

Renderer.propTypes = {
  type: PropTypes.string.isRequired,
};

export function Form({ type, ...props }) {
  const format = formats[type];

  if (!format) {
    return <Trans>Choose a format</Trans>;
  }

  return <format.module.Form {...props} />;
}

Form.propTypes = {
  type: PropTypes.string.isRequired,
};

export function Evaluation({ type, ...props }) {
  const format = formats[type];

  if (!format) {
    return <Trans>Not supported</Trans>;
  }

  return <format.module.Evaluation {...props} />;
}

Evaluation.propTypes = {
  type: PropTypes.string.isRequired,
};

export function Statistic({ type, ...props }) {
  const format = formats[type];

  if (!format || !format.module.Statistic) {
    return null;
  }

  return <format.module.Statistic {...props} />;
}

Statistic.propTypes = {
  type: PropTypes.string.isRequired,
};

export function reducer(state, action) {
  const type = (action?.type || '').split('/')[0];
  const format = formats[type];

  if (format) {
    return format.module.reducer(state, action);
  }
}

export function updateStatistic(type, statistic, solution, evaluation, task) {
  const format = formats[type];

  const score = evaluation?.correct ? 1 : 0;
  if (!statistic) {
    statistic = {
      avgScore: score,
      count: 1,
    };
  } else {
    statistic.avgScore =
      (statistic.avgScore * statistic.count + score) / (statistic.count + 1);
    statistic.count = statistic.count + 1;
  }

  if (format && format.module.updateStatistic) {
    return {
      ...statistic,
      format: format.module.updateStatistic(
        statistic.format,
        solution,
        evaluation,
        task.data,
        task.evaluation
      ),
    };
  } else {
    return statistic;
  }
}

export function clean(state) {
  const format = formats[state.format];

  if (format) {
    return format.module.clean(state);
  } else {
    return state;
  }
}
