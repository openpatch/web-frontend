import { memo, useEffect, useState, useRef } from 'react';
import { Trans, t } from '@lingui/macro';
import PropTypes from 'prop-types';
import produce from 'immer';
import { Field } from 'react-final-form';
import { useFieldArray } from 'react-final-form-arrays';
import Box from '@material-ui/core/Box';
import InputIcon from '@material-ui/icons/Input';
import { stateToHTML } from 'draft-js-export-html';
import TagCloud from 'react-tag-cloud';
import _isEqual from 'deep-equal';
import { useTheme } from '@material-ui/core/styles';

import { convertFromRaw } from '../../utils/editor';
import RichTextField from '../../components/fields/RichTextField';
import { useStyles as useEditorStyles } from '../../components/RichTextEditor';
import TextField from '../../components/fields/TextField';

export const CHANGE_VALUE_FOR_ID = 'fill-in/CHANGE_VALUE_FOR_ID';

const htmlStyleMap = {
  TITLE: {
    style: {
      fontSize: '1.5em',
    },
  },
  MONOSPACE: {
    style: {
      fontFamily: 'monospace',
    },
  },
};

export function changeValueForId(id, value) {
  return {
    type: CHANGE_VALUE_FOR_ID,
    payload: {
      id,
      value,
    },
  };
}

const initialState = {
  values: {},
};

export const reducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case CHANGE_VALUE_FOR_ID:
        draft.values[action.payload.id] = action.payload.value;
        break;
    }
  });

export function Renderer({ state, dispatch, text, variant }) {
  const [html, setHTML] = useState('');
  const htmlRef = useRef();
  const editorClasses = useEditorStyles();

  function handleChange(id, e) {
    if (variant === 'normal') {
      dispatch(changeValueForId(id, e.target.value));
    } else {
      e.preventDefault();
    }
  }

  useEffect(() => {
    const { values } = state;

    let count = 0;
    if (text) {
      const editorState = convertFromRaw(text);
      setHTML(
        stateToHTML(editorState.getCurrentContent(), {
          inlineStyles: {
            ...htmlStyleMap,
          },
          inlineStyleFn: (styles) => {
            let key = 'fill-in';
            let fillIn = styles.filter((value) => value === key).first();

            if (fillIn) {
              return {
                attributes: {
                  value: values[count] || '',
                  'data-id': count++,
                  readonly: variant === 'normal' ? undefined : true,
                },
                removeContent: true,
                style: {
                  margin: '0 6px',
                  width: 150,
                },
                element: 'input',
              };
            }
          },
        })
      );
    }
  }, [text]);

  useEffect(() => {
    const htmlInputs = [...htmlRef.current.getElementsByTagName('input')];
    htmlInputs.forEach((input) => {
      input.addEventListener('change', (e) =>
        handleChange(input.attributes['data-id'].value, e)
      );
      input.addEventListener('keyup', (e) =>
        handleChange(input.attributes['data-id'].value, e)
      );
    });

    return () => {
      htmlInputs.forEach((input) => {
        input.removeEventListener('change', handleChange);
        input.removeEventListener('keyup', handleChange);
      });
    };
  }, [html]);

  useEffect(() => {
    const { values } = state;
    const htmlInputs = [...htmlRef.current.getElementsByTagName('input')];
    htmlInputs.forEach((input) => {
      const value = values[input.attributes['data-id'].value] || '';
      input.value = value;
    });
  }, [state.values]);

  return (
    <Box>
      <div
        className={editorClasses.editor}
        ref={htmlRef}
        dangerouslySetInnerHTML={{ __html: html }}
      />
    </Box>
  );
}

Renderer.propTypes = {
  state: PropTypes.shape({
    values: PropTypes.object,
  }),
  dispatch: PropTypes.func.isRequired,
  text: PropTypes.object,
  variant: PropTypes.oneOf(['normal', 'result', 'player']),
};

Renderer.defaultProps = {
  state: initialState,
  text: {},
  variant: 'normal',
};

const customStyleMap = {
  'fill-in': {
    borderColor: '#222222',
    borderSize: 1,
    borderStyle: 'solid',
    textDecoration: 'line-through',
  },
};

const styleButtons = [
  {
    title: t`Fill-in`,
    style: 'fill-in',
    icon: <InputIcon />,
  },
];

export function Form({ field }) {
  return (
    <Field
      name={`${field}.text`}
      label={<Trans>Text with Fill-Ins</Trans>}
      customStyleMap={customStyleMap}
      extraButtons={styleButtons}
      deactivateFromToolbar={[
        'format-color-text',
        'format-color-fill',
        'image',
        'link',
        'embed',
      ]}
      component={RichTextField}
    />
  );
}

Form.propTypes = {
  field: PropTypes.string.isRequired,
};

function validateRegex(regex) {
  try {
    new RegExp(regex);
  } catch (e) {
    return <Trans>Not valid: {e.message}</Trans>;
  }
}

export function Evaluation({ field, data }) {
  const { fields } = useFieldArray(`${field}.inputs`);

  const { text } = data;

  useEffect(() => {
    let count = 0;

    text.blocks.forEach((block) => {
      block.inlineStyleRanges.forEach(({ style }) => {
        if (style == 'fill-in') {
          count++;
        }
      });
    });

    while (count < fields.length) {
      fields.pop();
      count++;
    }

    while (count > fields.length) {
      fields.push({});
      count--;
    }
  }, [text]);

  return fields.map((name, index) => (
    <Field
      key={index}
      name={`${name}.regex`}
      component={TextField}
      validate={validateRegex}
      fullWidth
      label={<Trans>Regex for input {index + 1}</Trans>}
    />
  ));
}

export const Statistic = memo(({ statistic }) => {
  const theme = useTheme();

  return (
    <>
      {Object.values(statistic.fillIns).map((fillIn, index) => (
        <TagCloud
          key={index}
          style={{
            borderBottomSize: 1,
            borderBottomStyle: 'solid',
            borderBottomColor: theme.palette.action.disabledBackground,
            fontFamily: 'Roboto',
            position: 'relative',
            height: 200,
          }}
        >
          {Object.keys(fillIn.frequencies).map((word) => {
            const frequency = fillIn.frequencies[word];
            return (
              <div
                key={word}
                title={frequency.count}
                style={{
                  fontSize: (frequency.count / statistic.count) * 32 + 8,
                  padding: theme.spacing(1),
                  color: frequency.correct
                    ? theme.palette.primary.main
                    : undefined,
                }}
              >
                {word}
              </div>
            );
          })}
        </TagCloud>
      ))}
    </>
  );
}, _isEqual);

Statistic.displayName = 'Statistic';
Statistic.propTypes = {
  statistic: PropTypes.shape({
    count: PropTypes.number,
    fillIns: PropTypes.arrayOf(
      PropTypes.shape({
        frequencies: PropTypes.object,
      })
    ),
  }),
};

function updateStatistic(
  statistic,
  solution,
  evaluation,
  task,
  taskEvaluation
) {
  if (!statistic) {
    statistic = {
      count: 0,
      fillIns: {},
    };
  }

  if (solution.values) {
    Object.keys(solution.values).forEach((index) => {
      const word = solution.values[index];
      const fillIn = statistic.fillIns[index] || {
        frequencies: {},
      };
      let frequencyWord = fillIn.frequencies[word];
      if (!frequencyWord) {
        let correct = false;
        if (taskEvaluation?.inputs) {
          const regex = taskEvaluation.inputs?.[index]?.regex;
          if (regex) {
            correct = RegExp(regex).test(word);
          }
        }
        frequencyWord = {
          count: 0,
          correct,
        };
      }
      frequencyWord.count += 1;
      fillIn.frequencies[word] = frequencyWord;
      statistic.fillIns[index] = fillIn;
    });
  }

  statistic.count += 1;

  return statistic;
}

export default {
  Renderer,
  Evaluation,
  Form,
  reducer,
  Statistic,
  updateStatistic,
};
