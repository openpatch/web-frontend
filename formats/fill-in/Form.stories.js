import { action } from '@storybook/addon-actions';
import arrayMutators from 'final-form-arrays';
import { Form } from './';
import MForm from '../../components/Form';

export default {
  title: 'Format/Fill-In/Form',
  component: Form,
};

export const Empty = () => (
  <MForm
    onSubmit={action('submit')}
    mutators={{ ...arrayMutators }}
    render={() => <Form field="form" />}
  />
);

export const PreFilled = () => (
  <MForm
    onSubmit={(v) => {
      action('submit')(v);
    }}
    initialValues={{
      form: {
        text: {
          blocks: [
            {
              key: '59l8t',
              text:
                'Lorem ipsum dolor sit amet, consetetur sadipscingelitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magnaaliquyam erat, sed diam voluptua. At vero eos et accusam et justo duodolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctusest Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetursadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore etdolore magna aliquyam erat, sed diam voluptua. At vero eos et accusamet justo duo dolores et ea rebum. Stet clita kasd gubergren, no seatakimata sanctus est Lorem ipsum dolor sit amet. Lorem ip',
              type: 'unstyled',
              depth: 0,
              inlineStyleRanges: [
                {
                  offset: 22,
                  length: 4,
                  style: 'fill-in',
                },
                {
                  offset: 65,
                  length: 6,
                  style: 'fill-in',
                },
                {
                  offset: 292,
                  length: 5,
                  style: 'fill-in',
                },
                {
                  offset: 397,
                  length: 8,
                  style: 'fill-in',
                },
                {
                  offset: 544,
                  length: 7,
                  style: 'fill-in',
                },
              ],
              entityRanges: [],
              data: {},
            },
          ],
          entityMap: {},
        },
      },
    }}
    mutators={{ ...arrayMutators }}
    render={() => <Form field="form" />}
  />
);
