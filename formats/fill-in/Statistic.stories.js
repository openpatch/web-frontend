import { Statistic } from './';

export default {
  title: 'Format/Fill-In/Statistic',
  component: Statistic,
};

export const PreFilled = () => (
  <Statistic
    statistic={{
      count: 80,
      fillIns: [
        {
          frequencies: {
            house: { count: 5 },
            hallo: { count: 20, correct: true },
            bbb: { count: 15 },
            na: { count: 20 },
            hause: { count: 18 },
          },
        },
        {
          frequencies: {
            house: { count: 5 },
            hallo: { count: 20, correct: true },
            bbb: { count: 15, correct: true },
            na: { count: 20 },
            hause: { count: 18 },
          },
        },
      ],
    }}
  />
);
