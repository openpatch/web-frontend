import { action } from '@storybook/addon-actions';
import Form from './Form';
import MForm from '../../components/Form';
import arrayMutators from 'final-form-arrays';

export default {
  title: 'Format/Yes-No/Form',
  component: Form,
};

export const Empty = () => (
  <MForm
    onSubmit={action('submit')}
    mutators={{ ...arrayMutators }}
    render={() => <Form field="form" />}
  />
);

export const PreFilled = () => (
  <MForm
    onSubmit={action('submit')}
    initialValues={{
      form: {},
    }}
    mutators={{ ...arrayMutators }}
    render={() => <Form field="form" />}
  />
);
