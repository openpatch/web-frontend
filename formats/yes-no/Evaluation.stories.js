import { action } from '@storybook/addon-actions';
import Evaluation from './Evaluation';
import MForm from '../../components/Form';
import arrayMutators from 'final-form-arrays';

export default {
  title: 'Format/Yes-No/Evaluation',
  component: Evaluation,
};

export const Empty = () => (
  <MForm
    onSubmit={action('submit')}
    mutators={{ ...arrayMutators }}
    render={() => <Evaluation field="evaluation" />}
  />
);

export const PreFilled = () => (
  <MForm
    onSubmit={action('submit')}
    initialValues={{
      evaluation: {},
    }}
    mutators={{ ...arrayMutators }}
    render={() => <Evaluation field="evaluation" />}
  />
);
