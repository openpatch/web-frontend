import { selectNo, selectYes } from './actions';
import reducer from './reducer';

describe('reducer', () => {
  test('initialState', () => {
    const state = reducer(undefined, {});

    expect(state).toEqual({});
  });

  test('selectNo with initialState', () => {
    const state = reducer(undefined, selectNo());

    expect(state).toEqual({ yes: false });
  });

  test('selectNo with yes selected', () => {
    const state = reducer({ yes: true }, selectNo());

    expect(state).toEqual({ yes: false });
  });

  test('selectYes with initialState', () => {
    const state = reducer(undefined, selectYes());

    expect(state).toEqual({ yes: true });
  });

  test('selectYes with no selected', () => {
    const state = reducer({ yes: false }, selectYes());

    expect(state).toEqual({ yes: true });
  });
});
