import Statistic from './Statistic';

export default {
  title: 'Format/Yes-No/Statistic',
  component: Statistic,
};

export const PreFilled = () => (
  <Statistic
    statistic={{
      yes: 8,
      count: 20,
    }}
  />
);
