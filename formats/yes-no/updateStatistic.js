function updateStatistic(statistic, solution) {
  if (!statistic) {
    statistic = {
      count: 0,
      yes: 0,
    };
  }

  statistic.count = statistic.count + 1;
  if (solution?.yes) {
    statistic.yes = statistic.yes + 1;
  }

  return statistic;
}

export default updateStatistic;
