import updateStatistic from './updateStatistic';

describe('updateStatistic', () => {
  test('initial statistic', () => {
    const statistic = updateStatistic();

    expect(statistic).toEqual({
      count: 1,
      yes: 0,
    });
  });

  test('many solutions', () => {
    const solutions = [
      {
        yes: true,
      },
      {
        yes: true,
      },
      {
        yes: false,
      },
      {},
      {
        yes: false,
      },
    ];

    const statistic = solutions.reduce(updateStatistic, undefined);

    expect(statistic).toEqual({
      count: solutions.length,
      yes: 2,
    });
  });
});
