import PropTypes from 'prop-types';
import { t } from '@lingui/macro';
import Box from '@material-ui/core/Box';
import { ResponsiveBar } from '@nivo/bar';

function Statistic({ statistic }) {
  const barData = [];

  if (statistic?.yes) {
    barData.push({ id: 'yes', value: statistic.yes });
    barData.push({ id: 'no', value: statistic.count - statistic.yes });
  }

  return statistic?.yes ? (
    <>
      <Box height={250}>
        <ResponsiveBar
          data={barData}
          animate={true}
          colors={{ scheme: 'nivo' }}
          margin={{ top: 20, right: 30, bottom: 50, left: 50 }}
          padding={0.3}
          axisBottom={{
            tickSize: 5,
            tickPadding: 5,
            legend: t`Choice`,
            legendPosition: 'middle',
            legendOffset: 32,
          }}
          axisLeft={{
            tickSize: 5,
            tickPadding: 5,
            tickRotation: 0,
            legend: t`Frequency`,
            legendPosition: 'middle',
            legendOffset: -40,
          }}
        />
      </Box>
    </>
  ) : null;
}

Statistic.propTypes = {
  statistic: PropTypes.shape({
    yes: PropTypes.number,
    count: PropTypes.number,
  }),
};

export default Statistic;
