import PropTypes from 'prop-types';
import { Trans } from '@lingui/macro';
import Box from '@material-ui/core/Box';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Button from '@material-ui/core/Button';

import { selectYes, selectNo } from './actions';

const initialState = {
  yes: undefined,
};

export function Renderer({ state, dispatch }) {
  function handleYes() {
    dispatch(selectYes());
  }

  function handleNo() {
    dispatch(selectNo());
  }

  const yes = state.yes;

  return (
    <Box textAlign="center">
      <ButtonGroup size="large">
        <Button onClick={handleYes} color={yes == true ? 'primary' : null}>
          <Trans>Yes</Trans>
        </Button>
        <Button onClick={handleNo} color={yes == false ? 'primary' : null}>
          <Trans>No</Trans>
        </Button>
      </ButtonGroup>
    </Box>
  );
}

Renderer.propTypes = {
  state: PropTypes.shape({
    yes: PropTypes.bool,
  }),
  dispatch: PropTypes.func.isRequired,
};

Renderer.defaultProps = {
  yes: undefined,
  state: initialState,
};

export default Renderer;
