import { SELECT_YES, SELECT_NO } from './constants';
import produce from 'immer';

const initialState = {
  yes: undefined,
};

const reducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case SELECT_YES:
        draft.yes = true;
        break;
      case SELECT_NO:
        draft.yes = false;
        break;
    }
  });

export default reducer;
