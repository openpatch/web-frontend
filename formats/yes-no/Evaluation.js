import { Trans } from '@lingui/macro';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import { useField } from 'react-final-form';
import PropTypes from 'prop-types';

function Evaluation({ field }) {
  const {
    input: { value, onChange },
  } = useField(`${field}.yes`);
  return (
    <ButtonGroup>
      <Button
        onClick={() => onChange(true)}
        color={value == true ? 'primary' : null}
      >
        <Trans>Yes</Trans>
      </Button>
      <Button
        onClick={() => onChange(false)}
        color={value == false ? 'primary' : null}
      >
        <Trans>No</Trans>
      </Button>
    </ButtonGroup>
  );
}

Evaluation.propTypes = {
  field: PropTypes.string.isRequired,
};

export default Evaluation;
