import { useReducer } from 'react';
import { action } from '@storybook/addon-actions';
import Renderer from './Renderer';
import reducer from './reducer';

export default {
  title: 'Format/Yes-No/Renderer',
  component: Renderer,
};

export const Empty = () => (
  <Renderer
    dispatch={action('dispatch')}
    state={{
      yes: true,
    }}
    variant="normal"
  />
);

export const WithDispatch = () => {
  const [state, dispatch] = useReducer(reducer);

  const handleDispatch = (a) => {
    action('dispatch')(a);
    dispatch(a);
  };

  return <Renderer dispatch={handleDispatch} state={state} variant="normal" />;
};
