import { SELECT_YES, SELECT_NO } from './constants';

export function selectYes() {
  return {
    type: SELECT_YES,
  };
}

export function selectNo() {
  return {
    type: SELECT_NO,
  };
}
