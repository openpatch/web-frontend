import Renderer from './Renderer';
import Evaluation from './Evaluation';
import Form from './Form';
import reducer from './reducer';
import Statistic from './Statistic';
import updateStatistic from './updateStatistic';

export default {
  Renderer,
  Evaluation,
  Form,
  reducer,
  Statistic,
  updateStatistic,
};
