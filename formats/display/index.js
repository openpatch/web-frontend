import { Trans } from '@lingui/macro';
import PropTypes from 'prop-types';
import produce from 'immer';
import { Field } from 'react-final-form';

import RichTextField from '../../components/fields/RichTextField';
import RichText from '../../components/RichText';
import { convertFromRaw } from '../../utils/editor';

const initialState = {};

export const reducer = (state = initialState) =>
  produce(state, (draft) => {
    return draft;
  });

export function Renderer({ text }) {
  return <RichText editorState={convertFromRaw(text)} />;
}

Renderer.propTypes = {
  text: PropTypes.object,
};

function Form({ field }) {
  return (
    <Field
      name={`${field}.text`}
      label={<Trans>Text to display</Trans>}
      component={RichTextField}
    />
  );
}

Form.propTypes = {
  field: PropTypes.string.isRequired,
};

function Statistic() {
  return <Trans>Nothing to display</Trans>;
}

function Evaluation() {
  return <Trans>Can not be evaluated</Trans>;
}

export default {
  Renderer,
  Evaluation,
  Form,
  reducer,
  Statistic,
};
