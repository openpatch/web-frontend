import { action } from '@storybook/addon-actions';
import { Evaluation } from './';
import MForm from '../../components/Form';
import arrayMutators from 'final-form-arrays';

export default {
  title: 'Format/Code/Evaluation',
  component: Evaluation,
};

export const Empty = () => (
  <MForm
    onSubmit={action('submit')}
    mutators={{ ...arrayMutators }}
    render={() => <Evaluation field="evaluation" />}
  />
);

export const PreFilled = () => (
  <MForm
    onSubmit={action('submit')}
    initialValues={{
      evaluation: {
        image: 'registry.gitlab.com/openpatch/runner-java:v0.1.0',
        tests: [{ fileName: 'TestHallo.java', source: 'import junit;' }],
      },
    }}
    mutators={{ ...arrayMutators }}
    render={() => <Evaluation field="evaluation" />}
  />
);
