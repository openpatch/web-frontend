import {
  reducer,
  initSources,
  includeText,
  removeText,
  changeSource,
  updateStatistic,
} from './';

describe('reducer', () => {
  test('initial state', () => {
    const state = reducer(undefined, {});

    expect(state).toEqual({
      currentSource: 0,
      sources: [],
    });
  });

  test('initSources', () => {
    const sources = ['test', 'hallo'];
    const state = reducer(undefined, initSources(sources));

    expect(state).toEqual({
      currentSource: 0,
      sources,
    });
  });

  test('includeText', () => {
    const delta = {
      start: { row: 0, column: 4 },
      end: { row: 0, column: 5 },
      action: 'insert',
      lines: ['e'],
      id: 3,
    };
    const state = reducer(
      {
        currentSource: 0,
        sources: [{ source: 'Hallo' }],
      },
      includeText(0, delta)
    );

    expect(state.sources).toEqual([{ source: 'Halleo' }]);
  });

  test('removeText', () => {
    const delta = {
      start: { row: 0, column: 4 },
      end: { row: 0, column: 5 },
      action: 'remove',
      lines: ['e'],
      id: 3,
    };
    const state = reducer(
      {
        currentSource: 0,
        sources: [{ source: 'Hallo' }],
      },
      removeText(0, delta)
    );

    expect(state.sources).toEqual([{ source: 'Hall' }]);
  });

  test('removeTextBeforeInit', () => {
    const delta = {
      start: { row: 0, column: 4 },
      end: { row: 0, column: 5 },
      action: 'remove',
      lines: ['e'],
      id: 3,
    };
    const state = reducer(undefined, removeText(0, delta));

    expect(state).toEqual({
      currentSource: 0,
      sources: [],
    });
  });

  test('changeSource', () => {
    const state = reducer({ currentSource: 0 }, changeSource(1));

    expect(state.currentSource).toBe(1);
  });
});

describe('updateStatistic', () => {
  test('empty solution', () => {
    const statistic = updateStatistic();
    expect(statistic).toEqual({
      avgExectime: 0,
      tests: {},
      count: 1,
    });
  });

  test('many solutions', () => {
    const evaluations = [
      {
        details: {
          srcRun: {
            exectime: 20,
          },
        },
      },
      {
        details: {
          srcRun: {
            exectime: 10,
          },
        },
      },
    ];

    const statistic = evaluations.reduce(
      (statistic, evaluation) => updateStatistic(statistic, null, evaluation),
      undefined
    );

    expect(statistic).toEqual({
      avgExectime: 15,
      tests: {},
      count: evaluations.length,
    });
  });

  test('many solutions with testResults', () => {
    const evaluations = [
      {
        details: {
          srcRun: {
            exectime: 10,
          },
          testResults: [
            {
              isCorrect: true,
              totalTestsFailed: 2,
              totalTestsRun: 4,
            },
          ],
        },
      },
    ];

    const statistic = evaluations.reduce(
      (statistic, evaluation) => updateStatistic(statistic, null, evaluation),
      undefined
    );

    expect(statistic).toEqual({
      avgExectime: 10,
      tests: {
        0: {
          avgScore: 1,
          avgFailed: 2,
          avgRun: 4,
          count: 1,
        },
      },
      count: evaluations.length,
    });
  });
});
