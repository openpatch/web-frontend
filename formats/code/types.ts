// types for generics
export interface IState {
  currentSource: number;
  sources: ISource[];
}
export type ICleanState = IState;
export interface IStatistic {
  avgExectime: number;
  tests: Record<string, { avgScore: number; count: number; avgFailed: number; avgRun: number }>;
  count: number;
}
export interface IEvaluation {
  details?: {
    srcRun?: {
      exectime: number
    }
    testResults?: {
      isCorrect: boolean;
      totalTestsRun: number;
      totalTestsFailed: number;
    }[]
  };
}
export interface ISolution {
  sources: ISource[];
}
export interface IItem {
  data: {
    sources: ISource[];
  };
  evaluation: {
    tests: ISource[];
  };
}

// custom internal types
export interface ISource {
  source: string;
  fileName: string;
}
