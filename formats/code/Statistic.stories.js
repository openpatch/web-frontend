import { Statistic } from './';

export default {
  title: 'Format/Code/Statistic',
  component: Statistic,
};

export const PreFilled = () => (
  <Statistic
    evaluation={{
      tests: [
        {
          fileName: 'Hallo.java',
        },
      ],
    }}
    statistic={{
      avgExectime: 20,
      tests: {
        '0': {
          avgScore: 10,
          count: 20,
        },
      },
      count: 20,
    }}
  />
);
