import { action } from '@storybook/addon-actions';
import { Form } from './';
import MForm from '../../components/Form';
import arrayMutators from 'final-form-arrays';

export default {
  title: 'Format/Code/Form',
  component: Form,
};

export const Empty = () => (
  <MForm
    onSubmit={action('submit')}
    mutators={{ ...arrayMutators }}
    render={() => <Form field="form" />}
  />
);

export const PreFilled = () => (
  <MForm
    onSubmit={action('submit')}
    initialValues={{
      form: {
        sources: [
          {
            fileName: 'Hallo.java',
            source: 'public class Hallo',
          },
          {
            fileName: 'Ciao.java',
            source: 'public class Ciao',
          },
        ],
      },
    }}
    mutators={{ ...arrayMutators }}
    render={() => <Form field="form" />}
  />
);
