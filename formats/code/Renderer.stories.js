import { useReducer } from 'react';
import { action } from '@storybook/addon-actions';
import { Renderer, reducer } from './';

export default {
  title: 'Format/Code/Renderer',
  component: Renderer,
};

export const Empty = () => (
  <Renderer
    dispatch={action('dispatch')}
    state={{
      currentSource: 0,
      sources: [
        {
          fileName: 'Hallo.java',
          source: 'public class Hallo {}',
        },
      ],
    }}
    sources={[
      {
        fileName: 'Hallo.java',
        source: 'public class Hallo {}',
      },
    ]}
    variant="normal"
  />
);

export const ReadOnlyRows = () => (
  <Renderer
    dispatch={action('dispatch')}
    state={{
      currentSource: 0,
      sources: [
        {
          fileName: 'Hallo.java',
          source: 'public class Hallo {}\nSecond row\nThird row\nFourth row',
        },
      ],
    }}
    readOnlyRows={[0, 3]}
    sources={[
      {
        fileName: 'Hallo.java',
        source: 'public class Hallo {}',
      },
    ]}
    variant="normal"
  />
);

export const WithDispatch = () => {
  const [state, dispatch] = useReducer(reducer);

  const handleDispatch = (a) => {
    action('dispatch')(a);
    dispatch(a);
  };

  return (
    <Renderer
      dispatch={handleDispatch}
      state={state}
      sources={[
        {
          fileName: 'Hallo.java',
          source: 'public class Hallo {}',
        },
      ]}
      variant="normal"
    />
  );
};
