import { useEffect } from 'react';
import { Trans } from '@lingui/macro';
import PropTypes from 'prop-types';
import produce from 'immer';
import { Field } from 'react-final-form';
import { FieldArray } from 'react-final-form-arrays';
import AppBar from '@material-ui/core/AppBar';
import Box from '@material-ui/core/Box';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import CodeEditor from '../../components/CodeEditor';
import CodeFieldArray from '../../components/fields/CodeFieldArray';
import SelectField from '../../components/fields/SelectField';

import { updateAvg, round } from '../../utils/math';

export const INIT_SOURCES = 'code/INIT_SOURCES';
export const INCLUDE_TEXT = 'code/INCLUDE_TEXT';
export const REMOVE_TEXT = 'code/REMOVE_TEXT';
export const CHANGE_SOURCE = 'code/CHANGE_SOURCE';

export function initSources(sources) {
  return {
    type: INIT_SOURCES,
    payload: {
      sources,
    },
  };
}

export function includeText(source, delta) {
  return {
    type: INCLUDE_TEXT,
    payload: {
      delta,
      source,
    },
  };
}

export function removeText(source, delta) {
  return {
    type: REMOVE_TEXT,
    payload: {
      delta,
      source,
    },
  };
}

export function changeSource(source) {
  return {
    type: CHANGE_SOURCE,
    payload: {
      source,
    },
  };
}

const initialState = {
  currentSource: 0,
  sources: [],
};

function applyDelta(source, delta) {
  /* eslint-disable @typescript-eslint/no-var-requires */
  const ace = require('ace-builds/src-noconflict/ace');
  const Document = ace.require('ace/document').Document;
  const doc = new Document(source || '');
  doc.applyDeltas([delta]);
  return doc.getValue();
  /* eslint-enable @typescript-eslint/no-var-requires */
}

export const reducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case INIT_SOURCES:
        draft.sources = action.payload.sources;
        break;
      case CHANGE_SOURCE:
        draft.currentSource = action.payload.source;
        break;
      case INCLUDE_TEXT:
      case REMOVE_TEXT: {
        if (draft?.sources?.[action.payload.source]) {
          const newValue = applyDelta(
            draft.sources[action.payload.source].source,
            action.payload.delta
          );
          draft.sources[action.payload.source].source = newValue;
          break;
        }
      }
    }
  });

export function Renderer({ state, dispatch, sources, variant, readOnlyRows }) {
  function handleChange(v, delta) {
    if (delta.action == 'remove') {
      dispatch(removeText(state.currentSource, delta));
    } else if (delta.action == 'insert') {
      dispatch(includeText(state.currentSource, delta));
    }
  }

  function handleTab(e, newTab) {
    if (variant !== 'player') {
      dispatch(changeSource(newTab));
    }
  }

  useEffect(() => {
    if (sources && variant === 'normal') {
      setTimeout(() => {
        dispatch(initSources(sources));
      }, 400);
    }
  }, []);

  return (
    <Box mr={-3} ml={-3} mb={-3}>
      <AppBar position="static" color="default" elevation={0}>
        <Tabs
          value={state.currentSource}
          onChange={handleTab}
          variant="scrollable"
          indicatorColor="primary"
          textColor="primary"
        >
          {state.sources.map((source) => (
            <Tab key={source.fileName} label={source.fileName} />
          ))}
        </Tabs>
      </AppBar>
      <CodeEditor
        readOnlyRows={readOnlyRows}
        readOnly={variant !== 'normal'}
        value={state.sources[state.currentSource]?.source || ''}
        onChange={handleChange}
      />
    </Box>
  );
}

Renderer.propTypes = {
  state: PropTypes.shape({
    sources: PropTypes.array,
    currentSource: PropTypes.number,
  }),
  sources: PropTypes.arrayOf(
    PropTypes.shape({
      fileName: PropTypes.string,
      source: PropTypes.string,
    })
  ),
  dispatch: PropTypes.func.isRequired,
  readOnlyRows: PropTypes.arrayOf(PropTypes.number),
  variant: PropTypes.oneOf(['normal', 'result', 'player']),
};

Renderer.defaultProps = {
  state: initialState,
  sources: [],
};

export function Form({ field }) {
  return (
    <FieldArray
      name={`${field}.sources`}
      label={<Trans>Sources</Trans>}
      fullWidth
      component={CodeFieldArray}
    />
  );
}

Form.propTypes = {
  field: PropTypes.string.isRequired,
};

const runners = [
  {
    label: 'Java 1.8',
    value: 'registry.gitlab.com/openpatch/runner-java:v1.0.0',
  },
];

export function Evaluation({ field }) {
  return (
    <>
      <Field
        name={`${field}.image`}
        label={<Trans>Runner</Trans>}
        fullWidth
        component={SelectField}
        options={runners}
        getValue={(o) => o.value}
        getText={(o) => o.label}
      />
      <FieldArray
        name={`${field}.tests`}
        label={<Trans>Tests</Trans>}
        fullWidth
        component={CodeFieldArray}
      />
    </>
  );
}

Evaluation.propTypes = {
  field: PropTypes.string.isRequired,
};

export function Statistic({ statistic, evaluation }) {
  const tests = evaluation?.tests || [];
  return (
    <Box>
      <Box>Execution Time: {round(statistic?.avgExectime) || 0}s</Box>
      {tests.map((test, index) => (
        <Box key={test.fileName}>
          {test.fileName}:{' '}
          {(statistic?.tests?.[index]?.avgScore /
            statistic?.tests?.[index]?.count) *
            100}
          %
        </Box>
      ))}
    </Box>
  );
}

Statistic.propTypes = {
  evaluation: PropTypes.shape({
    tests: PropTypes.arrayOf(
      PropTypes.shape({
        fileName: PropTypes.string,
      })
    ),
  }),
  statistic: PropTypes.shape({
    avgExectime: PropTypes.number,
    tests: PropTypes.arrayOf(
      PropTypes.shape({
        avgScore: PropTypes.number,
        count: PropTypes.count,
      })
    ),
  }),
};

Statistic.defaultProps = {
  statistic: {
    tests: [],
  },
  data: {},
  evaluation: {
    tests: [],
  },
};

export function updateStatistic(statistic, solution, evaluation) {
  const exectime = evaluation?.details?.srcRun?.exectime || 0;

  if (!statistic) {
    statistic = {
      avgExectime: 0,
      tests: {},
      count: 0,
    };
  }
  statistic.avgExectime = updateAvg(
    statistic.avgExectime,
    exectime,
    statistic.count
  );
  statistic.count = statistic.count + 1;
  if (evaluation?.details?.testResults) {
    evaluation.details.testResults.forEach((testResult, index) => {
      let testResultStatistic = statistic.tests[index];
      if (!testResultStatistic) {
        testResultStatistic = {
          avgScore: testResult.isCorrect ? 1 : 0,
          avgFailed: testResult.totalTestsFailed,
          avgRun: testResult.totalTestsRun,
          count: 1,
        };
      } else {
        testResultStatistic.avgScore = updateAvg(
          testResultStatistic.avgScore,
          testResult.isCorrect ? 1 : 0,
          testResultStatistic.count
        );
        testResultStatistic.avgFailed = updateAvg(
          testResultStatistic.avgFailed,
          testResult.totalTestsFailed,
          testResultStatistic.count
        );
        testResultStatistic.avgRun = updateAvg(
          testResultStatistic.avgRun,
          testResult.totalTestsRun,
          testResultStatistic.count
        );
      }
      statistic.tests[index] = testResultStatistic;
    });
  }
  return statistic;
}

export default {
  Renderer,
  Evaluation,
  Form,
  reducer,
  updateStatistic,
  Statistic,
};
