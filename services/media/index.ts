export const urls = {
  browser: process.env.NEXT_API_MEDIA_BROWSER,
  server: process.env.API_MEDIA_SERVER,
};
