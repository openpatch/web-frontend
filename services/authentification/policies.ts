import { Locale } from '@utils/i18n';
import { AxiosResponse } from 'axios';
import { RawDraftContentState } from 'draft-js';
import { GetServerSidePropsContext } from 'next';
import { urls } from '.';
import { makeApi, Query } from '../../utils/api';
import {
  RequestOptions,
  useRequest,
  useRequestInfinite,
} from '../../utils/request';

export type Policy = {
  id: string;
  content: RawDraftContentState;
  draft: boolean;
  language: string;
  type: string;
  createdOn: string;
};

export type GetNewPoliciesResponse = {
  policies: Policy[];
};

export function useNewPolicies(
  query: Query = {},
  pageSize = 20
): Omit<ReturnType<typeof useRequestInfinite>, 'data'> & {
  policies: Policy[] | null;
} {
  const url = `${urls.browser}/v1/new-policies`;
  const selector = (d: GetNewPoliciesResponse) => d.policies;
  const { data, ...others } = useRequestInfinite<
    GetNewPoliciesResponse,
    Policy
  >(url, query, pageSize, selector);
  return {
    policies: data,
    ...others,
  };
}

export type GetLatestPolicyResponse = {
  policy: Policy;
};

export function useLatestPolicy(
  type: string,
  lang: Locale = 'en',
  options?: RequestOptions<GetLatestPolicyResponse>
): Omit<ReturnType<typeof useRequest>, 'data'> & { policy: Policy | null } {
  const { data, ...others } = useRequest<GetLatestPolicyResponse>(
    {
      url: `${urls.browser}/v1/policies/latest`,
      params: {
        type,
        lang,
      },
    },
    options
  );
  return { policy: data ? data.policy : null, ...others };
}

export async function getLatestPolicy(
  ctx: GetServerSidePropsContext | null,
  type: string,
  lang: Locale = 'en'
): Promise<[Policy, AxiosResponse<GetLatestPolicyResponse>]> {
  const url = ctx === null ? urls.browser : urls.server;
  return makeApi(ctx)
    .get<GetLatestPolicyResponse>(`${url}/v1/policies/latest`, {
      params: {
        type,
        lang,
      },
    })
    .then((d) => [d.data.policy, d]);
}

export type GetPolicyResponse = {
  policy: Policy;
};

export function usePolicy(
  id: string,
  options: RequestOptions<GetPolicyResponse>
) {
  const {
    data,
    error,
    isValidating,
    revalidate,
  } = useRequest<GetPolicyResponse>(
    `${urls.browser}/v1/policies/${id}`,
    options
  );
  return { policy: data ? data.policy : null, error, isValidating, revalidate };
}

export async function getPolicy(
  ctx: GetServerSidePropsContext | null,
  id: string
): Promise<[Policy, AxiosResponse<GetPolicyResponse>]> {
  const url = ctx === null ? urls.browser : urls.server;
  return makeApi(ctx)
    .get<GetPolicyResponse>(`${url}/v1/policies/${id}`)
    .then((d) => [d.data.policy, d]);
}

export type GetPoliciesResponse = {
  policies: Policy[];
};

export function usePolicies(
  query: Query = {},
  pageSize = 20
): Omit<ReturnType<typeof useRequestInfinite>, 'data'> & {
  policies: Policy[] | null;
} {
  const url = `${urls.browser}/v1/policies`;
  const selector = (d: GetPoliciesResponse) => d.policies;
  const { data, ...others } = useRequestInfinite<GetPoliciesResponse, Policy>(
    url,
    query,
    pageSize,
    selector
  );
  return {
    policies: data,
    ...others,
  };
}

export async function getPolicies(
  ctx: GetServerSidePropsContext | null,
  query: Query = {},
  page = 0,
  pageSize = 20
): Promise<[Policy[], AxiosResponse<GetPoliciesResponse>]> {
  const url = ctx === null ? urls.browser : urls.server;
  return makeApi(ctx)
    .get<GetPoliciesResponse>(`${url}/v1/policies`, {
      params: {
        query: {
          offset: page * pageSize,
          limit: pageSize,
          ...query,
        },
      },
    })
    .then((d) => [d.data.policies, d]);
}

export type PostPoliciesResponse = {
  policyId: string;
};

export async function postPolicies(
  ctx: GetServerSidePropsContext | null,
  data: Partial<Policy>
): Promise<[string, AxiosResponse<PostPoliciesResponse>]> {
  const url = ctx === null ? urls.browser : urls.server;

  return makeApi(ctx)
    .post<PostPoliciesResponse>(`${url}/v1/policies`, data)
    .then((d) => [d.data.policyId, d]);
}

export type PutPolicyResponse = Record<string, never>;

export async function putPolicy(
  ctx: GetServerSidePropsContext | null,
  id: string,
  data: Partial<Policy>
): Promise<[PutPolicyResponse, AxiosResponse<PutPolicyResponse>]> {
  const url = ctx === null ? urls.browser : urls.server;

  return makeApi(ctx)
    .put<PutPolicyResponse>(`${url}/v1/policies/${id}`, data)
    .then((d) => [d.data, d]);
}
