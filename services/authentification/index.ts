export const urls = {
  browser: process.env.NEXT_API_AUTHENTIFICATION_BROWSER,
  server: process.env.API_AUTHENTIFICATION_SERVER,
};
