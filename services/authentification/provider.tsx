import { GetServerSidePropsContext, GetServerSidePropsResult } from 'next';
import Router from 'next/router';
import { createContext, ReactNode, useContext, useEffect } from 'react';
import { MemberClaims } from './members';
import { parseToken } from './utils';

export type AuthentificationProviderProps = {
  children: ReactNode;
  claims: MemberClaims;
};

const context = createContext<MemberClaims | null>(null);

export function getClaimsFromContext(
  ctx: GetServerSidePropsContext
): MemberClaims | null {
  const token = ctx.query.accessToken;
  if (token && !Array.isArray(token)) {
    return parseToken(token) as MemberClaims;
  }
  return null;
}

export function loginRedirect(
  ctx: GetServerSidePropsContext
): GetServerSidePropsResult<any> {
  return {
    redirect: {
      destination: `/login?redirect=${encodeURIComponent(ctx.resolvedUrl)}`,
      permanent: false,
    },
  };
}

export function useClaims() {
  const claims = useContext(context);
  return claims;
}

export const AuthentificationProvider = ({
  children,
  claims,
}: AuthentificationProviderProps) => {
  const syncLogout = (event: StorageEvent) => {
    if (event.key === 'logout') {
      Router.push('/');
    }
  };

  useEffect(() => {
    window.addEventListener('storage', syncLogout);

    return () => {
      window.removeEventListener('storage', syncLogout);
      window.localStorage.removeItem('logout');
    };
  }, []);

  return <context.Provider value={claims}>{children}</context.Provider>;
};
