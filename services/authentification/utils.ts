import humps from 'humps';

export function parseToken(token: string) {
  let parsed = { user_claims: {} };

  if (!token) {
    return humps.camelizeKeys(parsed);
  }

  try {
    const base64Url = token.split('.')[1];
    const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    if (typeof window !== 'undefined') {
      parsed = JSON.parse(window.atob(base64));
    } else {
      parsed = JSON.parse(Buffer.from(base64, 'base64').toString('binary'));
    }
  } catch (e) {
    parsed = { user_claims: {} };
  }
  return humps.camelizeKeys(parsed);
}
