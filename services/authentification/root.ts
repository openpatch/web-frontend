// resend-mail

import api, { makeApi } from '@utils/api';
import { removeCookie, setCookie } from '@utils/cookies';
import { AxiosRequestConfig } from 'axios';
import { GetServerSidePropsContext } from 'next';
import { urls } from '.';
import { getApiError } from './errors';

export type PostLoginResponse = {
  accessToken: string;
  refreshToken: string;
};

export async function postLogin(
  ctx: GetServerSidePropsContext | null,
  identifier: string,
  password: string
): Promise<PostLoginResponse> {
  let url = urls.browser;
  if (ctx) {
    url = urls.server;
  }
  return api
    .post<PostLoginResponse>(`${url}/v1/login`, {
      username: identifier,
      password: password,
    })
    .then((d) => {
      setCookie(ctx, 'accessToken', d.data.accessToken);
      setCookie(ctx, 'refreshToken', d.data.refreshToken);
      return d.data;
    })
    .catch((e) => {
      throw getApiError(e);
    });
}

export type PostRegisterResponse = {
  message: string;
};

export type PostRegisterProps = {
  username: string;
  email: string;
  password: string;
  fullName?: string;
  invitationCodeId?: string;
  policyId?: string;
  url: string;
};

export async function postRegister(
  ctx: GetServerSidePropsContext | null,
  data: Omit<PostRegisterProps, 'url'>,
  options?: AxiosRequestConfig
): Promise<PostRegisterResponse> {
  let url = urls.browser;
  if (ctx) {
    url = urls.server;
  }

  return makeApi(ctx)
    .post<PostRegisterResponse>(
      `${url}/v1/register`,
      {
        ...data,
        url: `${process.env.BASE_URL}/confirm-mail?token={}`,
      },
      options
    )
    .then((d) => d.data);
}

export type GetRefreshResponse = {
  accessToken: string;
};

export async function getRefresh(
  ctx: GetServerSidePropsContext | null
): Promise<GetRefreshResponse> {
  let url = urls.browser;
  if (ctx) {
    url = urls.server;
  }
  return makeApi(ctx)
    .get<GetRefreshResponse>(`${url}/v1/refresh`)
    .then((d) => d.data);
}

export async function getLogout(ctx: GetServerSidePropsContext | null) {
  let url = urls.browser;
  if (ctx) {
    url = urls.server;
  }
  removeCookie(null, 'accessToken');
  removeCookie(null, 'refreshToken');
  window.localStorage.setItem('logout', Date.now().toString());
  return makeApi(ctx).get(`${url}/v1/logout`);
}

export async function getVerify(ctx: GetServerSidePropsContext | null) {
  let url = urls.browser;
  if (ctx) {
    url = urls.server;
  }
  return makeApi(ctx).get(`${url}/v1/verfiy`);
}

export async function getResetPassword(
  ctx: GetServerSidePropsContext | null,
  email: string
) {
  let url = urls.browser;
  if (ctx) {
    url = urls.server;
  }
  const returnToUrl = `${process.env.BASE_URL}/reset-password?token={}`;
  return makeApi(ctx).get(`${url}/v1/reset-password`, {
    params: {
      email,
      url: returnToUrl,
    },
  });
}

export async function postResetPassword(
  ctx: GetServerSidePropsContext | null,
  password: string,
  token: string
) {
  let url = urls.browser;
  if (ctx) {
    url = urls.server;
  }

  return makeApi(ctx).post(`${url}/v1/reset-password`, {
    password,
    token,
  });
}

export async function postResendEmail(
  ctx: GetServerSidePropsContext | null,
  email: string
) {
  let url = urls.browser;
  if (ctx) {
    url = urls.server;
  }
  const returnToUrl = `${process.env.BASE_URL}/confirm-mail?token={}`;
  return makeApi(ctx).post(`${url}/v1/resend-email`, {
    email,
    url: returnToUrl,
  });
}

export async function postConfirmEmail(
  ctx: GetServerSidePropsContext | null,
  token: string
) {
  let url = urls.browser;
  if (ctx) {
    url = urls.server;
  }

  return makeApi(ctx).post(`${url}/v1/confirm-email`, {
    token,
  });
}
