import { makeApi, Query } from '@utils/api';
import {
  RequestInfiniteOptions,
  RequestOptions,
  useRequest,
  useRequestInfinite,
} from '@utils/request';
import { AxiosResponse } from 'axios';
import { GetServerSidePropsContext } from 'next';
import { revalidateType } from 'swr';
import { urls } from '.';

export type Member = {
  id: string;
  username: string;
  fullName?: string | null;
  avatarId?: string | null;
};

export type MemberClaims = Member & {
  role: string;
  email: string;
  confirmed: boolean;
};

export type MemberAdmin = Member & {
  role: string;
  confirmed: string;
  email: string;
};

export type GetMemberResponse = {
  member: Member;
};

export type GetMembersResponse = {
  members: MemberAdmin[];
  membersCount: number;
};

export function useMember(
  id: string,
  options?: RequestOptions<GetMemberResponse>
): {
  member: Member | null;
  error: any;
  isValidating: boolean;
  revalidate: revalidateType;
} {
  const {
    data,
    error,
    isValidating,
    revalidate,
  } = useRequest<GetMemberResponse>(
    `${urls.browser}/v1/members/${id}`,
    options
  );
  return { member: data ? data.member : null, error, isValidating, revalidate };
}

export async function getMember(
  ctx: GetServerSidePropsContext | null,
  id: string
): Promise<[Member, AxiosResponse<GetMemberResponse>]> {
  const url = ctx === null ? urls.browser : urls.server;

  return makeApi(ctx)
    .get<GetMemberResponse>(`${url}/v1/members/${id}`)
    .then((d) => [d.data.member, d]);
}

export function useMembers(
  query: Query = {},
  pageSize = 20,
  config?: RequestInfiniteOptions<GetMembersResponse>
): Omit<ReturnType<typeof useRequestInfinite>, 'data'> & {
  members: MemberAdmin[] | null;
} {
  const url = `${urls.browser}/v1/members`;
  const selector = (data: GetMembersResponse) => data.members;
  const { data, ...others } = useRequestInfinite<
    GetMembersResponse,
    MemberAdmin
  >(
    url,
    {
      query,
    },
    pageSize,
    selector,
    config
  );
  return {
    members: data,
    ...others,
  };
}

export async function getMembers(
  ctx: GetServerSidePropsContext | null,
  query: Query = {},
  page = 0,
  pageSize = 20
): Promise<[Member[], AxiosResponse<GetMembersResponse>]> {
  const url = ctx === null ? urls.browser : urls.server;

  return makeApi(ctx)
    .get<GetMembersResponse>(`${url}/v1/members`, {
      params: {
        ...query,
        offset: page * pageSize,
        limit: pageSize,
      },
    })
    .then((d) => [d.data.members, d]);
}

export type InvationCode = {
  id: string;
  used: boolean;
};

export type GetInvationCodesResponse = {
  invitationCodes: InvationCode[];
};

export function useInvitationCodes(
  query: Query = {},
  pageSize = 20,
  config?: RequestInfiniteOptions<GetInvationCodesResponse>
): Omit<ReturnType<typeof useRequestInfinite>, 'data'> & {
  invitationCodes: InvationCode[] | null;
} {
  const url = `${urls.browser}/v1/members`;
  const selector = (data: GetInvationCodesResponse) => data.invitationCodes;
  const { data, ...others } = useRequestInfinite<
    GetInvationCodesResponse,
    InvationCode
  >(
    url,
    {
      query,
    },
    pageSize,
    selector,
    config
  );
  return {
    invitationCodes: data,
    ...others,
  };
}

export async function getInvationCodes(
  ctx: GetServerSidePropsContext | null,
  id: string,
  query: Query = {},
  page = 0,
  pageSize = 20
): Promise<[InvationCode[], AxiosResponse<GetInvationCodesResponse>]> {
  const url = ctx === null ? urls.browser : urls.server;

  return makeApi(ctx)
    .get<GetInvationCodesResponse>(`${url}/v1/members/${id}/invitation-codes`, {
      params: {
        ...query,
        offset: page * pageSize,
        limit: pageSize,
      },
    })
    .then((d) => [d.data.invitationCodes, d]);
}

export type PostInvationCodesResponse = {
  invitationCode: string;
};

export async function postInvationCodes(
  ctx: GetServerSidePropsContext | null,
  id: string
): Promise<[string, AxiosResponse<PostInvationCodesResponse>]> {
  const url = ctx === null ? urls.browser : urls.server;

  return makeApi(ctx)
    .post<PostInvationCodesResponse>(`${url}/v1/members/${id}/invitation-codes`)
    .then((d) => [d.data.invitationCode, d]);
}
