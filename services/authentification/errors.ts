import { isApiError } from '@utils/api';

// see https://gitlab.com/openpatch/authentification-backend/blob/68850271dd88655739e1aba12cd65d633505105f/openpatch_authentification/api/v1/errors.py
export type AuthentificationApiError = {
  status: number;
  code: number;
  message?: string;
  details?: any;
};

class ApiError extends Error {
  status: number;
  code: number;
  details?: any;

  constructor({ status, code, message, details }: AuthentificationApiError) {
    super(message);
    this.status = status;
    this.code = code;
    this.details = details;
  }
}

export class AccessNotAllowedError extends ApiError {}
export class BadAuthentificationError extends ApiError {}
export class InvalidJsonError extends ApiError {}
export class BadUsernameOrPasswordError extends ApiError {}
export class EmailNotConfirmedError extends ApiError {}
export class InformationIsMissingError extends ApiError {}
export class PasswordIsInvalidError extends ApiError {}
export class UsernameIsInUseError extends ApiError {}
export class EmailIsInUseError extends ApiError {}
export class EmailIsInvalidError extends ApiError {}
export class AccessTokenIsInvalidError extends ApiError {}
export class RefreshTokenIsInvalidError extends ApiError {}
export class TokenIsInvalidError extends ApiError {}
export class ResourceNotFoundError extends ApiError {}
export class NeedOneAdminError extends ApiError {}
export class FileTypeNotAllowedError extends ApiError {}
export class FileNameNotFoundError extends ApiError {}
export class FileIsTooBigError extends ApiError {}
export class RegistrationDisabledError extends ApiError {}
export class InvidationCodeUsedError extends ApiError {}
export class NewPoliciesError extends ApiError {}

const errors = {
  1: AccessNotAllowedError,
  2: BadAuthentificationError,
  200: InvalidJsonError,
  100: BadUsernameOrPasswordError,
  101: EmailNotConfirmedError,
  102: InformationIsMissingError,
  103: PasswordIsInvalidError,
  104: UsernameIsInUseError,
  105: EmailIsInUseError,
  106: EmailIsInvalidError,
  107: AccessTokenIsInvalidError,
  108: RefreshTokenIsInvalidError,
  109: TokenIsInvalidError,
  110: ResourceNotFoundError,
  111: NeedOneAdminError,
  112: FileTypeNotAllowedError,
  113: FileNameNotFoundError,
  114: FileIsTooBigError,
  115: RegistrationDisabledError,
  116: InvidationCodeUsedError,
  117: NewPoliciesError,
} as const;

export function getApiError(e: any) {
  if (isApiError<AuthentificationApiError>(e)) {
    const code = e.response?.data.code;
    if (
      code !== undefined &&
      ((Object.keys(errors) as any) as number[]).includes(code)
    ) {
      const ApiError = errors[code as keyof typeof errors];
      const data = e.response?.data;
      if (!data) {
        return Error(e.message);
      }
      return new ApiError(data);
    }
  }

  return Error(e);
}

export function handleApiErrors(
  e: any,
  handlers: Partial<Record<keyof typeof errors, (e?: any) => void>>
) {
  if (isApiError<AuthentificationApiError>(e)) {
    const code = e.response?.data.code;
    if (
      code !== undefined &&
      ((Object.keys(errors) as any) as number[]).includes(code)
    ) {
      const handler = handlers[code as keyof typeof errors];
      if (handler) {
        handler(e);
      }
    }
  }
}
