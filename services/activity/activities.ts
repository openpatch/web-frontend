import { Member } from '@services/authentification/members';
import { makeApi, Query } from '@utils/api';
import { useRequestInfinite } from '@utils/request';
import { GetServerSidePropsContext } from 'next';
import { urls } from '.';
import { ResourceType } from './resources';

export type Activity = {
  id: string;
  type:
    | 'create'
    | 'update'
    | 'delete'
    | 'follow'
    | 'add'
    | 'remove'
    | 'like'
    | 'undo'
    | 'block'
    | 'create:version'
    | 'update:version'
    | 'comment';
  resource: {
    id: string;
    type: ResourceType;
    data: Record<string, any>;
  };
  member: Member;
  createdOn: string;
};

export type GetActivitiesResponse = {
  activities: Activity[];
  activitiesCount: number;
};

export function useActivities(
  query = {},
  pageSize = 20
): Omit<ReturnType<typeof useRequestInfinite>, 'data'> & {
  activities: Activity[] | null;
} {
  const selector = (data: GetActivitiesResponse) => data.activities;
  const { data, ...others } = useRequestInfinite<
    GetActivitiesResponse,
    Activity
  >(`${urls.browser}/v1/activities`, { query }, pageSize, selector);
  return { activities: data, ...others };
}

export async function getActivities(
  ctx: GetServerSidePropsContext,
  query: Query = {},
  page = 0,
  pageSize = 20
): Promise<Activity[]> {
  return makeApi(ctx)
    .get<GetActivitiesResponse>(`${urls.server}/v1/activities`, {
      params: {
        query: {
          offset: page * pageSize,
          limit: pageSize,
          ...query,
        },
      },
    })
    .then((d) => d.data.activities);
}
