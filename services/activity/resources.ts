import api, { makeApi } from '@utils/api';
import { RequestOptions, useRequest } from '@utils/request';
import { GetServerSidePropsContext } from 'next';
import { urls } from '.';

export type Resource = {
  likesCount: number;
  followsCount: number;
  isLiked: boolean;
  isFollowed: boolean;
};

export type ResourceType = 'item' | 'test' | 'collection' | 'member' | 'assessment';

export type Like = {
  id: string;
  resource: string;
  member: string;
};

export type Follow = {
  id: string;
  resource: string;
  member: string;
};

export type GetResourceResponse = Resource;

export type GetLikesResponse = {
  likes: Like[];
  likesCount: number;
  isLiked: boolean;
};

export type GetFollowsResponse = {
  follows: Follow[];
  followsCount: number;
  isFollowed: boolean;
};

export function useResource(
  id: string,
  options?: RequestOptions<GetResourceResponse>
): Omit<ReturnType<typeof useRequest>, 'data'> & {
  resource: Resource | null;
} {
  const { data, ...others } = useRequest<GetResourceResponse>(
    `${urls.browser}/v1/resources/${id}`,
    options
  );
  return {
    resource: data || null,
    ...others,
  };
}

export async function getResource(
  ctx: GetServerSidePropsContext,
  id: string
): Promise<{
  resource: Resource;
}> {
  return makeApi(ctx)
    .get<GetResourceResponse>(`${urls.server}/v1/resources/${id}`)
    .then((d) => ({
      resource: d.data,
    }));
}

export function useLikes(
  id: string,
  type: ResourceType,
  options?: RequestOptions<GetLikesResponse>
) {
  const { data, ...others } = useRequest<GetLikesResponse>(
    `${urls.browser}/v1/resources/${id}/likes`,
    options
  );

  function postLike(data: any) {
    return api.post(`${urls.browser}/v1/resources/${id}/like`, {
      type,
      data,
    });
  }

  function deleteLike() {
    return api.delete(`${urls.browser}/v1/resources/${id}/like`);
  }

  return {
    like: data || null,
    postLike,
    deleteLike,
    ...others,
  };
}

export async function getLikes(
  ctx: GetServerSidePropsContext,
  id: string
): Promise<GetLikesResponse> {
  return makeApi(ctx)
    .get(`${urls.server}/v1/resources/${id}/likes`)
    .then((d) => d.data);
}

export function useFollows(
  id: string,
  type: ResourceType,
  options?: RequestOptions<GetFollowsResponse>
) {
  const { data, ...others } = useRequest<GetFollowsResponse>(
    `${urls.browser}/v1/resources/${id}/follows`,
    options
  );

  async function postFollow(data: any) {
    return api.post(`${urls.browser}/v1/resources/${id}/follow`, {
      type,
      data,
    });
  }

  async function deleteFollow() {
    return api.delete(`${urls.browser}/v1/resources/${id}/follow`);
  }

  return {
    follows: data || null,
    postFollow,
    deleteFollow,
    ...others,
  };
}

export async function getFollows(
  ctx: GetServerSidePropsContext,
  id: string
): Promise<GetFollowsResponse> {
  return makeApi(ctx)
    .get(`${urls.server}/v1/resources/${id}/follows`)
    .then((d) => d.data);
}
