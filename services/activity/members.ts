import { makeApi } from '@utils/api';
import { RequestOptions, useRequest } from '@utils/request';
import { AxiosResponse } from 'axios';
import { GetServerSidePropsContext } from 'next';
import { urls } from '.';
import { Follow, Like } from './resources';

export type GetMemberLikesResponse = {
  likes: Like[];
  likesCount: number;
};

export type GetMemberFollowsResponse = {
  follows: Follow[];
  followsCount: number;
};

export function useMemberLikes(
  id: string | null,
  options?: RequestOptions<GetMemberLikesResponse>
): Omit<ReturnType<typeof useRequest>, 'data'> & GetMemberLikesResponse {
  const { data, ...others } = useRequest<GetMemberLikesResponse>(
    `${urls.browser}/v1/members/${id}/likes`,
    options
  );
  return {
    likes: data?.likes || [],
    likesCount: data?.likesCount || 0,
    ...others,
  };
}

export async function getMemberLikes(
  ctx: GetServerSidePropsContext,
  id: string
): Promise<[Like[], AxiosResponse<GetMemberLikesResponse>]> {
  return makeApi(ctx)
    .get<GetMemberLikesResponse>(`${urls.server}/v1/members/${id}/likes`)
    .then((d) => [d.data.likes, d]);
}

export function useMemberFollows(
  id: string | null,
  options?: RequestOptions<GetMemberFollowsResponse>
): Omit<ReturnType<typeof useRequest>, 'data'> & GetMemberFollowsResponse {
  const { data, ...others } = useRequest<GetMemberFollowsResponse>(
    `${urls.browser}/v1/members/${id}/follows`,
    options
  );
  return {
    follows: data?.follows || [],
    followsCount: data?.followsCount || 0,
    ...others,
  };
}

export async function getMemberFollows(
  ctx: GetServerSidePropsContext,
  id: string
): Promise<[Follow[], AxiosResponse<GetMemberFollowsResponse>]> {
  return makeApi(ctx)
    .get<GetMemberFollowsResponse>(`${urls.server}/v1/members/${id}/follows`)
    .then((d) => [d.data.follows, d]);
}
