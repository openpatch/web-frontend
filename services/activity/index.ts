export const urls = {
  browser: process.env.NEXT_API_ACTIVITY_BROWSER,
  server: process.env.API_ACTIVITY_SERVER,
};
