export const urls = {
  browser: process.env.NEXT_API_ASSESSMENT_BROWSER,
  server: process.env.API_ASSESSMENT_SERVER,
};
