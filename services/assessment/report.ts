import { updateStatistic } from '@formats/index';
import { TestNode } from '@services/itembank/tests';
import api from '@utils/api';
import { useInterval } from '@utils/interval';
import { updateAvg } from '@utils/math';
import { decrypt } from '@utils/openpgp';
import { useLocalStorage } from '@utils/storage';
import { useEffect, useRef, useState } from 'react';
import {
  getTestVersion,
  ItemVersionCache,
  TestVersionCache,
  useAssessmentPassword,
} from './assessments';
import { ItemResult } from './itemResults';

export type TestNodeStatistic = TestNode & {
  statistic: {
    avgTime: number;
    count: number;
    avgScore: number;
  };
};

export type TestItemStatistic = Partial<ItemVersionCache> & {
  encrypted?: boolean;
  statistic: {
    avgTime: number;
    count: number;
    avgScore: number;
    tasks?: Record<string, any>;
  };
};

export type Report = {
  updatedOn: Date;
  test: TestVersionCache;
  nodes: Record<string, TestNodeStatistic>;
  items: Record<string, TestItemStatistic>;
};

async function calcStatistics(
  report: Report,
  itemResults: ItemResult[],
  password: string | null,
  encryptionPrivateKey: string | null
): Promise<Report> {
  const newReport = { ...report };
  newReport.updatedOn = new Date();
  return new Promise((resolve, reject) => {
    itemResults.forEach(async (itemResult) => {
      if (itemResult.encrypted && !password) {
        return reject({
          noEncryptionPassword: true,
        });
      }

      const item = newReport.items[itemResult.testItemId];
      const node = newReport.nodes[itemResult.testNodeId];
      const time =
        new Date(itemResult.end).getTime() -
        new Date(itemResult.start).getTime();
      const score = itemResult.correct ? 1 : 0;

      if (!item.statistic) {
        item.statistic = {
          avgTime: 0,
          count: 0,
          avgScore: 0,
        };
      }

      item.statistic.avgTime = updateAvg(
        item.statistic.avgTime,
        time,
        item.statistic.count
      );
      item.statistic.avgScore = updateAvg(
        item.statistic.avgScore,
        score,
        item.statistic.count
      );
      item.statistic.count = item.statistic.count + 1;

      if (!node.statistic) {
        node.statistic = {
          avgTime: 0,
          avgScore: 0,
          count: 0,
        };
      }
      node.statistic.avgTime = updateAvg(
        node.statistic.avgTime,
        time,
        node.statistic.count
      );
      node.statistic.avgScore = updateAvg(
        node.statistic.avgScore,
        score,
        node.statistic.count
      );
      node.statistic.count = node.statistic.count + 1;

      let solution = itemResult.solution;
      const solutionEncrypted = itemResult.solutionEncrypted;
      item.encrypted = solutionEncrypted !== null;
      if (
        solutionEncrypted &&
        encryptionPrivateKey !== null &&
        password !== null
      ) {
        try {
          const decryptedSolution = await decrypt(
            encryptionPrivateKey,
            password,
            solutionEncrypted
          );
          solution = JSON.parse(decryptedSolution.data);
        } catch (e) {
          return reject({
            noEncryptionPassword: true,
          });
        }
      }
      const evaluation = itemResult.evaluation;
      const tasks = item.tasks;
      if (!item.statistic.tasks) {
        item.statistic.tasks = {};
      }

      tasks?.forEach(async (task, index) => {
        if (item.statistic.tasks) {
          const formatType = task.formatType;
          item.statistic.tasks[index] = updateStatistic(
            formatType, // TODO check format. Trust is good, control is better.
            item?.statistic?.tasks?.[index],
            solution?.[index],
            evaluation?.results?.[index],
            task
          );
        }
      });
    });
    return resolve(newReport);
  });
}

export function useAssessmentReport(
  id: string,
  autoUpdate: boolean,
  encryptionPrivateKey: string | null
): [report: Report | null, revalidate: () => void, error: any] {
  const encryptionPrivateKeyRef = useRef<string | null>(encryptionPrivateKey);
  const [report, setReport] = useLocalStorage<Report | null>(
    `${id}.report`,
    null
  );
  const [error, setError] = useState<any | null>(null);
  const [
    encryptionPassword,
    ,
    removeEncryptionPassword,
  ] = useAssessmentPassword(id);

  useEffect(() => {
    if (encryptionPrivateKey !== null) {
      encryptionPrivateKeyRef.current = encryptionPrivateKey;
    }
  }, [encryptionPrivateKey]);

  useEffect(() => {
    // give localstorage 1,5sec for loading a report if present
    const timer = setTimeout(() => {
      if (report == null) {
        getTestVersion(null, id, true).then(([testVersion]) => {
          if (!testVersion?.items) {
            return;
          }
          const newReport: Report = {
            updatedOn: new Date('1970-01-01'),
            test: {
              ...testVersion,
            },
            nodes: {},
            items: {},
          };
          testVersion.nodes.forEach((node) => {
            newReport.nodes[node.id] = {
              ...node,
              statistic: {
                avgScore: 0,
                avgTime: 0,
                count: 0,
              },
            };
            node.items.forEach((item) => {
              const itemVersion = testVersion.items.find(
                (i) =>
                  i.item == item.itemVersion.itemId &&
                  i.version == item.itemVersion.version
              );
              if (itemVersion) {
                newReport.items[item.id] = {
                  ...itemVersion,
                  statistic: {
                    avgScore: 0,
                    avgTime: 0,
                    count: 0,
                  },
                };
              }
            });
          });
          setReport(newReport);
        });
      }
    }, 1500);

    return () => clearTimeout(timer);
  }, [report, id]);

  useInterval(() => {
    if (report !== null && autoUpdate && error === null) {
      updateReport();
    }
  }, 10000);

  useEffect(() => {
    if (report?.updatedOn && new Date(report.updatedOn) < new Date()) {
      updateReport();
    }
  }, []);

  function updateReport() {
    if (report) {
      api
        .get<{ itemResults: ItemResult[] }>(
          `${process.env.NEXT_API_ASSESSMENT_BROWSER}/v1/assessments/${id}/item-results`,
          {
            params: {
              query: {
                filter: {
                  updated_on: {
                    gt: report?.updatedOn,
                  },
                },
              },
            },
          }
        )
        .then(({ data: { itemResults } }) =>
          calcStatistics(
            report,
            itemResults,
            encryptionPassword,
            encryptionPrivateKeyRef.current
          )
        )
        .then(setReport)
        .catch((e) => {
          removeEncryptionPassword();
          setError(e);
        });
    }
  }

  function revalidate() {
    setReport(null);
    setError(null);
  }

  return [report, revalidate, error];
}

export function useAssessmentItemReport(id: string, itemId: string) {
  const [report] = useLocalStorage<Report | null>(`${id}.report`, null);
  return report?.items?.[itemId];
}
