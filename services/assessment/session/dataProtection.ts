import api, { makeApi } from '@utils/api';
import { Locale } from '@utils/i18n';
import { RequestOptions, useRequest } from '@utils/request';
import { AxiosResponse } from 'axios';
import { RawDraftContentState } from 'draft-js';
import { GetServerSidePropsContext } from 'next';
import { getApiError } from '../errors';
import { urls } from '../index';
import { castLocale } from './utils';

export type GetDataProtectionReponse = {
  dataProtection: RawDraftContentState;
  language: Locale;
  encrypt: boolean;
  record: boolean;
};

export async function acceptDataProtection(sessionHash: string) {
  return api
    .get(`${urls.browser}/v1/sessions/start`, {
      params: {
        session_hash: sessionHash,
      },
    })
    .catch((e) => {
      throw getApiError(e);
    });
}

export function useDataProtection(
  sessionHash: string,
  options?: RequestOptions<GetDataProtectionReponse>
): Omit<ReturnType<typeof useRequest>, 'data'> & {
  dataProtection: RawDraftContentState | null;
  locale: Locale;
  encrypt: boolean;
  record: boolean;
  accept: () => Promise<AxiosResponse>;
} {
  const { data, ...others } = useRequest<GetDataProtectionReponse>(
    `${urls.browser}/v1/sessions/data-protection`,
    {
      ...options,
      axios: {
        params: {
          ...options?.axios?.params,
          session_hash: sessionHash,
        },
      },
    }
  );

  return {
    dataProtection: data?.dataProtection || null,
    locale: castLocale(data?.language),
    encrypt: data?.encrypt || false,
    record: data?.record || false,
    accept: () => acceptDataProtection(sessionHash),
    ...others,
  };
}

export async function getDataProtection(
  ctx: GetServerSidePropsContext,
  sessionHash: string
): Promise<
  [
    {
      dataProtection: RawDraftContentState | null;
      locale: Locale;
      encrypt: boolean;
      record: boolean;
    },
    AxiosResponse<GetDataProtectionReponse>
  ]
> {
  return makeApi(ctx)
    .get<GetDataProtectionReponse>(
      `${urls.server}/v1/sessions/data-protection`,
      {
        params: {
          session_hash: sessionHash,
        },
      }
    )
    .then((d) => {
      const data = d.data;

      return [
        {
          dataProtection: data?.dataProtection || null,
          locale: castLocale(data?.language),
          encrypt: data?.encrypt || false,
          record: data?.record || false,
        },
        d,
      ];
    });
}
