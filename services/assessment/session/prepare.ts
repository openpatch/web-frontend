import { makeApi } from '@utils/api';
import { Locale } from '@utils/i18n';
import { GetServerSidePropsContext } from 'next';
import { urls } from '..';
import { getApiError } from '../errors';

export type SessionPrepare = { session: string; language: Locale };

export type PostSessionPrepareResponse = SessionPrepare;

export async function postPrepare(
  ctx: GetServerSidePropsContext | null,
  id: string,
  password: string
): Promise<SessionPrepare> {
  return makeApi(ctx)
    .post<PostSessionPrepareResponse>(`${urls.server}/v1/sessions/prepare`, {
      id,
      password,
    })
    .then((d) => d.data)
    .catch((e) => {
      throw getApiError(e);
    });
}
