import { t } from '@lingui/macro';
import api from '@utils/api';
import { useNotifications } from '@utils/notification';
import { encrypt } from '@utils/openpgp';
import { CancelToken } from 'axios';
import { RawDraftContentState } from 'draft-js';
import { useEffect, useState } from 'react';
import { CleanSessionState, TaskState } from '.';
import { urls } from '..';

type State = 'FETCHING' | 'SUBMITTING' | 'WORKING';

export type SessionItem = {
  tasks: {
    data: any;
    formatType: string;
    formatVersion: number;
    text: RawDraftContentState;
    task: string;
  }[];
};

type GetSessionResponse = {
  language: string;
  sessionHash: string;
  items: SessionItem[];
  encrypted: boolean;
  encryptedPublicKey: string;
  recorded: boolean;
  recordingId: string;
};

type PostSessionResponse = {
  passed: boolean;
  checks: boolean[];
  sessionHash: string;
  needsToBeCorrect: boolean;
};

function encryptSolutions(encryptionPublicKey: string, solutions: any[]) {
  return Promise.all(
    solutions.map(async (solution: any) => {
      const solutionMessage = JSON.stringify(solution);
      return encrypt(encryptionPublicKey, solutionMessage).then((e) => e.data);
    })
  );
}

function postSolutions(sessionHash: string, solutions: any[]) {
  return api.post<PostSessionResponse>(
    `${urls.browser}/v1/sessions`,
    {
      solutions,
    },
    {
      timeout: 120000, // allows for 4 coding tasks (4x30sec) in one jumpable node, which nobody should do
      params: {
        session_hash: sessionHash,
      },
    }
  );
}

export function useItems(sessionHash: string) {
  const [items, setItems] = useState<SessionItem[]>([]);
  const [recordingId, setRecordingId] = useState<string | null>(null);
  const [error, setError] = useState<any>(null);
  const [encryptionPublicKey, setEncryptionPublicKey] = useState<string | null>(
    null
  );
  const [state, setState] = useState<State>('FETCHING');
  const [add] = useNotifications();

  async function next(cancelToken?: CancelToken) {
    setState('FETCHING');
    return api
      .get<GetSessionResponse>(`${urls.browser}/v1/sessions`, {
        cancelToken,
        params: {
          session_hash: sessionHash,
        },
      })
      .then((d) => {
        if (d.data.encrypted) {
          setEncryptionPublicKey(d.data.encryptedPublicKey);
        }
        if (d.data.recorded) {
          setRecordingId(d.data.recordingId);
        }
        setState('WORKING');
        setItems(d.data.items || []);
      })
      .catch((e) => setError(e));
  }

  useEffect(() => {
    const source = api?.CancelToken?.source();
    if (sessionHash) {
      next(source?.token);
    }
    return () => {
      source?.cancel();
    };
  }, [sessionHash]);

  async function submit(sessionState: CleanSessionState) {
    setState('SUBMITTING');
    let solutions: Record<string, TaskState>[] | string[] = Object.values(
      sessionState.items
    ).map((item) => item.tasks || []);

    if (encryptionPublicKey) {
      solutions = await encryptSolutions(encryptionPublicKey, solutions);
    }
    return postSolutions(sessionHash, solutions)
      .then(({ data: { needsToBeCorrect, passed } }) => {
        if (needsToBeCorrect && !passed) {
          next();
        } else {
          add({
            message: t`You need to answer correctly, before you can continue`,
            severity: 'info',
          });
        }
      })
      .catch((e) => {
        add({
          message: 'Something went wrong. Please try again',
          severity: 'error',
        });
        setState('WORKING');
        Promise.reject(e);
      });
  }

  return {
    items,
    recordingId,
    submit,
    state,
    isEmpty: items?.length === 0,
    encryptionPublicKey,
    error,
  };
}
