import { makeApi } from '@utils/api';
import { Locale } from '@utils/i18n';
import { RequestOptions, useRequest } from '@utils/request';
import { AxiosResponse } from 'axios';
import { RawDraftContentState } from 'draft-js';
import { GetServerSidePropsContext } from 'next';
import { urls } from '../index';
import { castLocale } from './utils';

type GetWelcomeReponse = {
  welcome: RawDraftContentState;
  language: string;
};

export function useWelcome(
  sessionHash: string,
  options: RequestOptions<GetWelcomeReponse>
): Omit<ReturnType<typeof useRequest>, 'data'> & {
  welcome: RawDraftContentState | null;
  locale: Locale;
} {
  const { data, ...others } = useRequest<GetWelcomeReponse>(
    `${urls.browser}/v1/sessions/welcome`,
    {
      ...options,
      axios: {
        params: {
          ...options?.axios?.params,
          session_hash: sessionHash,
        },
      },
    }
  );

  return {
    welcome: data?.welcome || null,
    locale: castLocale(data?.language),
    ...others,
  };
}

export async function getWelcome(
  ctx: GetServerSidePropsContext,
  sessionHash: string
): Promise<
  [
    {
      welcome: RawDraftContentState | null;
      locale: Locale;
    },
    AxiosResponse<GetWelcomeReponse>
  ]
> {
  return makeApi(ctx)
    .get<GetWelcomeReponse>(`${urls.server}/v1/sessions/welcome`, {
      params: {
        session_hash: sessionHash,
      },
    })
    .then((d) => {
      const data = d.data;
      return [
        {
          welcome: data?.welcome || null,
          locale: castLocale(data?.language),
        },
        d,
      ];
    });
}
