import { reducer as formatReducer } from '@formats/index';
import produce from 'immer';
import { Action, JUMP_TO_ITEM, RESET, SCOPE_FORMAT } from './actions';
import { SessionState } from './index';

export const initialState: SessionState = {
  currentItem: 0,
  items: {},
};

const reducer = (
  state: SessionState = initialState,
  action: Action
): SessionState =>
  produce(state, (draft) => {
    if (action.scope === SCOPE_FORMAT) {
      if (!draft.items[action.itemId]) {
        draft.items[action.itemId] = {
          tasks: {},
        };
      }
      const taskState = draft.items[action.itemId].tasks[action.taskId];
      draft.items[action.itemId].tasks[action.taskId] = formatReducer(
        taskState,
        action
      );
    } else {
      switch (action.type) {
        case RESET:
          return initialState;
        case JUMP_TO_ITEM:
          draft.currentItem = action.payload.itemId;
      }
    }
  });

export default reducer;
