import { createContext, useContext } from 'react';
import { Action, FormatAction } from './actions';

export type TaskState = {
  format: string;
};

export type ItemState = {
  tasks: Record<number, TaskState>;
};

export type SessionState = {
  currentItem: number;
  items: Record<number, ItemState>;
};

export type CleanSessionState = Omit<SessionState, 'currentItem'>;

type SessionContext = {
  state: SessionState;
  dispatch: (action: Action) => void;
  handleSubmit: () => void;
  handleReset: () => void;
};

export const context = createContext<SessionContext>({
  state: {
    currentItem: 0,
    items: {},
  },
  dispatch: () => null,
  handleReset: () => null,
  handleSubmit: () => null,
});

export function useSession(): {
  state: SessionContext['state'];
  dispatch: SessionContext['dispatch'];
  handleSubmit: SessionContext['handleSubmit'];
  handleReset: SessionContext['handleReset'];
} {
  const ctx = useContext(context);

  if (!ctx) {
    throw new Error('No SessionProvider is present.');
  }

  return {
    state: ctx.state,
    dispatch: ctx.dispatch,
    handleSubmit: ctx.handleSubmit,
    handleReset: ctx.handleReset,
  };
}

export function useSubmit(): SessionContext['handleSubmit'] {
  const { handleSubmit } = useSession();
  return handleSubmit;
}

export function useItemState(id: number) {
  const { state } = useSession();

  return state?.items?.[id];
}

export function useTask(itemId: number, taskId: number) {
  const { state, dispatch } = useSession();
  const taskState = state.items?.[itemId]?.tasks?.[taskId];

  function taskDispatch(action: Omit<FormatAction, 'scope'>) {
    return dispatch({ scope: 'FORMAT', ...action });
  }

  return { state: taskState, dispatch: taskDispatch };
}
