import { ReactNode, useCallback, useReducer, useRef } from 'react';
import {
  Action,
  reset,
  submit,
  submitError,
  submitResponse,
  TimeAction,
} from './actions';
import { CleanSessionState, context, SessionState } from './index';
import reducer, { initialState as ris } from './reducer';
import { cleanState } from './utils';

export type SessionProviderProps = {
  children: ReactNode;
  initialState?: SessionState;
  recorder?: (timeAction: TimeAction) => void;
  onSubmit?: (
    state: CleanSessionState,
    dispatchResponse: any,
    dispatchError: any
  ) => void;
  readOnly?: boolean;
};

export const SessionProvider = ({
  initialState = ris,
  children,
  recorder,
  readOnly,
  onSubmit,
}: SessionProviderProps) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const timestamp = useRef<number>(Date.now());

  const customDispatch = useCallback(
    (action: Action) => {
      if (!readOnly) {
        const timeAction: TimeAction = {
          ...action,
          timestamp: Date.now() - timestamp.current,
        };
        if (recorder) {
          recorder(timeAction);
        }
        dispatch(timeAction);
      } else {
        dispatch(action);
      }
    },
    [recorder, dispatch]
  );

  const dispatchResponse = useCallback(
    (res) => {
      customDispatch(submitResponse(res));
    },
    [customDispatch]
  );

  const dispatchError = useCallback(
    (err) => {
      customDispatch(submitError(err));
    },
    [customDispatch]
  );

  const handleSubmit = useCallback(() => {
    customDispatch(submit());

    if (onSubmit) {
      onSubmit(cleanState(state), dispatchResponse, dispatchError);
    }
  }, [customDispatch]);

  const handleReset = useCallback(() => {
    dispatch(reset());
  }, [dispatch]);

  return (
    <context.Provider
      value={{
        state,
        dispatch: customDispatch,
        handleSubmit,
        handleReset,
      }}
    >
      {children}
    </context.Provider>
  );
};
