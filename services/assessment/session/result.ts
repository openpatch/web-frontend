import { makeApi } from '@utils/api';
import { Locale } from '@utils/i18n';
import { RequestOptions, useRequest } from '@utils/request';
import { AxiosResponse } from 'axios';
import { GetServerSidePropsContext } from 'next';
import { urls } from '..';
import { ItemVersionCache } from '../assessments';
import { ItemResult } from '../itemResults';
import { castLocale } from './utils';

export type Result = {
  locale: Locale;
  itemResults: ItemResult[];
  itemVersions: ItemVersionCache[];
  score: number;
  aborted: boolean;
};

export type GetResultResponse = {
  language: string;
  itemResults: ItemResult[];
  itemVersions: ItemVersionCache[];
  score: number;
  aborted: boolean;
};

export function useResult(
  sessionHash: string,
  options?: RequestOptions<GetResultResponse>
): Omit<ReturnType<typeof useRequest>, 'data'> & Result {
  const { data, ...others } = useRequest<GetResultResponse>(
    `${urls.browser}/v1/session/result`,
    {
      ...options,
      axios: {
        params: {
          ...options?.axios?.params,
          session_hash: sessionHash,
        },
      },
    }
  );

  return {
    locale: castLocale(data?.language),
    itemResults: data?.itemResults || [],
    itemVersions: data?.itemVersions || [],
    score: data?.score || 0,
    aborted: data?.aborted || false,
    ...others,
  };
}

export async function getResult(
  ctx: GetServerSidePropsContext,
  sessionHash: string
): Promise<[Result, AxiosResponse<GetResultResponse>]> {
  return makeApi(ctx)
    .get<GetResultResponse>(`${urls.server}/v1/session/result`, {
      params: {
        session_hash: sessionHash,
      },
    })
    .then((d) => [
      {
        locale: castLocale(d.data?.language),
        itemResults: d.data?.itemResults || [],
        itemVersions: d.data?.itemVersions || [],
        score: d.data?.score || 0,
        aborted: d.data?.aborted || false,
      },
      d,
    ]);
}
