import { makeApi } from '@utils/api';
import { AxiosResponse } from 'axios';
import { GetServerSidePropsContext } from 'next';
import { urls } from '..';

export type PostAbortResponse = {
  msg: string;
};

export async function postAbort(
  ctx: GetServerSidePropsContext | null,
  sessionHash: string,
  message: string
): Promise<[PostAbortResponse, AxiosResponse<PostAbortResponse>]> {
  return makeApi(ctx)
    .post<PostAbortResponse>(
      `${urls.browser}/v1/sessions/abort`,
      { message },
      {
        params: {
          session_hash: sessionHash,
        },
      }
    )
    .then((d) => [d.data, d]);
}
