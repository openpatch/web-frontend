import { makeApi } from '@utils/api';
import { languages, Locale } from '@utils/i18n';
import { RequestOptions, useRequest } from '@utils/request';
import { AxiosResponse } from 'axios';
import { RawDraftContentState } from 'draft-js';
import { GetServerSidePropsContext } from 'next';
import { urls } from '../index';
import { castLocale } from './utils';

export type GetFarewellReponse = {
  farewell: RawDraftContentState;
  language: string;
  showResult: boolean;
};

export function useFarewell(
  sessionHash: string,
  options?: RequestOptions<GetFarewellReponse>
): Omit<ReturnType<typeof useRequest>, 'data'> & {
  farewell?: RawDraftContentState;
  locale: Locale;
  showResult: boolean;
} {
  const { data, ...others } = useRequest<GetFarewellReponse>(
    `${urls.browser}/v1/sessions/farewell`,
    {
      ...options,
      axios: {
        params: {
          ...options?.axios?.params,
          session_hash: sessionHash,
        },
      },
    }
  );

  let language = data?.language;

  if (!language || !Object.keys(languages).includes(language)) {
    language = 'en';
  }

  return {
    farewell: data?.farewell,
    locale: castLocale(data?.language),
    showResult: data?.showResult || false,
    ...others,
  };
}

export async function getFarewell(
  ctx: GetServerSidePropsContext,
  sessionHash: string
): Promise<
  [
    {
      farewell: RawDraftContentState;
      locale: Locale;
      showResult: boolean;
    },
    AxiosResponse<GetFarewellReponse>
  ]
> {
  return makeApi(ctx)
    .get<GetFarewellReponse>(`${urls.server}/v1/sessions/farewell`, {
      params: {
        session_hash: sessionHash,
      },
    })
    .then((d) => {
      const data = d.data;
      return [
        {
          farewell: data?.farewell,
          locale: castLocale(data?.language),
          showResult: data?.showResult || false,
        },
        d,
      ];
    });
}
