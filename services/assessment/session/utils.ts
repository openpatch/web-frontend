import { clean } from '@formats/index';
import { languages, Locale } from '@utils/i18n';
import produce, { Draft } from 'immer';
import { CleanSessionState, SessionState } from './index';

export const cleanState = (state: SessionState): CleanSessionState =>
  produce(state, (draft: Draft<SessionState>) => {
    Object.keys(draft.items).forEach((itemId) => {
      Object.keys(draft.items[Number(itemId)].tasks).forEach((taskId) => {
        draft.items[Number(itemId)].tasks[Number(taskId)] = clean(
          draft.items[Number(itemId)].tasks[Number(taskId)] as any
          // TODO check format. Trust is good, control is better.
        );
      });
    });

    return {
      items: draft.items,
    };
  });

export const castLocale = (locale?: string): Locale => {
  if (!locale || !Object.keys(languages).includes(locale)) {
    return 'en';
  }

  return locale as Locale;
};
