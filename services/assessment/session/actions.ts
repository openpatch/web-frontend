type BaseAction = {
  type: string;
  scope: string;
  payload?: any;
};

export const SCOPE = 'SESSION';
export const JUMP_TO_ITEM = 'JUMP_TO_ITEM';
export const SUBMIT = 'SUBMIT';
export const SUBMIT_RESPONSE = 'SUBMIT_RESPONSE';
export const SUBMIT_ERROR = 'SUBMIT_ERROR';
export const RESET = 'RESET';
export const SCOPE_FORMAT = 'FORMAT';

interface JumpToItemAction extends BaseAction {
  type: typeof JUMP_TO_ITEM;
  scope: typeof SCOPE;
  payload: {
    itemId: number;
  };
}

export function jumpToItem(itemId: number): JumpToItemAction {
  return {
    type: JUMP_TO_ITEM,
    scope: SCOPE,
    payload: {
      itemId,
    },
  };
}

interface SubmitAction extends BaseAction {
  type: typeof SUBMIT;
  scope: typeof SCOPE;
}

export function submit(): SubmitAction {
  return {
    type: SUBMIT,
    scope: SCOPE,
  };
}

interface SubmitResponseAction extends BaseAction {
  type: typeof SUBMIT_RESPONSE;
  scope: typeof SCOPE;
  payload: any;
}

export function submitResponse(res: any): SubmitResponseAction {
  return {
    type: SUBMIT_RESPONSE,
    scope: SCOPE,
    payload: {
      ...res,
    },
  };
}

interface SubmitErrorAction extends BaseAction {
  type: typeof SUBMIT_ERROR;
  scope: typeof SCOPE;
  payload: any;
}

export function submitError(err: any): SubmitErrorAction {
  return {
    type: SUBMIT_ERROR,
    scope: SCOPE,
    payload: { ...err },
  };
}

interface ResetAction extends BaseAction {
  type: typeof RESET;
  scope: typeof SCOPE;
}

export function reset(): ResetAction {
  return {
    type: RESET,
    scope: SCOPE,
  };
}

export interface FormatAction extends BaseAction {
  scope: typeof SCOPE_FORMAT;
  itemId: number;
  taskId: number;
}

export type Action =
  | JumpToItemAction
  | SubmitAction
  | SubmitResponseAction
  | SubmitErrorAction
  | ResetAction
  | FormatAction;

export type TimeAction = Action & {
  timestamp: number;
};
