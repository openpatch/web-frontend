import { makeApi } from '@utils/api';
import { RequestOptions, useRequest } from '@utils/request';
import { AxiosRequestConfig, AxiosResponse } from 'axios';
import { GetServerSidePropsContext } from 'next';
import { urls } from '.';
import { ItemResult } from './itemResults';

export type NodeResult = {
  id: string;
  assessment: string;
  encrypted: boolean;
  end: string;
  itemResults: ItemResult[];
  precision: number;
  recorded: boolean;
  score: number;
  seed: number;
  start: string;
  testId: string;
  testNodeId: string;
  testResult: string;
  testVersion: number;
};

export type GetNodeResultResponse = {
  nodeResult: NodeResult;
};

export function useNodeResult(
  id: string,
  options: RequestOptions<GetNodeResultResponse>
): Omit<ReturnType<typeof useRequest>, 'data'> & {
  nodeResult: NodeResult | null;
} {
  const { data, ...other } = useRequest<GetNodeResultResponse>(
    `${urls.browser}/v1/node-results/${id}`,
    options
  );
  return {
    nodeResult: data ? data.nodeResult : null,
    ...other,
  };
}

export async function getNodeResult(
  ctx: GetServerSidePropsContext,
  id: string,
  options?: AxiosRequestConfig
): Promise<[NodeResult, AxiosResponse<GetNodeResultResponse>]> {
  return makeApi(ctx)
    .get<GetNodeResultResponse>(`${urls.server}/v1/node-results/${id}`, options)
    .then((d) => [d.data.nodeResult, d]);
}
