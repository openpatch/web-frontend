import { Member } from '@services/authentification/members';
import { ItemVersion } from '@services/itembank/items';
import { TestVersion } from '@services/itembank/tests';
import { makeApi, Query } from '@utils/api';
import { initWorker } from '@utils/openpgp';
import { RequestOptions, useRequest, useRequestInfinite } from '@utils/request';
import { useSessionStorage } from '@utils/storage';
import { AxiosRequestConfig, AxiosResponse } from 'axios';
import { RawDraftContentState } from 'draft-js';
import { GetServerSidePropsContext } from 'next';
import { urls } from '.';
import { TestResult } from './testResults';

initWorker();

export type Assessment = {
  id: string;
  authors: string[];
  member: Member;
  name: string;
  startsOn: string;
  endsOn: string;
  public: boolean;
  language: string;
  publicDescription: RawDraftContentState;
  greeting: RawDraftContentState;
  farewell: RawDraftContentState;
  dataProtection: RawDraftContentState | null;
  showProgress: boolean;
  showResult: boolean;
  testId: string;
  testVersion: number;
  testResults: string[];
  record: boolean;
  encrypt: boolean;
  createdOn: string;
  passwordProtected: boolean;
  encryptionPrivateKey: string | null;
};

export type TestVersionCache = TestVersion;
export type TestVersionCacheWithItems = TestVersion & {
  items: ItemVersionCache[];
};
export type ItemVersionCache = {
  item: string;
  version: number;
  status: 'pilot' | 'faulty' | 'ready' | 'draft';
  tasks: ItemVersionTaskCache[];
};
export type ItemVersionTaskCache = {
  id: string;
  task: string;
  formatType: string;
  formatVersion: number;
  data: any;
  evaluation: any;
  text: RawDraftContentState;
};

export type AssessmentStatistic = {
  aborted: number;
  finished: number;
  count: number;
  avgScore: number;
  avgTime: number;
};

export type GetAssessmentResponse = {
  assessment: Assessment;
};

export type GetAssessmentsResponse = {
  assessments: Assessment[];
  assessmentsCount: number;
};

export type GetTestVersionCache =
  | {
      testVersion: TestVersion;
    }
  | {
      testVersion: Omit<TestVersion, 'items'> & {
        items: ItemVersion;
      };
    };

export type GetItemVersionCache = {
  itemVersion: ItemVersionCache;
};

export type GetNodeItemVersionsCache = {
  itemVersions: ItemVersionCache[];
};

export function useAssessment(
  id: string,
  options: RequestOptions<GetAssessmentResponse> = {}
): Omit<ReturnType<typeof useRequest>, 'data'> & {
  assessment: Assessment | null;
} {
  const { data, ...others } = useRequest<GetAssessmentResponse>(
    `${urls.browser}/v1/assessments/${id}`,
    options
  );
  return {
    assessment: data?.assessment || null,
    ...others,
  };
}

export async function getAssessment(
  ctx: GetServerSidePropsContext,
  id: string
): Promise<[Assessment, AxiosResponse<GetAssessmentResponse>]> {
  return makeApi(ctx)
    .get<GetAssessmentResponse>(`${urls.server}/v1/assessments/${id}`)
    .then((d) => [d.data.assessment, d]);
}

export type PutAssessmentResponse = Record<string, never>;

export async function putAssessment(
  ctx: GetServerSidePropsContext | null,
  id: string,
  data: Partial<Assessment>
): Promise<[PutAssessmentResponse, AxiosResponse<PutAssessmentResponse>]> {
  const url = ctx === null ? urls.browser : urls.server;
  return makeApi(ctx)
    .put<PutAssessmentResponse>(`${url}/v1/assessments/${id}`, data)
    .then((d) => [d.data, d]);
}

export function useAssessments(
  query: Query = {},
  pageSize = 20
): Omit<ReturnType<typeof useRequestInfinite>, 'data'> & {
  assessments: Assessment[] | null;
} {
  const url = `${urls.browser}/v1/assessments`;
  const selector = (data: GetAssessmentsResponse) => data.assessments;
  const { data, ...others } = useRequestInfinite<
    GetAssessmentsResponse,
    Assessment
  >(url, { query }, pageSize, selector);
  return {
    assessments: data,
    ...others,
  };
}

export async function getAssessments(
  ctx: GetServerSidePropsContext,
  query: Query = {},
  page = 0,
  pageSize = 20
): Promise<[Assessment[], AxiosResponse<GetAssessmentsResponse>]> {
  return makeApi(ctx)
    .get<GetAssessmentsResponse>(`${urls.server}/v1/assessments`, {
      params: {
        query: {
          offset: page * pageSize,
          limit: pageSize,
          ...query,
        },
      },
    })
    .then((d) => [d.data.assessments, d]);
}

export type PostAssessmentsResponse = {
  assessmentId: string;
};

export async function postAssessments(
  ctx: GetServerSidePropsContext | null,
  data: Partial<Assessment> & {
    encryptionPublicKey?: string | null;
    encryptionPrivateKey?: string | null;
    password?: string | null;
  }
): Promise<[string, AxiosResponse<PostAssessmentsResponse>]> {
  const url = ctx === null ? urls.browser : urls.server;
  return makeApi(ctx)
    .post<PostAssessmentsResponse>(`${url}/v1/assessments`, data)
    .then((d) => [d.data.assessmentId, d]);
}

export function useAssessmentPassword(
  id: string
): [string | null, (value: string) => void, () => void] {
  const [
    encryptionPassword,
    setEncryptionPassword,
    removeEncryptionPassword,
  ] = useSessionStorage<string>(`${id}.password`);
  return [encryptionPassword, setEncryptionPassword, removeEncryptionPassword];
}

export function useTestVersionCache(
  id: string,
  includeItems: boolean,
  options: RequestOptions<GetTestVersionCache> = {}
): Omit<ReturnType<typeof useRequest>, 'data'> & {
  testVersion: TestVersionCache | TestVersionCacheWithItems | null;
} {
  const { data, ...others } = useRequest<GetTestVersionCache>(
    `${urls.browser}/v1/assessments/${id}/test-version`,
    {
      ...options,
      axios: {
        params: {
          ...options?.axios?.params,
          items: includeItems,
        },
      },
    }
  );

  if (includeItems) {
    return {
      testVersion: data
        ? (data.testVersion as TestVersionCacheWithItems)
        : null,
      ...others,
    };
  }
  return {
    testVersion: data ? data.testVersion : null,
    ...others,
  };
}

export async function getTestVersion(
  ctx: GetServerSidePropsContext | null,
  id: string,
  includeItems: true,
  options?: AxiosRequestConfig
): Promise<
  [TestVersionCacheWithItems, AxiosResponse<TestVersionCacheWithItems>]
>;
export async function getTestVersion(
  ctx: GetServerSidePropsContext | null,
  id: string,
  includeItems: false,
  options?: AxiosRequestConfig
): Promise<[TestVersionCache, AxiosResponse<TestVersionCache>]>;
export async function getTestVersion(
  ctx: GetServerSidePropsContext | null,
  id: string,
  includeItems: boolean,
  options?: AxiosRequestConfig
): Promise<
  [
    TestVersionCache | TestVersionCacheWithItems,
    AxiosResponse<TestVersionCache> | AxiosResponse<TestVersionCacheWithItems>
  ]
> {
  const api = makeApi(ctx);
  if (includeItems) {
    return api
      .get<TestVersionCacheWithItems>(
        `${urls.server}/v1/assessments/${id}/test-version&items=true`,
        options
      )
      .then((d) => [d.data, d]);
  }

  return api
    .get<TestVersionCache>(`${urls.server}/v1/assessments/${id}/test-version`)
    .then((d) => [d.data, d]);
}

export function useItemVersionCache(
  id: string,
  itemId: string,
  itemVersion: number,
  options: RequestOptions<GetItemVersionCache> = {}
): Omit<ReturnType<typeof useRequest>, 'data'> & {
  itemVersion: ItemVersionCache | null;
} {
  const { data, ...others } = useRequest<GetItemVersionCache>(
    `${urls.browser}/v1/assessments/${id}/items/${itemId}/versions/${itemVersion}`,
    options
  );
  return {
    itemVersion: data ? data.itemVersion : null,
    ...others,
  };
}

export async function getItemVersion(
  ctx: GetServerSidePropsContext,
  id: string,
  itemId: string,
  itemVersion: number,
  options?: AxiosRequestConfig
): Promise<[ItemVersionCache, AxiosResponse<GetItemVersionCache>]> {
  return makeApi(ctx)
    .get<GetItemVersionCache>(
      `${urls.server}/v1/assessments/${id}/items/${itemId}/versions/${itemVersion}`,
      options
    )
    .then((d) => [d.data.itemVersion, d]);
}

export function useNodeItemVersionsCache(
  id: string,
  nodeId: string,
  options?: RequestOptions<GetNodeItemVersionsCache>
): Omit<ReturnType<typeof useRequest>, 'data'> & {
  itemVersions: ItemVersionCache[] | null;
} {
  const { data, ...others } = useRequest<GetNodeItemVersionsCache>(
    `${urls.browser}/v1/assessments/${id}/nodes/${nodeId}/item-versions`,
    options
  );
  return {
    itemVersions: data ? data.itemVersions : null,
    ...others,
  };
}

export async function getNodeItemVersions(
  ctx: GetServerSidePropsContext,
  id: string,
  nodeId: string,
  options?: AxiosRequestConfig
): Promise<[ItemVersionCache[], AxiosResponse<GetNodeItemVersionsCache>]> {
  return makeApi(ctx)
    .get<GetNodeItemVersionsCache>(
      `${urls.server}/v1/assessments/${id}/nodes/${nodeId}/item-versions`,
      options
    )
    .then((d) => [d.data.itemVersions, d]);
}

export type GetAssessmentStatisticResponse = {
  assessmentStatistic: AssessmentStatistic;
};

export function useAssessmentStatistic(
  id: string,
  options?: RequestOptions<GetAssessmentStatisticResponse>
): Omit<ReturnType<typeof useRequest>, 'data'> & {
  assessmentStatistic: AssessmentStatistic | null;
} {
  const { data, ...others } = useRequest<GetAssessmentStatisticResponse>(
    `${urls.browser}/v1/assessments/${id}/statistic`,
    options
  );
  return {
    assessmentStatistic: data ? data.assessmentStatistic : null,
    ...others,
  };
}

export async function getAssessmentStatistic(
  ctx: GetServerSidePropsContext | null,
  id: string,
  options?: AxiosRequestConfig
): Promise<
  [AssessmentStatistic, AxiosResponse<GetAssessmentStatisticResponse>]
> {
  const url = ctx === null ? urls.browser : urls.server;
  return makeApi(ctx)
    .get<GetAssessmentStatisticResponse>(
      `${url}/v1/assessments/${id}/statistic`,
      options
    )
    .then((d) => [d.data.assessmentStatistic, d]);
}

export type GetAssessmentResultsExportResponse = {
  testResults: TestResult[];
  itemVersions: ItemVersionCache[];
  testVersion: TestVersionCache;
};

export async function getAssessmentResultsExport(
  ctx: GetServerSidePropsContext | null,
  id: string,
  options?: AxiosRequestConfig
): Promise<
  [
    GetAssessmentResultsExportResponse,
    AxiosResponse<GetAssessmentResultsExportResponse>
  ]
> {
  const url = ctx === null ? urls.browser : urls.server;
  return makeApi(ctx)
    .get<GetAssessmentResultsExportResponse>(
      `${url}/v1/assessments/${id}/results/export`,
      options
    )
    .then((d) => [d.data, d]);
}

export type DeleteAssessmentResultsResponse = Record<string, never>;

export async function deleteAssessmentResults(
  ctx: GetServerSidePropsContext | null,
  id: string
): Promise<
  [
    DeleteAssessmentResultsResponse,
    AxiosResponse<DeleteAssessmentResultsResponse>
  ]
> {
  const url = ctx === null ? urls.browser : urls.server;
  return makeApi(ctx)
    .delete<DeleteAssessmentResultsResponse>(
      `${url}/v1/assessments/${id}/results`
    )
    .then((d) => [d.data, d]);
}

export type GetAssessmentResultsResponse = {
  testResults: TestResult[];
  testResultsCount: number;
};

export async function getAssessmentResults(
  ctx: GetServerSidePropsContext | null,
  id: string
): Promise<[TestResult[], AxiosResponse<GetAssessmentResultsResponse>]> {
  const url = ctx === null ? urls.browser : urls.server;
  return makeApi(ctx)
    .get<GetAssessmentResultsResponse>(`${url}/v1/assessments/${id}/results`)
    .then((d) => [d.data.testResults, d]);
}
