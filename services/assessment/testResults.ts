import { makeApi } from '@utils/api';
import { RequestOptions, useRequest } from '@utils/request';
import { AxiosRequestConfig, AxiosResponse } from 'axios';
import { GetServerSidePropsContext } from 'next';
import { urls } from '.';
import { NodeResult } from './nodeResults';

export type TestResult = {
  id: string;
  assessment: string;
  testId: string;
  testVersion: number;
  sessionHash: string;
  aborted: boolean;
  abortedReason: string;
  score: number;
  start: string;
  end: string;
  nodeResults: NodeResult[];
};

export type GetTestResultResponse = {
  testResult: TestResult;
};

export function useTestResult(
  id: string,
  options: RequestOptions<GetTestResultResponse>
): Omit<ReturnType<typeof useRequest>, 'data'> & {
  testResult: TestResult | null;
} {
  const { data, ...other } = useRequest<GetTestResultResponse>(
    `${urls.browser}/v1/test-results/${id}`,
    options
  );
  return {
    testResult: data ? data.testResult : null,
    ...other,
  };
}

export async function getTestResult(
  ctx: GetServerSidePropsContext,
  id: string,
  options?: AxiosRequestConfig
): Promise<[TestResult, AxiosResponse<GetTestResultResponse>]> {
  return makeApi(ctx)
    .get<GetTestResultResponse>(`${urls.server}/v1/test-results/${id}`, options)
    .then((d) => [d.data.testResult, d]);
}
