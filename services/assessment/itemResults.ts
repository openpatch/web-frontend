import { makeApi } from '@utils/api';
import { RequestOptions, useRequest } from '@utils/request';
import { AxiosRequestConfig, AxiosResponse } from 'axios';
import { GetServerSidePropsContext } from 'next';
import { urls } from '.';

export type ItemResult = {
  id: string;
  solution: any;
  solutionEncrypted: string | null;
  evaluation: {
    correct: boolean;
    results: {
      correct: boolean;
      details?: Record<string, any>;
    }[];
  };
  correct: boolean;
  errorEvaluation: boolean;
  encrypted: boolean;
  recorded: boolean;
  start: string;
  end: string;
  itemId: string;
  itemVersion: number;
  testNodeId: string;
  testItemId: string;
  assessment: string;
  nodeResult: string;
  position: number;
  encryptionPrivateKey: string;
};

export type GetItemResult = {
  itemResult: ItemResult;
};

export type GetItemResults = {
  itemResults: ItemResult[];
};

export function useItemResult(
  id: string,
  options: RequestOptions<GetItemResult>
): Omit<ReturnType<typeof useRequest>, 'data'> & {
  itemResult: ItemResult | null;
} {
  const { data, ...other } = useRequest<GetItemResult>(
    `${urls.browser}/v1/item-results/${id}`,
    options
  );
  return {
    itemResult: data ? data.itemResult : null,
    ...other,
  };
}

export async function getItemResult(
  ctx: GetServerSidePropsContext,
  id: string,
  options?: AxiosRequestConfig
): Promise<[ItemResult, AxiosResponse<GetItemResult>]> {
  return makeApi(ctx)
    .get<GetItemResult>(`${urls.server}/v1/item-results/${id}`, options)
    .then((d) => [d.data.itemResult, d]);
}
