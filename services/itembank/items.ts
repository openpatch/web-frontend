import { Member } from '@services/authentification/members';
import { makeApi, Query } from '@utils/api';
import { RequestOptions, useRequest, useRequestInfinite } from '@utils/request';
import { AxiosRequestConfig, AxiosResponse } from 'axios';
import { RawDraftContentState } from 'draft-js';
import { GetServerSidePropsContext } from 'next';
import { urls } from '.';

export type Item = {
  id: string;
  authors: Member[];
  createdOn: string;
  language: string;
  member: Member;
  name: string;
  privacy: 'private' | 'public' | 'notlisted';
  publicDescription: RawDraftContentState;
  publicDescriptionText: string;
  sourceReference: string;
  tags: string[];
  versions: Pick<
    ItemVersion,
    'version' | 'versionMessage' | 'status' | 'latest' | 'item'
  >[];
};

export type ItemVersion = {
  id: string;
  version: number;
  item: string;
  versionMessage: string;
  status: 'faulty' | 'pilot' | 'draft' | 'ready';
  latest: boolean;
  member: Member;
  tasks: ItemTask[];
};

export type ItemTask = {
  id: string;
  task: string;
  text: RawDraftContentState;
  formatType: string;
  formatVersion: number;
  data: Record<string, any>;
  evaluation?: Record<string, any>;
  position?: number;
};

export type GetItemResponse = {
  item: Item;
};

export type GetItemsResponse = {
  items: Item[];
  itemCount: number;
};

export type GetItemVersionResponse = {
  itemVersion: ItemVersion;
};

export function useItem(
  id: string,
  options?: RequestOptions<GetItemResponse>
): Omit<ReturnType<typeof useRequest>, 'data'> & {
  item: Item | null;
} {
  const { data, ...others } = useRequest<GetItemResponse>(
    `${urls.browser}/v1/items/${id}`,
    options
  );
  return {
    item: data ? data.item : null,
    ...others,
  };
}

export async function getItem(
  ctx: GetServerSidePropsContext | null,
  id: string,
  options?: AxiosRequestConfig
): Promise<[Item, AxiosResponse<GetItemResponse>]> {
  const url = ctx === null ? urls.browser : urls.server;
  return makeApi(ctx)
    .get<GetItemResponse>(`${url}/v1/items/${id}`, options)
    .then((d) => [d.data.item, d]);
}

export type PutItemResponse = {
  itemId: string;
};

export async function putItem(
  ctx: GetServerSidePropsContext | null,
  id: string,
  data: Partial<Item>
): Promise<[string, AxiosResponse<PutItemResponse>]> {
  let url = urls.browser;
  if (ctx) {
    url = urls.server;
  }
  return makeApi(ctx)
    .put<PutItemResponse>(`${url}/v1/items/${id}`, data)
    .then((d) => [d.data.itemId, d]);
}

export function useItems(
  query: Query = {},
  pageSize = 20
): Omit<ReturnType<typeof useRequestInfinite>, 'data'> & {
  items: Item[] | null;
} {
  const url = `${urls.browser}/v1/items`;
  const selector = (data: GetItemsResponse) => data.items;
  const { data, ...others } = useRequestInfinite<GetItemsResponse, Item>(
    url,
    {
      query,
    },
    pageSize,
    selector
  );

  return {
    items: data,
    ...others,
  };
}

export async function getItems(
  ctx: GetServerSidePropsContext,
  query: Query,
  page = 0,
  pageSize = 20
): Promise<[Item[], AxiosResponse<GetItemsResponse>]> {
  const url = ctx === null ? urls.browser : urls.server;
  return makeApi(ctx)
    .get<GetItemsResponse>(`${url}/v1/items`, {
      params: {
        query: {
          offset: page * pageSize,
          limit: pageSize,
          ...query,
        },
      },
    })
    .then((d) => [d.data.items, d]);
}

export type PostItemsResponse = {
  itemId: string;
};

export async function postItems(
  ctx: GetServerSidePropsContext | null,
  data: Pick<Item, 'name' | 'privacy' | 'publicDescription' | 'language'>
): Promise<[string, AxiosResponse<PostItemsResponse>]> {
  const url = ctx === null ? urls.browser : urls.server;
  return makeApi(ctx)
    .post<PostItemsResponse>(`${url}/v1/items`, data)
    .then((d) => [d.data.itemId, d]);
}

export function useItemVersion(
  id: string,
  version: ItemVersion['version'] | 'latest' | 'draft',
  options: RequestOptions<GetItemVersionResponse>
): Omit<ReturnType<typeof useRequest>, 'data'> & {
  itemVersion: ItemVersion | null;
} {
  const { data, ...others } = useRequest<GetItemVersionResponse>(
    `${urls.browser}/v1/items/${id}/versions/${version}`,
    options
  );
  return {
    itemVersion: data ? data.itemVersion : null,
    ...others,
  };
}

export async function getItemVersion(
  ctx: GetServerSidePropsContext | null,
  id: string,
  version: ItemVersion['version'] | 'latest' | 'draft'
): Promise<[ItemVersion, AxiosResponse<GetItemVersionResponse>]> {
  const url = ctx === null ? urls.browser : urls.server;
  return makeApi(ctx)
    .get<GetItemVersionResponse>(`${url}/v1/items/${id}/versions/${version}`)
    .then((d) => [d.data.itemVersion, d]);
}

export type PostItemVersionResponse = {
  itemVersionId: string;
  version: number;
};

export async function postItemVersion(
  ctx: GetServerSidePropsContext | null,
  id: string,
  message: string
): Promise<[PostItemVersionResponse, AxiosResponse<PostItemVersionResponse>]> {
  const url = ctx === null ? urls.browser : urls.server;

  return makeApi(ctx)
    .post<PostItemVersionResponse>(`${url}/v1/items/${id}/versions`, {
      message,
    })
    .then((d) => [d.data, d]);
}

export type PostItemVersionsEvaluate = {
  results: Record<string, any>;
  correct: boolean;
};

export async function postItemVersionsEvaluate(
  ctx: GetServerSidePropsContext | null,
  tasks: ItemTask[],
  solutions: Record<number, any>
): Promise<
  [PostItemVersionsEvaluate, AxiosResponse<PostItemVersionsEvaluate>]
> {
  const url = ctx === null ? urls.browser : urls.server;
  return makeApi(ctx)
    .post<PostItemVersionsEvaluate>(
      `${url}/v1/items/versions/evaluate`,
      {
        tasks,
        solutions,
      },
      {
        timeout: 120000,
      }
    )
    .then((d) => [d.data, d]);
}

export type PutItemVersionResponse = Record<string, never>;

export async function putItemVersionResponse(
  ctx: GetServerSidePropsContext | null,
  id: string,
  data: ItemVersion
): Promise<[PutItemVersionResponse, AxiosResponse<PutItemVersionResponse>]> {
  let url = urls.browser;
  if (ctx) {
    url = urls.server;
  }

  return makeApi(ctx)
    .put<PutItemVersionResponse>(`${url}/v1/items/${id}/versions`, data)
    .then((d) => [d.data, d]);
}

export type PutItemVersionStatusResponse = Record<string, never>;

export async function putItemVersionStatus(
  ctx: GetServerSidePropsContext | null,
  id: string,
  version: ItemVersion['version'] | 'latest' | 'draft',
  status: ItemVersion['status']
): Promise<
  [PutItemVersionStatusResponse, AxiosResponse<PutItemVersionStatusResponse>]
> {
  let url = urls.browser;
  if (ctx) {
    url = urls.server;
  }

  return makeApi(ctx)
    .put<PutItemVersionStatusResponse>(
      `${url}/v1/items/${id}/versions/${version}/status`,
      {
        status,
      }
    )
    .then((d) => [d.data, d]);
}
