import { makeApi, Query } from '@utils/api';
import { RequestOptions, useRequest, useRequestInfinite } from '@utils/request';
import { GetServerSidePropsContext } from 'next';
import { urls } from '.';

export type Tag = {
  id: string;
  color: string;
  items: string[];
};

export type GetTagResponse = {
  category: Tag;
};

export type GetTagsResponse = {
  tags: Tag[];
};

export function useTag(id: string, options: RequestOptions<GetTagResponse>) {
  const { data, ...others } = useRequest<GetTagResponse>(
    `${urls.browser}/v1/tags/${id}`,
    options
  );
  return {
    category: data ? data.category : null,
    ...others,
  };
}

export async function getTag(
  ctx: GetServerSidePropsContext,
  id: string
): Promise<Tag> {
  return makeApi(ctx)
    .get<GetTagResponse>(`${urls.server}/v1/tags/${id}`)
    .then((d) => d.data.category);
}

export function useTags(
  query: Query = {},
  pageSize = 20
): Omit<ReturnType<typeof useRequestInfinite>, 'data'> & {
  tags: Tag[] | null;
} {
  const url = `${urls.browser}/v1/tags`;
  const selector = (data: GetTagsResponse) => data.tags;
  const { data, ...others } = useRequestInfinite<GetTagsResponse, Tag>(
    url,
    { query },
    pageSize,
    selector
  );
  return {
    tags: data,
    ...others,
  };
}

export async function getTags(
  ctx: GetServerSidePropsContext,
  query: Query = {},
  page = 0,
  pageSize = 20
): Promise<Tag[]> {
  return makeApi(ctx)
    .get<GetTagsResponse>(`${urls.server}/v1/tags`, {
      params: {
        query: {
          offset: page * pageSize,
          limit: pageSize,
          ...query,
        },
      },
    })
    .then((d) => d.data.tags);
}
