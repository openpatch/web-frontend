import { Member } from '@services/authentification/members';
import { makeApi, Query } from '@utils/api';
import { RequestOptions, useRequest, useRequestInfinite } from '@utils/request';
import { AxiosResponse } from 'axios';
import { RawDraftContentState } from 'draft-js';
import { GetServerSidePropsContext } from 'next';
import { urls } from '.';

export type Test = {
  id: string;
  authors: Member[];
  createdOn: string;
  language: string;
  member: Member;
  name: string;
  privacy: 'private' | 'public' | 'notlisted';
  publicDescription: RawDraftContentState;
  publicDescriptionText: string;
  versions: TestVersionBase[];
};

export type TestVersionBase = {
  id: string;
  version: number;
  versionMessage: string;
  status: 'faulty' | 'pilot' | 'draft' | 'ready';
  latest: boolean;
  member: Member;
  testId: string;
};

export type TestItem = {
  id: string;
  node: string;
  itemVersion: {
    itemId: string;
    version: number;
  };
  index: number;
};

export type TestNode = {
  id: string;
  x: number;
  y: number;
  name: string;
  index: number;
  testVersion: string;
  flow: 'linear' | 'jumpable';
  encrypted: boolean;
  needsToBeCorrect: boolean;
  randomized: boolean;
  timeLimit: number;
  itemLimit: number;
  scoreLimit: number;
  precisionLimit: number;
  start: boolean;
  end: boolean;
  items: TestItem[];
  edges: TestEdge[];
};

export type TestEdge = {
  id: string;
  threshold: number;
  nodeFrom: string;
  nodeTo: string;
};

export type TestVersion = TestVersionBase & {
  nodes: TestNode[];
};

export type GetTestResponse = {
  test: Test;
};

export function useTest(
  id: string,
  options?: RequestOptions<GetTestResponse>
): Omit<ReturnType<typeof useRequest>, 'data'> & {
  test: Test | null;
} {
  const { data, ...others } = useRequest<GetTestResponse>(
    `${urls.browser}/v1/tests/${id}`,
    options
  );
  return {
    test: data ? data.test : null,
    ...others,
  };
}

export async function getTest(
  ctx: GetServerSidePropsContext | null,
  id: string
): Promise<[Test, AxiosResponse<GetTestResponse>]> {
  let url = urls.browser;
  if (ctx) {
    url = urls.server;
  }
  return makeApi(ctx)
    .get<GetTestResponse>(`${url}/v1/tests/${id}`)
    .then((d) => [d.data.test, d]);
}

export function useTests(query: Query = {}, pageSize = 20) {
  const url = `${urls.browser}/v1/tests`;
  const selector = (data: GetTestsResponse) => data.tests;
  const { data, ...others } = useRequestInfinite<GetTestsResponse, Test>(
    url,
    {
      query,
    },
    pageSize,
    selector
  );

  return {
    tests: data,
    ...others,
  };
}

export type PutTestResponse = {
  testId: string;
};

export async function putTest(
  ctx: GetServerSidePropsContext | null,
  id: string,
  data: Partial<Test>
): Promise<[string, AxiosResponse<PutTestResponse>]> {
  let url = urls.browser;
  if (ctx) {
    url = urls.server;
  }
  return makeApi(ctx)
    .put<PutTestResponse>(`${url}/v1/tests/${id}`, data)
    .then((d) => [d.data.testId, d]);
}

export type PostTestResponse = {
  testId: string;
};

export async function postTest(
  ctx: GetServerSidePropsContext | null,
  data: Partial<Test>
): Promise<[string, AxiosResponse<PostTestResponse>]> {
  let url = urls.browser;
  if (ctx) {
    url = urls.server;
  }
  return makeApi(ctx)
    .post<PutTestResponse>(`${url}/v1/tests`, data)
    .then((d) => [d.data.testId, d]);
}

export type GetTestsResponse = {
  tests: Test[];
  testCount: number;
};

export async function getTests(
  ctx: GetServerSidePropsContext | null,
  query: Query,
  page = 0,
  pageSize = 20
): Promise<[Test[], AxiosResponse<GetTestsResponse>]> {
  let url = urls.browser;
  if (ctx) {
    url = urls.server;
  }
  return makeApi(ctx)
    .get<GetTestsResponse>(`${url}/v1/tests`, {
      params: {
        query: {
          offset: page * pageSize,
          limit: pageSize,
          ...query,
        },
      },
    })
    .then((d) => [d.data.tests, d]);
}

export type GetTestVersionResponse = {
  testVersion: TestVersion;
};

export function useTestVersion(
  id: string,
  version: TestVersionBase['version'] | 'latest' | 'draft',
  options?: RequestOptions<GetTestVersionResponse>
) {
  const {
    data,
    error,
    isValidating,
    revalidate,
  } = useRequest<GetTestVersionResponse>(
    `${urls.browser}/v1/tests/${id}/versions/${version}`,
    options
  );
  return {
    testVersion: data ? data.testVersion : null,
    error,
    isValidating,
    revalidate,
  };
}

export async function getTestVersion(
  ctx: GetServerSidePropsContext | null,
  id: string,
  version: TestVersionBase['version'] | 'latest' | 'draft'
): Promise<[TestVersion, AxiosResponse<GetTestVersionResponse>]> {
  let url = urls.browser;
  if (ctx) {
    url = urls.server;
  }
  return makeApi(ctx)
    .get<GetTestVersionResponse>(`${url}/v1/tests/${id}/versions/${version}`)
    .then((d) => [d.data.testVersion, d]);
}

export type PostTestVersionResponse = {
  testVersionId: string;
  version: number;
};

export async function postTestVersion(
  ctx: GetServerSidePropsContext | null,
  id: string,
  message: string
): Promise<[PostTestVersionResponse, AxiosResponse<PostTestVersionResponse>]> {
  let url = urls.browser;
  if (ctx) {
    url = urls.server;
  }

  return makeApi(ctx)
    .post<PostTestVersionResponse>(`${url}/v1/tests/${id}/versions`, {
      message,
    })
    .then((d) => [d.data, d]);
}

export type PutTestVersionResponse = Record<string, never>;

export async function putTestVersionResponse(
  ctx: GetServerSidePropsContext | null,
  id: string,
  data: TestVersion
): Promise<[PutTestVersionResponse, AxiosResponse<PutTestVersionResponse>]> {
  let url = urls.browser;
  if (ctx) {
    url = urls.server;
  }

  return makeApi(ctx)
    .put<PutTestVersionResponse>(`${url}/v1/tests/${id}/versions`, data)
    .then((d) => [d.data, d]);
}

export type PutTestVersionStatusResponse = Record<string, never>;

export async function putTestVersionStatus(
  ctx: GetServerSidePropsContext | null,
  id: string,
  version: TestVersionBase['version'] | 'latest' | 'draft',
  status: TestVersionBase['status']
): Promise<
  [PutTestVersionStatusResponse, AxiosResponse<PutTestVersionStatusResponse>]
> {
  let url = urls.browser;
  if (ctx) {
    url = urls.server;
  }

  return makeApi(ctx)
    .put<PutTestVersionStatusResponse>(
      `${url}/v1/tests/${id}/versions/${version}/status`,
      {
        status,
      }
    )
    .then((d) => [d.data, d]);
}

export type GetTestVersionsResponse = {
  testVersions: TestVersion[];
  testVersionsCount: number;
};

export async function getTestVersions(
  ctx: GetServerSidePropsContext | null,
  id: string
): Promise<[TestVersion[], AxiosResponse<GetTestVersionsResponse>]> {
  let url = urls.browser;
  if (ctx) {
    url = urls.server;
  }
  return makeApi(ctx)
    .get<GetTestVersionsResponse>(`${url}/v1/tests/${id}/versions`)
    .then((d) => [d.data.testVersions, d]);
}
