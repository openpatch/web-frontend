import { isApiError } from '@utils/api';

// see https://gitlab.com/openpatch/assessment-backend/blob/master/openpatch_assessment/api/v1/errors.py
export type ItembankApiError = {
  status: number;
  code: number;
  message?: string;
  details?: any;
};

class ApiError extends Error {
  status: number;
  code: number;
  details?: any;

  constructor({ status, code, message, details }: ItembankApiError) {
    super(message);
    this.status = status;
    this.code = code;
    this.details = details;
  }
}

export class AccessNotAllowError extends ApiError {}
export class ResourceNotFoundError extends ApiError {}
export class InvalidJsonError extends ApiError {}
export class NoJsonError extends ApiError {}
export class ResourceExistsError extends ApiError {}
export class FormatServiceDownError extends ApiError {}
export class ItemIsInUseError extends ApiError {}

const errors = {
  1: AccessNotAllowError,
  110: ResourceNotFoundError,
  200: InvalidJsonError,
  201: NoJsonError,
  202: ResourceExistsError,
  203: FormatServiceDownError,
  204: ItemIsInUseError,
} as const;

export function getApiError(e: any): Error {
  if (isApiError<ItembankApiError>(e)) {
    const code = e.response?.data.code;
    if (
      code !== undefined &&
      ((Object.keys(errors) as any) as number[]).includes(code)
    ) {
      const ApiError = errors[code as keyof typeof errors];
      const data = e.response?.data;
      if (!data) {
        return Error(e.message);
      }
      return new ApiError(data);
    }
  }

  return Error(e);
}

export function handleApiErrors(
  e: any,
  handlers: Partial<Record<keyof typeof errors, (e?: any) => void>>
) {
  if (isApiError<ItembankApiError>(e)) {
    const code = e.response?.data.code;
    if (
      code !== undefined &&
      ((Object.keys(errors) as any) as number[]).includes(code)
    ) {
      const handler = handlers[code as keyof typeof errors];
      if (handler) {
        handler(e);
      }
    }
  }
}
