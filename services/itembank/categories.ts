import { makeApi, Query } from '@utils/api';
import {
  RequestInfiniteOptions,
  RequestOptions,
  useRequest,
  useRequestInfinite,
} from '@utils/request';
import { AxiosResponse } from 'axios';
import { GetServerSidePropsContext } from 'next';
import { urls } from '.';

export type Category = {
  id: string;
  color: string;
  items: string[];
};

export type GetCategoryResponse = {
  category: Category;
};

export type GetCategoriesResponse = {
  categories: Category[];
};

export function useCategory(
  id: string,
  options: RequestOptions<GetCategoryResponse>
) {
  const { data, ...others } = useRequest<GetCategoryResponse>(
    `${urls.browser}/v1/categories/${id}`,
    options
  );
  return {
    category: data ? data.category : null,
    ...others,
  };
}

export async function getCategory(
  ctx: GetServerSidePropsContext | null,
  id: string
): Promise<[Category, AxiosResponse<GetCategoryResponse>]> {
  const url = ctx === null ? urls.browser : urls.server;
  return makeApi(ctx)
    .get<GetCategoryResponse>(`${url}/v1/categories/${id}`)
    .then((d) => [d.data.category, d]);
}

export type DeleteCategoryResponse = Record<string, never>;

export async function deleteCategory(
  ctx: GetServerSidePropsContext | null,
  id: string
) {
  const url = ctx === null ? urls.browser : urls.server;

  return makeApi(ctx)
    .delete<DeleteCategoryResponse>(`${url}/v1/categories/${id}`)
    .then((d) => [d.data, d]);
}

export function useCategories(
  query: Query = {},
  pageSize = 20,
  config?: RequestInfiniteOptions<GetCategoriesResponse>
): Omit<ReturnType<typeof useRequestInfinite>, 'data'> & {
  categories: Category[] | null;
} {
  const url = `${urls.browser}/v1/categories`;
  const selector = (data: GetCategoriesResponse) => data.categories;
  const { data, ...others } = useRequestInfinite<
    GetCategoriesResponse,
    Category
  >(url, { query }, pageSize, selector, config);
  return {
    categories: data,
    ...others,
  };
}

export async function getCategories(
  ctx: GetServerSidePropsContext | null,
  query: Query = {},
  page = 0,
  pageSize = 20
): Promise<[Category[], AxiosResponse<GetCategoriesResponse>]> {
  const url = ctx === null ? urls.browser : urls.server;
  return makeApi(ctx)
    .get<GetCategoriesResponse>(`${url}/v1/categories`, {
      params: {
        query: {
          offset: page * pageSize,
          limit: pageSize,
          ...query,
        },
      },
    })
    .then((d) => [d.data.categories, d]);
}

export type PostCategoriesResponse = {
  categoryId: string;
};

export async function postCategories(
  ctx: GetServerSidePropsContext | null,
  data: Partial<Category>
): Promise<[string, AxiosResponse<PostCategoriesResponse>]> {
  const url = ctx === null ? urls.browser : urls.server;

  return makeApi(ctx)
    .post<PostCategoriesResponse>(`${url}/v1/categories`, data)
    .then((d) => [d.data.categoryId, d]);
}
