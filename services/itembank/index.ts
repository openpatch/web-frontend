export const urls = {
  server: process.env.API_ITEMBANK_SERVER,
  browser: process.env.NEXT_API_ITEMBANK_BROWSER,
};
