import { Member } from '@services/authentification/members';
import { makeApi, Query } from '@utils/api';
import { RequestOptions, useRequest, useRequestInfinite } from '@utils/request';
import { AxiosResponse } from 'axios';
import { RawDraftContentState } from 'draft-js';
import { GetServerSidePropsContext } from 'next';
import { urls } from '.';
import { Item } from './items';
import { Test } from './tests';

export type Collection = {
  id: string;
  member: Member;
  name: string;
  publicDescription: RawDraftContentState;
  publicDescriptionText: string;
  tests: string[];
  items: string[];
  privacy: 'private' | 'public' | 'notlisted';
  createdOn: string;
};

export type GetCollectionResponse = {
  collection: Collection;
  items: Item[];
  tests: Test[];
};

export type GetCollectionsResponse = {
  collections: Collection[];
};

export function useCollection(
  id: string,
  options?: RequestOptions<GetCollectionResponse>
): Omit<ReturnType<typeof useRequest>, 'data'> & {
  collection: Collection | undefined;
  items: Item[];
  tests: Test[];
} {
  const { data, ...others } = useRequest<GetCollectionResponse>(
    `${urls.browser}/v1/categories/${id}`,
    options
  );
  return {
    collection: data?.collection,
    items: data?.items || [],
    tests: data?.tests || [],
    ...others,
  };
}

export async function getCollection(
  ctx: GetServerSidePropsContext,
  id: string
): Promise<
  [
    {
      collection: Collection;
      items: Item[];
      tests: Test[];
    },
    AxiosResponse<GetCollectionResponse>
  ]
> {
  return makeApi(ctx)
    .get<GetCollectionResponse>(`${urls.server}/v1/collections/${id}`)
    .then((d) => [d.data, d]);
}

export type PutCollectionResponse = Record<string, never>;

export async function putCollection(
  ctx: GetServerSidePropsContext | null,
  id: string,
  data: Partial<Collection>
): Promise<[PutCollectionResponse, AxiosResponse<PutCollectionResponse>]> {
  const url = ctx === null ? urls.browser : urls.server;
  return makeApi(ctx)
    .put<PutCollectionResponse>(`${url}/v1/collections/${id}`, data)
    .then((d) => [d.data, d]);
}

export type DeleteCollectionTypeResponse = Record<string, never>;

export async function deleteCollectionType(
  ctx: GetServerSidePropsContext | null,
  collectionId: string,
  type: "items" | "tests",
  id: string,
): Promise<
  [DeleteCollectionTypeResponse, AxiosResponse<DeleteCollectionTypeResponse>]
> {
  const url = ctx === null ? urls.browser : urls.server;
  return makeApi(ctx)
    .delete<DeleteCollectionTypeResponse>(`${url}/v1/collections/${collectionId}/${type}/${id}`)
    .then((d) => [d.data, d]);
}

export type PutCollectionTypeResponse = Record<string, never>;

export async function putCollectionType(
  ctx: GetServerSidePropsContext | null,
  collectionId: string,
  type: "items" | "tests",
  id: string,
): Promise<
  [PutCollectionTypeResponse, AxiosResponse<PutCollectionTypeResponse>]
> {
  const url = ctx === null ? urls.browser : urls.server;
  return makeApi(ctx)
    .put<PutCollectionTypeResponse>(`${url}/v1/collections/${collectionId}/${type}/${id}`)
    .then((d) => [d.data, d]);
}

export function useCollections(
  query: Query = {},
  pageSize = 20
): Omit<ReturnType<typeof useRequestInfinite>, 'data'> & {
  collections: Collection[] | null;
} {
  const url = `${urls.browser}/v1/collections`;
  const selector = (data: GetCollectionsResponse) => data.collections;
  const { data, ...others } = useRequestInfinite<
    GetCollectionsResponse,
    Collection
  >(url, { query }, pageSize, selector);
  return {
    collections: data,
    ...others,
  };
}

export async function getCollections(
  ctx: GetServerSidePropsContext,
  query: Query = {},
  page = 0,
  pageSize = 20
): Promise<[Collection[], AxiosResponse<GetCollectionsResponse>]> {
  return makeApi(ctx)
    .get<GetCollectionsResponse>(`${urls.server}/v1/collections`, {
      params: {
        query: {
          offset: page * pageSize,
          limit: pageSize,
          ...query,
        },
      },
    })
    .then((d) => [d.data.collections, d]);
}

export type PostCollectionsResponse = {
  colelctionId: string;
};

export async function postCollections(
  ctx: GetServerSidePropsContext | null,
  data: Partial<Collection>
): Promise<[string, AxiosResponse<PostCollectionsResponse>]> {
  const url = ctx === null ? urls.browser : urls.server;
  return makeApi(ctx)
    .post<PostCollectionsResponse>(`${url}/v1/collections`, data)
    .then((d) => [d.data.colelctionId, d]);
}
