import { Member } from '@services/authentification/members';
import api, { makeApi } from '@utils/api';
import { RequestOptions, useRequest } from '@utils/request';
import { AxiosResponse } from 'axios';
import { GetServerSidePropsContext } from 'next';
import { useEffect } from 'react';
import { urls } from '.';

export type Comment = {
  id: string;
  message: string;
  member: Member;
  approved: boolean;
  room: string;
  parentId: string;
  createdOn: string;
  extra?: Record<string, any>;
};

export type Room = {
  id: string;
  comments: Comment[];
  type: string;
  createdOn: string;
};

export type GetRoomResponse = {
  room: Room;
};

export function useRoom(
  id: string,
  type: string,
  options?: RequestOptions<GetRoomResponse>
) {
  const { data, ...others } = useRequest<GetRoomResponse>(
    `${urls.browser}/v1/rooms/${id}`,
    options
  );

  useEffect(() => {
    api
      .post(`${urls.browser}/v1/rooms`, {
        id,
        type,
      })
      .then(() => {
        others.revalidate({});
      })
      // eslint-disable-next-line @typescript-eslint/no-empty-function
      .catch(() => {});
  }, [id, type]);

  return { room: data ? data.room : null, ...others };
}

export async function getRoom(
  ctx: GetServerSidePropsContext | null,
  id: string
): Promise<Room> {
  const url = ctx === null ? urls.browser : urls.server;
  return makeApi(ctx)
    .get<GetRoomResponse>(`${url}/v1/rooms/${id}`)
    .then((d) => d.data.room);
}

export type PostRoomResponse = {
  commentId: string;
};

export async function postRoom(
  ctx: GetServerSidePropsContext | null,
  id: string,
  message: Comment['message'],
  extra?: Comment['extra']
): Promise<[string, AxiosResponse<PostRoomResponse>]> {
  const url = ctx === null ? urls.browser : urls.server;
  return makeApi(ctx)
    .post<PostRoomResponse>(`${url}/v1/rooms/${id}/comments`, {
      message,
      extra,
    })
    .then((d) => [d.data.commentId, d]);
}

export type PutRoomCommentResponse = Record<string, never>;

export async function putRoomComment(
  ctx: GetServerSidePropsContext | null,
  id: string,
  commentId: string,
  message: string
): Promise<[PutRoomCommentResponse, AxiosResponse<PutRoomCommentResponse>]> {
  const url = ctx === null ? urls.browser : urls.server;
  return makeApi(ctx)
    .put<PutRoomCommentResponse>(
      `${url}/v1/rooms/${id}/comments/${commentId}`,
      {
        message,
      }
    )
    .then((d) => [d.data, d]);
}

export type DeleteRoomCommentResponse = Record<string, never>;

export async function deleteRoomComment(
  ctx: GetServerSidePropsContext | null,
  id: string,
  commentId: string
): Promise<
  [DeleteRoomCommentResponse, AxiosResponse<DeleteRoomCommentResponse>]
> {
  const url = ctx === null ? urls.browser : urls.server;
  return makeApi(ctx)
    .delete<DeleteRoomCommentResponse>(
      `${url}/v1/rooms/${id}/comments/${commentId}`
    )
    .then((d) => [d.data, d]);
}
