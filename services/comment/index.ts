export const urls = {
  browser: process.env.NEXT_API_COMMENT_BROWSER,
  server: process.env.API_COMMENT_SERVER,
};
