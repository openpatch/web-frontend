import { useEffect, useRef, useState } from 'react';
import { Frame, postFrame, postRecordings } from './recordings';

export type RecordedFrame = Pick<
  Frame,
  'payload' | 'timestamp' | 'type' | 'itemId' | 'taskId'
>;

export function useRecorder(id: string | null) {
  const [isRecording, setIsRecording] = useState(false);
  const frameQueue = useRef<RecordedFrame[]>([]);

  useEffect(() => {
    if (id) {
      postRecordings(null, id)
        .then(() => {
          start();
        })
        .catch(() => {
          stop();
        });
    }
  }, [id]);

  function record(a: RecordedFrame) {
    if (isRecording) {
      frameQueue.current.push(a);
      processFrameQueue();
    }
  }

  function start() {
    setIsRecording(true);
  }

  function stop() {
    setIsRecording(false);
  }

  function processFrameQueue() {
    if (id && frameQueue.current.length > 0) {
      const frame = frameQueue.current.shift();
      postFrame(null, id, frame)
        .then(() => {
          processFrameQueue();
        })
        .catch(() => {
          if (frame) {
            frameQueue.current.unshift(frame);
          }
          setTimeout(processFrameQueue, 1000);
        });
    }
  }

  return { record, isRecording, start, stop };
}
