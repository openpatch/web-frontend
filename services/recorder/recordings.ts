import { makeApi } from '@utils/api';
import { RequestOptions, useRequest } from '@utils/request';
import { AxiosResponse } from 'axios';
import { GetServerSidePropsContext } from 'next';
import { urls } from '.';

export type Snapshot = {
  id: string;
  timestamp: number;
  state: any;
  recording: string;
};

export type Frame = {
  id: string;
  type: string;
  payload?: Record<string, any>;
  itemId?: number;
  taskId?: number;
  timestamp: number;
  recording: string;
};

export type Recording = {
  id: string;
  start: string;
  end: string;
  snapshots: Snapshot[];
  frames: Frame[];
};

export type GetRecordingResponse = {
  recording: Recording;
};

export function useRecording(
  id: string,
  options: RequestOptions<GetRecordingResponse>
): Omit<ReturnType<typeof useRequest>, 'data'> & {
  recording: Recording | null;
} {
  const { data, ...others } = useRequest<GetRecordingResponse>(
    `${urls.browser}/v1/recordings/${id}`,
    options
  );
  return {
    recording: data ? data.recording : null,
    ...others,
  };
}

export async function getRecording(
  ctx: GetServerSidePropsContext,
  id: string
): Promise<[Recording, AxiosResponse<GetRecordingResponse>]> {
  return makeApi(ctx)
    .get<GetRecordingResponse>(`${urls.server}/v1/recordings/${id}`)
    .then((d) => [d.data.recording, d]);
}

export type PostFrameResponse = Record<string, never>;

export async function postFrame(
  ctx: GetServerSidePropsContext | null,
  id: string,
  frame: any
) {
  let url = urls.browser;
  if (ctx) {
    url = urls.server;
  }
  return makeApi(ctx).post<PostFrameResponse>(
    `${url}/v1/recordings/${id}/frames`,
    frame
  );
}

export type PostRecordingsResponse = Record<string, never>;

export async function postRecordings(
  ctx: GetServerSidePropsContext | null,
  id: string
) {
  let url = urls.browser;
  if (ctx) {
    url = urls.server;
  }
  return makeApi(ctx).post<PostRecordingsResponse>(`${url}/v1/recordings`, {
    recordingId: id,
  });
}
