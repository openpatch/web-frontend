export type ColorRGB = {
  r: number;
  g: number;
  b: number;
};

const colorMappings: Record<string, ColorRGB> = {};

export function colorHash(s: string) {
  let color = colorMappings[s];
  if (!color) {
    const colorHash = hashString(s, 360) / 360.0;
    color = HSVtoRGB(colorHash, 0.2, 0.9);
    colorMappings[s] = color;
  }
  return `rgb(${color.r}, ${color.g}, ${color.b})`;
}

function hashString(s: string, n: number) {
  let hash = 0,
    i,
    chr;
  if (s.length === 0) return hash;
  for (i = 0; i < s.length; i++) {
    chr = s.charCodeAt(i);
    hash = (hash << 5) - hash + chr;
    hash |= 0; // Convert to 32bit integer
  }
  return Math.abs(hash % n);
}

function HSVtoRGB(h: number, s: number, v: number) {
  let r: number, g: number, b: number;
  const i = Math.floor(h * 6);
  const f = h * 6 - i;
  const p = v * (1 - s);
  const q = v * (1 - f * s);
  const t = v * (1 - (1 - f) * s);
  switch (i % 6) {
    case 0:
      (r = v), (g = t), (b = p);
      break;
    case 1:
      (r = q), (g = v), (b = p);
      break;
    case 2:
      (r = p), (g = v), (b = t);
      break;
    case 3:
      (r = p), (g = q), (b = v);
      break;
    case 4:
      (r = t), (g = p), (b = v);
      break;
    case 5:
      (r = v), (g = p), (b = q);
      break;
    default:
      (r = 0), (b = 0), (g = 0);
  }
  return {
    r: Math.round(r * 255),
    g: Math.round(g * 255),
    b: Math.round(b * 255),
  };
}
