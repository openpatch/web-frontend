export const urls = {
  browser: process.env.NEXT_API_RECORDER_BROWSER,
  server: process.env.API_RECORDER_SERVER,
};
