import PropTypes from 'prop-types';
import { FieldArray } from 'react-final-form-arrays';
import arrayMutators from 'final-form-arrays';

import Form from './Form';
import EvaluationFieldArray from './fields/EvaluationFieldArray';

function ItemVersionEvaluation({ tasks }) {
  return (
    tasks && (
      <Form
        initialValues={{ tasks }}
        mutators={{ ...arrayMutators }}
        submitVariant="hidden"
        render={() => (
          <FieldArray name="tasks" component={EvaluationFieldArray} />
        )}
      />
    )
  );
}

ItemVersionEvaluation.propTypes = {
  tasks: PropTypes.array,
};

export default ItemVersionEvaluation;
