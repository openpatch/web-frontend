import { number } from '@storybook/addon-knobs';
import ItemMetaView from './ItemMetaView';

export default {
  title: 'ItemMetaView',
  component: 'ItemMetaView',
};

export const Default = () => (
  <ItemMetaView
    id="some-id"
    member={{
      username: 'test-user',
    }}
    createdOn={new Date().toISOString()}
    name="Test Item"
    categories={['computer-science']}
    usedInTests={number('usedInTests', 0)}
    language="de"
  />
);
