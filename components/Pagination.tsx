import { Trans } from '@lingui/macro';
import Box from '@material-ui/core/Box';
import Grid, { GridProps } from '@material-ui/core/Grid';
import { QueryFilter, QuerySortDirection } from '@utils/api';
import { AxiosResponse } from 'axios';
import { ComponentType, ReactNode } from 'react';
import { Selector, useRequestInfinite } from '../utils/request';
import NoEntries from './NoEntries';
import ProgressButton from './ProgressButton';

export type PaginationProps<R, T> = {
  pageSize?: number;
  filter?: QueryFilter;
  order?: QuerySortDirection;
  orderBy?: string;
  url: string;
  selector: Selector<R, T>;
  Renderer: ComponentType<T>;
  initialData?: AxiosResponse<R>[];
  RendererProps?: Record<string, any>;
  variant?: 'list' | 'grid';
  spacing?: GridProps['spacing'];
  placeholder?: ReactNode;
};

export type PaginationData = {
  id: string | number;
};

function Pagination<R, T extends PaginationData>({
  pageSize = 20,
  filter,
  order = 'desc',
  orderBy = 'createdOn',
  url,
  selector,
  Renderer,
  initialData,
  RendererProps,
  variant = 'list',
  spacing = 3,
  placeholder = <NoEntries />,
}: PaginationProps<R, T>) {
  const {
    data,
    size,
    setSize,
    isLoadingMore,
    isReachingEnd,
    isEmpty,
  } = useRequestInfinite<R, T>(
    url,
    {
      query: {
        sort: {
          [orderBy]: order,
        },
        filter,
      },
    },
    pageSize,
    selector,
    {
      initialData,
    }
  );

  return (
    <Box display="flex" flexDirection="column">
      {!isEmpty ? (
        <>
          <Grid
            container
            spacing={spacing}
            justify="center"
            alignItems="stretch"
          >
            {variant === 'grid'
              ? data?.map((d: any) => (
                  <Grid item key={d.id} xs={12} lg={6}>
                    <Renderer
                      style={{ height: '100%' }}
                      {...d}
                      {...RendererProps}
                    />
                  </Grid>
                ))
              : data?.map((d: any) => (
                  <Grid item key={d.id} xs={12}>
                    <Renderer
                      style={{ height: '100%' }}
                      {...d}
                      {...RendererProps}
                    />
                  </Grid>
                ))}
          </Grid>
          <Box
            marginTop={4}
            display="flex"
            flexDirection="column"
            alignItems="center"
          >
            <ProgressButton
              onClick={() => setSize(size + 1)}
              disabled={isReachingEnd || isLoadingMore}
              loading={isLoadingMore}
            >
              {isLoadingMore ? (
                '. . .'
              ) : isReachingEnd ? (
                <Trans>no more data</Trans>
              ) : (
                <Trans>load more</Trans>
              )}
            </ProgressButton>
          </Box>
        </>
      ) : (
        placeholder
      )}
    </Box>
  );
}

export default Pagination;
