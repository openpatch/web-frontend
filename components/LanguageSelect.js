import { useRouter } from 'next/router';
import { Trans } from '@lingui/macro';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

import { languages } from '../utils/i18n';

function LanguageSelect(props) {
  const router = useRouter();

  function handleLanguage(e) {
    let locale = e.target.value;

    if (!Object.keys(languages).includes(locale)) {
      locale = 'en';
    }
    router.push(router.pathname, router.asPath, { locale });
  }

  return (
    <Select
      labelId="language-select"
      onChange={handleLanguage}
      value={router.locale}
      variant="outlined"
      {...props}
    >
      {Object.keys(languages).map((lang) => (
        <MenuItem key={lang} value={lang}>
          <Trans id={languages[lang]} />
        </MenuItem>
      ))}
    </Select>
  );
}

export default LanguageSelect;
