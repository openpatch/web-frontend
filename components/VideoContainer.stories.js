import VideoContainer from './VideoContainer';

export default {
  title: 'VideoContainer',
  component: VideoContainer,
};

export const Default = () => (
  <VideoContainer
    title="Interactive Tasks"
    description="Higlight, Puzzles and Coding to name a few. Use interactive tasks in your assessments."
    src="https://f002.backblazeb2.com/file/openpatch-static/interactive.mp4"
    maxWidth={400}
  />
);
