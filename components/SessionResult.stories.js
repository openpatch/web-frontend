import { boolean, number } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import SessionResult from './SessionResult';

export default {
  title: 'SessionResult',
  component: SessionResult
};

const data = {
  aborted: null,
  itemResults: [
    {
      assessmentId: 'b0424fb2-d584-46b6-bd7d-a48fd768503e',
      correct: true,
      created_on: '2020-03-21T07:42:06',
      encrypted: false,
      end: '2020-03-21T07:42:06.857259',
      errorEvalution: null,
      evaluation: { correct: true },
      id: 'b03d7b12-c417-4292-a04b-bbad7c92fc58',
      itemId: '9f2dbe2c-a18b-4b18-961d-6d564f4b530e',
      itemVersion: 1,
      nodeResult: 'e1761abf-95f5-4213-b86f-7f1c16a06919',
      position: 0,
      recorded: null,
      solution: {
        '0': {
          choices: { '0': true }
        }
      },
      solutionEncrypted: null,
      start: '2020-03-21T07:42:06.830084',
      testItemId: '3b2f479c-a215-4ba1-8846-8994ee21da54',
      updatedOn: '2020-03-21T07:42:06'
    },
    {
      assessmentId: 'b0424fb2-d584-46b6-bd7d-a48fd768503e',
      correct: false,
      createdOn: '2020-03-21T07:42:06',
      encrypted: false,
      end: '2020-03-21T07:42:06.884726',
      errorEvalution: null,
      evaluation: { correct: false },
      id: 'b683da76-32b9-4f07-8f47-cf8925517f00',
      itemId: 'e2e833ff-ddab-40ff-98f8-da84948c3830',
      itemVersion: 1,
      nodeResult: 'e1761abf-95f5-4213-b86f-7f1c16a06919',
      position: 1,
      recorded: null,
      solution: {
        '0': {
          choices: { '0': true }
        }
      },
      solutionEncrypted: null,
      start: '2020-03-21T07:42:06.869869',
      testItemId: '72a489a7-b186-47c2-98ef-ce6eb10b8b16',
      updatedOn: '2020-03-21T07:42:06'
    },
    {
      assessmentId: 'b0424fb2-d584-46b6-bd7d-a48fd768503e',
      correct: true,
      createdOn: '2020-03-21T07:42:06',
      encrypted: false,
      end: '2020-03-21T07:42:06.911889',
      errorEvalution: null,
      evaluation: { correct: true },
      id: '4d80452c-7478-42c6-96d9-0401a87afbde',
      itemId: 'ca302a12-e277-4ae7-b8ee-7ae4f5ffe8b6',
      itemVersion: 1,
      nodeResult: 'e1761abf-95f5-4213-b86f-7f1c16a06919',
      position: 2,
      recorded: null,
      solution: {
        '0': {
          choices: { '0': true }
        }
      },
      solutionEncrypted: null,
      start: '2020-03-21T07:42:06.896689',
      testItemId: '8cfc4cd8-e6a7-458e-9dfe-1ac5d6161b29',
      updatedOn: '2020-03-21T07:42:06'
    },
    {
      assessmentId: 'b0424fb2-d584-46b6-bd7d-a48fd768503e',
      correct: true,
      createdOn: '2020-03-21T07:42:06',
      encrypted: true,
      end: '2020-03-21T07:42:06.911889',
      errorEvalution: null,
      evaluation: { correct: true },
      id: '4d80652c-7478-42c6-96d9-0401a87afbde',
      itemId: 'ca302a12-e277-4ae7-b8ee-7ae4f5ffe8b6',
      itemVersion: 1,
      nodeResult: 'e1761abf-95f5-4213-b86f-7f1c16a06919',
      position: 2,
      recorded: null,
      solution: null,
      solutionEncrypted: null,
      start: '2020-03-21T07:42:06.896689',
      testItemId: '8cfc4cd8-e6a7-458e-9dfe-1ac5d6161b29',
      updatedOn: '2020-03-21T07:42:06'
    }
  ],
  itemVersions: [
    {
      createdOn: '2020-03-21T07:42:06',
      item: '9f2dbe2c-a18b-4b18-961d-6d564f4b530e',
      status: 'latest',
      tasks: [
        {
          data: { choices: ['1', '2', '3'] },
          evaluation: { choices: { '1': true } },
          formatType: 'choice',
          formatVersion: 1,
          task: 'Number One'
        }
      ],
      updatedOn: '2020-03-21T07:42:06',
      version: 1
    },
    {
      createdOn: '2020-03-21T07:42:06',
      item: 'e2e833ff-ddab-40ff-98f8-da84948c3830',
      status: 'pilot',
      tasks: [
        {
          data: { choices: ['1', '2', '3'] },
          evaluation: { choices: { '2': true } },
          formatType: 'choice',
          formatVersion: 1,
          task: 'Number Two'
        }
      ],
      updatedOn: '2020-03-21T07:42:06',
      version: 1
    },
    {
      createdOn: '2020-03-21T07:42:06',
      item: 'ca302a12-e277-4ae7-b8ee-7ae4f5ffe8b6',
      status: 'pilot',
      tasks: [
        {
          data: { choices: ['1', '2', '3'] },
          evaluation: { choices: { '3': true } },
          formatType: 'choice',
          formatVersion: 1,
          task: 'Number Three'
        }
      ],
      updatedOn: '2020-03-21T07:42:06',
      version: 1
    }
  ],
  score: 2
};

export const Default = () => (
  <SessionResult
    score={number('score', 3)}
    aborted={boolean('aborted')}
    onShow={action('show')}
    itemVersions={data.itemVersions}
    itemResults={data.itemResults}
  />
);
