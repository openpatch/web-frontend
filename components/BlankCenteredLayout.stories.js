import BlankCenteredLayout from './BlankCenteredLayout';

export default {
  title: 'Layout/BlankCenteredLayout',
  component: BlankCenteredLayout
};

export const Plain = () => <BlankCenteredLayout />;

export const WithDiv = () => (
  <BlankCenteredLayout>
    <div>Hi! I should be centered</div>
  </BlankCenteredLayout>
);
