import { action } from '@storybook/addon-actions';
import ConfirmButton from './ConfirmButton';

export default {
  title: 'Button/ConfirmButton',
  component: ConfirmButton
};

export const Default = () => (
  <ConfirmButton
    onOK={action('ok')}
    onCancel={action('cancel')}
    title="This is a title"
    text="This is a text"
  >
    ConfirmButton
  </ConfirmButton>
);
