import AppBarLayout from './AppBarLayout';

export default {
  title: 'Layout/AppBarLayout',
  component: AppBarLayout
};

export const Default = () => <AppBarLayout />;

export const WithBreadcrumbs = () => (
  <AppBarLayout
    breadcrumbs={[
      {
        href: 'hi',
        title: 'hi'
      },
      {
        href: 'hu',
        title: 'hu',
        active: true
      }
    ]}
  />
);

export const Inverted = () => <AppBarLayout inverted />;

export const WithEnhancedAppBar = () => (
  <AppBarLayout left="left" right="right" center="center" title="title" />
);
