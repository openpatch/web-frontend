import { action } from '@storybook/addon-actions';
import AssessmentForm from './AssessmentForm';

export default {
  title: 'AssessmentForm',
  component: AssessmentForm
};

export const WithValues = () => (
  <AssessmentForm
    onSubmit={action('submit')}
    initialValues={{
      name: 'An Assessment',
      public: true
    }}
  />
);
