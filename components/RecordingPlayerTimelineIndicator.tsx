import Box from '@material-ui/core/Box';
import { useTheme } from '@material-ui/core/styles';

export type RecordingPlayerTimelineIndicatorProps = {
  currentTime: number;
  scale: number;
};

function RecordingPlayerTimelineIndicator({
  currentTime,
  scale,
}: RecordingPlayerTimelineIndicatorProps) {
  const theme = useTheme();
  return (
    <Box
      zIndex={99}
      width={4}
      height={20}
      position="absolute"
      bgcolor={theme.palette.primary.dark}
      left={currentTime / scale - 2}
    ></Box>
  );
}

export default RecordingPlayerTimelineIndicator;
