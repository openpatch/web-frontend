import { boolean } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import SignUpForm from './SignUpForm';

export default {
  title: 'SignUpForm',
  component: SignUpForm,
};

export const Default = () => (
  <SignUpForm loading={boolean('loading')} onSignUp={action('sign up')} />
);

export const CheckEmail = () => (
  <SignUpForm checkMail onSignUp={action('sign up')} />
);

export const InvitationCode = () => (
  <SignUpForm invitationCode="hallo-invited" onSignUp={action('sign up')} />
);
