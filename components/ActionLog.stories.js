import ActionLog from './ActionLog';

export default {
  title: 'ActionLog',
  component: ActionLog
};

export const SomeActions = () => (
  <ActionLog
    actions={[
      {
        type: 'ACTION_TYPE',
        timestamp: 100,
        payload: {
          a: 'payload'
        }
      },
      {
        type: 'ANOTHER_ACTION_TYPE',
        timestamp: 400,
        payload: {
          a: 'payload',
          b: 'some stuff',
          c: {
            d: 'see'
          }
        }
      }
    ]}
  />
);
