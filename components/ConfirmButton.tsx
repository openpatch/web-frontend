import { Trans } from '@lingui/macro';
import Button, { ButtonProps } from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { ReactNode, useState } from 'react';

export type ConfirmButtonProps = ButtonProps & {
  onOK: () => void;
  onCancel: () => void;
  title: string;
  children: ReactNode;
  text: string;
};

function ConfirmButton({
  onOK,
  onCancel,
  title,
  children,
  text,
  ...buttonProps
}: ConfirmButtonProps) {
  const [open, setOpen] = useState(false);
  function handleClose() {
    setOpen(false);
    onCancel();
  }

  function handleOpen() {
    setOpen(true);
  }

  function handleOK() {
    setOpen(false);
    onOK();
  }
  return (
    <>
      <Button {...buttonProps} onClick={handleOpen}>
        {children}
      </Button>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>{title}</DialogTitle>
        <DialogContent>
          <DialogContentText>{text}</DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            <Trans>Cancel</Trans>
          </Button>
          <Button onClick={handleOK} color="primary">
            <Trans>OK</Trans>
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}

export default ConfirmButton;
