import { useState } from 'react';
import { Trans } from '@lingui/macro';
import PropTypes from 'prop-types';
import Link from 'next/link';
import Box from '@material-ui/core/Box';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';

import Logo from '../components/Logo';

function LogInForm({ onLogin }) {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  function handleLogin(e) {
    e.preventDefault();
    onLogin(username, password);
  }

  return (
    <Box width={400}>
      <Box mb={2} display="flex" alignItems="center" justifyContent="center">
        <Logo size="large" />
      </Box>
      <Card component="form" onSubmit={handleLogin}>
        <CardContent>
          <TextField
            label={<Trans>Username or E-Mail</Trans>}
            variant="outlined"
            margin="dense"
            fullWidth
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
          <TextField
            label={<Trans>Password</Trans>}
            variant="outlined"
            margin="dense"
            fullWidth
            type="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </CardContent>
        <CardActions>
          <Button type="submit" onClick={handleLogin} color="primary" fullWidth>
            <Trans>Log In</Trans>
          </Button>
          <Link href="/request-password">
            <Button color="primary" fullWidth>
              <Trans>Forgot password?</Trans>
            </Button>
          </Link>
        </CardActions>
      </Card>
      <Box mt={2}>
        <Link href="/sign-up">
          <Typography
            style={{ color: 'white', paddingTop: 4, cursor: 'pointer' }}
            align="center"
            as="a"
          >
            <Trans>Do not have an account? Sign Up</Trans>
          </Typography>
        </Link>
      </Box>
    </Box>
  );
}

LogInForm.propTypes = {
  onLogin: PropTypes.func.isRequired,
};

export default LogInForm;
