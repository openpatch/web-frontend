import { action } from '@storybook/addon-actions';
import ItemVersionForm from './ItemVersionForm';

export default {
  title: 'ItemVersionForm',
  component: ItemVersionForm,
};

export const Default = () => <ItemVersionForm onSubmit={action('submit')} />;
