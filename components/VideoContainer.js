import PropTypes from 'prop-types';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles((theme) => ({
  videoBox: {
    boxShadow: theme.shadows[20],
    borderRadius: theme.shape.borderRadius,
  },
  center: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
}));

const patterns = [
  'static/images/featurepattern01.png',
  'static/images/featurepattern02.png',
  'static/images/featurepattern03.png',
];

let patternIndex = 0;

const Pattern = () => (
  <div
    style={{
      position: 'absolute',
      backgroundImage: `url(${patterns[patternIndex++ % patterns.length]})`,
      zIndex: -1,
      top: -20,
      left: -20,
      right: -20,
      bottom: -20,
    }}
  />
);

function VideoContainer({
  src,
  title,
  description,
  width,
  height,
  maxWidth,
  maxHeight,
  reverse,
}) {
  const classes = useStyles();
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.down('xs'));

  const descriptionBox = (
    <Grid item xs={12} sm={6} className={classes.center}>
      <div>
        <Typography variant="h4" component="h2">
          {title}
        </Typography>
        <Typography>{description}</Typography>
      </div>
    </Grid>
  );

  const videoBox = (
    <Grid
      item
      xs={12}
      sm={6}
      style={{ position: 'relative' }}
      className={classes.center}
    >
      <video
        loop
        className={classes.videoBox}
        autoPlay
        height={height}
        width={width}
        style={{
          maxWidth,
          maxHeight,
        }}
      >
        <source src={src} />
      </video>
      <Pattern />
    </Grid>
  );

  return (
    <Grid container spacing={8}>
      {reverse && !matches ? (
        <>
          {videoBox}
          {descriptionBox}
        </>
      ) : (
        <>
          {descriptionBox}
          {videoBox}
        </>
      )}
    </Grid>
  );
}

VideoContainer.propTypes = {
  src: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  maxWidth: PropTypes.number,
  maxHeight: PropTypes.number,
  reverse: PropTypes.bool,
};

VideoContainer.defaultProps = {
  width: '100%',
  height: 'auto',
  maxWidth: null,
  maxHeight: null,
};

export default VideoContainer;
