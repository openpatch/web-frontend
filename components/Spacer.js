import Typography from '@material-ui/core/Typography';

function Spacer(props) {
  return (
    <Typography
      variant="caption"
      style={{ marginRight: 4, marginLeft: 4 }}
      {...props}
    >
      •
    </Typography>
  );
}

export default Spacer;
