import { t, Trans } from '@lingui/macro';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { useClaims } from '@services/authentification/provider';
import { deleteRoomComment, postRoom, useRoom } from '@services/comment/rooms';
import { useState } from 'react';
import { useNotifications } from '../utils/notification';
import Comment from './Comment';
import MemberAvatar from './MemberAvatar';

export type CommentsProps = {
  id: string;
  type: string;
};

function Comments({ id, type }: CommentsProps) {
  const claims = useClaims();
  const { room, revalidate } = useRoom(id, type);
  const [comment, setComment] = useState('');
  const [hasError, setHasError] = useState(false);
  const [isPosting, setIsPosting] = useState(false);
  const [add] = useNotifications();

  function handleCommentChange(e: any) {
    if (e.target.value.trim()) {
      setHasError(false);
    } else {
      setHasError(true);
    }
    setComment(e.target.value);
  }

  function sendComment() {
    if (!hasError) {
      setIsPosting(true);
      postRoom(null, id, comment.trim())
        .then(() => {
          setIsPosting(false);
          add({
            message: t`Comment saved`,
            severity: 'success',
          });
          setComment('');
          revalidate({});
        })
        .catch(() => {
          setHasError(true);
          setIsPosting(false);
          add({
            message: t`Your comment could not be saved`,
            severity: 'error',
          });
        });
    }
  }

  function deleteComment(commentId: string) {
    deleteRoomComment(null, id, commentId)
      .then(() => {
        revalidate({});
        add({
          message: t`Comment deleted`,
          severity: 'success',
        });
      })
      .catch(() => {
        add({
          message: t`Something went wrong`,
          severity: 'error',
        });
      });
  }

  return (
    <Box>
      <Box
        display="flex"
        alignItems="start"
        justifyContent="center"
        flexWrap="wrap"
      >
        <MemberAvatar {...claims} />
        <TextField
          style={{ marginLeft: 16, marginRight: 16, flex: 1, minWidth: 200 }}
          multiline
          placeholder={t`Comment`}
          value={comment}
          helperText={
            hasError ? <Trans>No empty comment allowed!</Trans> : null
          }
          error={hasError}
          onChange={handleCommentChange}
        />
        <Button
          disabled={isPosting || hasError || comment === ''}
          onClick={sendComment}
        >
          <Trans>Comment</Trans>
        </Button>
      </Box>
      {room?.comments &&
        room?.comments.map((comment) => (
          <Comment
            {...comment}
            key={comment.id}
            onDelete={() => deleteComment(comment.id)}
          />
        ))}
    </Box>
  );
}

export default Comments;
