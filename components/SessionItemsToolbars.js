import { useState } from 'react';
import { Trans } from '@lingui/macro';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Videocam from '@material-ui/icons/Videocam';
import Security from '@material-ui/icons/Security';
import Tooltip from '@material-ui/core/Tooltip';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

export function SessionItemsToolbarLeft({ onAbort }) {
  const [open, setOpen] = useState(false);
  const [message, setMessage] = useState('');

  function handleOpen() {
    setOpen(true);
  }

  function handleClose() {
    setOpen(false);
  }

  function handleAbort() {
    onAbort(message);
  }

  return (
    <>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>
          <Trans>Abort</Trans>
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            <Trans>
              Are you sure you want to abort the assessment? If so, you can
              leave a message on why you want to abort. This helps improving the
              assessment.
            </Trans>
          </DialogContentText>
          <TextField
            label="Message"
            fullWidth
            value={message}
            onChange={(e) => setMessage(e.target.value)}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>
            <Trans>Continue with the Assessment</Trans>
          </Button>
          <Button onClick={handleAbort}>
            <Trans>Abort</Trans>
          </Button>
        </DialogActions>
      </Dialog>
      <Button onClick={handleOpen}>
        <Trans>Abort</Trans>
      </Button>
    </>
  );
}

SessionItemsToolbarLeft.propTypes = {
  onAbort: PropTypes.func,
};

export function SessionItemsToolbarCenter({ session, encrypt, record }) {
  return (
    <Typography
      style={{
        display: 'flex',
        alignItems: 'center',
      }}
    >
      {session}{' '}
      {encrypt && (
        <Tooltip title="Encrypted">
          <Security fontSize="inherit" />
        </Tooltip>
      )}{' '}
      {record && (
        <Tooltip title="Recording">
          <Videocam fontSize="inherit" />
        </Tooltip>
      )}
    </Typography>
  );
}

SessionItemsToolbarCenter.propTypes = {
  session: PropTypes.string,
  encrypt: PropTypes.bool,
  record: PropTypes.bool,
};

export function SessionItemsToolbarRight({ onSubmit, ...props }) {
  return (
    <Button onClick={onSubmit} {...props}>
      <Trans>Submit</Trans>
    </Button>
  );
}

SessionItemsToolbarRight.propTypes = {
  onSubmit: PropTypes.func,
};
