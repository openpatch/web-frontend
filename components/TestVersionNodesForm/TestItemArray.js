import { memo } from 'react';
import PropTypes from 'prop-types';

import TestItem from './TestItem';

function TestItemArray({ fields, onView }) {
  return fields.map((field, index) => {
    const { id } = fields.value[index];
    return (
      <TestItem
        key={id}
        index={index}
        field={field}
        onView={onView}
        value={fields.value[index]}
        remove={() => fields.remove(index)}
      />
    );
  });
}

TestItemArray.propTypes = {
  fields: PropTypes.shape({
    remove: PropTypes.func,
    map: PropTypes.func,
  }),
  onView: PropTypes.func,
};

export default memo(TestItemArray);
