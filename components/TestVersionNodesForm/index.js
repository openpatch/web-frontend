import { FieldArray } from 'react-final-form-arrays';

import TestNodeArray from './TestNodeArray';

function TestVersionNodes() {
  return <FieldArray name="nodes" component={TestNodeArray} />;
}

export default TestVersionNodes;
