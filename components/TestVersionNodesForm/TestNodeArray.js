import { useState, useCallback } from 'react';
import { t } from '@lingui/macro';
import PropTypes from 'prop-types';
import { useForm } from 'react-final-form';
import { DragDropContext, Droppable } from 'react-beautiful-dnd';
import { styled } from '@material-ui/core/styles';
import AddIcon from '@material-ui/icons/Add';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import Box from '@material-ui/core/Box';

import TestNode from './TestNode';
import ItemResultDialog from '../ItemResultDialog';
import uuid4 from '../../utils/uuid';
import { useItemVersion } from '../../services/itembank/items';

const Container = styled(Box)({
  display: 'inline-flex',
});

function TestNodes({ fields, onView }) {
  return fields.map((field, index) => {
    const { id } = fields.value[index];
    return (
      <TestNode
        key={id}
        field={field}
        index={index}
        remove={() => fields.remove(index)}
        onView={onView}
      />
    );
  });
}

function TestNodesArray({ fields }) {
  const {
    getFieldState,
    mutators: { insert, remove, move },
  } = useForm();
  const [viewOpen, setViewOpen] = useState(false);
  const [itemVersion, setItemVersion] = useState(null);
  const { itemVersion: itemVersionData } = useItemVersion(
    itemVersion?.item,
    itemVersion?.version,
    {
      revalidateOnFocus: false,
      revalidateOnReconnect: false,
    }
  );

  function handleDragEnd(result) {
    if (!result.destination) {
      return;
    }

    const { source, destination, type } = result;

    if (
      source.droppableId === destination.droppableId &&
      source.index === destination.index
    ) {
      return;
    }

    if (type === 'NODE') {
      fields.move(source.index, destination.index);
    } else if (type === 'ITEM') {
      if (source.droppableId !== destination.droppableId) {
        const sourceField = `${source.droppableId}.items`;
        const destField = `${destination.droppableId}.items`;

        const nodeField = `${sourceField}[${source.index}]`;
        const node = getFieldState(nodeField);
        console.log(nodeField, node);

        remove(sourceField, source.index);
        insert(destField, destination.index, node.value);
      } else {
        const field = `${destination.droppableId}.items`;
        move(field, source.index, destination.index);
      }
    }
  }

  function add() {
    fields.push({
      id: uuid4(),
      name: '',
      items: [],
    });
  }

  function handleViewClose() {
    setViewOpen(false);
    setItemVersion(null);
  }

  const handleView = useCallback(
    (item, version) => {
      setItemVersion({
        item,
        version,
      });
      setViewOpen(true);
    },
    [setItemVersion, setViewOpen]
  );

  return (
    <>
      <ItemResultDialog
        open={viewOpen}
        onClose={handleViewClose}
        itemVersion={itemVersionData}
      />
      <DragDropContext onDragEnd={handleDragEnd}>
        <Droppable droppableId="nodes" type="NODE" direction="horizontal">
          {(provided) => (
            <Container ref={provided.innerRef} {...provided.droppableProps}>
              <TestNodes fields={fields} onView={handleView} />
              {provided.placeholder}
              <Tooltip title={t`Add a new Node`}>
                <IconButton onClick={add} style={{ alignSelf: 'flex-start' }}>
                  <AddIcon />
                </IconButton>
              </Tooltip>
            </Container>
          )}
        </Droppable>
      </DragDropContext>
    </>
  );
}

TestNodesArray.propTypes = {
  fields: PropTypes.shape({
    move: PropTypes.func,
    push: PropTypes.func,
    map: PropTypes.func,
    remove: PropTypes.func,
  }).isRequired,
};

export default TestNodesArray;
