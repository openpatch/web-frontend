import { useState, memo } from 'react';
import { Trans, t } from '@lingui/macro';
import PropTypes from 'prop-types';
import { Field, useForm, useField } from 'react-final-form';
import { FieldArray } from 'react-final-form-arrays';
import { Draggable, Droppable } from 'react-beautiful-dnd';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import { styled, useTheme } from '@material-ui/core/styles';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import _isEqual from 'deep-equal';

import TestItemArray from './TestItemArray';
import AddItemDialog from './AddItemDialog';
import Checkbox from '../fields/Checkbox';
import TextField from '../fields/TextField';

import uuid4 from '../../utils/uuid';

const NodeContainer = styled(Card)(({ theme }) => ({
  width: 240,
  margin: theme.spacing(1),
  display: 'flex',
  flexDirection: 'column',
}));

const DragHeader = styled(
  /* eslint-disable no-unused-vars, @typescript-eslint/no-unused-vars */ ({
    isDragging,
    ...others
  }) => <CardHeader {...others} />
)({
  backgroundColor: (props) => (props.isDragging ? '#EEEEEE' : null),
  color: (props) => (props.isDragging ? '#000' : null),
});

function TestNode({ field, index, remove, onView }) {
  const {
    input: {
      value: { id },
    },
  } = useField(field);
  const theme = useTheme();
  const [addItemDialogOpen, setAddItemDialogOpen] = useState(false);

  const {
    mutators: { push },
  } = useForm();

  function showAddItemDialog() {
    setAddItemDialogOpen(true);
  }

  function hideAddItemDialog() {
    setAddItemDialogOpen(false);
  }

  function handleAddItems(items) {
    items.forEach((item) => {
      const latest = item.versions.find((v) => v.latest);
      push(`${field}.items`, {
        id: uuid4(),
        itemVersion: {
          itemId: item.id,
          version: latest ? latest.version : 1,
        },
      });
    });
  }

  return (
    <Draggable key={id} draggableId={id} index={index}>
      {(provided, snapshot) => (
        <NodeContainer ref={provided.innerRef} {...provided.draggableProps}>
          <DragHeader
            title={
              <Field
                name={`${field}.name`}
                label={t`Name`}
                component={TextField}
              />
            }
            action={
              <Tooltip title={t`Remove`}>
                <IconButton onClick={remove}>
                  <DeleteIcon />
                </IconButton>
              </Tooltip>
            }
            isDragging={snapshot.isDragging}
            {...provided.dragHandleProps}
          />
          <CardContent>
            <Field
              name={`${field}.flow`}
              label={t`Flow`}
              component={TextField}
              select
              fullWidth
            >
              <MenuItem value="linear">
                <Trans>Linear</Trans>
              </MenuItem>
              <MenuItem value="jumpable">
                <Trans>Jumpable</Trans>
              </MenuItem>
            </Field>
            <Field
              name={`${field}.start`}
              label={t`Start`}
              component={Checkbox}
              fullWidth
              type="checkbox"
            />
            <Field
              name={`${field}.encrypted`}
              label={t`Encrypt`}
              component={Checkbox}
              type="checkbox"
              fullWidth
            />
          </CardContent>
          <Droppable droppableId={field} type="ITEM">
            {(dropProvided, dropSnapshot) => (
              <CardContent
                ref={dropProvided.innerRef}
                {...dropProvided.droppableProps}
                style={{
                  backgroundColor: dropSnapshot.isDraggingOver
                    ? theme.palette.background.default
                    : null,
                  flex: 1,
                }}
              >
                <FieldArray
                  name={`${field}.items`}
                  component={TestItemArray}
                  onView={onView}
                />
                {dropProvided.placeholder}
              </CardContent>
            )}
          </Droppable>
          <CardActions>
            <Button fullWidth onClick={showAddItemDialog}>
              {t`Add Item`}
            </Button>
          </CardActions>
          <AddItemDialog
            onClose={hideAddItemDialog}
            add={handleAddItems}
            open={addItemDialogOpen}
          />
        </NodeContainer>
      )}
    </Draggable>
  );
}

TestNode.propTypes = {
  onView: PropTypes.func.isRequired,
  field: PropTypes.string.isRequired,
  index: PropTypes.number.isRequired,
  remove: PropTypes.func.isRequired,
};

export default memo(TestNode, _isEqual);
