import { memo } from 'react';
import PropTypes from 'prop-types';
import { Draggable } from 'react-beautiful-dnd';
import { useField } from 'react-final-form';
import { styled } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import Typography from '@material-ui/core/Typography';
import _isEqual from 'deep-equal';

import { useItem } from '@services/itembank/items';
import TextField from '../fields/TextField';

const DragHeader = styled((
  /* eslint-disable no-unused-vars, @typescript-eslint/no-unused-vars */ {
    isDragging,
    ...others
  }
) => <CardHeader {...others} />)({
  backgroundColor: (props) => {
    return props.isDragging ? '#EEEEEE' : null;
  },
  color: (props) => (props.isDragging ? '#000' : null),
});

const ItemField = ({ name, itemVersions }) => {
  // Cannot update a component (`ItemField`) while rendering a different component (`ItemField`).
  // see https://github.com/final-form/react-final-form/issues/850
  const {
    input: { value, onChange },
    meta,
  } = useField(name);

  const {
    itemVersion: { version },
  } = value;

  function handleChange(e) {
    const value = e.currentTarget.value;
    onChange({
      ...value,
      itemVersion: {
        ...value.itemVersion,
        version: value,
      },
    });
  }
  return itemVersions ? (
    <TextField
      input={{
        value: version,
        onChange: handleChange,
      }}
      meta={meta}
      fullWidth
      select
      variant="standard"
      size="small"
    >
      {itemVersions.map(({ status, version, versionMessage }) => (
        <MenuItem key={version} value={version}>
          <Box display="flex" flexDirection="column">
            <Typography variant="overline">
              v{version} ({status})
            </Typography>
            <Typography variant="caption">{versionMessage}</Typography>
          </Box>
        </MenuItem>
      ))}
    </TextField>
  ) : null;
};

ItemField.propTypes = {
  name: PropTypes.string,
  itemVersions: PropTypes.array,
};

ItemField.defaultProps = {
  itemVersions: null,
};

function TestItem({ field, index, remove, onView, value }) {
  const {
    id,
    itemVersion: { itemId, version },
  } = value;

  const { item } = useItem(itemId, {
    revalidateOnReconnect: false,
    revalidateOnFocus: false,
  });

  function handleView() {
    onView(itemId, version);
  }

  return (
    <Draggable key={id} draggableId={id} index={index}>
      {(provided, snapshot) => (
        <Box pb={2} ref={provided.innerRef} {...provided.draggableProps}>
          <Card variant="outlined">
            <DragHeader
              title={item?.name}
              isDragging={snapshot.isDragging}
              titleTypographyProps={{
                style: { fontSize: '0.9rem' },
              }}
              subheaderTypographyProps={{
                style: { fontSize: '0.8rem' },
              }}
              style={{
                paddingBottom: 0,
              }}
              {...provided.dragHandleProps}
            />
            <CardContent
              style={{
                paddingTop: 0,
                paddingBottom: 0,
              }}
            >
              <ItemField itemVersions={item?.versions} name={`${field}`} />
            </CardContent>
            <CardActions>
              <Button onClick={handleView} size="small">
                View
              </Button>
              <Button onClick={remove} size="small">
                Delete
              </Button>
            </CardActions>
          </Card>
        </Box>
      )}
    </Draggable>
  );
}

TestItem.propTypes = {
  field: PropTypes.string,
  index: PropTypes.number,
  remove: PropTypes.func,
  onView: PropTypes.func,
  value: PropTypes.shape({
    id: PropTypes.string,
    itemVersion: PropTypes.shape({
      itemId: PropTypes.string,
      version: PropTypes.number,
    }),
  }),
};

export default memo(TestItem, _isEqual);
