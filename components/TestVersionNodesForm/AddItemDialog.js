import { useState, Fragment, useEffect } from 'react';
import { Trans, t } from '@lingui/macro';
import PropTypes from 'prop-types';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import AddIcon from '@material-ui/icons/Add';
import IconButton from '@material-ui/core/IconButton';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';

import ProgressButton from '../ProgressButton';
import TextField from '../fields/TextField';
import { useItems } from '@services/itembank/items';

function ItemList({ search, add }) {
  const { items, setSize, isReachingEnd, isLoadingMore, revalidate } = useItems(
    {
      filter: {
        name: {
          like: `%${search}%`,
        },
      },
    }
  );

  useEffect(() => {
    revalidate();
  }, [search]);

  console.log(isReachingEnd);

  return (
    <Fragment>
      <List>
        {items?.map((d) => (
          <ListItem key={d.id}>
            <ListItemText primary={d?.name} />
            <ListItemSecondaryAction>
              <IconButton onClick={() => add([d])} edge="end" aria-label="add">
                <AddIcon />
              </IconButton>
            </ListItemSecondaryAction>
          </ListItem>
        ))}
      </List>
      <ProgressButton
        variant="text"
        onClick={() => setSize((s) => s + 1)}
        disabled={isReachingEnd || isLoadingMore}
        loading={isLoadingMore}
        fullWidth
      >
        {isLoadingMore ? (
          '. . .'
        ) : isReachingEnd ? (
          <Trans>no more data</Trans>
        ) : (
          <Trans>load more</Trans>
        )}
      </ProgressButton>
    </Fragment>
  );
}

ItemList.propTypes = {
  search: PropTypes.string,
  add: PropTypes.func,
};

function AddItemDialog({ add, onClose, open }) {
  const [search, setSearch] = useState('');

  const handleSearch = (e) => {
    const value = e.currentTarget.value;
    setSearch(value);
  };

  return (
    <Dialog onClose={onClose} open={open}>
      <DialogTitle>
        <Trans>Add Items</Trans>
      </DialogTitle>
      <DialogContent>
        <TextField
          meta={{}}
          input={{
            value: search,
            onChange: handleSearch,
          }}
          label={t`Search`}
        />
      </DialogContent>
      <DialogContent dividers>
        <ItemList add={add} search={search} />
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} color="primary">
          Close
        </Button>
      </DialogActions>
    </Dialog>
  );
}

AddItemDialog.propTypes = {
  add: PropTypes.func,
  onClose: PropTypes.func,
  open: PropTypes.bool,
};

export default AddItemDialog;
