import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    paddingLeft: theme.spacing(2),
    flex: 2,
  },
}));

const FormSectionContent = (props) => {
  const classes = useStyles();
  return <div className={classes.container} {...props} />;
};

export default FormSectionContent;
