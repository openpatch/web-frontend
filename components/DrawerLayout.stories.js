import { boolean } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import DashboardIcon from '@material-ui/icons/Dashboard';

import DrawerLayout, { DrawerLayoutView } from './DrawerLayout';

export default {
  title: 'Layout/DrawerLayoutView',
  component: DrawerLayoutView,
};

export const Default = () => (
  <DrawerLayoutView
    onDrawerToggle={action('drawer layout toggle')}
    open={boolean('open')}
  />
);

export const WithLinks = () => (
  <DrawerLayoutView
    onDrawerToggle={action('drawer layout toggle')}
    open={boolean('open', true)}
    mainLinks={[
      {
        icon: <DashboardIcon />,
        text: 'Dashboard',
        onClick: action('on dashboard'),
      },
    ]}
    bottomLinks={[
      {
        text: 'Contact',
        onClick: action('on contact'),
      },
    ]}
  />
);

export const Real = () => <DrawerLayout />;
