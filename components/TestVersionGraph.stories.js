import TestVersionGraph from './TestVersionGraph';
import 'vis-network/styles/vis-network.css';

export default {
  title: 'TestVersionGraph',
  component: TestVersionGraph
};

export const Default = () => (
  <TestVersionGraph
    nodes={[
      {
        id: 'some-id',
        name: 'Hallo',
        edges: [
          {
            nodeTo: 'some-id',
            nodeFrom: 'some-other-id',
            threshold: 4
          }
        ]
      },
      {
        id: 'some-other-id',
        name: 'Du'
      },
      {
        id: 'another-id',
        name: 'Ba',
        start: true,
        edges: [
          {
            nodeTo: 'some-other-id',
            nodeFrom: 'another-id',
            threshold: 0
          }
        ]
      }
    ]}
  />
);

export const WithPath = () => (
  <TestVersionGraph
    path={[
      {
        from: 'some-id',
        to: 'some-other-id',
        label: `Step: 1`
      },
      {
        to: 'another-id',
        from: 'some-other-id',
        label: `Step: 2`
      }
    ]}
    nodes={[
      {
        id: 'some-id',
        name: 'Hallo',
        edges: [
          {
            nodeTo: 'some-id',
            nodeFrom: 'some-other-id',
            threshold: 4
          }
        ]
      },
      {
        id: 'some-other-id',
        name: 'Du'
      },
      {
        id: 'another-id',
        name: 'Ba',
        start: true,
        edges: [
          {
            nodeTo: 'some-other-id',
            nodeFrom: 'another-id',
            threshold: 0
          }
        ]
      }
    ]}
  />
);
