import Logo from './Logo';

export default {
  component: Logo,
  title: 'Logo'
};

export const logo = () => <Logo />;

export const largeLogo = () => <Logo size="large" />;
