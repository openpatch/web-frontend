import { Trans } from '@lingui/macro';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import BlankCanvas from 'mui-undraw/lib/BlankCanvas';

function NoEntriesCreate() {
  return (
    <Box display="flex" flexDirection="column" alignItems="center">
      <BlankCanvas style={{ maxWidth: 600, marginBottom: 8 }} />
      <Typography variant="subtitle1" align="center">
        <Trans>
          You have not created something until now. Hit the plus button in the
          bottom right corner to start your own creation.
        </Trans>
      </Typography>
    </Box>
  );
}

NoEntriesCreate.propTypes = {};

export default NoEntriesCreate;
