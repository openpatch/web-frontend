import PropTypes from 'prop-types';
import { Trans } from '@lingui/macro';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import BookIcon from '@material-ui/icons/Book';
import AssignmentIcon from '@material-ui/icons/Assignment';
import PeopleIcon from '@material-ui/icons/People';
import BallotIcon from '@material-ui/icons/Ballot';

const Stat = ({ count, title, Icon }) => (
  <Grid item xs>
    <Card evaluation={20}>
      <CardContent
        style={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <Icon
          color="primary"
          fontSize="large"
          style={{
            marginTop: 8,
            marginBottom: 8,
          }}
        />
        <Typography variant="h5" component="span">
          {count}
        </Typography>
        <Typography variant="subtitle1" component="span">
          {title}
        </Typography>
      </CardContent>
    </Card>
  </Grid>
);

Stat.propTypes = {
  count: PropTypes.number,
  title: PropTypes.string,
  Icon: PropTypes.object,
};

function Stats({ items, tests, members }) {
  return (
    <Grid container spacing={3}>
      <Stat count={items} title={<Trans>Items</Trans>} Icon={AssignmentIcon} />
      <Stat count={13} title={<Trans>Formats</Trans>} Icon={BallotIcon} />
      <Stat count={tests} title={<Trans>Tests</Trans>} Icon={BookIcon} />
      <Stat count={members} title={<Trans>Members</Trans>} Icon={PeopleIcon} />
    </Grid>
  );
}

Stats.propTypes = {
  items: PropTypes.number,
  tests: PropTypes.number,
  members: PropTypes.number,
};

export default Stats;
