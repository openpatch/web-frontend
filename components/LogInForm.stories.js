import { action } from '@storybook/addon-actions';
import LogInForm from './LogInForm';

export default {
  title: 'LogInForm',
  component: LogInForm
};

export const Default = () => <LogInForm onLogin={action('login')} />;
