import { useState } from 'react';
import { Trans, t } from '@lingui/macro';
import PropTypes from 'prop-types';
import arrayMutators from 'final-form-arrays';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Divider from '@material-ui/core/Divider';

import { uuidv4 } from '../utils/uuid';
import Joyride from './Joyride';
import Form from './Form';
import TestVersionNodesForm from './TestVersionNodesForm';
import TestVersionGraphForm from './TestVersionGraphForm';
import TabPanel from './TabPanel';

const steps = [
  {
    target: '#nodes',
    content: t`Here you can add nodes to your test. A node is a container for your items.`,
  },
  {
    target: '#graph',
    content: t`Here you can determine how a test plays out. You can connect nodes and set thresholds on the connection between them.`,
  },
];

function TestVersionForm({ onSubmit, initialValues, ...props }) {
  const [tab, setTab] = useState(0);

  if (initialValues?.nodes?.length === 0) {
    const node1 = uuidv4();
    const node2 = uuidv4();
    const node3 = uuidv4();
    const node4 = uuidv4();
    initialValues.nodes = [
      {
        id: node1,
        name: t`Start`,
        flow: 'linear',
        start: true,
        items: [],
        edges: [
          {
            nodeFrom: node1,
            nodeTo: node2,
            threshold: 0,
          },
          {
            nodeFrom: node1,
            nodeTo: node3,
            threshold: 2,
          },
        ],
      },
      {
        id: node2,
        flow: 'linear',
        name: t`Split 1`,
        items: [],
        edges: [
          {
            nodeFrom: node2,
            nodeTo: node4,
            threshold: 0,
          },
        ],
      },
      {
        id: node3,
        flow: 'linear',
        name: t`Split 2`,
        items: [],
        edges: [
          {
            nodeFrom: node3,
            nodeTo: node4,
            threshold: 0,
          },
        ],
      },
      {
        id: node4,
        flow: 'linear',
        encrypted: true,
        name: t`End`,
        items: [],
        edges: [],
      },
    ];
  }
  return (
    <>
      <Joyride steps={steps} name="TestVersionForm" />
      <Tabs
        value={tab}
        onChange={(e, newValue) => setTab(newValue)}
        indicatorColor="primary"
        textColor="primary"
        variant="scrollable"
        scrollButtons="auto"
      >
        <Tab id="nodes" label={<Trans>Nodes</Trans>} />
        <Tab id="graph" label={<Trans>Graph</Trans>} />
      </Tabs>
      <Divider />
      <Form
        {...props}
        onSubmit={onSubmit}
        initialValues={initialValues}
        subscription={{
          submitting: true,
          pristine: true,
        }}
        mutators={{ ...arrayMutators }}
        render={() => (
          <>
            <TabPanel style={{ overflow: 'auto' }} value={tab} index={0}>
              <TestVersionNodesForm />
            </TabPanel>
            <TabPanel value={tab} index={1}>
              <TestVersionGraphForm />
            </TabPanel>
          </>
        )}
      />
    </>
  );
}

TestVersionForm.propTypes = {
  initialValues: PropTypes.object,
  onSubmit: PropTypes.func,
};

export default TestVersionForm;
