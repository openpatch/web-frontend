import Button, { ButtonProps } from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import { green } from '@material-ui/core/colors';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import { useEffect, useRef } from 'react';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    alignItems: 'center',
  },
  wrapper: {
    margin: theme.spacing(1),
    position: 'relative',
  },
  buttonSuccess: {
    backgroundColor: green[500],
    '&:hover': {
      backgroundColor: green[700],
    },
  },
  buttonProgress: {
    color: green[500],
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
}));

export type ProgressButtonProps = ButtonProps & {
  loading?: boolean;
  success?: boolean;
};

export default function ProgressButton({
  loading = false,
  success = false,
  children = null,
  fullWidth = false,
  color = 'primary',
  variant = 'contained',
  ...props
}: ProgressButtonProps) {
  const classes = useStyles();
  const timer = useRef();

  const buttonClassname = clsx({
    [classes.buttonSuccess]: success,
  });

  useEffect(() => {
    return () => {
      clearTimeout(timer.current);
    };
  }, []);

  return (
    <div className={classes.root}>
      <div
        className={classes.wrapper}
        style={{
          width: fullWidth ? '100%' : undefined,
        }}
      >
        <Button
          variant={variant}
          color={color}
          className={buttonClassname}
          disabled={loading}
          fullWidth={fullWidth}
          {...props}
        >
          {children}
        </Button>
        {loading && (
          <CircularProgress size={24} className={classes.buttonProgress} />
        )}
      </div>
    </div>
  );
}
