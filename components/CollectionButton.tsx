import { useState } from 'react';
import { t, Trans } from '@lingui/macro';
import Box from '@material-ui/core/Box';
import Tooltip from '@material-ui/core/Tooltip';
import Button, { ButtonProps } from '@material-ui/core/Button';
import PlaylistAddIcon from '@material-ui/icons/PlaylistAdd';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import Checkbox from '@material-ui/core/Checkbox';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';

import { useNotifications } from '@utils/notification';
import PrivacySelectField from './fields/PrivacySelectField';
import PrivacyIndicator from './PrivacyIndicator';
import { useClaims } from '@services/authentification/provider';
import {
  Collection,
  deleteCollectionType,
  postCollections,
  putCollectionType,
  useCollections,
} from '@services/itembank/collections';
import { QueryFilter } from '@utils/api';

export type CollectionButtonViewProps = {
  collections: Collection[];
  inCollections: string[];
  onToggle: (id: string) => void;
  onCreate: (name: string) => void;
} & ButtonProps;

export function CollectionButtonView({
  collections,
  inCollections,
  onToggle,
  onCreate,
  ...props
}: CollectionButtonViewProps) {
  const [open, setOpen] = useState(false);
  const [name, setName] = useState('');
  const [privacy, setPrivacy] = useState('private');

  return (
    <>
      <Dialog
        open={open}
        onClose={() => setOpen(false)}
        fullWidth
        maxWidth="xs"
      >
        <DialogTitle>
          <Trans>Save in...</Trans>
        </DialogTitle>
        <DialogContent dividers>
          <FormControl component="fieldset" fullWidth>
            <FormGroup>
              {collections &&
                collections.map(({ name, id, privacy }) => (
                  <FormControlLabel
                    key={id}
                    control={
                      <Checkbox
                        checked={inCollections.includes(id)}
                        onChange={() => onToggle(id)}
                        name={id}
                      />
                    }
                    label={
                      <Box display="flex" alignItems="center">
                        <Box flex={1} mr={1}>
                          {name}
                        </Box>
                        <PrivacyIndicator privacy={privacy} />
                      </Box>
                    }
                  />
                ))}
            </FormGroup>
          </FormControl>
        </DialogContent>
        <Box p={2}>
          <Typography>
            <Trans>Create Collection</Trans>
          </Typography>
          <TextField
            fullWidth
            label={<Trans>Name</Trans>}
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
          <PrivacySelectField
            fullWidth
            variant="standard"
            meta={{}}
            input={{
              name: 'privacy',
              value: privacy,
              onChange: (e) => setPrivacy(e.target.value),
              onBlur: () => null,
              onFocus: () => null,
            }}
          />
        </Box>
        <DialogActions>
          <Button onClick={() => onCreate(name)}>
            <Trans>Create</Trans>
          </Button>
          <Button onClick={() => setOpen(false)}>
            <Trans>Close</Trans>
          </Button>
        </DialogActions>
      </Dialog>
      <Tooltip title={<Trans>Save in Collection</Trans>}>
        <Button
          startIcon={<PlaylistAddIcon />}
          onClick={() => setOpen(true)}
          {...props}
        >
          <Trans>Save</Trans>
        </Button>
      </Tooltip>
    </>
  );
}

export type CollectionButtonProps = {
  collections: string[];
  inCollection: string[];
  type: 'tests' | 'items';
  id: string;
  onToggle: () => void;
};

function CollectionButton({
  collections: inCollections,
  type,
  id,
  onToggle,
}: CollectionButtonProps) {
  const [add] = useNotifications();
  const claims = useClaims();
  const filter: QueryFilter = {
    memberId: {
      equals: claims?.id || '',
    },
  };
  const { collections, revalidate } = useCollections({
    filter,
  });

  function handleToggle(collectionId: string) {
    if (inCollections.includes(collectionId)) {
      deleteCollectionType(null, collectionId, type, id)
        .then(() => {
          add({
            message: t`Removed from Collection`,
            severity: 'success',
          });
          onToggle();
        })
        .catch(() => {
          add({
            message: t`Something went wrong`,
            severity: 'error',
          });
        });
    } else {
      putCollectionType(null, collectionId, type, id)
        .then(() => {
          add({
            message: t`Saved in Collection`,
            severity: 'success',
          });
          onToggle();
        })
        .catch(() => {
          add({
            message: t`Something went wrong`,
            severity: 'error',
          });
        });
    }
  }

  function handleCreate(name: string) {
    postCollections(null, {
      name,
      items: type === 'items' ? [id] : undefined,
      tests: type === 'tests' ? [id] : undefined,
    })
      .then(([collectionId]) => {
        return collectionId;
      })
      .then(() => {
        add({
          message: t`Saved in new Collection`,
          severity: 'success',
        });
        revalidate({});
        onToggle();
      })
      .catch(() => {
        add({
          message: t`Something went wrong`,
          severity: 'error',
        });
      });
  }

  return (
    claims &&
    collections && (
      <CollectionButtonView
        collections={collections}
        inCollections={inCollections}
        onToggle={handleToggle}
        onCreate={handleCreate}
      />
    )
  );
}

export default CollectionButton;
