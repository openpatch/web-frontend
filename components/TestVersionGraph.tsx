import { t } from '@lingui/macro';
import { useTheme } from '@material-ui/core/styles';
import { useEffect, useRef, useState } from 'react';
import { Edge, Network, Node, Options } from 'vis-network';

export type TestVersionGraph = {
  nodes?: {
    name: string;
    id: string;
    items: any[];
    x: number;
    y: number;
    start?: boolean;
    edges: {
      id: string;
      nodeFrom: string;
      nodeTo: string;
      threshold: number;
    }[];
  }[];
  path?: Edge[];
};

function TestVersionGraph({ nodes: tvNodes = [], path }: TestVersionGraph) {
  const theme = useTheme();
  const network = useRef<Network | null>(null);
  const container = useRef<HTMLDivElement>(null);
  const [edges, setEdges] = useState<Edge[]>([]);
  const [nodes, setNodes] = useState<Node[]>([]);

  useEffect(() => {
    create();
  }, [nodes, edges]);

  useEffect(() => {
    const tmpNodes: Node[] = [];
    const tmpEdges: Edge[] = [];
    tvNodes.forEach((n) => {
      tmpNodes.push({
        label: n.name,
        id: n.id,
        value: n.items?.length,
        x: n.x,
        y: n.y,
        color: {
          background: n.start ? 'green' : undefined,
        },
      });
      if (n.edges) {
        n.edges.forEach((e) => {
          tmpEdges.push({
            id: e.id,
            from: e.nodeFrom,
            to: e.nodeTo,
            label: e.threshold == 0 ? t`default` : `>= ${e.threshold}`,
          });
        });
      }
    });
    setNodes(tmpNodes);

    if (path) {
      setEdges(path);
    } else {
      setEdges(tmpEdges);
    }
  }, [tvNodes]);

  function create() {
    const defaultOptions: Options = {
      physics: {
        enabled: false,
        stabilization: false,
      },
      autoResize: true,
      interaction: {
        navigationButtons: true,
      },
      layout: {
        randomSeed: 1,
      },
      manipulation: {
        enabled: false,
      },
      nodes: {
        scaling: {
          min: 1,
          max: 20,
        },
        shape: 'box',
        color: {
          border: theme.palette.primary.dark,
          background: theme.palette.primary.main,
          highlight: {
            border: theme.palette.primary.main,
            background: theme.palette.primary.light,
          },
        },
        font: {
          face: 'Roboto',
          color: theme.palette.primary.contrastText,
        },
      },
      edges: {
        smooth: {
          enabled: true,
          type: 'dynamic',
          forceDirection: 'none',
          roundness: 0.5,
        },
        color: theme.palette.primary.dark,
        font: {
          face: 'Roboto',
          color: theme.palette.primary.dark,
        },
        width: 0.5,
        arrows: {
          to: {
            enabled: true,
            scaleFactor: 0.5,
          },
        },
      },
    };
    network.current?.destroy();
    if (container.current) {
      network.current = new Network(
        container.current,
        {
          nodes,
          edges,
        },
        defaultOptions
      );
    }
  }

  return (
    <div
      ref={container}
      style={{
        height: 400,
      }}
    />
  );
}

export default TestVersionGraph;
