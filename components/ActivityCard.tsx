import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import { Trans } from '@lingui/macro';

import Link from './Link';
import MemberAvatar from './MemberAvatar';
import { useDateUtils } from '@utils/date';
import { ResourceType } from '@services/activity/resources';
import { Activity } from '@services/activity/activities';
import { useClaims } from '@services/authentification/provider';

/*
    create = 1
    update = 2
    delete = 3
    follow = 4
    add = 5
    remove = 6
    like = 7
    undo = 8
    block = 9
    */

/*
type item,test,member
 */

export function TypeLabel({ type }: {type: Activity["type"]}) {
  switch (type) {
    case 'like':
      return <Trans>liked</Trans>;
    case 'follow':
      return <Trans>followed</Trans>;
    case 'create':
      return <Trans>created</Trans>;
    case 'update':
      return <Trans>updated</Trans>;
    case 'create:version':
      return <Trans>version created</Trans>;
    case 'update:version':
      return <Trans>version update</Trans>;
    default:
      return null;
  }
}

export type ResourceLinkProps = {
  type: ResourceType;
  data: any;
  id: string;
}

export function ResourceLink({ type, data, id }: ResourceLinkProps) {
  let label = '';
  switch (type) {
    case 'item':
    case 'test':
    case 'assessment':
      label = data?.name;
      break;
    case 'member':
      label = data?.username;
      break;
  }
  return (
    <Link href={`/${type}/[id]`} as={`/${type}/${id}`}>
      {label}
    </Link>
  );
}

export type ActivityCardProps = Pick<Activity, "createdOn" | "member" | "resource" | "type">;

function ActivityCard({ createdOn, member, resource, type }: Activity) {
  const { formatDistanceStrict, utcDate } = useDateUtils();
  const claims = useClaims();
  let distance = null;
  try {
    distance = formatDistanceStrict(new Date(createdOn), utcDate());
  } catch (e) {
    distance = null;
  }

  let you = false;
  if (claims?.username === member?.username) {
    you = true;
  }

  return (
    <Card>
      <CardHeader
        avatar={
          <MemberAvatar
            id={member?.id}
            avatarId={member?.avatarId}
            username={member?.username}
          />
        }
        title={
          <>
            <Link href={`/member/[id]`} as={`/member/${member?.id}`}>
              {you ? <Trans>You</Trans> : member?.username}
            </Link>{' '}
            <TypeLabel type={type} /> <ResourceLink {...resource} />
          </>
        }
        subheader={<Trans>{distance} ago</Trans>}
      />
    </Card>
  );
}

ActivityCard.propTypes = {
  createdOn: PropTypes.string,
  member: PropTypes.shape({
    id: PropTypes.string,
    data: PropTypes.object,
  }),
  type: PropTypes.string,
  resource: PropTypes.shape({
    id: PropTypes.string,
    data: PropTypes.object,
  }),
};

export default ActivityCard;
