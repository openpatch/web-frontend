import {
  default as Editor,
  EditorPlugin,
  PluginEditorProps,
} from '@draft-js-plugins/editor';
import { makeStyles } from '@material-ui/core/styles';
import { plugins } from '@utils/editor/index';
import { DraftStyleMap } from 'draft-js';
import { forwardRef, memo, useEffect, useState } from 'react';

export type RichTextEditorProps = {
  customStyleMap?: DraftStyleMap;
  customPlugins?: EditorPlugin[];
  forwardRef?: any;
} & PluginEditorProps;

export const useStyles = makeStyles((theme) => ({
  editor: {
    fontSize: 16,
    '& blockquote': {
      background: theme.palette.background.paper,
      borderLeft: '10px solid #ccc',
      margin: '1.5em 10px',
      padding: '0.5em 10px',
      quotes: '"\\201C""\\201D""\\2018""\\2019"',
    },
    '& .public-DraftStyleDefault-pre': {
      background: theme.palette.background.paper,
      fontSize: '0.8rem',
      padding: 8,
      borderRadius: theme.shape.borderRadius,
      marginTop: 4,
      marginBottom: 4,
    },
  },
}));

function RichTextEditor({
  customStyleMap,
  customPlugins = [],
  forwardRef,
  ...props
}: RichTextEditorProps) {
  const classes = useStyles();
  // draft does make a ssr csr mismatch, therefore we are not
  // rendering it on the server
  const [show, setShow] = useState(false);

  useEffect(() => {
    setShow(true);
  }, []);

  return (
    <div className={classes.editor}>
      {show && (
        <Editor
          {...props}
          customStyleMap={customStyleMap}
          plugins={[...plugins, ...customPlugins]}
          stripPastedStyles={true}
          ref={forwardRef}
        />
      )}
    </div>
  );
}

export default memo<RichTextEditorProps>(
  forwardRef<Editor, RichTextEditorProps>((props, ref) => (
    <RichTextEditor {...props} forwardRef={ref} />
  )),
  (prev, next) =>
    JSON.stringify(prev.customStyleMap) ==
      JSON.stringify(next.customStyleMap) &&
    prev.editorState == next.editorState
);
