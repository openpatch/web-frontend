import { select } from '@storybook/addon-knobs';
import ActivityCard from './ActivityCard';

export default {
  title: 'ActivityCard',
  component: ActivityCard
};

export const Item = () => (
  <ActivityCard
    createdOn={new Date().toString()}
    member={{
      id: 'some-uuid',
      data: {
        username: 'a username'
      }
    }}
    type={select('type', ['like', 'follow', 'create', 'update'], 'like')}
    resource={{
      id: 'some-uuid',
      type: 'item',
      data: {
        name: 'A Item'
      }
    }}
  />
);

export const Member = () => (
  <ActivityCard
    createdOn={new Date().toString()}
    member={{
      id: 'some-uuid',
      data: {
        username: 'a username'
      }
    }}
    type={select('type', ['like', 'follow', 'create', 'update'], 'like')}
    resource={{
      id: 'some-uuid',
      type: 'member',
      data: {
        username: 'Another Username'
      }
    }}
  />
);
