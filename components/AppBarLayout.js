import PropTypes from 'prop-types';
import Link from 'next/link';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { makeStyles } from '@material-ui/core/styles';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import MUILink from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

import Copyright from './Copyright';
import Logo from './Logo';

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'absolute',
    bottom: 0,
    top: 0,
    right: 0,
    left: 0,
    display: 'flex',
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    position: 'fixed',
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  title: {
    flexGrow: 1,
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
  },
}));

const AppBarLayout = ({
  inverted,
  children,
  left,
  title,
  center,
  right,
  breadcrumbs,
  fab,
}) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar
        position="absolute"
        color={inverted ? 'secondary' : 'primary'}
        className={classes.appBar}
      >
        <Toolbar className={classes.toolbar}>
          {left && <Box mr={2}>{left}</Box>}
          <Box className={classes.title}>
            {title || (
              <Link href="/" passHref>
                <MUILink>
                  <Logo inverted={inverted} />
                </MUILink>
              </Link>
            )}
          </Box>
          {center && (
            <Box mr={2} ml={2}>
              {center}
            </Box>
          )}
          <Box flexGrow={1} />
          {right && <Box ml={2}>{right}</Box>}
        </Toolbar>
      </AppBar>
      <main className={classes.content}>
        <div className={classes.appBarSpacer} />
        <Box p={4}>
          {breadcrumbs.length > 0 && (
            <Box pb={2}>
              <Breadcrumbs maxItems={5} aria-label="breadcrumb">
                {breadcrumbs.map(({ href, as, title = '', active }) => (
                  <Link href={href} as={as || href} key={href} passHref>
                    {active ? (
                      <Typography color="textPrimary">{title}</Typography>
                    ) : (
                      <MUILink color="inherit">{title}</MUILink>
                    )}
                  </Link>
                ))}
              </Breadcrumbs>
            </Box>
          )}
          {children}
          <Box pt={4}>
            <Copyright />
          </Box>
        </Box>
        <Box style={{ position: 'absolute', right: 16, bottom: 16 }}>{fab}</Box>
      </main>
    </div>
  );
};

AppBarLayout.defaultProps = {
  title: null,
  breadcrumbs: [],
  fab: null,
};

AppBarLayout.propTypes = {
  inverted: PropTypes.bool,
  children: PropTypes.any,
  left: PropTypes.any,
  title: PropTypes.any,
  center: PropTypes.any,
  right: PropTypes.any,
  fab: PropTypes.any,
  breadcrumbs: PropTypes.arrayOf(
    PropTypes.shape({
      href: PropTypes.string,
      as: PropTypes.string,
      title: PropTypes.string,
      active: PropTypes.bool,
    })
  ),
};

export default AppBarLayout;
