import { action } from '@storybook/addon-actions';
import PolicyDialog from './PolicyDialog';

import { getLoremIpsum } from '../utils/editor';

export default {
  title: 'PolicyDialog',
  component: PolicyDialog
};

export const Default = () => (
  <PolicyDialog
    type="privacy"
    content={getLoremIpsum()}
    open={true}
    onClose={action('close')}
    onAgree={action('agree')}
  />
);
