import { t, Trans } from '@lingui/macro';
import Box from '@material-ui/core/Box';
import MUILink from '@material-ui/core/Link';
import { useTheme } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
import BlockIcon from '@material-ui/icons/Block';
import { TestResult } from '@services/assessment/testResults';
import { useDateUtils } from '@utils/date';
import Link from 'next/link';

export type TestResultEntryProps = Pick<
  TestResult,
  | 'id'
  | 'assessment'
  | 'start'
  | 'end'
  | 'aborted'
  | 'abortedReason'
  | 'score'
  | 'sessionHash'
>;

function TestResultEntry({
  id,
  assessment,
  start,
  end,
  aborted,
  abortedReason,
  score,
  sessionHash,
}: TestResultEntryProps) {
  const { format, formatDistance } = useDateUtils();
  const theme = useTheme();
  return (
    <Box
      display="flex"
      width="100%"
      padding={2}
      borderBottom={`1px solid ${theme.palette.divider}`}
      flexWrap="wrap"
      alignItems="center"
    >
      <Box flex={1} textAlign="center">
        <Link
          href={`/assessment/[id]/test-result/[tid]`}
          as={`/assessment/${assessment}/test-result/${id}`}
          passHref
        >
          <MUILink>{sessionHash}</MUILink>
        </Link>
      </Box>
      <Box flex={3} textAlign="center">
        {start ? format(new Date(start), 'dd. LLL yy, HH:mm') : null}
      </Box>
      <Box flex={3} textAlign="center">
        {end ? format(new Date(end), 'dd. LLL yy, HH:mm') : null}
      </Box>
      <Box flex={3} textAlign="center">
        {end && start ? formatDistance(new Date(end), new Date(start)) : null}
      </Box>
      <Box flex={2} textAlign="center">
        {score != null ? <Trans>{score} Points</Trans> : null}
      </Box>
      <Box flex={0.5} textAlign="center">
        {aborted ? (
          <Tooltip title={t`Aborted: ${abortedReason}`}>
            <BlockIcon />
          </Tooltip>
        ) : null}
      </Box>
    </Box>
  );
}

export default TestResultEntry;
