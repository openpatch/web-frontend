import { t, Trans } from '@lingui/macro';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Security from '@material-ui/icons/Security';
import Videocam from '@material-ui/icons/Videocam';
import { Assessment } from '@services/assessment/assessments';
import { convertFromRaw } from '../utils/editor';
import CheckboxField from './fields/Checkbox';
import RichText from './RichText';

export type SessionDataProtectionProps = Pick<
  Assessment,
  'dataProtection' | 'record' | 'encrypt'
> & {
  accept: boolean;
  onAccept: () => void;
};

const SessionDataProtection = ({
  dataProtection,
  record,
  encrypt,
  accept,
  onAccept,
}: SessionDataProtectionProps) => (
  <>
    <Typography style={{ marginTop: 16 }} variant="h4">
      <Trans>Privacy Policy</Trans>
    </Typography>
    <RichText editorState={convertFromRaw(dataProtection || undefined)} />
    {record && (
      <>
        <Typography style={{ marginTop: 16 }} variant="h4">
          <Trans>Recording</Trans>
        </Typography>
        <Typography id="session.data-protection.record">
          <Trans>
            Some tasks will be recorded. This means that all interactions will
            be sent and saved on the OpenPatch Server. This allows the creator
            of this assessment to review your progress.
            <br /> You will see this icon <Videocam fontSize="inherit" /> at the
            bottom, if a task will be recorded.
          </Trans>
        </Typography>
      </>
    )}
    {encrypt && (
      <>
        <Typography style={{ marginTop: 16 }} variant="h4">
          <Trans>Encryption</Trans>
        </Typography>
        <Typography id="session.data-protection.record">
          <Trans>
            Some tasks will be encrypted to protect you. We use opengpg to
            encrypt you data. The creator of the assessment will be able to
            decrypt your data with a secret key. <br /> You will see this icon{' '}
            <Security fontSize="inherit" /> at the bottom, if a task will be
            encrypted.
          </Trans>
        </Typography>
      </>
    )}
    <Box textAlign="center" mt={2}>
      <CheckboxField
        meta={{}}
        input={{
          name: 'agree',
          checked: accept,
          value: accept,
          onChange: onAccept,
          onBlur: () => null,
          onFocus: () => null,
        }}
        type="checkbox"
        label={t`I have read and agree to the privacy policy`}
      />
    </Box>
  </>
);

export default SessionDataProtection;
