import { Trans } from '@lingui/macro';
import MUILink from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';

import Link from './Link';

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link href="/">OpenPatch</Link> {new Date().getFullYear()}
      {' | '}
      <Link href="/policy/privacy">
        <Trans>Privacy Policy</Trans>
      </Link>
      {' | '}
      <Link href="/contact">
        <Trans>Contact</Trans>
      </Link>
      {' | '}
      <MUILink href="https://status.openpatch.app">
        <Trans>Status</Trans>
      </MUILink>
    </Typography>
  );
}

export default Copyright;
