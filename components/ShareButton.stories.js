import { text } from '@storybook/addon-knobs';
import ShareButton from './ShareButton';

export default {
  title: 'ShareButton',
  component: ShareButton,
};

export const Default = () => (
  <ShareButton
    url={text('url', 'http://test.com')}
    title={text('title', 'My thing')}
  />
);
