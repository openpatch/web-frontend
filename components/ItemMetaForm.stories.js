import { action } from '@storybook/addon-actions';
import ItemMetaForm from './ItemMetaForm';

export default {
  title: 'ItemMetaForm',
  component: ItemMetaForm,
};

export const Default = () => <ItemMetaForm onSubmit={action('submit')} />;

export const Prefilled = () => (
  <ItemMetaForm
    onSubmit={action('submit')}
    initialValues={{
      name: 'hallo',
      privacy: 'notlisted',
      versions: [
        {
          status: 'draft',
          version: 1,
        },
      ],
    }}
  />
);
