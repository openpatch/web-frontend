import { action } from '@storybook/addon-actions';
import EditFab from './EditFab';

export default {
  title: 'Fab/EditFab',
  component: EditFab
};

export const Default = () => <EditFab title="Edit" onClick={action('click')} />;
