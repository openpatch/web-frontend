import PropTypes from 'prop-types';
import { t } from '@lingui/macro';
import Tooltip from '@material-ui/core/Tooltip';
import Button from '@material-ui/core/Button';
import ToysIcon from '@material-ui/icons/Toys';

function RemixButton({ remixes, ...props }) {
  return (
    <Tooltip title={t`Remix`}>
      <Button startIcon={<ToysIcon />} {...props}>
        {remixes}
      </Button>
    </Tooltip>
  );
}

RemixButton.propTypes = {
  remixes: PropTypes.number,
};

RemixButton.defaultProps = {
  remixes: 0,
};

export default RemixButton;
