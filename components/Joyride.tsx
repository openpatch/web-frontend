import { t } from '@lingui/macro';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
import JR, { CallBackProps, Props as JRProps, STATUS } from 'react-joyride';
import { useLocalStorage } from '../utils/storage';

const useStyles = makeStyles((theme) => ({
  helper: {
    position: 'fixed',
    cursor: 'pointer',
    right: 0,
    bottom: '50%',
    border: 0,
    fontWeight: 'bolder',
    boxShadow: theme.shadows[1],
    padding: theme.spacing(1),
    borderTopLeftRadius: theme.shape.borderRadius,
    borderBottomLeftRadius: theme.shape.borderRadius,
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.primary.contrastText,
    zIndex: theme.zIndex.drawer + 1,
  },
}));

export type Joyride = {
  name: string;
} & JRProps;

const Joyride = ({ name, callback, steps, ...props }: Joyride) => {
  const theme = useTheme();
  const classes = useStyles();
  const [passed, setPassed] = useLocalStorage(`joyride.${name}`, false);

  const locale = {
    back: t`Back`,
    close: t`Close`,
    last: t`Finish`,
    next: t`Continue`,
    skip: t`Skip`,
  };

  function reset() {
    setPassed(false);
  }

  return (
    <>
      {!passed && (
        <JR
          showSkipButton
          continuous
          styles={{
            options: {
              primaryColor: theme.palette.error.main,
              zIndex: theme.zIndex.drawer + 1,
            },
          }}
          steps={[
            ...steps,
            {
              target: '#drawer-help',
              content: t`Need more help? Visit our help section`,
            },
            {
              target: '#guided-tour',
              isFixed: true,
              placement: 'left-end',
              content: t`You can restart the guided tour by clicking the question mark`,
            },
          ]}
          {...props}
          locale={locale}
          callback={(d: CallBackProps) => {
            const status = d.status;
            if ([STATUS.FINISHED, STATUS.SKIPPED].includes(status as any)) {
              setPassed(true);
            }
            if (callback) {
              callback(d);
            }
          }}
        />
      )}
      <Tooltip title={t`Show Guided Tour`}>
        <button id="guided-tour" onClick={reset} className={classes.helper}>
          ?
        </button>
      </Tooltip>
    </>
  );
};

export default Joyride;
