import PropTypes from 'prop-types';
import ErrorPage from './ErrorPage';
import sentry from '../utils/sentry';

function ErrorPageSentry({ errorEventId }) {
  const { Sentry } = sentry();
  return (
    <ErrorPage
      onReport={() => {
        Sentry.showReportDialog({ eventId: errorEventId });
      }}
    />
  );
}

ErrorPageSentry.propTypes = {
  errorEventId: PropTypes.string
};

export default ErrorPageSentry;
