import { action } from '@storybook/addon-actions';
import { boolean } from '@storybook/addon-knobs';
import { AvatarMenuView } from './AvatarMenu';

export default {
  title: 'AvatarMenuView',
  component: AvatarMenuView
};

export const Default = () => (
  <AvatarMenuView
    user={{
      username: 'my-username'
    }}
    onClick={action('click')}
    onClose={action('close')}
    onLogout={action('logout')}
    onProfile={action('profile')}
    open={boolean('open', false)}
  />
);
