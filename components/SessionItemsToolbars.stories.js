import { action } from '@storybook/addon-actions';
import { boolean } from '@storybook/addon-knobs';
import {
  SessionItemsToolbarLeft,
  SessionItemsToolbarRight,
  SessionItemsToolbarCenter
} from './SessionItemsToolbars';

export default {
  title: 'SessionItemsToolbars'
};

export const Left = () => <SessionItemsToolbarLeft onAbort={action('abort')} />;

export const Right = () => (
  <SessionItemsToolbarRight onSubmit={action('submit')} />
);

export const Center = () => (
  <SessionItemsToolbarCenter
    session="ABCD"
    encrypt={boolean('encrypt')}
    record={boolean('record')}
  />
);
