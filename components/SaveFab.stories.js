import { action } from '@storybook/addon-actions';
import SaveFab from './SaveFab';

export default {
  title: 'Fab/SaveFab',
  component: SaveFab,
};

export const Default = () => <SaveFab title="Save" onClick={action('click')} />;
