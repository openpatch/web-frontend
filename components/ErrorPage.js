import { Trans } from '@lingui/macro';
import PropTypes from 'prop-types';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import BugFixing from 'mui-undraw/lib/BugFixing';
import Typography from '@material-ui/core/Typography';

import AppBarLayout from './AppBarLayout';

const ErrorPage = ({ onReport }) => {
  return (
    <AppBarLayout>
      <Container>
        <Typography variant="h5">
          <Trans>An error occurred</Trans>
        </Typography>
        <BugFixing style={{ maxWidth: 600 }} />
        <Box mt={1} mb={1}>
          <Button variant="outlined" color="primary" onClick={onReport}>
            <Trans>Report this error</Trans>
          </Button>
        </Box>
        <Button
          onClick={() => {
            window.location.reload(true);
          }}
        >
          <Trans>Or, try reloading the page</Trans>
        </Button>
      </Container>
    </AppBarLayout>
  );
};

ErrorPage.propTypes = {
  onReport: PropTypes.func.isRequired,
};

export default ErrorPage;
