import { t, Trans } from '@lingui/macro';
import Box from '@material-ui/core/Box';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Grid from '@material-ui/core/Grid';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { SessionState, useSubmit } from '@services/assessment/session';
import { TimeAction } from '@services/assessment/session/actions';
import { SessionProvider } from '@services/assessment/session/provider';
import { ItemTask, postItemVersionsEvaluate } from '@services/itembank/items';
import { useNotifications } from '@utils/notification';
import dynamic from 'next/dynamic';
import { useState } from 'react';
import ActionLog from './ActionLog';
import ItemVersionRenderer from './ItemVersionRenderer';
import ProgressButton, { ProgressButtonProps } from './ProgressButton';

const JSONTree = dynamic<any>(() => import('react-json-tree'));

const useStyles = makeStyles((theme) => ({
  json: {
    '& ul': {
      backgroundColor: theme.palette.background.paper,
    },
  },
}));

type EvaluateButtonProps = {
  loading: boolean;
} & ProgressButtonProps;

function EvaluateButton({ loading, ...props }: EvaluateButtonProps) {
  const handleSubmit = useSubmit();
  return (
    <ProgressButton loading={loading} onClick={handleSubmit} {...props}>
      <Trans>Evaluate</Trans>
    </ProgressButton>
  );
}

type LogProps = {
  actions: TimeAction[];
  evaluation: Record<string, any> | null;
};

function Log({ actions, evaluation }: LogProps) {
  const theme = useTheme();
  const classes = useStyles();

  return (
    <>
      <ExpansionPanel>
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="action-log"
        >
          <Trans>Action Log</Trans>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <ActionLog actions={actions} />
        </ExpansionPanelDetails>
      </ExpansionPanel>
      <ExpansionPanel>
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="action-evaluation"
        >
          <Box display="flex" width="100%" alignItems="center">
            <Trans>Evaluation</Trans>
            <Box flex={1} />
            <Box
              bgcolor={
                evaluation ? (evaluation.correct ? 'green' : 'red') : 'orange'
              }
              width={10}
              height={10}
              borderRadius="50%"
            />
          </Box>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <Box className={classes.json}>
            {evaluation ? (
              <JSONTree
                data={evaluation}
                theme="greenscreen"
                hideRoot
                sortObjectKeys
                invertTheme={theme.palette.type !== 'light'}
              />
            ) : (
              <Trans>Click Evaluate</Trans>
            )}
          </Box>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    </>
  );
}

export type ItemVersionData = {
  tasks?: ItemTask[];
};

function ItemVersionData({ tasks = [] }: ItemVersionData) {
  const [submitting, setSubmitting] = useState(false);
  const [actions, setActions] = useState<TimeAction[]>([]);
  const [evaluation, setEvaluation] = useState<Record<string, any> | null>(
    null
  );
  const [add] = useNotifications();

  function handleSubmit(
    state: Pick<SessionState, 'items'>,
    dispatchResponse: any,
    dispatchError: any
  ) {
    setSubmitting(true);
    postItemVersionsEvaluate(null, tasks, state?.items?.[0]?.tasks || {})
      .then(([data]) => {
        dispatchResponse(data);
        const { correct } = data;
        setEvaluation(data);
        if (correct) {
          add({
            message: t`Correct`,
            severity: 'success',
          });
        } else {
          add({
            message: t`Wrong`,
            severity: 'error',
          });
        }
        setSubmitting(false);
      })
      .catch((err: any) => {
        dispatchError(err);
        add({
          message: t`Evaluation Service is not available`,
          severity: 'error',
        });
        setSubmitting(false);
      });
  }

  function recorder(action: TimeAction) {
    setActions((actions) => [...actions, action]);
  }

  return (
    <SessionProvider recorder={recorder} onSubmit={handleSubmit}>
      {process.env.NODE_ENV === 'production' ? (
        <>
          <ItemVersionRenderer tasks={tasks} index={0} />
          <EvaluateButton variant="outlined" loading={submitting} />
        </>
      ) : (
        <Grid container spacing={3}>
          <Grid item xs={12} lg={8}>
            <ItemVersionRenderer tasks={tasks} index={0} />
            <EvaluateButton variant="outlined" loading={submitting} />
          </Grid>
          <Grid item xs={12} lg={4}>
            <Log actions={actions} evaluation={evaluation} />
          </Grid>
        </Grid>
      )}
    </SessionProvider>
  );
}

export default ItemVersionData;
