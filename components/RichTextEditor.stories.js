import { useState } from 'react';
import { button } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import RichTextEditor from './RichTextEditor';

import { convertFromRaw, getLoremIpsum } from '../utils/editor';

import imagePlugin from '../utils/editor/image-plugin';

export default {
  title: 'RichTextEditor',
  component: RichTextEditor,
};

export const Default = () => (
  <RichTextEditor
    editorState={convertFromRaw(getLoremIpsum())}
    onChange={action('change')}
  />
);

export const WithState = () => {
  const [editorState, setEditorState] = useState(
    convertFromRaw(getLoremIpsum())
  );

  return <RichTextEditor editorState={editorState} onChange={setEditorState} />;
};

export const WithImage = () => {
  const [editorState, setEditorState] = useState(
    convertFromRaw(getLoremIpsum())
  );

  function insertImage() {
    setEditorState((editorState) =>
      imagePlugin.add(
        editorState,
        'https://upload.wikimedia.org/wikipedia/commons/3/32/Telefunken_FuBK_test_pattern.svg'
      )
    );
  }

  button('insert image', insertImage);

  return <RichTextEditor editorState={editorState} onChange={setEditorState} />;
};
