import { action } from '@storybook/addon-actions';
import PolicyForm from './PolicyForm';

export default {
  title: 'PolicyForm',
  component: PolicyForm
};

export const Default = () => <PolicyForm onSubmit={action('submit')} />;

export const WithValue = () => (
  <PolicyForm
    onSubmit={action('submit')}
    initialValues={{
      type: 'privacy',
      language: 'de',
      content: {}
    }}
  />
);
