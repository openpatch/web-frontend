import { Trans } from '@lingui/macro';
import PropTypes from 'prop-types';
import PublicIcon from '@material-ui/icons/Public';
import PrivateIcon from '@material-ui/icons/Lock';
import NotListedIcon from '@material-ui/icons/Link';
import Tooltip from '@material-ui/core/Tooltip';

function PrivacyIndicator({ privacy, ...props }) {
  let Icon = PublicIcon;
  if (privacy === 'notlisted') {
    Icon = NotListedIcon;
  } else if (privacy === 'private') {
    Icon = PrivateIcon;
  }

  return (
    <Tooltip title={<Trans id={privacy} />}>
      <Icon {...props} />
    </Tooltip>
  );
}

PrivacyIndicator.propTypes = {
  privacy: PropTypes.oneOf(['public', 'notlisted', 'private']),
};

PrivacyIndicator.defaultProps = {
  fontSize: 'inherit',
};

export default PrivacyIndicator;
