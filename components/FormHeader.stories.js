import FormHeader from './FormHeader';

export default {
  title: 'Form/FormHeader',
  component: FormHeader
};

export const Default = () => <FormHeader>Header</FormHeader>;
