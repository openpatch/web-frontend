import { useState, useEffect, useRef, FormEvent } from 'react';
import { useRouter } from 'next/router';
import { t, Trans } from '@lingui/macro';
import { Field, useForm, useField } from 'react-final-form';
import { Network, Node, Edge, Options } from 'vis-network';
import { useTheme } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Dialog, { DialogProps } from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';

import TextField from './fields/TextField';
import uuid4 from '@utils/uuid';
import { TestEdge, TestNode } from '@services/itembank/tests';

type EditEdgePopupProps = {
  edge: {
    field: [string, number];
  };
  onClose: () => void;
} & DialogProps;

function EditEdgePopup({ edge, ...props }: EditEdgePopupProps) {
  const {
    input: { onChange, value },
  } = useField(`${edge.field[0]}[${edge.field[1]}].threshold`);

  function handleChange(e: FormEvent) {
    onChange(e);
  }

  function handleClose() {
    props.onClose();
  }
  return (
    <Dialog {...props}>
      <DialogTitle>Edit Edge</DialogTitle>
      <DialogContent>
        <TextField
          type="number"
          fullWidth
          label={<Trans>Threshold</Trans>}
          meta={{}}
          input={{
            name: 'threshold',
            onChange: handleChange,
            onBlur: () => null,
            onFocus: () => null,
            value,
          }}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>
          <Trans>Close</Trans>
        </Button>
      </DialogActions>
    </Dialog>
  );
}

type TestNodesProps = {
  input: {
    value: TestNode[];
    onChange: (e: any) => void;
    name: string;
  };
};

type FieldEdge = {
  threshold: number;
  field: [string, number];
} & Edge;

type FieldNode = {
  field: string;
  edges: TestEdge[];
} & Node;

function TestNodes({ input: { value, onChange, name } }: TestNodesProps) {
  const theme = useTheme();
  const router = useRouter();
  const network = useRef<Network | null>(null);
  const container = useRef<HTMLDivElement | null>(null);
  const [edges, setEdges] = useState<FieldEdge[]>([]);
  const [nodes, setNodes] = useState<FieldNode[]>([]);
  const [popupOpen, setPopupOpen] = useState(false);
  const [selectedEdge, setSelectedEdge] = useState<FieldEdge | null>(null);
  const {
    mutators: { push, remove },
  } = useForm();

  function handleNodesPositionChange() {
    const nodePositions = network.current?.getPositions();
    const newValue: any[] = [];

    value.forEach((v) => {
      newValue.push({
        ...v,
        x: nodePositions?.[v.id].x,
        y: nodePositions?.[v.id].y,
      });
    });

    onChange(newValue);
  }

  useEffect(() => {
    create();
    network.current?.on('dragEnd', handleNodesPositionChange);

    return () => {
      network.current?.off('dragEnd', handleNodesPositionChange);
    };
  }, [nodes, edges]);

  useEffect(() => {
    const tmpNodes: FieldNode[] = [];
    const tmpEdges: FieldEdge[] = [];
    value.forEach((n, ni) => {
      tmpNodes.push({
        label: n.name,
        id: n.id,
        value: n.items?.length,
        x: n.x,
        y: n.y,
        field: `${name}[${ni}]`,
        edges: n.edges || [],
        color: {
          background: n.start ? 'green' : undefined,
        },
      });
      if (n.edges) {
        n.edges.forEach((e, ei) => {
          tmpEdges.push({
            id: e.id,
            from: e.nodeFrom,
            to: e.nodeTo,
            threshold: e.threshold || 0,
            label: e.threshold == 0 ? t`default` : `>= ${e.threshold}`,
            field: [`${name}[${ni}].edges`, ei],
          });
        });
      }
    });
    setNodes(tmpNodes);
    setEdges(tmpEdges);
  }, [value]);

  function handlePopupClose() {
    setPopupOpen(false);
  }

  function handleDeleteEdge(data: any, callback: any) {
    const edge = edges.find((e) => e.id === data?.edges?.[0]);
    remove(`${edge?.field[0]}`, edge?.field[1]);
    callback(data);
  }

  function handleAddEdge(data: any, callback: any) {
    const node = nodes.find((n) => n.id === data.from);
    if (node) {
      const field = `${node.field}.edges`;

      push(field, {
        id: uuid4(),
        nodeFrom: data.from,
        nodeTo: data.to,
        threshold: 0,
      });
      callback(data);
    }
  }

  function handleEditEdge(data: any, _: any) {
    const edge = edges.find((e) => e.id === data.id);
    if (edge) {
      setSelectedEdge(edge);
      setPopupOpen(true);
    }
  }

  function create() {
    const defaultOptions: Options = {
      locale: router.locale,
      physics: {
        stabilization: false,
      },
      autoResize: true,
      interaction: {
        navigationButtons: true,
      },
      layout: {
        randomSeed: 1,
      },
      manipulation: {
        enabled: true,
        initiallyActive: true,
        addNode: false,
        deleteNode: false,
        deleteEdge: handleDeleteEdge,
        addEdge: handleAddEdge,
        editEdge: {
          editWithoutDrag: handleEditEdge,
        },
      },
      nodes: {
        physics: false,
        scaling: {
          min: 1,
          max: 20,
        },
        shape: 'box',
        color: {
          border: theme.palette.primary.dark,
          background: theme.palette.primary.main,
          highlight: {
            border: theme.palette.primary.main,
            background: theme.palette.primary.light,
          },
        },
        font: {
          face: 'Roboto',
          color: theme.palette.primary.contrastText,
        },
      },
      edges: {
        smooth: {
          enabled: true,
          type: 'dynamic',
          forceDirection: 'none',
          roundness: 0.5,
        },
        color: theme.palette.primary.dark,
        font: {
          face: 'Roboto',
          color: theme.palette.primary.dark,
        },
        width: 0.5,
        arrows: {
          to: {
            enabled: true,
            scaleFactor: 0.5,
          },
        },
      },
    };
    network.current?.destroy();
    if (container.current !== null) {
      network.current = new Network(
        container.current,
        {
          nodes,
          edges,
        },
        defaultOptions
      );
    }
  }

  return (
    <Paper>
      <div ref={container} style={{ height: 400 }} />
      {Boolean(popupOpen) && selectedEdge !== null && (
        <EditEdgePopup
          open={Boolean(popupOpen)}
          onClose={handlePopupClose}
          edge={selectedEdge}
        />
      )}
    </Paper>
  );
}

function TestVersionGraph() {
  return <Field name="nodes" render={(props) => <TestNodes {...props} />} />;
}

export default TestVersionGraph;
