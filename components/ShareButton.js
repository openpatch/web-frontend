import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Trans, t } from '@lingui/macro';
import { makeStyles } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import ShareIcon from '@material-ui/icons/Share';
import TwitterIcon from '@material-ui/icons/Twitter';
import RedditIcon from '@material-ui/icons/Reddit';
import EmailIcon from '@material-ui/icons/Email';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

function clipboardCopy(text) {
  // Use the Async Clipboard API when available. Requires a secure browsing
  // context (i.e. HTTPS)
  if (navigator.clipboard) {
    return navigator.clipboard.writeText(text).catch(function (err) {
      throw err !== undefined
        ? err
        : new DOMException('The request is not allowed', 'NotAllowedError');
    });
  }

  // ...Otherwise, use document.execCommand() fallback

  // Put the text to copy into a <span>
  var span = document.createElement('span');
  span.textContent = text;

  // Preserve consecutive spaces and newlines
  span.style.whiteSpace = 'pre';

  // Add the <span> to the page
  document.body.appendChild(span);

  // Make a selection object representing the range of text selected by the user
  var selection = window.getSelection();
  var range = window.document.createRange();
  selection.removeAllRanges();
  range.selectNode(span);
  selection.addRange(range);

  // Copy text to the clipboard
  var success = false;
  try {
    success = window.document.execCommand('copy');
  } catch (err) {
    console.log('error', err);
  }

  // Cleanup
  selection.removeAllRanges();
  window.document.body.removeChild(span);

  return success
    ? Promise.resolve()
    : Promise.reject(
        new DOMException('The request is not allowed', 'NotAllowedError')
      );
}

const useStyles = makeStyles((theme) => ({
  urlBox: {
    display: 'flex',
    alignItems: 'center',
    borderWidth: 1,
    borderStyle: 'solid',
    borderRadius: theme.shape.borderRadius,
    borderColor: theme.palette.divider,
    padding: theme.spacing(1),
    marginTop: theme.spacing(1),
  },
  url: {
    flex: 1,
    marginRight: theme.spacing(1),
  },
  shareTargets: {
    display: 'flex',
    alignItems: 'center',
  },
  shareTarget: {
    margin: theme.spacing(1),
  },
}));

function ShareButton({ url, title, ...props }) {
  const [open, setOpen] = useState(false);
  const classes = useStyles();
  // is need for ssr, because window is undefined
  const [innerUrl, setInnerUrl] = useState(url);
  const [canCopy, setCanCopy] = useState(false);

  useEffect(() => {
    if (!url) {
      setInnerUrl(window.location.href);
    }
  }, []);

  useEffect(() => {
    setCanCopy(document.queryCommandSupported('copy'));
  });

  function copy() {
    clipboardCopy(innerUrl);
  }

  function shareTwitter() {
    window.open(
      `https://twitter.com/intent/tweet?url=${encodeURIComponent(
        innerUrl
      )}&text=${encodeURIComponent(title)}&via=OpenPatch&related=OpenPatch`,
      '_blank'
    );
  }

  function shareReddit() {
    window.open(
      `https://www.reddit.com/submit?url=${encodeURIComponent(
        innerUrl
      )}&title=${encodeURIComponent(title)}`,
      '_blank'
    );
  }

  function shareEmail() {
    window.location = `mailto:?subject=${encodeURIComponent(
      `OpenPatch: ${title}`
    )}&body=${encodeURIComponent(innerUrl)}`;
  }
  return (
    <>
      <Dialog open={open} onClose={() => setOpen(false)}>
        <DialogTitle>
          <Trans>Share</Trans>
        </DialogTitle>
        <DialogContent dividers>
          <div className={classes.shareTargets}>
            <Tooltip title="Twitter">
              <IconButton
                className={classes.shareTarget}
                onClick={shareTwitter}
              >
                <TwitterIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title="Reddit">
              <IconButton className={classes.shareTarget} onClick={shareReddit}>
                <RedditIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title={t`E-Mail`}>
              <IconButton className={classes.shareTarget} onClick={shareEmail}>
                <EmailIcon />
              </IconButton>
            </Tooltip>
          </div>
          <div className={classes.urlBox}>
            <div className={classes.url}>{innerUrl}</div>
            {canCopy && (
              <Button color="primary" size="small" onClick={copy}>
                <Trans>Copy</Trans>
              </Button>
            )}
          </div>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setOpen(false)}>
            <Trans>Close</Trans>
          </Button>
        </DialogActions>
      </Dialog>
      <Tooltip title={t`Share`}>
        <Button
          startIcon={<ShareIcon />}
          {...props}
          onClick={() => setOpen(true)}
        >
          <Trans>Share</Trans>
        </Button>
      </Tooltip>
    </>
  );
}

ShareButton.propTypes = {
  url: PropTypes.string,
  title: PropTypes.string,
};

export default ShareButton;
