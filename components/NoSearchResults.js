import { Trans } from '@lingui/macro';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import NoData from 'mui-undraw/lib/NoData';

function NoEntries() {
  return (
    <Box display="flex" flexDirection="column" alignItems="center">
      <NoData style={{ maxWidth: 600, marginBottom: 8 }} />
      <Typography variant="subtitle1" align="center">
        <Trans>No Search Results</Trans>
      </Typography>
    </Box>
  );
}

NoEntries.propTypes = {};

export default NoEntries;
