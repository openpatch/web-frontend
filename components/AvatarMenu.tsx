import { useState, MouseEvent } from 'react';
import { Trans } from '@lingui/macro';
import { useRouter } from 'next/router';
import Link from 'next/link';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';

import MemberAvatar from './MemberAvatar';
import { useClaims } from '@services/authentification/provider';
import { MemberClaims } from '@services/authentification/members';
import { getLogout } from '@services/authentification/root';

type AvatarMenuViewProps = {
  claims: MemberClaims | null;
  onClick: (event: MouseEvent<HTMLElement>) => void;
  onClose: () => void;
  onLogout: () => void;
  anchorEl: HTMLElement | null;
  open: boolean;
  onProfile: () => void;
};

export function AvatarMenuView({
  claims,
  onClick,
  onClose,
  onLogout,
  anchorEl,
  open,
  onProfile,
}: AvatarMenuViewProps) {
  return claims?.username ? (
    <>
      <IconButton
        aria-controls="user-menu"
        aria-haspopup="true"
        onClick={onClick}
      >
        <MemberAvatar username={claims?.username} avatarId={claims?.avatarId} />
      </IconButton>
      <Menu
        id="user-menu"
        anchorEl={anchorEl}
        keepMounted
        open={open}
        onClose={onClose}
      >
        <MenuItem onClick={onProfile}>
          <Trans>Profile</Trans>
        </MenuItem>
        <MenuItem onClick={onLogout}>
          <Trans>Logout</Trans>
        </MenuItem>
      </Menu>{' '}
    </>
  ) : (
    <Link href="/login">
      <Button variant="outlined" color="secondary">
        <Trans>Log In</Trans>
      </Button>
    </Link>
  );
}

function AvatarMenu() {
  const router = useRouter();
  const claims = useClaims();
  const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);

  function handleClick(event: MouseEvent<HTMLElement>) {
    setAnchorEl(event.currentTarget);
  }

  function handleClose() {
    setAnchorEl(null);
  }

  function handleProfile() {
    router.push(`/member/[id]`, `/member/${claims?.id}`);
  }

  return (
    <AvatarMenuView
      onClick={handleClick}
      onClose={handleClose}
      onProfile={handleProfile}
      onLogout={() => getLogout(null)}
      open={Boolean(anchorEl)}
      anchorEl={anchorEl}
      claims={claims}
    />
  );
}

export default AvatarMenu;
