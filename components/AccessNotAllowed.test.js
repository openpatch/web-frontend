import AccessNotAllowed from './AccessNotAllowed';
import { render, act } from '@testing-library/react';
import { TestingProvider } from '../test-utils';
import { dynamicActivate } from '../utils/i18n';

describe('<AccessNotAllowed />', () => {
  it('should render', () => {
    act(() => {
      dynamicActivate('en');
    });
    const { getByText } = render(<AccessNotAllowed />, {
      wrapper: TestingProvider,
    });
    expect(getByText('Access not allowed')).toBeInTheDocument();
  });
});
