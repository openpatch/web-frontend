import PropTypes from 'prop-types';
import { Trans } from '@lingui/macro';

import ProgressButton from './ProgressButton';
import RichText from './RichText';
import { convertFromRaw } from '../utils/editor';

function PolicyAgree({ content, type, onAgree, isSending }) {
  return (
    <>
      <RichText editorState={convertFromRaw(content)} />
      <ProgressButton fullWidth loading={isSending} onClick={onAgree}>
        <Trans>I have read and agree to the {type} policy</Trans>
      </ProgressButton>
    </>
  );
}

PolicyAgree.propTypes = {
  content: PropTypes.object,
  type: PropTypes.string,
  onAgree: PropTypes.func,
  isSending: PropTypes.bool,
};

export default PolicyAgree;
