import { select } from '@storybook/addon-knobs';
import CollectionCard from './CollectionCard';

export default {
  title: 'CollectionCard',
  component: CollectionCard,
};

export const Default = () => (
  <CollectionCard
    id="mock-collection"
    name="Mock Collection"
    privacy={select('privacy', ['public', 'private', 'not-listed'])}
    tests={['1']}
    items={['1', '2']}
    createdOn={new Date().toISOString()}
    member={{
      id: 'mock-member',
      username: 'mock-member',
    }}
  />
);
