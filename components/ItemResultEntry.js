import PropTypes from 'prop-types';
import Router from 'next/router';
import Link from 'next/link';
import { useTheme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import CancelIcon from '@material-ui/icons/Cancel';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import VideocamIcon from '@material-ui/icons/Videocam';

import { useDateUtils } from '../utils/date';

function ItemResultEntry({
  id,
  assessmentId,
  start,
  end,
  correct,
  evaluation,
  recorded,
}) {
  const { format, formatDistance } = useDateUtils();
  const theme = useTheme();
  return (
    <Box
      display="flex"
      width="100%"
      paddingTop={2}
      paddingBottom={2}
      borderBottom={`1px solid ${theme.palette.divider}`}
      flexWrap="wrap"
      alignItems="center"
    >
      <Tooltip title="Show">
        <Box
          flex={1}
          textAlign="center"
          style={{ cursor: 'pointer' }}
          aria-label="Show Solution"
          onClick={() =>
            Router.push(
              `/assessment/[id]/item-result/[iid]`,
              `/assessment/${assessmentId}/item-result/${id}`
            )
          }
        >
          {correct ? (
            <CheckCircleIcon style={{ color: 'green' }} />
          ) : (
            <CancelIcon style={{ color: 'red' }} />
          )}
          <Box display="flex" alignItems="center" justifyContent="center">
            {evaluation?.results &&
              Object.values(evaluation.results).map((result, index) => (
                <Box
                  key={index}
                  width={10}
                  m={0.2}
                  borderRadius={5}
                  height={10}
                  bgcolor={result.correct ? 'green' : 'red'}
                />
              ))}
          </Box>
        </Box>
      </Tooltip>
      <Box flex={2} textAlign="center">
        {start ? format(new Date(start), 'dd. LLL yy, HH:mm') : null}
      </Box>
      <Box flex={2} textAlign="center">
        {end ? format(new Date(end), 'dd. LLL yy, HH:mm') : null}
      </Box>
      <Box flex={2} textAlign="center">
        {end && start ? formatDistance(new Date(end), new Date(start)) : null}
      </Box>
      <Box textAlign="center">
        {recorded ? (
          <Link
            href="/assessment/[id]/item-result/[iid]/recording"
            as={`/assessment/${assessmentId}/item-result/${id}/recording`}
          >
            <IconButton>
              <VideocamIcon />
            </IconButton>
          </Link>
        ) : null}
      </Box>
    </Box>
  );
}

ItemResultEntry.propTypes = {
  id: PropTypes.string,
  assessmentId: PropTypes.string,
  start: PropTypes.string,
  end: PropTypes.string,
  correct: PropTypes.bool,
  recorded: PropTypes.bool,
  evaluation: PropTypes.object,
};

ItemResultEntry.defaultProps = {
  evaluation: {
    details: [],
  },
};

export default ItemResultEntry;
