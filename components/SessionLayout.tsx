import AppBar from '@material-ui/core/AppBar';
import Box from '@material-ui/core/Box';
import CssBaseline from '@material-ui/core/CssBaseline';
import { makeStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import { Locale } from '@utils/i18n';
import { ReactNode } from 'react';
import I18nProvider from './I18nProvider';
import Logo from './Logo';

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'absolute',
    bottom: 0,
    top: 0,
    right: 0,
    left: 0,
    display: 'flex',
    overflow: 'hidden',
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  bottomToolbar: {
    position: 'absolute',
    zIndex: theme.zIndex.drawer + 1,
    backgroundColor: theme.palette.background.paper,
    bottom: 0,
    left: 0,
    right: 0,
    borderTopStyle: 'solid',
    borderTopSize: 1,
    borderTopColor: theme.palette.primary.main,
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: 'none',
  },
  title: {
    flexGrow: 1,
  },
  appBarSpacer: theme.mixins.toolbar,
  contentContainer: {
    zIndex: 1,
    position: 'absolute',
    top: theme.mixins.toolbar.minHeight,
    bottom: theme.mixins.toolbar.minHeight,
    right: 0,
    left: 0,
    overflowY: 'auto',
  },
  content: {
    height: '100%',
    maxWidth: 1024,
    padding: theme.spacing(2),
    margin: '0 auto',
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
}));

export type SessionLayoutProps = {
  children: ReactNode;
  toolbarBottom?: {
    left?: ReactNode;
    right?: ReactNode;
    center?: ReactNode;
  };
  toolbarTop?: ReactNode;
  locale?: Locale;
};

function SessionLayout({
  children,
  toolbarBottom,
  toolbarTop,
  locale,
}: SessionLayoutProps) {
  const classes = useStyles();

  return (
    <I18nProvider locale={locale}>
      <div className={classes.root}>
        <CssBaseline />
        <AppBar position="absolute" className={classes.appBar}>
          <Toolbar className={classes.toolbar}>
            <Box className={classes.title}>
              <Logo />
            </Box>
            <Box flexGrow={1} />
            {toolbarTop}
          </Toolbar>
        </AppBar>
        <Toolbar className={classes.bottomToolbar}>
          {toolbarBottom?.left}
          <Box flexGrow={1} />
          {toolbarBottom?.center}
          <Box flexGrow={1} />
          {toolbarBottom?.right}
        </Toolbar>
        <div className={classes.contentContainer}>
          <main className={classes.content}>{children}</main>
        </div>
      </div>
    </I18nProvider>
  );
}

export default SessionLayout;
