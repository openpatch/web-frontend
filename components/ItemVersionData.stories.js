import { boolean } from '@storybook/addon-knobs';
import ItemVersionData from './ItemVersionData';

export default {
  title: 'ItemVersionData',
  component: ItemVersionData,
};

export const Default = () => (
  <ItemVersionData showDebug={boolean('showDebug', true)} />
);

export const WithTasks = () => (
  <ItemVersionData
    showDebug={boolean('showDebug', true)}
    tasks={[
      {
        formatType: 'choice',
        task: 'Choice Test',
        text: 'This is the description',
        data: {
          choices: ['hi', 'test'],
        },
      },
    ]}
  />
);
