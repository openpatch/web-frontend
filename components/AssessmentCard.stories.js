import { getLoremIpsum } from '../utils/editor';
import { AssessmentCard } from './AssessmentCard';

export default {
  title: 'AssessmentCard',
  component: AssessmentCard
};

export const Public = () => (
  <AssessmentCard
    id="some-uuid"
    name="My Assessment"
    public={true}
    member={{
      username: 'some-user'
    }}
    publicDescription={getLoremIpsum()}
    createdOn={new Date().toString()}
  />
);

export const Private = () => (
  <AssessmentCard
    id="some-uuid"
    name="My Assessment"
    public={false}
    member={{
      username: 'some-user'
    }}
    publicDescription={getLoremIpsum()}
    createdOn={new Date().toString()}
  />
);
