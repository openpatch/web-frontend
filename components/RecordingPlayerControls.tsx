import Box from '@material-ui/core/Box';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import PauseIcon from '@material-ui/icons/Pause';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import StopIcon from '@material-ui/icons/Stop';

function dateToTime(date: Date) {
  return `${date
    .getMinutes()
    .toString()
    .padStart(2, '0')}:${date.getSeconds().toString().padStart(2, '0')}`;
}

export type RecordingPlayerControlsProps = {
  onPause: () => void;
  onPlay: () => void;
  onStop: () => void;
  currentTime: number;
  endTime: number;
  isPlaying: boolean;
};

function RecordingPlayerControls({
  onPause,
  onPlay,
  onStop,
  currentTime,
  endTime,
  isPlaying,
}: RecordingPlayerControlsProps) {
  const currentDate = new Date(currentTime);
  const endDate = new Date(endTime);

  return (
    <Box display="flex" alignItems="center" color="white">
      {!isPlaying ? (
        <IconButton onClick={onPlay} color="inherit">
          <PlayArrowIcon />
        </IconButton>
      ) : (
        <IconButton onClick={onPause} color="inherit">
          <PauseIcon />
        </IconButton>
      )}
      <IconButton onClick={onStop} color="inherit">
        <StopIcon />
      </IconButton>
      <Typography>
        {dateToTime(currentDate)} / {dateToTime(endDate)}
      </Typography>
      <Box flex={1} />
    </Box>
  );
}

export default RecordingPlayerControls;
