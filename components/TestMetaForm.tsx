import PrivacySelectField from '@components/fields/PrivacySelectField';
import RichTextField from '@components/fields/RichTextField';
import TextField from '@components/fields/TextField';
import Form, { FormProps } from '@components/Form';
import FormSection from '@components/FormSection';
import FormSectionContent from '@components/FormSectionContent';
import FormSectionHeader from '@components/FormSectionHeader';
import Joyride from '@components/Joyride';
import { t, Trans } from '@lingui/macro';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Typography from '@material-ui/core/Typography';
import RemoveRedEyeIcon from '@material-ui/icons/RemoveRedEye';
import {
  postTestVersion,
  putTestVersionStatus,
  Test,
  TestVersionBase,
} from '@services/itembank/tests';
import { useNotifications } from '@utils/notification';
import { RawDraftContentState } from 'draft-js';
import Link from 'next/link';
import { FormEvent, MouseEvent, useState } from 'react';
import { Field } from 'react-final-form';

const required = (value: any) => (value ? undefined : 'Required');

type TestVersionEntryProps = {
  status: TestVersionBase['status'];
  version: TestVersionBase['version'];
  versionMessage: TestVersionBase['versionMessage'];
  testId: string;
  onRelease: (event: MouseEvent) => void;
  revalidate?: () => void;
};

function TestVersionEntry({
  status,
  version,
  versionMessage,
  onRelease,
  testId,
  revalidate,
}: TestVersionEntryProps) {
  const [anchorEl, setAnchorEl] = useState<Element | null>(null);
  const [add] = useNotifications();

  function handleOpen(e: MouseEvent) {
    setAnchorEl(e.currentTarget);
  }

  function handleClose() {
    setAnchorEl(null);
  }

  function handleStatus(status: TestVersionBase['status']) {
    // call api
    putTestVersionStatus(null, testId, version, status)
      .then(() => {
        add({
          message: t`Status changed`,
          severity: 'success',
        });
        if (revalidate) {
          revalidate();
        }
      })
      .catch(() => {
        add({
          message: t`Setting Status failed`,
          severity: 'error',
        });
      });
  }

  return (
    <ListItem divider>
      <ListItemIcon>
        <Typography id={status === 'draft' ? 'version-status' : undefined}>
          {status}
        </Typography>
      </ListItemIcon>
      <ListItemText primary={versionMessage} secondary={`Version ${version}`} />
      <ListItemSecondaryAction>
        {status === 'draft' ? (
          <>
            <Button
              style={{ marginRight: 8 }}
              onClick={onRelease}
              id="version-release"
              variant="outlined"
            >
              <Trans>Release</Trans>
            </Button>
            <Link href="/test/[id]/edit-draft" as={`/test/${test}/edit-draft`}>
              <Button id="version-edit" variant="outlined" color="primary">
                <Trans>Edit</Trans>
              </Button>
            </Link>
          </>
        ) : (
          <>
            <Link
              href={`/test/[id]?version=${version}`}
              as={`/test/${test}?version=${version}`}
            >
              <IconButton>
                <RemoveRedEyeIcon />
              </IconButton>
            </Link>
            <Button
              variant="outlined"
              aria-haspopup="true"
              onClick={handleOpen}
            >
              Set Status
            </Button>
            <Menu
              id={`${version}-status`}
              anchorEl={anchorEl}
              open={Boolean(anchorEl)}
              onClose={handleClose}
            >
              <MenuItem
                selected={'pilot' === status}
                onClick={() => handleStatus('pilot')}
              >
                Pilot
              </MenuItem>
              <MenuItem
                selected={'ready' === status}
                onClick={() => handleStatus('ready')}
              >
                Ready
              </MenuItem>
              <MenuItem
                selected={'faulty' === status}
                onClick={() => handleStatus('faulty')}
              >
                Faulty
              </MenuItem>
            </Menu>
          </>
        )}
      </ListItemSecondaryAction>
    </ListItem>
  );
}

const steps = [
  {
    target: '#name',
    content: t`Here you can give your test a name`,
  },
  {
    target: '#privacy',
    content: t`Determine if you want to share your test with the public`,
  },
  {
    target: '#description',
    content: t`Describe what one can except of your test`,
  },
  {
    target: '#version-status',
    content: t`Here you can see the status of the versions`,
  },
  {
    target: '#version-message',
    content: t`Here you can see the release message of the versions`,
  },
  {
    target: '#version-release',
    content: t`If you are happy with your draft you can release it`,
  },
  {
    target: '#version-edit',
    content: t`Edit the draft of your item here`,
  },
];

export type TestMetaFormProps = {
  initialValues?: Test;
  revalidate?: () => void;
} & Pick<
  FormProps<
    Pick<Test, 'id' | 'versions' | 'name' | 'privacy' | 'publicDescription'>
  >,
  'onSubmit' | 'submitVariant'
>;

function TestMetaForm({
  onSubmit,
  initialValues,
  submitVariant = 'fab',
  revalidate,
  ...props
}: TestMetaFormProps) {
  const [versionMessage, setVersionMessage] = useState('');
  const [showReleaseDialog, setShowReleaseDialog] = useState(false);
  const [releasing, setReleasing] = useState(false);
  const [add] = useNotifications();

  function handleRelease() {
    setReleasing(true);
    if (initialValues?.id) {
      postTestVersion(null, initialValues.id, versionMessage)
        .then(() => {
          setShowReleaseDialog(false);
          add({
            message: t`New version released`,
            severity: 'success',
          });
          if (revalidate) {
            revalidate();
          }
          setReleasing(false);
        })
        .catch(() => {
          setShowReleaseDialog(false);
          add({
            message: t`Something went wrong`,
            severity: 'error',
          });
          setReleasing(false);
        });
    }
  }

  return (
    <>
      <Joyride steps={steps} name="TestMetaForm" />
      <Form
        onSubmit={onSubmit}
        submitVariant={submitVariant}
        initialValues={initialValues}
        render={() => (
          <>
            <FormSection>
              <FormSectionHeader>{t`Meta`}</FormSectionHeader>
              <FormSectionContent>
                <Field<string>
                  name="name"
                  id="name"
                  label={t`Name`}
                  validate={required}
                  fullWidth
                  component={TextField}
                />
                <Field<Test['privacy']>
                  name="privacy"
                  id="privacy"
                  defaultValue="private"
                  fullWidth
                  component={PrivacySelectField}
                />
                <Field<RawDraftContentState>
                  name="publicDescription"
                  id="description"
                  label={t`Description`}
                  fullWidth
                  deactivate={[
                    'link',
                    'image',
                    'embed',
                    'font-size',
                    'format-color-text',
                    'format-color-fill',
                    'code-block',
                    'ordered-list',
                    'unordered-list',
                    'blockquote',
                  ]}
                  component={RichTextField}
                />
              </FormSectionContent>
            </FormSection>
            {initialValues && (
              <FormSection>
                <FormSectionHeader>
                  <Trans>Versions</Trans>
                </FormSectionHeader>
                <FormSectionContent>
                  <List style={{ width: '100%' }}>
                    {initialValues?.versions
                      ? initialValues?.versions.map((version) => (
                          <TestVersionEntry
                            key={version.version}
                            {...version}
                            revalidate={revalidate}
                            onRelease={() => setShowReleaseDialog(true)}
                          />
                        ))
                      : null}
                  </List>
                </FormSectionContent>
              </FormSection>
            )}
          </>
        )}
        {...props}
      ></Form>
      <Dialog open={showReleaseDialog}>
        <DialogTitle>
          <Trans>Release Draft</Trans>
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            <Trans>
              Release the current draft. Once it is released the content can not
              be changed anymore.
            </Trans>
          </DialogContentText>
          <TextField
            meta={{
              touched: false,
              error: null,
            }}
            label={t`Version Message`}
            multiline
            rows={4}
            fullWidth
            input={{
              name: 'version-message',
              value: versionMessage,
              onBlur: () => null,
              onFocus: () => null,
              onChange: (e: FormEvent<HTMLInputElement>) =>
                setVersionMessage(e.currentTarget.value),
            }}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleRelease} disabled={releasing} color="primary">
            <Trans>Release</Trans>
          </Button>
          <Button onClick={() => setShowReleaseDialog(false)}>
            <Trans>Cancel</Trans>
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}

export default TestMetaForm;
