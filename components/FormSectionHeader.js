import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

const styles = (theme) => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
    justifyContent: 'flex-start',
    [theme.breakpoints.down('sm')]: {
      marginBottom: theme.spacing(1),
    },
  },
});

const FormSectionHeader = ({ classes, caption, ...props }) => (
  <div className={classes.container}>
    <Typography variant="h6" component="div" {...props} />
    {caption && <Typography variant="caption">{caption}</Typography>}
  </div>
);

FormSectionHeader.propTypes = {
  classes: PropTypes.object.isRequired,
  caption: PropTypes.string,
};

FormSectionHeader.defaultProps = {
  caption: null,
};

export default withStyles(styles)(FormSectionHeader);
