import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import { useTask } from '@services/assessment/session';
import { SessionItem } from '@services/assessment/session/items';
import { ItemTask } from '@services/itembank/items';
import { convertFromRaw } from '@utils/editor';
import _isEqual from 'deep-equal';
import dynamic from 'next/dynamic';
import { memo } from 'react';
import { RichTextProps } from './RichText';

const RichText = dynamic<RichTextProps>(() => import('./RichText'));
const Renderer = dynamic<any>(() =>
  import('../formats').then((m) => m.Renderer)
);

const useStyles = makeStyles((theme) => ({
  container: {
    marginBottom: theme.spacing(3),
  },
  header: {
    padding: theme.spacing(3),
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.primary.contrastText,
    textAlign: 'center',
  },
  body: {
    padding: theme.spacing(3),
  },
}));

type TaskRendererProps = {
  itemId: number;
  taskId: number;
  variant?: 'normal' | 'player' | 'result';
} & Pick<ItemTask, 'formatType' | 'data' | 'task' | 'text'>;

function TaskRenderer({
  itemId,
  taskId,
  data,
  formatType,
  task,
  text,
  variant = 'normal',
}: TaskRendererProps) {
  const { state, dispatch } = useTask(itemId, taskId);
  const classes = useStyles();

  return (
    <Paper className={classes.container}>
      {task && <Box className={classes.header}>{task}</Box>}
      <Box className={classes.body}>
        {text && (
          <Box mb={2}>
            <RichText editorState={convertFromRaw(text)} />
          </Box>
        )}
        <Renderer
          type={formatType}
          {...data}
          state={state}
          dispatch={dispatch}
          variant={variant}
        />
      </Box>
    </Paper>
  );
}

export type ItemVersionRendererProps = {
  index: number;
  tasks: SessionItem['tasks'];
  variant?: TaskRendererProps['variant'];
};

const ItemVersionRenderer = memo<ItemVersionRendererProps>(
  ({ index, tasks, variant }) => {
    return (
      <>
        {tasks.map((task, taskIndex) => (
          <TaskRenderer
            key={taskIndex}
            variant={variant}
            itemId={index}
            taskId={taskIndex}
            {...task}
          />
        ))}
      </>
    );
  },
  _isEqual
);

ItemVersionRenderer.displayName = 'ItemVersionRenderer';

export default ItemVersionRenderer;
