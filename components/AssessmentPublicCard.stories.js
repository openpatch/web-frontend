import { boolean } from '@storybook/addon-knobs';
import { getLoremIpsum } from '../utils/editor';
import { AssessmentPublicCard } from './AssessmentPublicCard';

export default {
  title: 'AssessmentPublicCard',
  component: AssessmentPublicCard
};

export const Default = () => (
  <AssessmentPublicCard
    id="some-uuid"
    name="My Assessment"
    passwordProtected={boolean('passwordProtected', true)}
    encrypt={boolean('encrypt', false)}
    record={boolean('record', false)}
    member={{
      username: 'some-user'
    }}
    publicDescription={getLoremIpsum()}
    createdOn={new Date().toString()}
  />
);
