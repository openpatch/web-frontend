import { Trans } from '@lingui/macro';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import CancelIcon from '@material-ui/icons/Cancel';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import SecurityIcon from '@material-ui/icons/Security';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';

import { useDateUtils } from '../utils/date';
import { round } from '../utils/math';

function SessionResult({ itemResults, score, aborted, onShow }) {
  const { formatDistance } = useDateUtils();
  const percentage = (score / itemResults.length) * 100;
  const encrypted = itemResults.find((ir) => ir.encrypted) !== undefined;

  function handleShow(itemId, itemVersion, solution, evaluation) {
    onShow(itemId, itemVersion, solution, evaluation);
  }

  return (
    <div>
      <Typography variant="h2" align="center">
        <Trans>Result</Trans>
      </Typography>
      <Typography variant="h3" align="center">
        <Trans>{round(percentage)}% correct</Trans>
      </Typography>
      {encrypted && (
        <>
          <Typography variant="subtitle1" align="center">
            <Trans>
              Encrypted tasks are not evaluated, but will be counted as correct.
            </Trans>
          </Typography>
        </>
      )}
      <Grid container spacing={2} style={{ marginTop: 8 }}>
        {itemResults.map(
          ({
            id,
            start,
            end,
            correct,
            evaluation,
            solution,
            encrypted,
            itemId,
            itemVersion,
          }) => (
            <Grid xs={12} item key={id}>
              <Paper
                style={{ padding: 16, display: 'flex', alignItems: 'center' }}
              >
                {encrypted ? (
                  <Tooltip title={<Trans>Encrypted</Trans>}>
                    <SecurityIcon fontSize="large" />
                  </Tooltip>
                ) : correct ? (
                  <Tooltip title={<Trans>Correct</Trans>}>
                    <CheckCircleIcon
                      style={{ color: 'green' }}
                      fontSize="large"
                    />
                  </Tooltip>
                ) : (
                  <Tooltip title={<Trans>Wrong</Trans>}>
                    <CancelIcon style={{ color: 'red' }} fontSize="large" />
                  </Tooltip>
                )}
                <Box flex={2} mr={1} ml={1} textAlign="center">
                  {end && start ? (
                    <Typography align="center">
                      <Trans>
                        Solved in{' '}
                        {formatDistance(new Date(end), new Date(start))}
                      </Trans>
                    </Typography>
                  ) : null}
                </Box>
                <Box flex={1}>
                  {encrypted ? (
                    <Button
                      fullWidth
                      variant="outlined"
                      onClick={() => handleShow(itemId, itemVersion)}
                    >
                      <Trans>Show Task</Trans>
                    </Button>
                  ) : (
                    <Button
                      fullWidth
                      variant="outlined"
                      onClick={() =>
                        handleShow(itemId, itemVersion, solution, evaluation)
                      }
                    >
                      <Trans>Show my Solution</Trans>
                    </Button>
                  )}
                </Box>
              </Paper>
            </Grid>
          )
        )}
        {aborted && (
          <Grid item xs={12}>
            <Paper style={{ padding: 16 }}>
              <Typography align="center">
                <Trans>
                  ... you have aborted the assessment. Therefore, some tasks are
                  missing.
                </Trans>
              </Typography>
            </Paper>
          </Grid>
        )}
      </Grid>
    </div>
  );
}

SessionResult.propTypes = {
  itemResults: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      evaluation: PropTypes.object,
      solution: PropTypes.object,
      encrypted: PropTypes.bool,
      start: PropTypes.string,
      end: PropTypes.string,
      correct: PropTypes.bool,
      assessmentId: PropTypes.string,
      itemId: PropTypes.string,
      itemVersion: PropTypes.number,
    })
  ),
  itemVersions: PropTypes.array,
  score: PropTypes.number,
  aborted: PropTypes.bool,
  onShow: PropTypes.func,
};

SessionResult.defaultProps = {
  itemResults: [],
  itemVersions: [],
  score: 0,
  aborted: false,
};

export default SessionResult;
