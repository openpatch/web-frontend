import Stats from './Stats';

export default {
  title: 'Stats',
  component: Stats,
};

export const Default = () => <Stats items={2031} tests={390} members={4} />;
