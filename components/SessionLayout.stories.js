import SessionLayout from './SessionLayout';

export default {
  title: 'Layout/SessionLayout',
  component: SessionLayout
};

export const Default = () => <SessionLayout />;

export const WithToolbar = () => (
  <SessionLayout
    toolbarTop="Toolbar Top"
    toolbarBottom={{
      left: 'left',
      center: 'center',
      right: 'right'
    }}
  />
);

