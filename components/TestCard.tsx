import Box from '@material-ui/core/Box';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import { styled } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { Test } from '@services/itembank/tests';
import Link from 'next/link';
import { CSSProperties } from 'react';
import { useDateUtils } from '../utils/date';
import { convertFromRaw } from '../utils/editor';
import MemberAvatar from './MemberAvatar';
import PrivacyIndicator from './PrivacyIndicator';
import RichText from './RichText';

export type TestCardProps = Pick<
  Test,
  'id' | 'name' | 'privacy' | 'publicDescription' | 'member' | 'createdOn'
> & {
  style?: CSSProperties;
};

const ClickableCard = styled(Card)({
  cursor: 'pointer',
});

function TestCard({
  id,
  name,
  privacy,
  publicDescription,
  member,
  style,
  createdOn,
}: TestCardProps) {
  const { formatDistance, utcDate } = useDateUtils();
  return (
    <Link href={`/test/[id]`} as={`/test/${id}`}>
      <ClickableCard style={style}>
        <CardHeader
          avatar={<MemberAvatar {...member} />}
          title={name}
          subheader={
            <Box display="flex" alignItems="center" flexWrap="wrap">
              <PrivacyIndicator privacy={privacy} />
              <Typography
                variant="caption"
                style={{ marginRight: 4, marginLeft: 4 }}
              >
                •
              </Typography>
              {member?.username}
              <Typography
                variant="caption"
                style={{ marginRight: 4, marginLeft: 4 }}
              >
                •
              </Typography>
              {formatDistance(new Date(createdOn), utcDate())}
            </Box>
          }
        />
        <CardContent>
          <RichText editorState={convertFromRaw(publicDescription)} />
        </CardContent>
      </ClickableCard>
    </Link>
  );
}

export default TestCard;
