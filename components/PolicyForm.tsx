import { Trans } from '@lingui/macro';
import { Policy } from '@services/authentification/policies';
import { Field } from 'react-final-form';
import Checkbox from './fields/Checkbox';
import RichTextField from './fields/RichTextField';
import TextField from './fields/TextField';
import Form, { FormProps } from './Form';

export type PolicyFormProps = {
  initialValues?: Policy;
} & Pick<
  FormProps<Pick<Policy, 'draft' | 'language' | 'type' | 'content'>>,
  'onSubmit' | 'submitVariant'
>;

function PolicyForm({
  initialValues,
  onSubmit,
  submitVariant,
}: PolicyFormProps) {
  return (
    <Form
      initialValues={initialValues}
      onSubmit={onSubmit}
      submitVariant={submitVariant}
      render={() => (
        <>
          <Field<Policy['draft']>
            name="draft"
            label={<Trans>Draft</Trans>}
            fullWidth
            type="checkbox"
            defaultValue={true}
            component={Checkbox}
          />
          <Field<Policy['language']>
            name="language"
            label={<Trans>Language</Trans>}
            fullWidth
            defaultValue="en"
            component={TextField}
          />
          <Field<Policy['type']>
            name="type"
            label={<Trans>Type</Trans>}
            fullWidth
            defaultValue="privacy"
            component={TextField}
          />
          <Field<Policy['content']>
            name="content"
            label={<Trans>Content</Trans>}
            component={RichTextField}
          />
        </>
      )}
    />
  );
}

export default PolicyForm;
