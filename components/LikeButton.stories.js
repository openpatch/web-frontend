import { number, boolean } from '@storybook/addon-knobs';
import { LikeButtonView } from './LikeButton';

export default {
  title: 'LikeButton',
  component: LikeButtonView,
};

export const Default = () => (
  <LikeButtonView
    likes={number('likes', 20)}
    isLiked={boolean('isLiked', false)}
  />
);
