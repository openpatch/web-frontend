import { select } from '@storybook/addon-knobs';
import ItemCard from './ItemCard';

export default {
  title: 'ItemCard',
  component: ItemCard,
};

export const Default = () => (
  <ItemCard
    id="mock-item"
    name="Mock Item"
    privacy={select('privacy', ['public', 'private', 'notlisted'])}
    tags={[]}
    createdOn={new Date().toISOString()}
    member={{
      id: 'mock-member',
    }}
  />
);
