import { Trans } from '@lingui/macro';
import Box from '@material-ui/core/Box';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Frame } from '@services/recorder/recordings';
import { colorHash } from '@services/recorder/utils';

function dateToTime(date: Date) {
  return `${date
    .getMinutes()
    .toString()
    .padStart(2, '0')}:${date.getSeconds().toString().padStart(2, '0')}`;
}
function truncateString(str: string, num: number) {
  if (str.length > num) {
    return str.slice(0, num) + '...';
  } else {
    return str;
  }
}

export type RecordingTranscriptProps = {
  frames: Frame[];
};

function RecordingTranscript({ frames }: RecordingTranscriptProps) {
  return (
    <Table>
      <TableHead>
        <TableRow>
          <TableCell>
            <Trans>Time</Trans>
          </TableCell>
          <TableCell>
            <Trans>Item</Trans>
          </TableCell>
          <TableCell>
            <Trans>Task</Trans>
          </TableCell>
          <TableCell>
            <Trans>Type</Trans>
          </TableCell>
          <TableCell>
            <Trans>Payload</Trans>
          </TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {frames.map(({ id, itemId, payload, taskId, type, timestamp }) => (
          <TableRow key={id}>
            <TableCell>{dateToTime(new Date(timestamp))}</TableCell>
            <TableCell>
              {itemId !== undefined && <Trans>Item {itemId + 1}</Trans>}
            </TableCell>
            <TableCell>
              {taskId !== undefined && <Trans>Task {taskId + 1}</Trans>}
            </TableCell>
            <TableCell>
              <Box
                bgcolor={colorHash(type)}
                borderRadius={8}
                color="rgba(0, 0, 0, 0.87)"
                p={1}
              >
                {type}
              </Box>
            </TableCell>
            <TableCell>
              {truncateString(JSON.stringify(payload), 100)}
            </TableCell>
          </TableRow>
        ))}
      </TableBody>
    </Table>
  );
}

export default RecordingTranscript;
