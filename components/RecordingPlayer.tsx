import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';
import { useTheme } from '@material-ui/core/styles';
import { ItemVersionCache } from '@services/assessment/assessments';
import { useSession } from '@services/assessment/session';
import { Action } from '@services/assessment/session/actions';
import { Frame, Recording } from '@services/recorder/recordings';
import { useCallback, useEffect, useState } from 'react';
import { useAnimationFrame } from '../utils/animationFrame';
import ItemsRenderer from './ItemsRenderer';
import RecordingPlayerControls from './RecordingPlayerControls';
import RecordingPlayerTimeline from './RecordingPlayerTimeline';

export type RecordingPlayerProps = {
  items: ItemVersionCache[];
  recording: Recording;
};

function RecordingPlayer({ items, recording }: RecordingPlayerProps) {
  const { dispatch, handleReset: reset } = useSession();
  const theme = useTheme();
  const [currentTime, setCurrentTime] = useState(0);
  const [deltaTime, setDeltaTime] = useState(0);
  const [isPlaying, setIsPlaying] = useState(false);
  const [endTime, setEndTime] = useState(0);
  const [currentFrameIndex, setCurrentFrameIndex] = useState(0);

  useEffect(() => {
    if (recording) {
      const lastFrame = recording.frames.reduce((a, b) =>
        a.timestamp > b.timestamp ? a : b
      );
      setEndTime(lastFrame.timestamp);
    }
  }, [recording]);

  useAnimationFrame(setDeltaTime);

  useEffect(() => {
    if (isPlaying && currentTime < endTime) {
      setCurrentTime((prevTime) => Math.min(prevTime + deltaTime, endTime));
    } else if (isPlaying) {
      setCurrentTime(endTime);
      setIsPlaying(false);
    }
  }, [deltaTime, endTime]);

  useEffect(() => {
    if (isPlaying && currentFrameIndex < recording.frames.length) {
      const frame = recording.frames[currentFrameIndex];
      if (frame.timestamp <= currentTime) {
        dispatchFrame(frame);
        setCurrentFrameIndex((f) => f + 1);
      }
    }
  }, [currentTime, currentFrameIndex, dispatch]);

  const handlePlay = useCallback(() => {
    setIsPlaying(true);
  }, []);

  const handlePause = useCallback(() => {
    setIsPlaying(false);
  }, []);

  const dispatchFrame = useCallback(
    (frame: Frame) => {
      const [scope, type] = frame.type.split('$');
      if (scope === 'SESSION') {
        dispatch(({
          payload: frame.payload,
          scope: 'SESSION',
          type,
        } as unknown) as Action);
      } else if (scope === 'FORMAT' && frame.itemId && frame.taskId) {
        dispatch({
          payload: frame.payload,
          itemId: frame.itemId,
          taskId: frame.taskId,
          scope: 'FORMAT',
          type,
        });
      }
    },
    [dispatch]
  );

  const handleSkip = useCallback(
    (time) => {
      setIsPlaying(false);
      reset();
      let frameIndex = 0;
      let frame = recording.frames[frameIndex];
      while (frame && frame.timestamp <= time) {
        dispatchFrame(frame);
        frame = recording.frames[frameIndex];
        frameIndex += 1;
      }
      setCurrentTime(time);
      setCurrentFrameIndex(() =>
        Math.min(recording.frames.length - 1, Math.max(frameIndex - 1, 0))
      );
    },
    [dispatch, reset, currentFrameIndex, recording]
  );

  const handleStop = useCallback(() => {
    setIsPlaying(false);
    setCurrentTime(0);
    setCurrentFrameIndex(0);
    reset();
  }, []);

  return (
    <Paper style={{ maxWidth: 1280, margin: '0 auto' }}>
      <Box bgcolor={theme.palette.grey['900']}>
        <RecordingPlayerControls
          isPlaying={isPlaying}
          currentTime={currentTime}
          endTime={endTime}
          onPlay={handlePlay}
          onPause={handlePause}
          onStop={handleStop}
        />
        <RecordingPlayerTimeline
          onClick={handleSkip}
          frames={recording?.frames}
          currentTime={currentTime}
          endTime={endTime}
        ></RecordingPlayerTimeline>
      </Box>
      <div
        onClickCapture={(e) => e.stopPropagation()}
        style={{ position: 'relative' }}
      >
        <ItemsRenderer items={items} variant="player" />
      </div>
    </Paper>
  );
}

export default RecordingPlayer;
