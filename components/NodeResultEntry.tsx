import Box from '@material-ui/core/Box';
import IconButton from '@material-ui/core/IconButton';
import Paper from '@material-ui/core/Paper';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';
import CancelIcon from '@material-ui/icons/Cancel';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import VideocamIcon from '@material-ui/icons/Videocam';
import { ItemResult } from '@services/assessment/itemResults';
import { NodeResult } from '@services/assessment/nodeResults';
import { useDateUtils } from '@utils/date';
import Link from 'next/link';
import Router from 'next/router';
import ItemResultEntry from './ItemResultEntry';

type LinearNodeResultEntryProps = {
  name: string;
  encrypted: boolean;
  score: number;
  assessmentId: string;
  itemResults: ItemResult[];
};
function LinearNodeResultEntry({
  name,
  itemResults,
  assessmentId,
}: LinearNodeResultEntryProps) {
  return (
    <>
      <Typography>{name}</Typography>
      {itemResults.map((i) => (
        <ItemResultEntry key={i.id} {...i} assessmentId={assessmentId} />
      ))}
    </>
  );
}

type JumpableNodeResultEntryProps = {
  name: string;
  assessmentId: string;
  id: string;
  start: string;
  end: string;
  recorded: boolean;
  itemResults: ItemResult[];
};

function JumpableNodeResultEntry({
  assessmentId,
  name,
  id,
  start,
  end,
  recorded,
  itemResults,
}: JumpableNodeResultEntryProps) {
  const { format, formatDistance } = useDateUtils();
  return (
    <>
      <Box display="flex">
        <Box flex={1}>
          <Typography>{name}</Typography>
        </Box>
        <Box flex={2} textAlign="center">
          {start ? format(new Date(start), 'dd. LLL yy, HH:mm') : null}
        </Box>
        <Box flex={2} textAlign="center">
          {end ? format(new Date(end), 'dd. LLL yy, HH:mm') : null}
        </Box>
        <Box flex={2} textAlign="center">
          {end && start ? formatDistance(new Date(end), new Date(start)) : null}
        </Box>
        <Box textAlign="center">
          {recorded ? (
            <Link
              href="/assessment/[id]/node-result/[nid]/recording"
              as={`/assessment/${assessmentId}/node-result/${id}/recording`}
            >
              <IconButton>
                <VideocamIcon />
              </IconButton>
            </Link>
          ) : null}
        </Box>
      </Box>
      <Box display="flex" justifyContent="center">
        {itemResults.map((itemResult, i) => (
          <Box key={itemResult.id}>
            <Tooltip title={`Item ${i + 1} Result`}>
              <Box
                textAlign="center"
                style={{ cursor: 'pointer' }}
                aria-label="Show Solution"
                onClick={() =>
                  Router.push(
                    `/assessment/[id]/item-result/[iid]`,
                    `/assessment/${assessmentId}/item-result/${itemResult?.id}`
                  )
                }
              >
                {itemResult?.correct ? (
                  <CheckCircleIcon style={{ color: 'green' }} />
                ) : (
                  <CancelIcon style={{ color: 'red' }} />
                )}
                <Box display="flex" alignItems="center" justifyContent="center">
                  {itemResult?.evaluation?.results &&
                    Object.values(
                      itemResult?.evaluation.results
                    ).map((result, index) => (
                      <Box
                        key={index}
                        width={10}
                        m={0.2}
                        borderRadius={5}
                        height={10}
                        bgcolor={result.correct ? 'green' : 'red'}
                      />
                    ))}
                </Box>
              </Box>
            </Tooltip>
          </Box>
        ))}
      </Box>
    </>
  );
}

export type NodeResultEntryProps = {
  assessmentId: string;
  node?: {
    flow?: 'linear' | 'jumpable';
    name: string;
  };
} & NodeResult;

function NodeResultEntry({ node, ...props }: NodeResultEntryProps) {
  if (!node) {
    return null;
  }
  if (node.flow === 'jumpable') {
    return (
      <Paper style={{ marginTop: 16, padding: 16 }}>
        <JumpableNodeResultEntry name={node.name} {...props} />
      </Paper>
    );
  } else {
    return (
      <Paper style={{ marginTop: 16, padding: 16 }}>
        <LinearNodeResultEntry name={node.name} {...props} />
      </Paper>
    );
  }
}

export default NodeResultEntry;
