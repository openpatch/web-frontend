import { t, Trans } from '@lingui/macro';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Typography from '@material-ui/core/Typography';
import RemoveRedEyeIcon from '@material-ui/icons/RemoveRedEye';
import { Item, ItemVersion, postItemVersion } from '@services/itembank/items';
import Link from 'next/link';
import { MouseEvent, useState } from 'react';
import { Field, FormProps } from 'react-final-form';
import api from '../utils/api';
import { languages } from '../utils/i18n';
import { useNotifications } from '../utils/notification';
import PrivacySelectField from './fields/PrivacySelectField';
import RichTextField from './fields/RichTextField';
import SelectField from './fields/SelectField';
import TextField from './fields/TextField';
import Form from './Form';
import FormSection from './FormSection';
import FormSectionContent from './FormSectionContent';
import FormSectionHeader from './FormSectionHeader';
import Joyride from './Joyride';

const required = (value: any) => (value ? undefined : 'Required');

type ItemVersionEntryProps = {
  status: ItemVersion['status'];
  version: ItemVersion['version'];
  versionMessage: ItemVersion['versionMessage'];
  item: string;
  onRelease: (event: MouseEvent) => void;
  revalidate?: () => void;
};

function ItemVersionEntry({
  status,
  version,
  versionMessage,
  item,
  revalidate,
  onRelease,
}: ItemVersionEntryProps) {
  const [anchorEl, setAnchorEl] = useState<Element | null>(null);
  const [add] = useNotifications();

  function handleOpen(e: MouseEvent) {
    setAnchorEl(e.currentTarget);
  }

  function handleClose() {
    setAnchorEl(null);
  }

  function handleStatus(status: ItemVersion['status']) {
    // call api
    api
      .put(
        `${process.env.NEXT_API_ITEMBANK_BROWSER}/v1/items/${item}/versions/${version}/status`,
        {
          status,
        }
      )
      .then(() => {
        add({
          message: t`Status changed`,
          severity: 'success',
        });
        if (revalidate) {
          revalidate();
        }
      })
      .catch(() => {
        add({
          message: t`Setting Status failed`,
          severity: 'error',
        });
      });
  }

  return (
    <ListItem divider>
      <ListItemIcon>
        <Typography id={status === 'draft' ? 'version-status' : undefined}>
          {status}
        </Typography>
      </ListItemIcon>
      <ListItemText primary={versionMessage} secondary={`Version ${version}`} />
      <ListItemSecondaryAction>
        {status === 'draft' ? (
          <>
            <Button
              style={{ marginRight: 8 }}
              onClick={onRelease}
              id="version-release"
              variant="outlined"
            >
              <Trans>Release</Trans>
            </Button>
            <Link href="/item/[id]/edit-draft" as={`/item/${item}/edit-draft`}>
              <Button id="version-edit" variant="outlined" color="primary">
                <Trans>Edit</Trans>
              </Button>
            </Link>
          </>
        ) : (
          <>
            <Link
              href={`/item/[id]?version=${version}`}
              as={`/item/${item}?version=${version}`}
            >
              <IconButton>
                <RemoveRedEyeIcon />
              </IconButton>
            </Link>
            <Button
              variant="outlined"
              aria-haspopup="true"
              onClick={handleOpen}
            >
              <Trans>Set Status</Trans>
            </Button>
            <Menu
              id={`${version}-status`}
              anchorEl={anchorEl}
              open={Boolean(anchorEl)}
              onClose={handleClose}
            >
              <MenuItem
                selected={'pilot' === status}
                onClick={() => handleStatus('pilot')}
              >
                <Trans>Pilot</Trans>
              </MenuItem>
              <MenuItem
                selected={'ready' === status}
                onClick={() => handleStatus('ready')}
              >
                <Trans>Ready</Trans>
              </MenuItem>
              <MenuItem
                selected={'faulty' === status}
                onClick={() => handleStatus('faulty')}
              >
                <Trans>Faulty</Trans>
              </MenuItem>
            </Menu>
          </>
        )}
      </ListItemSecondaryAction>
    </ListItem>
  );
}

const steps = [
  {
    target: '#name',
    content: t`Here you can give your item a name`,
  },
  {
    target: '#privacy',
    content: t`Determine if you want to share your item with the public`,
  },
  {
    target: '#description',
    content: t`Describe what one can except of your item`,
  },
  {
    target: '#version-status',
    content: t`Here you can see the status of the versions`,
  },
  {
    target: '#version-message',
    content: t`Here you can see the release message of the versions`,
  },
  {
    target: '#version-release',
    content: t`If you are happy with your draft you can release it`,
  },
  {
    target: '#version-edit',
    content: t`Edit the draft of your item here`,
  },
];

export type ItemMetaFormProps = {
  initialValues?: Item;
  revalidate?: () => void;
} & Pick<
  FormProps<
    Pick<
      Item,
      'id' | 'versions' | 'name' | 'privacy' | 'publicDescription' | 'language'
    >
  >,
  'onSubmit' | 'submitVariant'
>;

function ItemMetaForm({
  onSubmit,
  initialValues,
  submitVariant = 'fab',
  revalidate,
  ...props
}: ItemMetaFormProps) {
  const [versionMessage, setVersionMessage] = useState('');
  const [showReleaseDialog, setShowReleaseDialog] = useState(false);
  const [releasing, setReleasing] = useState(false);
  const [add] = useNotifications();

  function handleRelease() {
    setReleasing(true);
    if (initialValues?.id) {
      postItemVersion(null, initialValues.id, versionMessage)
        .then(() => {
          setShowReleaseDialog(false);
          add({
            message: t`New version released`,
            severity: 'success',
          });
          if (revalidate) {
            revalidate();
          }
          setReleasing(false);
        })
        .catch(() => {
          setShowReleaseDialog(false);
          add({
            message: t`Something went wrong`,
            severity: 'error',
          });
          setReleasing(false);
        });
    }
  }

  return (
    <>
      <Joyride steps={steps} name="ItemMetaForm" />
      <Form
        onSubmit={onSubmit}
        submitVariant={submitVariant}
        initialValues={initialValues}
        render={() => (
          <>
            <FormSection>
              <FormSectionHeader>{t`Meta`}</FormSectionHeader>
              <FormSectionContent>
                <Field<string>
                  name="name"
                  id="name"
                  label={t`Name`}
                  validate={required}
                  fullWidth
                  component={TextField}
                />
                <Field<Item['privacy']>
                  name="privacy"
                  id="privacy"
                  defaultValue="private"
                  fullWidth
                  component={PrivacySelectField}
                />
                <Field<Item['language']>
                  name="language"
                  id="language"
                  label={t`Language`}
                  fullWidth
                  defaultValue="en"
                  getText={(o: string) => languages[o]}
                  component={SelectField}
                  options={Object.keys(languages)}
                />
                <Field<Item['publicDescription']>
                  name="publicDescription"
                  id="description"
                  label={t`Description`}
                  fullWidth
                  deactivateFromToolbar={[
                    'link',
                    'image',
                    'embed',
                    'font-size',
                    'format-color-text',
                    'format-color-fill',
                    'code-block',
                    'ordered-list',
                    'unordered-list',
                    'blockquote',
                  ]}
                  component={RichTextField}
                />
              </FormSectionContent>
            </FormSection>
            {initialValues && (
              <FormSection>
                <FormSectionHeader>
                  <Trans>Versions</Trans>
                </FormSectionHeader>
                <FormSectionContent>
                  <List style={{ width: '100%' }}>
                    {initialValues?.versions
                      ? initialValues?.versions.map((version) => (
                          <ItemVersionEntry
                            key={version.version}
                            {...version}
                            revalidate={revalidate}
                            onRelease={() => setShowReleaseDialog(true)}
                          />
                        ))
                      : null}
                  </List>
                </FormSectionContent>
              </FormSection>
            )}
          </>
        )}
        {...props}
      ></Form>
      <Dialog open={showReleaseDialog}>
        <DialogTitle>
          <Trans>Release Draft</Trans>
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            <Trans>
              Release the current draft. Once it is released the content can not
              be changed anymore.
            </Trans>
          </DialogContentText>
          <TextField
            meta={{}}
            label={t`Version Message`}
            multiline
            rows={4}
            fullWidth
            input={{
              name: 'version-message',
              value: versionMessage,
              onBlur: () => null,
              onFocus: () => null,
              onChange: (e) => setVersionMessage(e.target.value),
            }}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleRelease} disabled={releasing} color="primary">
            <Trans>Release</Trans>
          </Button>
          <Button onClick={() => setShowReleaseDialog(false)}>
            <Trans>Cancel</Trans>
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}

export default ItemMetaForm;
