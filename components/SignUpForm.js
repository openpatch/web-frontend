import { useState } from 'react';
import { Trans } from '@lingui/macro';
import PropTypes from 'prop-types';
import Box from '@material-ui/core/Box';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Mail from 'mui-undraw/lib/Mail';
import OnlineParty from 'mui-undraw/lib/OnlineParty';

import ProgressButton from './ProgressButton';
import Logo from './Logo';
import Link from './Link';

function SignUpForm({ onSignUp, checkMail, invitationCode, loading }) {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [passwordRepeat, setPasswordRepeat] = useState('');
  const [email, setEmail] = useState('');

  function handleSignUp(e) {
    e.preventDefault();

    if (
      password === passwordRepeat &&
      password.length >= 8 &&
      password.length <= 64
    ) {
      onSignUp({ username, password, email });
    }
  }

  return (
    <Box width={400}>
      <Box mb={2} display="flex" alignItems="center" justifyContent="center">
        <Logo size="large" />
      </Box>
      <Card component="form" onSubmit={handleSignUp}>
        {!checkMail ? (
          <>
            <CardContent>
              {invitationCode && (
                <>
                  <OnlineParty />
                  <Typography align="center">
                    You are invited to OpenPatch. Enjoy!
                  </Typography>
                </>
              )}
              <TextField
                label={<Trans>Username</Trans>}
                variant="outlined"
                margin="dense"
                required
                fullWidth
                value={username}
                onChange={(e) => setUsername(e.target.value)}
              />
              <TextField
                label={<Trans>E-Mail</Trans>}
                variant="outlined"
                margin="dense"
                required
                fullWidth
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
              <TextField
                label={<Trans>Password</Trans>}
                variant="outlined"
                required
                margin="dense"
                fullWidth
                type="password"
                value={password}
                helperText={
                  <Trans>Needs to be between 8 and 64 characters long</Trans>
                }
                error={password.length < 8 || password.length >= 64}
                onChange={(e) => setPassword(e.target.value)}
              />
              <TextField
                label={<Trans>Password Repeat</Trans>}
                variant="outlined"
                margin="dense"
                fullWidth
                required
                type="password"
                value={passwordRepeat}
                error={passwordRepeat !== password}
                onChange={(e) => setPasswordRepeat(e.target.value)}
              />
            </CardContent>
            <CardActions>
              <ProgressButton
                type="submit"
                loading={loading}
                onClick={handleSignUp}
                color="primary"
                fullWidth
              >
                <Trans>Sign Up</Trans>
              </ProgressButton>
            </CardActions>
          </>
        ) : (
          <CardContent>
            <Mail />
            <Typography variant="subtitle1" align="center">
              <Trans>
                Check your E-Mail and follow the link. We wish you much joy in
                our community.
              </Trans>
            </Typography>
          </CardContent>
        )}
      </Card>
      {!checkMail && (
        <Box mt={2}>
          <Link href="/login">
            <Typography
              style={{ color: 'white', paddingTop: 4, cursor: 'pointer' }}
              align="center"
              as="a"
            >
              <Trans>Already have an account? Sign In</Trans>
            </Typography>
          </Link>
        </Box>
      )}
    </Box>
  );
}

SignUpForm.propTypes = {
  loading: PropTypes.bool,
  onSignUp: PropTypes.func.isRequired,
  invitationCode: PropTypes.string,
  checkMail: PropTypes.bool,
};

export default SignUpForm;
