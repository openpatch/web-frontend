import MuiLink, { LinkProps as MuiLinkProps } from '@material-ui/core/Link';
import NextLink, { LinkProps as NextLinkProps } from 'next/link';

export type LinkProps = Omit<NextLinkProps, 'passHref'> & MuiLinkProps;
function Link({
  href,
  as,
  prefetch,
  replace,
  scroll,
  shallow,
  locale,
  children,
  ...props
}: LinkProps) {
  return (
    <NextLink
      href={href}
      passHref
      as={as}
      prefetch={prefetch}
      replace={replace}
      scroll={scroll}
      shallow={shallow}
      locale={locale}
    >
      <MuiLink {...props}>{children}</MuiLink>
    </NextLink>
  );
}

export default Link;
