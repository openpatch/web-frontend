import { t, Trans } from '@lingui/macro';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Typography from '@material-ui/core/Typography';
import { Assessment } from '@services/assessment/assessments';
import { castLocale } from '@services/assessment/session/utils';
import {
  GetTestsResponse,
  GetTestVersionsResponse,
  Test,
  TestVersion,
} from '@services/itembank/tests';
import { languages } from '@utils/i18n';
import PropTypes from 'prop-types';
import { useState } from 'react';
import { Field, FormSpy } from 'react-final-form';
import AutocompleteTextField from './fields/AutocompleteTextField';
import Checkbox from './fields/Checkbox';
import DateTimeField from './fields/DateTimeField';
import RichTextField from './fields/RichTextField';
import SelectField from './fields/SelectField';
import TextField from './fields/TextField';
import Form, { FormProps } from './Form';
import FormSection from './FormSection';
import FormSectionContent from './FormSectionContent';
import FormSectionHeader from './FormSectionHeader';
import I18nProvider from './I18nProvider';
import Joyride from './Joyride';
import SessionDataProtection from './SessionDataProtection';

const required = (value: any) => (value ? undefined : 'Required');

const steps = [
  {
    target: '#meta',
    content: t`Here you can set same meta data about your test`,
  },
  {
    target: '#messages',
    content: t`Here you can set messages which will be displayed at the begin and the end of your test`,
  },
  {
    target: '#settings',
    content: t`Here you can set some settings`,
  },
];

function DataProtectionPreview() {
  const [open, setOpen] = useState(false);
  return (
    <>
      <Button onClick={() => setOpen(true)}>
        <Trans>Preview Data Protection</Trans>
      </Button>
      <Dialog open={open} onClose={() => setOpen(false)}>
        <DialogTitle>Data Protection Preview</DialogTitle>
        <DialogContent dividers>
          <FormSpy<AssessmentFormValues>>
            {({ values }) => (
              <I18nProvider locale={castLocale(values.language)}>
                <SessionDataProtection
                  {...values}
                  accept={false}
                  onAccept={() => null}
                  encrypt={
                    values?.encryptionPublicKey !== undefined ||
                    values?.encryptionPassword !== undefined
                  }
                />
              </I18nProvider>
            )}
          </FormSpy>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setOpen(false)}>
            <Trans>Close</Trans>
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}

type AssessmentFormValues = Pick<
  Assessment,
  | 'name'
  | 'language'
  | 'public'
  | 'publicDescription'
  | 'testId'
  | 'testVersion'
  | 'greeting'
  | 'farewell'
  | 'dataProtection'
  | 'startsOn'
  | 'endsOn'
  | 'showResult'
  | 'record'
> & {
  password?: string;
  encryptionPassword?: string | null;
  encryptionPublicKey?: string | null;
  encryptionPrivateKey?: string | null;
};

export type AssessmentFormProps = {
  initialValues?: AssessmentFormValues;
  edit?: boolean;
  encryptionPublicKey?: string;
} & Pick<FormProps<AssessmentFormValues>, 'onSubmit' | 'submitVariant'>;

function AssessmentForm({
  onSubmit,
  initialValues,
  submitVariant,
  edit = false,
  encryptionPublicKey,
  ...props
}: AssessmentFormProps) {
  return (
    <Form
      onSubmit={onSubmit}
      submitVariant={submitVariant}
      initialValues={initialValues}
      render={({ form }) => (
        <>
          <Joyride steps={steps} name="AssessmentForm" />
          <FormSection id="meta">
            <FormSectionHeader>Meta</FormSectionHeader>
            <FormSectionContent>
              <Field<Assessment['name']>
                name="name"
                id="name"
                label={<Trans>Name</Trans>}
                validate={required}
                fullWidth
                component={TextField}
              />
              <Field<Assessment['language']>
                name="language"
                id="language"
                helperText={<Trans>Will set the language of the ui</Trans>}
                label={<Trans>Language</Trans>}
                validate={required}
                fullWidth
                defaultValue="en"
                getText={(o: string) => <Trans id={languages[o]} />}
                component={SelectField}
                options={Object.keys(languages)}
              />
              <Field<Assessment['public']>
                name="public"
                id="public"
                label={<Trans>Public</Trans>}
                helperText={<Trans>Will be shown on the front page</Trans>}
                type="checkbox"
                fullWidth
                component={Checkbox}
              />
              <Field<Assessment['publicDescription']>
                name="publicDescription"
                id="description"
                label={<Trans>Public Description</Trans>}
                helperText={
                  <Trans>
                    If you assessment is public, this will be shown to describe
                    it.
                  </Trans>
                }
                multiline
                fullWidth
                deactivateFromToolbar={[
                  'link',
                  'image',
                  'embed',
                  'font-size',
                  'format-color-text',
                  'format-color-fill',
                  'code-block',
                  'ordered-list',
                  'unordered-list',
                  'blockquote',
                ]}
                component={RichTextField}
              />
            </FormSectionContent>
          </FormSection>
          <FormSection>
            <FormSectionHeader>
              <Trans>Test Version</Trans>
            </FormSectionHeader>
            <FormSectionContent>
              <Field<Assessment['testId']>
                disabled={edit}
                name="testId"
                id="test"
                label={<Trans>Test</Trans>}
                fullWidth
                validate={required}
                render={(props) => (
                  <AutocompleteTextField<GetTestsResponse, Test>
                    {...props}
                    url={`${process.env.NEXT_API_ITEMBANK_BROWSER}/v1/tests`}
                    selector={(d) => d.tests}
                    getText={(o) => o.name}
                    getValue={(o) => o.id}
                  />
                )}
              />
              {form?.getFieldState('testId')?.value !== undefined ? (
                <Field<Assessment['testVersion']>
                  disabled={edit}
                  name="testVersion"
                  label={<Trans>Test Version</Trans>}
                  fullWidth
                  validate={required}
                  render={(props) => (
                    <AutocompleteTextField<GetTestVersionsResponse, TestVersion>
                      {...props}
                      getText={(o) =>
                        `v${o.version} - ${o.versionMessage} (${o.status})`
                      }
                      url={`${process.env.NEXT_API_ITEMBANK_BROWSER}/v1/tests/${
                        form.getFieldState('testId')?.value
                      }/versions`}
                      getValue={(o) => o.version}
                      selector={(d) => d.testVersions}
                    />
                  )}
                />
              ) : null}
            </FormSectionContent>
          </FormSection>
          <FormSection id="messages">
            <FormSectionHeader>
              <Trans>Messages</Trans>
            </FormSectionHeader>
            <FormSectionContent>
              <Field<Assessment['greeting']>
                name="greeting"
                id="greeting"
                label={<Trans>Greeting</Trans>}
                helperText={
                  <Trans>
                    Will be shown at the beginning of the assessment.
                  </Trans>
                }
                multiline
                fullWidth
                component={RichTextField}
              />
              <Field<Assessment['farewell']>
                name="farewell"
                id="farewell"
                label={<Trans>Farewell</Trans>}
                helperText={
                  <Trans>Will be shown at the end of the assessment.</Trans>
                }
                multiline
                fullWidth
                component={RichTextField}
              />
              <Field<Assessment['dataProtection']>
                name="dataProtection"
                id="data-protection"
                label={<Trans>Data Protection</Trans>}
                helperText={
                  <Trans>
                    Explain what you will do with the data. This will be shown
                    in conjunction of our data protection policy.
                  </Trans>
                }
                multiline
                fullWidth
                component={RichTextField}
              />
              <DataProtectionPreview />
            </FormSectionContent>
          </FormSection>
          <FormSection id="settings">
            <FormSectionHeader>
              <Trans>Settings</Trans>
            </FormSectionHeader>
            <FormSectionContent>
              <Field<Assessment['startsOn']>
                name="startsOn"
                id="starts-on"
                label={<Trans>Starts On</Trans>}
                fullWidth
                render={(props) => <DateTimeField {...props} />}
              />
              <Field<Assessment['endsOn']>
                name="endsOn"
                id="ends-on"
                label={<Trans>Ends On</Trans>}
                fullWidth
                render={(props) => <DateTimeField {...props} />}
              />
              <Field<Assessment['showResult']>
                name="showResult"
                id="show-result"
                label={<Trans>Show Result</Trans>}
                type="checkbox"
                fullWidth
                helperText={
                  <Trans>
                    Show the score and the solutions to user at the end of the
                    assessment. The correct solution will not be shown.
                  </Trans>
                }
                component={Checkbox}
              />
              <Field<string>
                name="password"
                id="password"
                label={<Trans>Password</Trans>}
                type="password"
                helperText={
                  <Trans>Protect the assessment with a password.</Trans>
                }
                fullWidth
                component={TextField}
              />
              {!edit && (
                <Field<string>
                  name="encryptionPassword"
                  id="encryption-password"
                  label={<Trans>Encryption Password</Trans>}
                  type="password"
                  helperText={
                    <Trans>
                      Encrypt results of Encrypt-Nodes, if the test has some.
                    </Trans>
                  }
                  fullWidth
                  component={TextField}
                />
              )}
              {encryptionPublicKey && (
                <Typography>
                  <Trans>
                    Encryption is setup. Results of Encrypt-Nodes will be
                    encrypted.
                  </Trans>
                </Typography>
              )}
              <Field<Assessment['record']>
                name="record"
                id="record"
                label={<Trans>Record</Trans>}
                type="checkbox"
                fullWidth
                helperText={
                  <Trans>Only non encrypted nodes will be recorded.</Trans>
                }
                component={Checkbox}
              />
            </FormSectionContent>
          </FormSection>
        </>
      )}
      {...props}
    />
  );
}

AssessmentForm.propTypes = {
  edit: PropTypes.bool,
  onSubmit: PropTypes.func,
  initialValues: PropTypes.object,
  encryptionPublicKey: PropTypes.string,
};

export default AssessmentForm;
