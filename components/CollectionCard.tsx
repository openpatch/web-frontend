import { Plural } from '@lingui/macro';
import Box from '@material-ui/core/Box';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import { styled } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { Collection } from '@services/itembank/collections';
import Link from 'next/link';
import { CSSProperties } from 'react';
import { useDateUtils } from '../utils/date';
import { convertFromRaw } from '../utils/editor';
import MemberAvatar from './MemberAvatar';
import PrivacyIndicator from './PrivacyIndicator';
import RichText from './RichText';

export type CollectionCardProps = Pick<
  Collection,
  | 'id'
  | 'name'
  | 'privacy'
  | 'publicDescription'
  | 'member'
  | 'tests'
  | 'items'
  | 'createdOn'
> & {
  style?: CSSProperties;
};

const ClickableCard = styled(Card)({
  cursor: 'pointer',
});

function CollectionCard({
  id,
  name,
  privacy,
  publicDescription,
  member,
  tests = [],
  items = [],
  style,
  createdOn,
}: CollectionCardProps) {
  const { formatDistance, utcDate } = useDateUtils();

  return (
    <Link href={`/collection/[id]`} as={`/collection/${id}`}>
      <ClickableCard style={style}>
        <CardHeader
          avatar={<MemberAvatar {...member} />}
          title={name}
          subheader={
            <Box display="flex" alignItems="center" flexWrap="wrap">
              <PrivacyIndicator privacy={privacy} />
              <Typography
                variant="caption"
                style={{ marginRight: 4, marginLeft: 4 }}
              >
                •
              </Typography>
              {member?.username}
              <Typography
                variant="caption"
                style={{ marginRight: 4, marginLeft: 4 }}
              >
                •
              </Typography>
              {formatDistance(new Date(createdOn), utcDate())}
              <Typography
                variant="caption"
                style={{ marginRight: 4, marginLeft: 4 }}
              >
                •
              </Typography>
              <Plural value={items.length} one="# item" other="# items" />
              {`, `}
              <Plural value={tests.length} one="# test" other="# tests" />
            </Box>
          }
        />
        <CardContent>
          <RichText editorState={convertFromRaw(publicDescription)} />
        </CardContent>
      </ClickableCard>
    </Link>
  );
}

export default CollectionCard;
