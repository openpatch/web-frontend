import Box from '@material-ui/core/Box';
import { useTheme } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
import { Frame } from '@services/recorder/recordings';
import { colorHash } from '@services/recorder/utils';
import { useWindowSize } from '@utils/layout';
import {
  KeyboardEvent,
  memo,
  MouseEvent,
  useCallback,
  useEffect,
  useRef,
  useState,
} from 'react';
import RecordingPlayerTimelineIndicator from './RecordingPlayerTimelineIndicator';

type FrameIndicatorProps = {
  type: string;
  timestamp: number;
  scale: number;
  onClick: (t: number) => void;
};

const FrameIndicator = memo<FrameIndicatorProps>(function FrameIndicator({
  type,
  timestamp,
  scale,
  onClick,
}) {
  return (
    <Tooltip title={type}>
      <Box
        onClick={(e) => {
          e.stopPropagation();
          onClick(timestamp);
        }}
        width={4}
        height={20}
        bgcolor={colorHash(type)}
        position="absolute"
        left={timestamp / scale - 2}
      />
    </Tooltip>
  );
});

export type FrameIndicatorsProps = {
  frames: Frame[];
  onClick: (t: number) => void;
  scale: number;
};

const FrameIndicators = memo<FrameIndicatorsProps>(function FrameIndicators({
  frames,
  onClick,
  scale,
}) {
  return (
    <>
      {frames.map((frame) => (
        <FrameIndicator
          key={frame.id}
          {...frame}
          scale={scale}
          onClick={onClick}
        />
      ))}
    </>
  );
});

export type RecordingPlayerTimelineProps = {
  currentTime: number;
  endTime: number;
  onClick: (t: number) => void;
  frames: Frame[];
};

function RecordingPlayerTimeline({
  currentTime,
  endTime,
  onClick,
  frames,
}: RecordingPlayerTimelineProps) {
  const theme = useTheme();
  const container = useRef<HTMLDivElement | null>(null);
  const size = useWindowSize();
  const [scale, setScale] = useState(1);

  useEffect(() => {
    if (container.current) {
      setScale(endTime / (container.current.clientWidth - 2));
    }
  }, [container, size, endTime]);

  const handleSkip = useCallback<(e: MouseEvent) => void>(
    (e) => {
      onClick(e.nativeEvent.offsetX * scale);
    },
    [onClick, scale]
  );

  const handleKeyPress = useCallback<(e: KeyboardEvent) => void>(
    (e) => {
      if (e.key === 'ArrowLeft') {
        if (e.ctrlKey) {
          onClick(currentTime - 10000);
        } else {
          onClick(currentTime - 1000);
        }
      } else if (e.key === 'ArrowRight') {
        if (e.ctrlKey) {
          onClick(currentTime + 10000);
        } else {
          onClick(currentTime + 1000);
        }
      } else if (e.key === 'Home') {
        onClick(0);
      } else if (e.key === 'End') {
        onClick(endTime);
      }
    },
    [onClick, scale]
  );

  return (
    <div
      ref={container}
      tabIndex={0}
      role="button"
      onKeyPress={handleKeyPress}
      onClick={handleSkip}
      style={{
        height: 20,
        overflow: 'hidden',
        width: '100%',
        position: 'relative',
        backgroundColor: theme.palette.grey['500'],
      }}
    >
      <RecordingPlayerTimelineIndicator
        currentTime={currentTime}
        scale={scale}
      />
      <FrameIndicators onClick={onClick} scale={scale} frames={frames} />
    </div>
  );
}

export default RecordingPlayerTimeline;
