import { action } from '@storybook/addon-actions';
import CollectionButton, { CollectionButtonView } from './CollectionButton';

export default {
  title: 'CollectionButton',
  component: CollectionButton,
};

export const View = () => (
  <CollectionButtonView
    onToggle={action('toggle')}
    onCreate={action('create')}
    inCollections={['my-one']}
    collections={[
      {
        name: 'My One',
        id: 'my-one',
        public: true,
      },
      {
        name: 'Second',
        id: 'second',
        public: true,
      },
      {
        name: 'Private',
        id: 'private',
        public: false,
      },
    ]}
  />
);
