import { action } from '@storybook/addon-actions';
import TestMetaForm from './TestMetaForm';

export default {
  title: 'TestMetaForm',
  component: TestMetaForm,
};

export const Default = () => <TestMetaForm onSubmit={action('submit')} />;

export const Prefilled = () => (
  <TestMetaForm
    onSubmit={action('submit')}
    initialValues={{
      name: 'hallo',
      privacy: 'notlisted',
      versions: [
        {
          status: 'draft',
          version: 1,
        },
      ],
    }}
  />
);
