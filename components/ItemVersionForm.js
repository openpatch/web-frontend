import { useState } from 'react';
import { Trans, t } from '@lingui/macro';
import PropTypes from 'prop-types';
import { FieldArray } from 'react-final-form-arrays';
import arrayMutators from 'final-form-arrays';
import { FormSpy } from 'react-final-form';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import Joyride from './Joyride';
import Form from './Form';
import ItemVersionData from './ItemVersionData';
import TabPanel from './TabPanel';
import TaskFieldArray from './fields/TaskFieldArray';
import EvaluationFieldArray from './fields/EvaluationFieldArray';

const steps = [
  {
    target: '#tasks',
    content: t`Add some tasks here`,
  },
  {
    target: '#evaluation',
    content: t`Define how the tasks should be evaluated here`,
  },
  {
    target: '#preview',
    content: t`Preview the item here`,
  },
];

function ItemVersionForm({ initialValues, onSubmit, ...props }) {
  const [tab, setTab] = useState(0);

  if (initialValues?.tasks?.length == 0) {
    initialValues.tasks = [
      {
        formatType: 'choice',
        task: t`(Demo) Multiple Choice!`,
        text: t`You can select one or more`,
        data: {
          allowMultiple: true,
          choices: [t`Option 1`, t`Option 2`],
        },
        evaluation: {
          choices: {
            0: true,
          },
        },
      },
    ];
  }

  return (
    <>
      <Joyride steps={steps} name="ItemVersionForm" />
      <Tabs
        value={tab}
        onChange={(e, value) => setTab(value)}
        indicatorColor="primary"
        textColor="primary"
        variant="scrollable"
        scrollButtons="auto"
      >
        <Tab id="tasks" label={<Trans>Tasks</Trans>} />
        <Tab id="evaluation" label={<Trans>Evaluation</Trans>} />
        <Tab id="preview" label={<Trans>Preview</Trans>} />
      </Tabs>
      <Form
        onSubmit={onSubmit}
        subscription={{
          submitting: true,
          pristine: true,
        }}
        initialValues={initialValues}
        mutators={{ ...arrayMutators }}
        render={() => (
          <>
            <TabPanel value={tab} index={0}>
              <FieldArray
                name="tasks"
                component={TaskFieldArray}
                subscription={{
                  submitting: true,
                  pristine: true,
                }}
              />
            </TabPanel>
            <TabPanel value={tab} index={1}>
              <FieldArray name="tasks" component={EvaluationFieldArray} />
            </TabPanel>
            <TabPanel value={tab} index={2}>
              <FormSpy>
                {({ values }) => <ItemVersionData tasks={values?.tasks} />}
              </FormSpy>
            </TabPanel>
          </>
        )}
        {...props}
      />
    </>
  );
}

ItemVersionForm.propTypes = {
  initialValues: PropTypes.object,
  onSubmit: PropTypes.func,
};

export default ItemVersionForm;
