import ProgressButton from '@components/ProgressButton';
import SaveFab from '@components/SaveFab';
import { t, Trans } from '@lingui/macro';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';
import { MouseEvent } from 'react';
import { Form as FForm, FormProps as FFormProps } from 'react-final-form';

type SubmitButtonProps = {
  loading?: boolean;
  disabled?: boolean;
  onClick?: (event: MouseEvent) => void;
  variant?: 'fab' | 'button' | 'hidden';
};

const SubmitButton = ({
  loading = false,
  disabled = false,
  onClick,
  variant = 'fab',
}: SubmitButtonProps) => {
  if (variant === 'fab') {
    return (
      <Box style={{ position: 'fixed', right: 16, bottom: 16 }}>
        <SaveFab
          title={t`Save`}
          disabled={disabled}
          loading={loading}
          onClick={onClick}
        />
      </Box>
    );
  }

  if (variant === 'button') {
    return (
      <Box textAlign="right" p={2}>
        <ProgressButton
          color="primary"
          variant="contained"
          loading={loading}
          disabled={disabled}
          onClick={onClick}
        >
          <Trans>Save</Trans>
        </ProgressButton>
      </Box>
    );
  }

  return null;
};

export type FormProps<T> = {
  submitVariant?: SubmitButtonProps['variant'];
} & FFormProps<T, Partial<T>>;

const Form = <T,>({
  submitVariant = 'fab',
  render,
  ...props
}: FormProps<T>) => (
  <FForm
    {...props}
    render={(renderProps) => (
      <Typography component="form" onSubmit={renderProps.handleSubmit}>
        {render && render(renderProps)}
        <SubmitButton
          variant={submitVariant}
          loading={renderProps.submitting && !renderProps.pristine}
          disabled={
            (renderProps.submitting && !renderProps.pristine) ||
            renderProps.hasValidationErrors
          }
          onClick={renderProps.handleSubmit}
        />
      </Typography>
    )}
  />
);

Form.propTypes = {
  render: PropTypes.func,
  submitVariant: PropTypes.oneOf(['hidden', 'button', 'fab']),
};

Form.defaultProps = {
  render: () => null,
  submitVariant: 'button',
};

export default Form;
