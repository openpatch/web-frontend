import Avatar from '@material-ui/core/Avatar';
import Box from '@material-ui/core/Box';
import { Member } from '@services/authentification/members';
import Link from 'next/link';
import { CSSProperties } from 'react';

export type MemberAvatarProps = Pick<
  Partial<Member>,
  'username' | 'avatarId' | 'id'
> & {
  style?: CSSProperties;
  className?: string;
};

function MemberAvatar({
  username,
  avatarId,
  id,
  style,
  className,
}: MemberAvatarProps) {
  let avatar;
  const myStyle = { cursor: 'pointer' };
  if (avatarId) {
    avatar = (
      <Avatar
        alt={username}
        src={`${process.env.NEXT_API_MEDIA_BROWSER}/v1/${avatarId}/serve`}
        style={style}
        className={className}
      />
    );
  } else if (username) {
    avatar = (
      <Avatar style={style} className={className}>
        {username[0] + username[1]}
      </Avatar>
    );
  } else {
    avatar = <Avatar style={style} className={className} />;
  }

  if (id) {
    return (
      <Link href="/member/[id]" as={`/member/${id}`}>
        <Box style={myStyle}>{avatar}</Box>
      </Link>
    );
  }

  return avatar;
}

export default MemberAvatar;
