import { action } from '@storybook/addon-actions';
import { select } from '@storybook/addon-knobs';

import Form from './Form';
import FormHeader from './FormHeader';
import FormSection from './FormSection';
import FormSectionContent from './FormSectionContent';
import FormSectionHeader from './FormSectionHeader';

export default {
  title: 'Form/Form',
  component: Form,
};

export const Default = () => <Form onSubmit={action('submit')} />;

export const WithSection = () => (
  <Form
    onSubmit={action('submit')}
    submitVariant={select('submitVariant', ['button', 'fab', 'hidden'])}
    render={() => (
      <>
        <FormHeader>This is a form</FormHeader>
        <FormSection>
          <FormSectionHeader>Section 1</FormSectionHeader>
          <FormSectionContent>With some content</FormSectionContent>
        </FormSection>
        <FormSection>
          <FormSectionHeader>Section 2</FormSectionHeader>
          <FormSectionContent>With some other content</FormSectionContent>
        </FormSection>
      </>
    )}
  />
);
