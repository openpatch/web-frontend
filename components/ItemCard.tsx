import Box from '@material-ui/core/Box';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import { styled } from '@material-ui/core/styles';
import { Item } from '@services/itembank/items';
import { useDateUtils } from '@utils/date';
import { convertFromRaw } from '@utils/editor';
import Link from 'next/link';
import { CSSProperties } from 'react';
import MemberAvatar from './MemberAvatar';
import PrivacyIndicator from './PrivacyIndicator';
import RichText from './RichText';
import Spacer from './Spacer';

const ClickableCard = styled(Card)({
  cursor: 'pointer',
});

export type ItemCardProps = Pick<
  Item,
  'id' | 'name' | 'privacy' | 'publicDescription' | 'member' | 'createdOn'
> & { style?: CSSProperties };

function ItemCard({
  id,
  name,
  privacy,
  publicDescription,
  member,
  style,
  createdOn,
}: ItemCardProps) {
  const { formatDistance, utcDate } = useDateUtils();
  return (
    <Link href={`/item/[id]`} as={`/item/${id}`}>
      <ClickableCard style={style}>
        <CardHeader
          avatar={<MemberAvatar {...member} />}
          title={name}
          subheader={
            <Box display="flex" alignItems="center" flexWrap="wrap">
              <PrivacyIndicator privacy={privacy} />
              <Spacer />
              {member?.username}
              {createdOn ? (
                <>
                  <Spacer />
                  {formatDistance(new Date(createdOn), utcDate())}{' '}
                </>
              ) : null}
            </Box>
          }
        />
        <CardContent>
          <RichText editorState={convertFromRaw(publicDescription)} />
        </CardContent>
      </ClickableCard>
    </Link>
  );
}

export default ItemCard;
