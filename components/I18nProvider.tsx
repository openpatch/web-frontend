import DateFnsUtils from '@date-io/date-fns';
import { i18n } from '@lingui/core';
import { I18nProvider as I18nLingui } from '@lingui/react';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import { ReactNode, useEffect } from 'react';
import { locales } from '../utils/date';
import { dynamicActivate, Locale } from '../utils/i18n';

export type I18nProviderProps = {
  locale?: Locale;
  children: ReactNode;
};

function I18nProvider({ locale = 'en', children }: I18nProviderProps) {
  useEffect(() => {
    dynamicActivate(locale);
  }, [locale]);

  return (
    <I18nLingui i18n={i18n}>
      <MuiPickersUtilsProvider utils={DateFnsUtils} locale={locales[locale]}>
        {children}
      </MuiPickersUtilsProvider>
    </I18nLingui>
  );
}

export default I18nProvider;
