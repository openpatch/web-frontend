import { Trans } from '@lingui/macro';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

import MemberAvatar from './MemberAvatar';
import { useFollows } from '@services/activity/resources';
import { Member } from '@services/authentification/members';

export type MemberHeaderProps = Pick<Member, 'username' | 'avatarId' | 'id'> & {
  variant?: 'large' | 'default';
};

function MemberHeader({
  username,
  avatarId,
  id,
  variant = 'default',
}: MemberHeaderProps) {
  const { follows, postFollow, deleteFollow, revalidate } = useFollows(
    id,
    'member'
  );

  function handleFollow() {
    if (!follows?.isFollowed) {
      postFollow({ username, avatarId }).then(() => revalidate({}));
    } else {
      deleteFollow().then(() => revalidate({}));
    }
  }

  let size = 40;
  let fontSize = 16;

  if (variant == 'large') {
    size = 64;
    fontSize = 24;
  }

  return (
    <Box mt={2} mb={2} display="flex" alignItems="center">
      <MemberAvatar
        style={{ width: size, height: size, marginRight: 16 }}
        username={username}
        avatarId={avatarId}
        id={id}
      />
      <Box flex={1}>
        <Typography style={{ fontSize }}>{username}</Typography>
        <Typography variant="caption">
          <Trans>{follows?.followsCount} Follower</Trans>
        </Typography>
      </Box>
      <Button
        onClick={handleFollow}
        variant="outlined"
        color={follows?.isFollowed ? 'primary' : 'default'}
      >
        {follows?.isFollowed ? <Trans>Unfollow</Trans> : <Trans>Follow</Trans>}
      </Button>
    </Box>
  );
}

export default MemberHeader;
