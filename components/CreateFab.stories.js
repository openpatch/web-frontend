import { action } from '@storybook/addon-actions';
import CreateFab from './CreateFab';

export default {
  title: 'Fab/CreateFab',
  component: CreateFab
};

export const Default = () => (
  <CreateFab title="Create" onClick={action('click')} />
);
