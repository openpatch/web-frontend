import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

const styles = (theme) => ({
  container: {
    display: 'flex',
    justifyContent: 'flex-start',
    borderBottom: `1px solid ${theme.palette.grey[300]}`,
    padding: theme.spacing(3),
  },
});

const FormHeader = ({ classes, ...props }) => (
  <Typography variant="h5" className={classes.container} {...props} />
);

FormHeader.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(FormHeader);
