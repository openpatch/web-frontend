import PropTypes from 'prop-types';
import { Trans } from '@lingui/macro';
import Link from 'next/link';
import { styled } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import LockIcon from '@material-ui/icons/Lock';
import LockOpenIcon from '@material-ui/icons/LockOpen';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';
import Videocam from '@material-ui/icons/Videocam';
import Security from '@material-ui/icons/Security';

import { useDateUtils } from '../utils/date';
import RichText from './RichText';
import { convertFromRaw } from '../utils/editor';
import { languages } from '../utils/i18n';

const ClickableCard = styled(Card)({
  cursor: 'pointer',
});

export function AssessmentPublicCard({
  id,
  name,
  language,
  passwordProtected,
  record,
  encrypt,
  member,
  style,
  publicDescription,
  createdOn,
}) {
  // passwordHash render lock
  // startsOn endsOn render timespan
  const { formatDistance, utcDate } = useDateUtils();
  return (
    <Link
      href={`/session/prepare?assessment=${id}`}
      as={`/session/prepare?assessment=${id}`}
    >
      <ClickableCard style={style}>
        <CardHeader
          avatar={
            passwordProtected ? (
              <Tooltip title={<Trans>Password Needed</Trans>}>
                <LockIcon />
              </Tooltip>
            ) : (
              <Tooltip title={<Trans>Open for Everyone</Trans>}>
                <LockOpenIcon />
              </Tooltip>
            )
          }
          title={
            <span>
              {name}{' '}
              {record && (
                <Tooltip title={<Trans>Some Tasks are recorded</Trans>}>
                  <Videocam fontSize="inherit" />
                </Tooltip>
              )}
              {encrypt && (
                <Tooltip title={<Trans>Some Tasks are encrypted</Trans>}>
                  <Security fontSize="inherit" />
                </Tooltip>
              )}
            </span>
          }
          subheader={
            <>
              {member?.username}
              <Typography
                variant="caption"
                style={{ marginRight: 4, marginLeft: 4 }}
              >
                •
              </Typography>
              <Trans id={languages[language]} />
              <Typography
                variant="caption"
                style={{ marginRight: 4, marginLeft: 4 }}
              >
                •
              </Typography>
              <Trans>
                created {formatDistance(new Date(createdOn), utcDate())} ago
              </Trans>
            </>
          }
        />
        <CardContent>
          <RichText editorState={convertFromRaw(publicDescription)} />
        </CardContent>
      </ClickableCard>
    </Link>
  );
}

AssessmentPublicCard.propTypes = {
  id: PropTypes.string,
  language: PropTypes.string,
  passwordHash: PropTypes.string,
  startsOn: PropTypes.string,
  record: PropTypes.bool,
  encrypt: PropTypes.bool,
  passwordProtected: PropTypes.bool,
  style: PropTypes.object,
  createdOn: PropTypes.string,
  name: PropTypes.string,
  member: PropTypes.shape({
    username: PropTypes.string,
  }),
  publicDescription: PropTypes.object,
};

AssessmentPublicCard.defaultProps = {
  language: 'en',
};

export default AssessmentPublicCard;
