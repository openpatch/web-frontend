import { number } from '@storybook/addon-knobs';
import RemixButton from './RemixButton';

export default {
  title: 'RemixButton',
  component: RemixButton,
};

export const Default = () => <RemixButton remixes={number('remixes', 20)} />;
