import PropTypes from 'prop-types';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import ListItemText from '@material-ui/core/ListItemText';

function ActionLog({ actions }) {
  return (
    <TableContainer>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>Time</TableCell>
            <TableCell>Action</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {actions.map((action) => (
            <TableRow key={action.timestamp}>
              <TableCell>{action?.timestamp}</TableCell>
              <TableCell>
                <ListItemText
                  primary={action?.type}
                  secondary={JSON.stringify(action?.payload)}
                />
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

ActionLog.propTypes = {
  actions: PropTypes.arrayOf(
    PropTypes.shape({
      type: PropTypes.string,
      timestamp: PropTypes.number,
      payload: PropTypes.object,
    })
  ),
};

ActionLog.defaultProps = {
  actions: [],
};

export default ActionLog;
