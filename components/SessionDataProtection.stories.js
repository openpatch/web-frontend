import { action } from '@storybook/addon-actions';
import { boolean } from '@storybook/addon-knobs';

import { getLoremIpsum } from '../utils/editor';
import SessionDataProtection from './SessionDataProtection';

export default {
  title: 'SessionDataProtection',
  component: SessionDataProtection
};

export const Default = () => (
  <SessionDataProtection
    dataProtection={getLoremIpsum()}
    record={boolean('record')}
    encrypt={boolean('encrypt')}
    onAccept={action('accept')}
    accept={boolean('accept')}
  />
);
