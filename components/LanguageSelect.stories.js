import LanguageSelect from './LanguageSelect';

export default {
  title: 'LanguageSelect',
  component: LanguageSelect
};

export const Default = () => <LanguageSelect />;
