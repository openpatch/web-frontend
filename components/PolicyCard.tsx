import { Trans } from '@lingui/macro';
import Box from '@material-ui/core/Box';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import { styled } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { Policy } from '@services/authentification/policies';
import Link from 'next/link';
import { CSSProperties } from 'react';
import { useDateUtils } from '../utils/date';

const ClickableCard = styled(Card)({
  cursor: 'pointer',
});

export type PolicyCardProps = Pick<
  Policy,
  'id' | 'draft' | 'type' | 'createdOn' | 'language'
> & { style?: CSSProperties };

function PolicyCard({
  id,
  draft,
  type,
  style,
  createdOn,
  language,
}: PolicyCardProps) {
  const { formatDistance, utcDate } = useDateUtils();
  return (
    <Link href={`/admin/policy/[id]/edit`} as={`/admin/policy/${id}/edit`}>
      <ClickableCard style={style}>
        <CardHeader
          title={type}
          subheader={
            <Box display="flex" alignItems="center">
              {draft ? <Trans>Draft</Trans> : <Trans>Published</Trans>}
              <Typography
                variant="caption"
                style={{ marginRight: 4, marginLeft: 4 }}
              >
                •
              </Typography>
              {language}
              <Typography
                variant="caption"
                style={{ marginRight: 4, marginLeft: 4 }}
              >
                •
              </Typography>
              {formatDistance(new Date(createdOn), utcDate())}
            </Box>
          }
        />
      </ClickableCard>
    </Link>
  );
}

export default PolicyCard;
