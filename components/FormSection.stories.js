import FormSection from './FormSection';

export default {
  title: 'Form/FormSection',
  component: FormSection
};

export const Default = () => <FormSection>Section</FormSection>;

