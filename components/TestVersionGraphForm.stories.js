import { action } from '@storybook/addon-actions';
import arrayMutators from 'final-form-arrays';
import 'vis-network/styles/vis-network.css';

import MForm from './Form';
import TestVersionGraphForm from './TestVersionGraphForm';

export default {
  title: 'TestVersionGraphForm',
  component: TestVersionGraphForm
};

export const Default = () => (
  <MForm
    onSubmit={action('submit')}
    mutators={{ ...arrayMutators }}
    initialValues={{
      nodes: [
        {
          id: 'some-id',
          name: 'Hallo',
          edges: [
            {
              nodeTo: 'some-id',
              nodeFrom: 'some-other-id',
              threshold: 4
            }
          ]
        },
        {
          id: 'some-other-id',
          name: 'Du'
        },
        {
          id: 'another-id',
          name: 'Ba',
          start: true,
          edges: [
            {
              nodeTo: 'some-other-id',
              nodeFrom: 'another-id',
              threshold: 0
            }
          ]
        }
      ]
    }}
    render={() => <TestVersionGraphForm />}
  />
);
