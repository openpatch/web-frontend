import Box from '@material-ui/core/Box';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import { styled } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { Assessment } from '@services/assessment/assessments';
import { useDateUtils } from '@utils/date';
import { convertFromRaw } from '@utils/editor';
import Link from 'next/link';
import { CSSProperties } from 'react';
import MemberAvatar from './MemberAvatar';
import PrivacyIndicator from './PrivacyIndicator';
import RichText from './RichText';

export type AssessmentCardProps = Pick<
  Assessment,
  | 'id'
  | 'name'
  | 'language'
  | 'public'
  | 'member'
  | 'createdOn'
  | 'publicDescription'
> & {
  style?: CSSProperties;
};

const ClickableCard = styled(Card)({
  cursor: 'pointer',
});

export function AssessmentCard({
  id,
  name,
  language,
  public: isPublic,
  member,
  style,
  createdOn,
  publicDescription,
}: AssessmentCardProps) {
  // passwordHash render lock
  // startsOn endsOn render timespan
  const { formatDistance, utcDate } = useDateUtils();
  return (
    <Link href={`/assessment/[id]`} as={`/assessment/${id}`}>
      <ClickableCard style={style}>
        <CardHeader
          avatar={<MemberAvatar {...member} />}
          title={name}
          subheader={
            <Box display="flex" alignItems="center">
              <PrivacyIndicator privacy={isPublic ? 'public' : 'private'} />
              <Typography
                variant="caption"
                style={{ marginRight: 4, marginLeft: 4 }}
              >
                •
              </Typography>
              {member?.username}
              <Typography
                variant="caption"
                style={{ marginRight: 4, marginLeft: 4 }}
              >
                •
              </Typography>
              {formatDistance(new Date(createdOn), utcDate())}
              <Typography
                variant="caption"
                style={{ marginRight: 4, marginLeft: 4 }}
              >
                •
              </Typography>
              {language}
            </Box>
          }
        />
        <CardContent>
          <RichText editorState={convertFromRaw(publicDescription)} />
        </CardContent>
      </ClickableCard>
    </Link>
  );
}

export default AssessmentCard;
