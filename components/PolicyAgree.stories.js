import { action } from '@storybook/addon-actions';
import PolicyAgree from './PolicyAgree';

import { getLoremIpsum } from '../utils/editor';

export default {
  title: 'PolicyAgree',
  component: PolicyAgree
};

export const Default = () => (
  <PolicyAgree
    content={getLoremIpsum()}
    type="privacy"
    onAgree={action('agree')}
  />
);

export const isSending = () => (
  <PolicyAgree
    content={getLoremIpsum()}
    type="privacy"
    onAgree={action('agree')}
    isSending={true}
  />
);
