import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import { useSession } from '@services/assessment/session';
import { jumpToItem } from '@services/assessment/session/actions';
import { SessionItem } from '@services/assessment/session/items';
import { useEffect } from 'react';
import ItemVersionRenderer from './ItemVersionRenderer';
import TabPanel from './TabPanel';

function a11yProps(index: number) {
  return {
    id: `scrollable-auto-tab-${index}`,
    'aria-controls': `scrollable-auto-tabpanel-${index}`,
  };
}

export type ItemsRendererProps = {
  items: SessionItem[];
  variant?: 'normal' | 'result' | 'player';
};

function ItemsRenderer({ items, variant = 'normal' }: ItemsRendererProps) {
  const { state, dispatch, handleReset } = useSession();

  useEffect(() => {
    handleReset();
  }, [items]);

  function handleTabChange(e: any, newValue: number) {
    dispatch(jumpToItem(newValue));
  }

  return (
    <>
      {items.length > 1 && (
        <>
          <Paper style={{ position: 'absolute', top: 10, right: 0, left: 0 }}>
            <Tabs
              textColor="primary"
              variant="scrollable"
              scrollButtons="auto"
              indicatorColor="primary"
              value={state?.currentItem || 0}
              onChange={handleTabChange}
            >
              {items.map((item, index) => (
                <Tab
                  label={`Item ${index + 1}`}
                  key={index}
                  {...a11yProps(0)}
                />
              ))}
            </Tabs>
          </Paper>
          <Box height={45} />
        </>
      )}
      {items.map((item, index) => (
        <TabPanel value={state?.currentItem || 0} index={index} key={index}>
          <ItemVersionRenderer
            tasks={item.tasks}
            index={index}
            variant={variant}
          />
        </TabPanel>
      ))}
    </>
  );
}

export default ItemsRenderer;
