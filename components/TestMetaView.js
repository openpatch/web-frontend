import { Trans } from '@lingui/macro';
import Router from 'next/router';
import PropTypes from 'prop-types';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';

import PrivacyIndicator from './PrivacyIndicator';
import Spacer from './Spacer';
import MemberHeader from './MemberHeader';
import LikeButton from './LikeButton';
import RemixButton from './RemixButton';
import ShareButton from './ShareButton';
import CollectionButton from './CollectionButton';
import RichText from './RichText';
import { convertFromRaw } from '../utils/editor';
import { useDateUtils } from '../utils/date';
import api from '../utils/api';
import { useNotifications } from '../utils/notification';

function TestMetaView({
  id,
  name,
  member,
  createdOn,
  publicDescription,
  collections,
  remixes,
  revalidate,
  privacy,
}) {
  const { formatDistance } = useDateUtils();
  const [add] = useNotifications();

  let distance = null;
  if (createdOn) {
    distance = formatDistance(new Date(createdOn), new Date());
  }

  function handleRemix() {
    api
      .post(`${process.env.NEXT_API_ITEMBANK_BROWSER}/v1/tests/${id}/remix`)
      .then(({ data: { testId } }) => {
        Router.push('/test/[id]/edit', `/test/${testId}/edit`);
      })
      .catch(() => {
        add({
          message: <Trans>Something went wrong</Trans>,
          severity: 'error',
        });
      });
  }

  return (
    <Box>
      <Box display="flex" alignItems="center" flexWrap="wrap" mb={1}>
        <Box flex={1}>
          <Typography>{name}</Typography>
          <Box display="flex" minWidth={200} alignItems="center">
            <PrivacyIndicator privacy={privacy} />
            <Spacer />
            <Typography variant="caption">{distance}</Typography>
          </Box>
        </Box>
        <Box display="flex" justifyContent="center" alignItems="center">
          <CollectionButton
            collections={collections}
            id={id}
            type="tests"
            onToggle={revalidate}
          />
          <ShareButton title={name} />
          <RemixButton remixes={remixes.length} onClick={handleRemix} />
          <LikeButton id={id} type="test" data={{ name, publicDescription }} />
        </Box>
      </Box>
      <Divider />
      <MemberHeader {...member} />
      <RichText editorState={convertFromRaw(publicDescription)} />
    </Box>
  );
}

TestMetaView.propTypes = {
  id: PropTypes.string,
  language: PropTypes.string,
  name: PropTypes.string,
  member: PropTypes.object,
  createdOn: PropTypes.string,
  collections: PropTypes.arrayOf(PropTypes.string),
  remixes: PropTypes.arrayOf(PropTypes.string),
  publicDescription: PropTypes.object,
  privacy: PropTypes.oneOf(['private', 'public', 'notlisted']),
  revalidate: PropTypes.func,
};

TestMetaView.defaultProps = {
  privacy: 'private',
  revalidate: () => null,
  collections: [],
  remixes: [],
};

export default TestMetaView;
