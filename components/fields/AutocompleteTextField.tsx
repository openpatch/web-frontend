import Box from '@material-ui/core/Box';
import CircularProgress from '@material-ui/core/CircularProgress';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import { useRequest } from '@utils/request';
import { useEffect, useState } from 'react';
import { FieldRenderProps } from 'react-final-form';

export type AutoCompleteTextFieldProps<R, T> = (
  | (FieldRenderProps<string, HTMLElement> & {
      getValue: (t: T) => string;
    })
  | (FieldRenderProps<number, HTMLElement> & {
      getValue: (t: T) => number;
    })
) & {
  url: string;
  selector: (d: R) => T[];
  getText: (t: T) => string;
};

export default function AutocompleteTextField<R, T>({
  url,
  selector,
  input,
  label,
  meta: { touched, error },
  getValue,
  getText,
  ...custom
}: AutoCompleteTextFieldProps<R, T>) {
  const [open, setOpen] = useState(false);
  const { data } = useRequest<R>(url, {
    axios: {
      params: {
        limit: 20,
      },
    },
  });
  const [options, setOptions] = useState<T[]>([]);
  const loading = open && options.length === 0;

  useEffect(() => {
    if (data) {
      setOptions(selector(data));
    }
  }, [data]);

  return (
    <TextField
      label={label}
      error={touched && Boolean(error)}
      variant="outlined"
      margin="dense"
      select
      {...input}
      {...custom}
      SelectProps={{
        open: open,
        onOpen: () => {
          setOpen(true);
        },
        onClose: () => {
          setOpen(false);
        },
      }}
    >
      {loading ? (
        <Box textAlign="center">
          <CircularProgress color="inherit" size={20} />
        </Box>
      ) : null}
      {options.map((option) => (
        <MenuItem key={getValue(option)} value={getValue(option)}>
          {getText(option)}
        </MenuItem>
      ))}
    </TextField>
  );
}
