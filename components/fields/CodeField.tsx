import FormControl, { FormControlProps } from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import FormHelperText from '@material-ui/core/FormHelperText';

import CodeEditor, { CodeEditorProps } from '../CodeEditor';
import { FieldRenderProps } from 'react-final-form';

export type CodeFieldProps = FieldRenderProps<
  { source: string; readOnlyRows: number[] },
  HTMLElement
> & {
  label?: string;
  helperText?: string;
} & Pick<FormControlProps, 'margin' | 'fullWidth' | 'required'> &
  Omit<
    CodeEditorProps,
    'value' | 'readOnlyRows' | 'onGutterClick' | 'onChange'
  >;

function CodeField({
  input: { value, onChange },
  meta: { touched, error },
  label,
  fullWidth,
  margin,
  helperText,
  required,
  ...custom
}: CodeFieldProps) {
  function handleGutterClick(row: number) {
    let { readOnlyRows } = value;
    if (!readOnlyRows) {
      readOnlyRows = [row];
    } else {
      readOnlyRows = [...readOnlyRows];
      if (readOnlyRows.includes(row)) {
        readOnlyRows = readOnlyRows.filter((r) => r !== row);
      } else {
        readOnlyRows.push(row);
      }
    }
    onChange({ ...value, readOnlyRows });
  }

  function handleSourceChange(source: string) {
    onChange({ ...value, source });
  }

  return (
    <FormControl
      margin={margin}
      fullWidth={fullWidth}
      error={touched && error}
      required={required}
    >
      {label && <FormLabel>{label}</FormLabel>}
      <CodeEditor
        height="100%"
        maxLines={40}
        minLines={10}
        value={value?.source}
        readOnlyRows={value?.readOnlyRows}
        onGutterClick={handleGutterClick}
        onChange={handleSourceChange}
        {...custom}
      />
      {helperText && <FormHelperText>{helperText}</FormHelperText>}
    </FormControl>
  );
}

export default CodeField;
