import { action } from '@storybook/addon-actions';
import DateTimeField from './DateTimeField';

export default {
  title: 'Field/DateTimeField',
  component: DateTimeField,
};

export const Default = () => (
  <DateTimeField
    meta={{}}
    input={{
      onChange: action('change'),
      value: '',
    }}
  />
);
