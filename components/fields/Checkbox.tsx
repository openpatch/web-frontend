import MuiCheckbox from '@material-ui/core/Checkbox';
import FormControl, { FormControlProps } from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import _isEqual from 'deep-equal';
import { memo } from 'react';
import { FieldRenderProps } from 'react-final-form';

export type CheckboxProps = FieldRenderProps<boolean, HTMLElement> &
  Omit<FormControlProps, 'error'>;

const Checkbox = memo<CheckboxProps>(
  ({ input, label, meta: { touched, error }, helperText, ...props }) => (
    <FormControl error={touched && error} {...props}>
      <FormControlLabel
        control={
          <MuiCheckbox
            checked={input.checked ? true : false}
            onChange={input.onChange}
            color="primary"
          />
        }
        label={label}
      />
      {helperText && <FormHelperText>{helperText}</FormHelperText>}
    </FormControl>
  ),
  _isEqual
);

Checkbox.displayName = 'Checkbox';

export default Checkbox;
