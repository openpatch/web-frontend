import { useRef, useState, useEffect, useCallback, memo } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import { EditorState } from 'draft-js';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import _isEqual from 'deep-equal';

import Toolbar from './Toolbar';
import RichTextEditor from '../../RichTextEditor';
import RichText from '../../RichText';
import { convertFromRaw, convertToRaw } from '../../../utils/editor';

const useStyles = makeStyles((theme) => {
  const borderColor =
    theme.palette.type === 'light'
      ? 'rgba(0, 0, 0, 0.23)'
      : 'rgba(255, 255, 255, 0.23)';
  return {
    focus: {},
    hover: {},
    disabled: {},
    error: {},
    label: {
      position: 'absolute',
      top: 0,
      left: 0,
      fontSize: '1rem',
      fontWeight: 400,
      display: 'block',
      transformOrigin: 'top left',
      transform: 'translate(14px, -8px) scale(0.75)',
      backgroundColor: theme.palette.background.default,
      paddingLeft: 2,
      color: theme.palette.text.secondary,
      paddingRight: 2,
      '&$hover': {
        color: theme.palette.text.primary,
        '@media (hover:none)': {
          color: borderColor,
        },
      },
      '&$disabled': {
        color: theme.palette.text.disabled,
      },
      '&$focus': {
        color: theme.palette.primary.main,
      },
      '&$error': {
        color: theme.palette.error.main,
      },
    },
    marginNormal: {
      marginTop: 16,
      marginBottom: 8,
    },
    /* Styles applied to the root element if `margin="dense"`. */ marginDense: {
      marginTop: 8,
      marginBottom: 4,
    },
    /* Styles applied to the root element if `fullWidth={true}`. */
    fullWidth: {
      width: '100%',
    },
    container: {
      display: 'flex',
      position: 'relative',
      flexDirection: 'column',
      borderColor,
      borderRadius: theme.shape.borderRadius,
      borderStyle: 'solid',
      borderWidth: 1,
      transition: 'color 200ms cubic-bezier(0.0, 0, 0.2, 1) 0ms',
      '&:hover': {
        borderColor: theme.palette.text.primary,
        '@media (hover:none)': {
          borderColor,
        },
      },
      '&$focus': {
        borderColor: theme.palette.primary.main,
      },
      '&$error': {
        borderColor: theme.palette.error.main,
      },
      '&$disabled': {
        borderColor: theme.palette.action.disabled,
      },
    },
    editor: {
      padding: '18.5px 14px',
      overflowY: 'auto',
      flex: 1,
    },
  };
});

const RichTextField = memo(
  ({
    preview,
    input,
    label,
    meta: { touched, error },
    extraButtons,
    deactivateFromToolbar,
    readOnly,
    id,
    ...custom
  }) => {
    const rte = useRef();
    const [hasFocus, setHasFocus] = useState(false);
    const [isHovered, setIsHovered] = useState(false);
    const [editorState, setEditorState] = useState(EditorState.createEmpty());
    const [lock, setLock] = useState(false);
    const classes = useStyles();

    useEffect(() => {
      setEditorState(convertFromRaw(input.value));
    }, []);

    const focusEditor = useCallback(() => {
      rte.current.focus();
    }, [rte.current]);

    const handleChange = useCallback((e) => {
      setEditorState(e);
    }, []);

    const handleMouseOut = useCallback(() => {
      setIsHovered(false);
    }, []);

    const handleMouseOver = useCallback(() => {
      setIsHovered(true);
    }, []);

    const handleBlur = useCallback(() => {
      setHasFocus(false);
      setIsHovered(false);
      if (!editorState.getCurrentContent().hasText()) {
        input.onChange(null);
      } else {
        input.onChange(convertToRaw(editorState.getCurrentContent()));
      }
    }, [editorState]);

    return (
      <Box
        id={id}
        className={clsx(
          classes.container,
          classes.fullWidth,
          classes.marginDense,
          {
            [classes.error]: touched && error,
            [classes.disabled]: custom.disabled,
            [classes.focus]: hasFocus,
          }
        )}
        onMouseOver={handleMouseOver}
        maxHeight={400}
        onFocus={handleMouseOver}
        onMouseOut={handleMouseOut}
        onBlur={handleMouseOut}
      >
        <Typography
          component="span"
          className={clsx(classes.label, {
            [classes.error]: touched && error,
            [classes.disabled]: custom.disabled,
            [classes.focus]: hasFocus,
            [classes.hover]: isHovered,
          })}
        >
          {label}
        </Typography>
        {!preview && (
          <Toolbar
            extraButtons={extraButtons}
            editorState={editorState}
            onChange={handleChange}
            focusEditor={focusEditor}
            lockEditor={setLock}
            deactivate={deactivateFromToolbar}
          />
        )}
        <Box className={classes.editor}>
          {!preview ? (
            <>
              <RichTextEditor
                {...custom}
                ref={rte}
                onBlur={handleBlur}
                editorState={editorState}
                onChange={handleChange}
                readOnly={lock || readOnly}
              />
            </>
          ) : (
            <RichText editorState={editorState} />
          )}
        </Box>
      </Box>
    );
  },
  (prev, next) =>
    prev.meta.touched == next.meta.touched &&
    _isEqual(prev?.input?.value, next?.input?.value)
);

RichTextField.displayName = 'RichTextField';

RichTextField.propTypes = {
  deactivateFromToolbar: PropTypes.arrayOf(PropTypes.string),
  preview: PropTypes.bool,
  input: PropTypes.shape({
    value: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
    onChange: PropTypes.func,
  }).isRequired,
  meta: PropTypes.shape({
    touched: PropTypes.bool,
    error: PropTypes.string,
  }),
  extraButtons: PropTypes.array,
  label: PropTypes.string,
  helperText: PropTypes.string,
  readOnly: PropTypes.bool,
  id: PropTypes.string,
};

RichTextField.defaultProps = {
  extraButtons: [],
  meta: {},
};

export default RichTextField;
