import { action } from '@storybook/addon-actions';
import EmbedButton from './EmbedButton';
import { EditorState } from 'draft-js';

export default {
  title: 'Field/RichTextField/EmbedButton',
  component: EmbedButton
};

export const Default = () => (
  <EmbedButton
    focusEditor={action('focus editor')}
    onChange={action('change')}
    editorState={EditorState.createEmpty()}
  />
);
