import { action } from '@storybook/addon-actions';
import ImageButton from './ImageButton';
import { EditorState } from 'draft-js';

export default {
  title: 'Field/RichTextField/ImageButton',
  component: ImageButton
};

export const Default = () => (
  <ImageButton
    focusEditor={action('focus editor')}
    onChange={action('change')}
    editorState={EditorState.createEmpty()}
  />
);
