import { useState } from 'react';
import PropTypes from 'prop-types';
import InsertVideo from '@material-ui/icons/VideoCall';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';

import EditorButton from './EditorButton';
import embedPlugin from '../../../utils/editor/embed-plugin';

const EmbedButton = ({ focusEditor, editorState, onChange }) => {
  const [open, setOpen] = useState(false);
  const [url, setUrl] = useState('');
  const [width, setWidth] = useState('100%');
  const [height, setHeight] = useState('auto');
  const [title, setTitle] = useState('');

  function handleClick(e) {
    e.preventDefault();
    setOpen(true);
  }

  function handleClose(e) {
    e.preventDefault();
    setOpen(false);
    focusEditor();
  }

  function handleInsert() {
    setOpen(false);
    focusEditor();
    onChange(
      embedPlugin.add(editorState, url, {
        height,
        width,
        title,
      })
    );
  }
  return (
    <>
      <Dialog open={open} onClose={handleClose} aria-labelledby="insert-link">
        <DialogTitle id="insert-link">Insert Link</DialogTitle>
        <DialogContent>
          <DialogContentText>
            To insert a link insert the url and press insert.
          </DialogContentText>
          <TextField
            margin="dense"
            label="URL"
            fullWidth
            value={url}
            onChange={(e) => setUrl(e.target.value)}
          />
          <TextField
            margin="dense"
            label="Width"
            fullWidth
            value={width}
            onChange={(e) => setWidth(e.target.value)}
          />
          <TextField
            margin="dense"
            label="Height"
            fullWidth
            value={height}
            onChange={(e) => setHeight(e.target.value)}
          />
          <TextField
            margin="dense"
            label="Title"
            fullWidth
            value={title}
            onChange={(e) => setTitle(e.target.value)}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <Button onClick={handleInsert} color="primary">
            Insert
          </Button>
        </DialogActions>
      </Dialog>
      <EditorButton title="Embed" onClick={handleClick}>
        <InsertVideo />
      </EditorButton>
    </>
  );
};

EmbedButton.propTypes = {
  focusEditor: PropTypes.func.isRequired,
  editorState: PropTypes.object,
  lockEditor: PropTypes.func.isRequired,
  onChange: PropTypes.func,
};

export default EmbedButton;
