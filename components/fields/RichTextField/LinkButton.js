import { useState } from 'react';
import PropTypes from 'prop-types';
import InsertLinkIcon from '@material-ui/icons/InsertLink';
import LinkOffIcon from '@material-ui/icons/LinkOff';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';

import EditorButton from './EditorButton';
import linkPlugin from '../../../utils/editor/link-plugin';

const LinkButton = ({ focusEditor, onChange, editorState, lockEditor }) => {
  const [open, setOpen] = useState(false);
  const [url, setUrl] = useState('');

  function handleClick(e) {
    e.preventDefault();
    lockEditor(true);
    setOpen(true);
  }

  function handleClose(e) {
    e.preventDefault();
    setOpen(false);
    focusEditor();
    lockEditor(false);
  }

  function handleURL(e) {
    setUrl(e.target.value);
  }

  function handleInsert(e) {
    handleClose(e);
    onChange(linkPlugin.add(editorState, url));
  }

  function handleRemove() {
    onChange(linkPlugin.remove(editorState));
  }

  return (
    <>
      <Dialog open={open} onClose={handleClose} aria-labelledby="insert-link">
        <DialogTitle id="insert-link">Insert Link</DialogTitle>
        <DialogContent>
          <DialogContentText>
            To insert a link insert the url and press insert.
          </DialogContentText>
          <TextField
            margin="dense"
            label="URL"
            fullWidth
            value={url}
            onChange={handleURL}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <Button onClick={handleInsert} color="primary">
            Insert
          </Button>
        </DialogActions>
      </Dialog>
      <EditorButton title="Link" onClick={handleClick}>
        <InsertLinkIcon />
      </EditorButton>
      <EditorButton title="Remove Link" onClick={handleRemove}>
        <LinkOffIcon />
      </EditorButton>
    </>
  );
};

LinkButton.propTypes = {
  lockEditor: PropTypes.func.isRequired,
  focusEditor: PropTypes.func.isRequired,
  editorState: PropTypes.object,
  onChange: PropTypes.func,
};

export default LinkButton;
