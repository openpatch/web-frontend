import { action } from '@storybook/addon-actions';
import InsertPhoto from '@material-ui/icons/InsertPhoto';
import EditorButton from './EditorButton';

export default {
  title: 'Field/RichTextField/EditorButton',
  component: EditorButton
};

export const Default = () => (
  <EditorButton title="A button" onClick={action('click')}>
    <InsertPhoto />
  </EditorButton>
);
