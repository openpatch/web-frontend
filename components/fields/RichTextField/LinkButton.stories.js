import { action } from '@storybook/addon-actions';
import LinkButton from './LinkButton';
import { EditorState } from 'draft-js';

export default {
  title: 'Field/RichTextField/LinkButton',
  component: LinkButton
};

export const Default = () => (
  <LinkButton
    focusEditor={action('focus editor')}
    onChange={action('change')}
    editorState={EditorState.createEmpty()}
  />
);
