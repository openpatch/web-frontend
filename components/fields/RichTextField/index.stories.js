import { useState } from 'react';
import { action } from '@storybook/addon-actions';
import { boolean } from '@storybook/addon-knobs';
import '@draft-js-plugins/emoji/lib/plugin.css';

import RichTextField from './';
import { getLoremIpsum } from '../../../utils/editor';

export default {
  title: 'Field/RichTextField',
  component: RichTextField,
};

export const Default = () => (
  <RichTextField
    input={{
      value: getLoremIpsum(),
      onChange: action('change'),
    }}
  />
);

export const WithState = () => {
  const [value, setValue] = useState(getLoremIpsum());

  function handleChange(value) {
    setValue(value);
    action('change')(value);
  }

  return (
    <RichTextField
      input={{
        value,
        onChange: handleChange,
      }}
    />
  );
};

export const Preview = () => {
  return (
    <RichTextField
      preview={boolean('preview', true)}
      input={{
        value: getLoremIpsum(),
        onChange: action('change'),
      }}
    />
  );
};
