import { action } from '@storybook/addon-actions';
import { EditorState } from 'draft-js';
import Home from '@material-ui/icons/Home';

import Toolbar from './Toolbar';

export default {
  title: 'Field/RichTextField/Toolbar',
  component: Toolbar
};

export const Default = () => (
  <Toolbar
    focusEditor={action('focus editor')}
    editorState={EditorState.createEmpty()}
    onChange={action('change')}
  />
);

export const WithExtraButtons = () => (
  <Toolbar
    focusEditor={action('focus editor')}
    editorState={EditorState.createEmpty()}
    onChange={action('change')}
    extraButtons={[
      {
        title: 'Extra Button',
        style: 'BIGGER',
        icon: <Home />
      }
    ]}
  />
);

export const Deactivate = () => (
  <Toolbar
    focusEditor={action('focus editor')}
    editorState={EditorState.createEmpty()}
    onChange={action('change')}
    deactivate={[
      'link',
      'image',
      'embed',
      'font-size',
      'format-color-text',
      'format-color-fill',
      'code-block',
      'ordered-list',
      'unordered-list',
      'blockquote'
    ]}
  />
);
