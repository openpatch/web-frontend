import { memo } from 'react';
import PropTypes from 'prop-types';
import { RichUtils } from 'draft-js';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import FormatBold from '@material-ui/icons/FormatBold';
import FormatItalic from '@material-ui/icons/FormatItalic';
import FormatTitle from '@material-ui/icons/FormatSize';
import FormatUnderlined from '@material-ui/icons/FormatUnderlined';
import FormatCode from '@material-ui/icons/Code';
import FormatListBulleted from '@material-ui/icons/FormatListBulleted';
import FormatListNumbered from '@material-ui/icons/FormatListNumbered';
import FormatQuote from '@material-ui/icons/FormatQuote';
import FormatSpaceBar from '@material-ui/icons/SpaceBar';
import FormatColorText from '@material-ui/icons/FormatColorText';
import FormatColorFill from '@material-ui/icons/FormatColorFill';
import FormatClear from '@material-ui/icons/FormatClear';
import _isEqual from 'deep-equal';

import EditorButton from './EditorButton';
import MenuButton from './MenuButton';
import LinkButton from './LinkButton';
import EmbedButton from './EmbedButton';
import ImageButton from './ImageButton';
import { removeAllStylesFromSelection } from '../../../utils/editor';
import colorPlugin from '../../../utils/editor/color-plugin';

const useStyles = makeStyles((theme) => {
  const borderColor =
    theme.palette.type === 'light'
      ? 'rgba(0, 0, 0, 0.23)'
      : 'rgba(255, 255, 255, 0.23)';
  return {
    editorBar: {
      display: 'flex',
      flexWrap: 'wrap',
      borderBottomWidth: 1,
      borderBottomStyle: 'solid',
      borderColor,
    },
  };
});

const Toolbar = memo(
  ({
    focusEditor,
    editorState,
    onChange,
    extraButtons,
    lockEditor,
    deactivate,
  }) => {
    const classes = useStyles();

    const setStyle = (style) => {
      onChange(RichUtils.toggleInlineStyle(editorState, style));
    };

    const setBlock = (block) => {
      onChange(RichUtils.toggleBlockType(editorState, block));
    };

    const removeAllStyles = () => {
      onChange(removeAllStylesFromSelection(editorState));
    };

    return (
      <Box className={classes.editorBar}>
        {!deactivate.includes('font-size') && (
          <MenuButton
            id="format-size"
            title="Text Size"
            setStyle={setStyle}
            setBlock={setBlock}
            focusEditor={focusEditor}
            options={[
              { title: 'Normal', block: 'unstyled' },
              { title: 'Title', block: 'header-one' },
              { title: 'Subtitle', block: 'header-two' },
            ]}
          >
            <FormatTitle />
          </MenuButton>
        )}
        <EditorButton
          title="Bold"
          onClick={() => {
            setStyle('BOLD');
          }}
        >
          <FormatBold />
        </EditorButton>
        {!deactivate.includes('bold') && (
          <EditorButton
            title="Italic"
            onClick={() => {
              setStyle('ITALIC');
            }}
          >
            <FormatItalic />
          </EditorButton>
        )}
        {!deactivate.includes('underline') && (
          <EditorButton
            title="Underline"
            onClick={() => {
              setStyle('UNDERLINE');
            }}
          >
            <FormatUnderlined />
          </EditorButton>
        )}
        {!deactivate.includes('monospace') && (
          <EditorButton
            title="Monospace"
            onClick={() => {
              setStyle('MONOSPACE');
            }}
          >
            <FormatSpaceBar />
          </EditorButton>
        )}
        {!deactivate.includes('format-color-text') && (
          <MenuButton
            id="format-color-text"
            title="Color Text"
            setStyle={setStyle}
            setBlock={setBlock}
            focusEditor={focusEditor}
            options={colorPlugin.colors.map(({ label }) => ({
              title: label,
              style: `TEXT-COLOR-${label}`,
            }))}
          >
            <FormatColorText />
          </MenuButton>
        )}
        {!deactivate.includes('format-color-fill') && (
          <MenuButton
            id="format-color-fill"
            title="Color Background"
            setStyle={setStyle}
            setBlock={setBlock}
            focusEditor={focusEditor}
            options={colorPlugin.colors.map(({ label }) => ({
              title: label,
              style: `HIGHLIGHT-${label}`,
            }))}
          >
            <FormatColorFill />
          </MenuButton>
        )}
        {!deactivate.includes('unordered-list') && (
          <EditorButton
            title="List Bulleted"
            onClick={() => {
              setBlock('unordered-list-item');
            }}
          >
            <FormatListBulleted />
          </EditorButton>
        )}
        {!deactivate.includes('ordered-list') && (
          <EditorButton
            title="List Numbered"
            onClick={() => {
              setBlock('ordered-list-item');
            }}
          >
            <FormatListNumbered />
          </EditorButton>
        )}
        {!deactivate.includes('code-block') && (
          <EditorButton
            title="Code Block"
            onClick={() => {
              setBlock('code-block');
            }}
          >
            <FormatCode />
          </EditorButton>
        )}
        {!deactivate.includes('blockquote') && (
          <EditorButton
            title="Blockquote"
            onClick={() => {
              setBlock('blockquote');
            }}
          >
            <FormatQuote />
          </EditorButton>
        )}
        {extraButtons.map(({ title, style, icon }) => (
          <EditorButton
            key={style}
            title={title}
            onClick={() => {
              setStyle(style);
            }}
          >
            {icon}
          </EditorButton>
        ))}
        <EditorButton title="Remove All Styles" onClick={removeAllStyles}>
          <FormatClear />
        </EditorButton>
        {!deactivate.includes('image') && (
          <ImageButton
            editorState={editorState}
            onChange={onChange}
            focusEditor={focusEditor}
            lockEditor={lockEditor}
          />
        )}
        {!deactivate.includes('link') && false && (
          <LinkButton
            editorState={editorState}
            onChange={onChange}
            focusEditor={focusEditor}
            lockEditor={lockEditor}
          />
        )}
        {!deactivate.includes('embed') && (
          <EmbedButton
            editorState={editorState}
            onChange={onChange}
            focusEditor={focusEditor}
            lockEditor={lockEditor}
          />
        )}
      </Box>
    );
  },
  _isEqual
);

Toolbar.displayName = 'Toolbar';

Toolbar.propTypes = {
  deactivate: PropTypes.arrayOf(PropTypes.string),
  focusEditor: PropTypes.func.isRequired,
  lockEditor: PropTypes.func.isRequired,
  editorState: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  extraButtons: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string,
      style: PropTypes.string,
      icon: PropTypes.any,
    })
  ),
};

Toolbar.defaultProps = {
  extraButtons: [],
  deactivate: [],
};

export default Toolbar;
