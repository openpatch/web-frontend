import PropTypes from 'prop-types';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';

function EditorButton({ title, children, onClick }) {
  return (
    <Tooltip title={title}>
      <IconButton
        onClick={onClick}
        onMouseDown={e => {
          e.stopPropagation();
          e.preventDefault();
        }}
      >
        {children}
      </IconButton>
    </Tooltip>
  );
}

EditorButton.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.any,
  onClick: PropTypes.func
};

export default EditorButton;
