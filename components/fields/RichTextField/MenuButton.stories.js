import InsertPhoto from '@material-ui/icons/InsertPhoto';
import { action } from '@storybook/addon-actions';
import MenuButton from './MenuButton';

export default {
  title: 'Field/RichTextField/MenuButton',
  component: MenuButton
};

export const Default = () => (
  <MenuButton
    id="my-menu"
    title="My Menu"
    focusEditor={action('focus editor')}
    setStyle={action('set style')}
    setBlock={action('set block')}
    options={[
      {
        title: 'Style COOL',
        style: 'COOL'
      },
      {
        title: 'Block SMALL',
        block: 'SMALL'
      },
      {
        title: 'Combined',
        style: 'COOL',
        block: 'SMALL'
      }
    ]}
  >
    <InsertPhoto />
  </MenuButton>
);
