import { useState } from 'react';
import PropTypes from 'prop-types';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

import EditorButton from './EditorButton';

const MenuButton = ({
  id,
  title,
  children,
  focusEditor,
  setStyle,
  setBlock,
  options
}) => {
  const [anchorEl, setAnchorEl] = useState(null);

  function open(e) {
    setAnchorEl(e.currentTarget);
  }

  function close(block, style) {
    setAnchorEl(null);
    focusEditor();
    if (block) {
      setBlock(block);
    }
    if (style) {
      setStyle(style);
    }
  }

  return (
    <>
      <EditorButton
        aria-controls={id}
        aria-haspopup="true"
        title={title}
        onClick={open}
      >
        {children}
      </EditorButton>
      <Menu
        id={id}
        disableAutoFocus={true}
        disableEnforceFocus={true}
        disableRestoreFocus={true}
        disableAutoFocusItem={true}
        autoFocus={false} // eslint-disable-line jsx-a11y/no-autofocus
        open={Boolean(anchorEl)}
        anchorEl={anchorEl}
        onClose={close}
      >
        {options.map(({ title, block, style }) => (
          <MenuItem
            key={title}
            onMouseDown={e => e.preventDefault()}
            onClick={e => {
              e.preventDefault();
              close(block, style);
            }}
          >
            {title}
          </MenuItem>
        ))}
      </Menu>
    </>
  );
};

MenuButton.propTypes = {
  id: PropTypes.string,
  title: PropTypes.string,
  focusEditor: PropTypes.func.isRequired,
  setStyle: PropTypes.func.isRequired,
  setBlock: PropTypes.func.isRequired,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string,
      block: PropTypes.string,
      style: PropTypes.string
    })
  ).isRequired,
  children: PropTypes.any.isRequired
};

export default MenuButton;
