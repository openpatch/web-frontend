import { memo } from 'react';
import { Trans, t } from '@lingui/macro';
import PropTypes from 'prop-types';
import { Field, useField } from 'react-final-form';
import MenuItem from '@material-ui/core/MenuItem';
import ListSubheader from '@material-ui/core/ListSubheader';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';

import TextField from './TextField';
import RichTextField from './RichTextField';
import groups, { Form } from '../../formats';
import FormSection from '../FormSection';
import FormSectionHeader from '../FormSectionHeader';
import FormSectionContent from '../FormSectionContent';

const formatMenuItems = [];
groups.forEach(({ id, label, formats }) => {
  formatMenuItems.push(<ListSubheader key={id}>{label}</ListSubheader>);
  formats
    .sort((f1, f2) => f1.label > f2.label)
    .forEach(({ id, label }) => {
      formatMenuItems.push(
        <MenuItem key={id} value={id}>
          {label}
        </MenuItem>
      );
    });
});

const TaskField = memo(({ field, moveUp, moveDown, deleteTask }) => {
  const {
    input: { value: formatType },
  } = useField(`${field}.formatType`);
  const {
    input: { value, onChange },
  } = useField(`${field}.data`);

  return (
    <FormSection>
      <FormSectionHeader>
        <ButtonGroup>
          <Button onClick={moveUp}>
            <Trans>Up</Trans>
          </Button>
          <Button onClick={moveDown}>
            <Trans>Down</Trans>
          </Button>
          <Button onClick={deleteTask}>
            <Trans>Remove</Trans>
          </Button>
        </ButtonGroup>
      </FormSectionHeader>
      <FormSectionContent>
        <Field
          name={`${field}.formatType`}
          label={t`Type`}
          fullWidth
          component={TextField}
          select
        >
          {formatMenuItems}
        </Field>
        <Field
          name={`${field}.task`}
          label={t`Task Title`}
          fullWidth
          component={TextField}
        />
        <Field
          name={`${field}.text`}
          label={t`Task Description`}
          fullWidth
          component={RichTextField}
        />
        <Form
          type={formatType}
          onChange={onChange}
          value={value}
          field={`${field}.data`}
        />
      </FormSectionContent>
    </FormSection>
  );
});

TaskField.displayName = 'TaskField';

TaskField.propTypes = {
  field: PropTypes.string,
  moveUp: PropTypes.func,
  moveDown: PropTypes.func,
  deleteTask: PropTypes.func,
};

export default TaskField;
