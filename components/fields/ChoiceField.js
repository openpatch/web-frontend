import PropTypes from 'prop-types';
import Radio from '@material-ui/core/Radio';
import Checkbox from '@material-ui/core/Checkbox';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import FormHelperText from '@material-ui/core/FormHelperText';

const ChoiceField = ({
  input: { value, onChange },
  label,
  helperText,
  meta: { touched, error },
  options,
  allowMultiple,
  ...custom
}) => {
  const handleChange = (id, checked) => {
    let nextValue = allowMultiple ? { ...value } : {};
    if (checked) {
      nextValue[id] = true;
    } else {
      delete nextValue[id];
    }
    onChange(nextValue);
  };

  const ControlComponent = allowMultiple ? Checkbox : Radio;
  return (
    <FormControl
      error={touched && error}
      margin="dense"
      variant="outlined"
      {...custom}
    >
      {label && <FormLabel>{label}</FormLabel>}
      <FormGroup>
        {options.map((option, i) => (
          <FormControlLabel
            key={i}
            control={
              <ControlComponent
                checked={value[i] || false}
                onChange={(_, checked) => handleChange(i, checked)}
                color="primary"
              />
            }
            label={option}
          />
        ))}
      </FormGroup>
      {helperText && <FormHelperText>{helperText}</FormHelperText>}
    </FormControl>
  );
};

ChoiceField.propTypes = {
  input: PropTypes.shape({
    onChange: PropTypes.func.isRequired,
    value: PropTypes.object,
  }).isRequired,
  label: PropTypes.string,
  helperText: PropTypes.string,
  meta: PropTypes.shape({
    touched: PropTypes.bool,
    error: PropTypes.any,
  }),
  options: PropTypes.arrayOf(PropTypes.string).isRequired,
  allowMultiple: PropTypes.bool,
};

ChoiceField.defaultProps = {
  meta: {},
};

export default ChoiceField;
