import PropTypes from 'prop-types';
import EvaluationField from './EvaluationField';

function EvaluationFieldArray({ fields }) {
  return fields.map((field, i) => <EvaluationField key={i} field={field} />);
}

EvaluationFieldArray.propTypes = {
  fields: PropTypes.shape({
    map: PropTypes.func.isRequired,
  }),
};

export default EvaluationFieldArray;
