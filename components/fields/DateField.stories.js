import { action } from '@storybook/addon-actions';
import DateField from './DateField';

export default {
  title: 'Field/DateField',
  component: DateField,
};

export const Default = () => (
  <DateField
    input={{
      onChange: action('change'),
    }}
  />
);
