import PropTypes from 'prop-types';
import { DatePicker } from '@material-ui/pickers';

function DateField({ input, label, meta: { touched, error }, ...custom }) {
  return (
    <DatePicker
      clearable
      label={label}
      error={touched && Boolean(error)}
      ampm={false}
      showTodayButton
      inputVariant="outlined"
      margin="dense"
      {...input}
      value={input.value == '' ? null : input.value}
      {...custom}
    ></DatePicker>
  );
}

DateField.propTypes = {
  input: PropTypes.shape({
    value: PropTypes.string,
    onChange: PropTypes.func,
  }).isRequired,
  label: PropTypes.string,
  meta: PropTypes.shape({
    touched: PropTypes.bool,
    error: PropTypes.string,
  }),
};

DateField.defaultProps = {
  meta: {},
};

export default DateField;
