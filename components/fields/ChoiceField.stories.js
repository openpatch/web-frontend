import { action } from '@storybook/addon-actions';
import ChoiceField from './ChoiceField';

export default {
  title: 'Field/ChoiceField',
  component: ChoiceField,
};

export const Default = () => (
  <ChoiceField
    input={{
      value: { '0': true },
      onChange: action('change'),
    }}
    label="Single Choice"
    options={['test', 'foo', 'bar']}
  />
);

export const Multiple = () => (
  <ChoiceField
    input={{
      value: { '0': true, '1': true },
      onChange: action('change'),
    }}
    label="Multiple Choice"
    options={['test', 'foo', 'bar']}
    allowMultiple={true}
  />
);

export const Error = () => (
  <ChoiceField
    meta={{
      touched: true,
      error: true,
    }}
    label="Multiple Choice"
    helperText="There is an error"
    input={{
      value: { '0': true, '1': true },
      onChange: action('change'),
    }}
    options={['test', 'foo', 'bar']}
    allowMultiple={true}
  />
);
