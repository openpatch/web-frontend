import { useRef } from 'react';
import { t, Trans } from '@lingui/macro';
import PropTypes from 'prop-types';
import { Field } from 'react-final-form';
import { FieldArray } from 'react-final-form-arrays';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import DragIndicatorIcon from '@material-ui/icons/DragIndicator';
import Tooltip from '@material-ui/core/Tooltip';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';

import { uuidv4 } from '../../utils/uuid';

export function Element({
  index,
  name,
  onDelete,
  component,
  locales,
  draggableId,
  ...props
}) {
  return (
    <Draggable draggableId={draggableId} index={index}>
      {(provided) => (
        <Box
          mb={1}
          display="flex"
          ref={provided.innerRef}
          {...provided.draggableProps}
        >
          <Box
            mr={1}
            display="flex"
            alignItems="center"
            justifyContent="center"
            {...provided.dragHandleProps}
          >
            <DragIndicatorIcon />
          </Box>
          <Field
            name={name}
            key={draggableId}
            {...props}
            component={component}
          />
          {onDelete && (
            <Box
              ml={1}
              display="flex"
              alignItems="center"
              justifyContent="center"
              {...provided.dragHandleProps}
            >
              <Tooltip
                title={locales.remove(index)}
                aria-label={locales.remove(index)}
              >
                <IconButton onClick={() => onDelete(index)} size="small">
                  <DeleteIcon />
                </IconButton>
              </Tooltip>
            </Box>
          )}
        </Box>
      )}
    </Draggable>
  );
}

Element.propTypes = {
  draggableId: PropTypes.string,
  component: PropTypes.func,
  name: PropTypes.string,
  onDelete: PropTypes.func,
  index: PropTypes.number.isRequired,
  locales: PropTypes.shape({
    remove: PropTypes.func,
  }),
};

Element.defaultProps = {
  value: () => null,
  RendererProps: {},
  locales: {
    remove: (id) => t`Remove ${id}`,
  },
};

const InnerList = ({
  fields,
  keys,
  componentProps,
  component,
  locales,
  ...props
}) =>
  fields.map((field, index) => {
    let key = keys.current?.[index];
    if (!key) {
      key = uuidv4();
      keys.current.push(key);
    }
    return (
      <Element
        {...props}
        key={key}
        draggableId={key}
        name={field}
        locales={locales}
        index={index}
        {...componentProps}
        component={component}
      />
    );
  });

const DnDFieldArray = ({
  fields,
  label,
  helperText,
  fullWidth,
  locales,
  addValue,
  elementComponent,
  elementComponentProps,
  ...custom
}) => {
  let keys = useRef([]);

  function handleDragEnd(result) {
    if (!result.destination) {
      return;
    }

    if (result.destination.index === result.source.index) {
      return;
    }

    keys.current.splice(
      result.destination.index,
      0,
      keys.current.splice(result.source.index, 1)[0]
    );
    fields.move(result.source.index, result.destination.index);
  }

  function handleAdd() {
    keys.current.push(uuidv4());
    fields.push(addValue);
  }

  function handleDelete(index) {
    keys.current.splice(index, 1);
    fields.remove(index);
  }

  return (
    <FormControl fullWidth={fullWidth} margin="dense" {...custom}>
      {label && <FormLabel>{label}</FormLabel>}
      <DragDropContext onDragEnd={handleDragEnd}>
        <Droppable droppableId="list">
          {(provided) => (
            <div ref={provided.innerRef} {...provided.droppableProps}>
              <InnerList
                keys={keys}
                fields={fields}
                onDelete={handleDelete}
                component={elementComponent}
                componentProps={elementComponentProps}
                locales={locales}
              />
              {provided.placeholder}
            </div>
          )}
        </Droppable>
      </DragDropContext>
      <Button onClick={handleAdd}>{locales.add}</Button>
      {helperText && <FormHelperText>{helperText}</FormHelperText>}
    </FormControl>
  );
};

DnDFieldArray.propTypes = {
  fields: PropTypes.object.isRequired,
  label: PropTypes.node,
  helperText: PropTypes.node,
  fullWidth: PropTypes.bool,
  addValue: PropTypes.any,
  elementComponent: PropTypes.element.isRequired,
  elementComponentProps: PropTypes.object,
  locales: PropTypes.shape({
    add: PropTypes.node,
    remove: PropTypes.func,
    element: PropTypes.func,
  }),
};

DnDFieldArray.defaultProps = {
  addValue: undefined,
  elementComponentProps: {},
  locales: {
    add: <Trans>Add Element</Trans>,
    remove: (id) => t`Remove ${id}`,
    element: (id) => t`Element ${id}`,
  },
};

const DnDFieldArrayOuter = ({ name, ...props }) => (
  <FieldArray name={name} component={DnDFieldArray} {...props} />
);

DnDFieldArrayOuter.propTypes = {
  name: PropTypes.string.isRequired,
};

export default DnDFieldArrayOuter;
