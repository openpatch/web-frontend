import { useState } from 'react';
import { action } from '@storybook/addon-actions';
import PrivacySelectField from './PrivacySelectField';

export default {
  title: 'Field/PrivacySelectField',
  component: PrivacySelectField,
};

export const Default = () => (
  <PrivacySelectField
    meta={{}}
    input={{
      value: 'public',
      onChange: action('change'),
    }}
  />
);

export const WithState = () => {
  const [value, setValue] = useState('public');
  return (
    <PrivacySelectField
      meta={{}}
      input={{
        value: value,
        onChange: (e) => setValue(e.target.value),
      }}
    />
  );
};
