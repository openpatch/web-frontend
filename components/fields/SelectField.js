import { memo } from 'react';
import MuiTextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import PropTypes from 'prop-types';
import _isEqual from 'deep-equal';

const SelectField = memo(
  ({
    input,
    label,
    meta: { touched, error },
    options,
    getValue,
    getText,
    ...custom
  }) => {
    return (
      <MuiTextField
        label={label}
        error={touched && Boolean(error)}
        variant="outlined"
        margin="dense"
        select
        {...input}
        {...custom}
      >
        {options.map((option) => (
          <MenuItem key={getValue(option)} value={getValue(option)}>
            {getText(option)}
          </MenuItem>
        ))}
      </MuiTextField>
    );
  },
  _isEqual
);

SelectField.displayName = 'SelectField';

SelectField.propTypes = {
  options: PropTypes.array,
  getText: PropTypes.func,
  getValue: PropTypes.func,
  input: PropTypes.object,
  label: PropTypes.string,
  meta: PropTypes.shape({
    touched: PropTypes.bool,
    error: PropTypes.string,
  }),
};

SelectField.defaultProps = {
  options: [],
  getText: (o) => o,
  getValue: (o) => o,
};

export default SelectField;
