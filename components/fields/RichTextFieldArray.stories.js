import { action } from '@storybook/addon-actions';
import arrayMutators from 'final-form-arrays';
import { FieldArray } from 'react-final-form-arrays';

import RichTextFieldArray from './RichTextFieldArray';
import MForm from '../../components/Form';

export default {
  title: 'Field/RichTextFieldArray',
  component: RichTextFieldArray,
};

export const Empty = () => (
  <MForm
    onSubmit={action('submit')}
    mutators={{ ...arrayMutators }}
    render={() => <FieldArray component={RichTextFieldArray} name="form" />}
  />
);
