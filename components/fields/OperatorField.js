import SelectField from './SelectField';

function OperatorField(props) {
  return (
    <SelectField
      {...props}
      options={['>', '>=', '==', '<=', '<']}
      getValue={(o) => o}
      getText={(o) => o}
    />
  );
}

OperatorField.propTypes = {};

export default OperatorField;
