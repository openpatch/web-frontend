import { Trans, t } from '@lingui/macro';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import DeleteIcon from '@material-ui/icons/Delete';
import { Field } from 'react-final-form';

import TextField from './TextField';

const TextFieldArray = ({ fields, label, helperText, fullWidth, ...props }) => {
  <FormControl fullWidth={fullWidth} margin="dense" {...props}>
    {label && <FormLabel>{label}</FormLabel>}
    {fields.map((field, index) => (
      <div key={index} style={{ display: 'flex', alignItems: 'center' }}>
        <Field name={field} fullWidth={fullWidth} component={TextField} />
        <Tooltip title={t`Remove`} aria-label="Remove">
          <IconButton onClick={() => fields.remove(index)}>
            <DeleteIcon />
          </IconButton>
        </Tooltip>
      </div>
    ))}
    <Button onClick={() => fields.push()}>
      <Trans>Add Element</Trans>
    </Button>
    {helperText && <FormHelperText>{helperText}</FormHelperText>}
  </FormControl>;
};

TextFieldArray.propTypes = {
  fields: PropTypes.object.isRequired,
  label: PropTypes.node,
  helperText: PropTypes.node,
  fullWidth: PropTypes.bool,
};

TextFieldArray.defaultProps = {};

export default TextFieldArray;
