import { t, Trans } from '@lingui/macro';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import DeleteIcon from '@material-ui/icons/Delete';
import { Field } from 'react-final-form';

import SelectField from './SelectField';

const SelectFieldArray = ({
  fields,
  label,
  helperText,
  fullWidth,
  getText,
  getValue,
  options,
  ...custom
}) => (
  <FormControl fullWidth={fullWidth} margin="dense" {...custom}>
    {label && <FormLabel>{label}</FormLabel>}
    {fields.map((field, index) => (
      <div key={index} style={{ display: 'flex', alignItems: 'center' }}>
        <Field
          name={field}
          fullWidth={fullWidth}
          component={SelectField}
          options={options}
          getValue={getValue}
          getText={getText}
        />
        <Tooltip title={t`Remove`} aria-label="Remove">
          <IconButton onClick={() => fields.remove(index)}>
            <DeleteIcon />
          </IconButton>
        </Tooltip>
      </div>
    ))}
    <Button onClick={() => fields.push()}>
      <Trans>Add Element</Trans>
    </Button>
    {helperText && <FormHelperText>{helperText}</FormHelperText>}
  </FormControl>
);

SelectFieldArray.propTypes = {
  fields: PropTypes.object.isRequired,
  label: PropTypes.node,
  helperText: PropTypes.node,
  fullWidth: PropTypes.bool,
  options: PropTypes.array.isRequired,
  getText: PropTypes.func.isRequired,
  getValue: PropTypes.func.isRequired,
};

SelectFieldArray.defaultProps = {};

export default SelectFieldArray;
