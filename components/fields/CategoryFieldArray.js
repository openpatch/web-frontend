import { Fragment } from 'react';
import PropTypes from 'prop-types';
import Checkbox from '@material-ui/core/Checkbox';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';

import { useCategories } from '../../utils/category';

const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
const checkedIcon = <CheckBoxIcon fontSize="small" />;

function CategoryFieldArray({ input: { onChange, value } }) {
  const { categories } = useCategories();

  return (
    <Autocomplete
      multiple
      id="checkboxes-tags-demo"
      options={categories}
      loading={Boolean(categories)}
      disableCloseOnSelect
      getOptionLabel={(option) => option.title}
      onChange={onChange}
      value={value}
      renderOption={(option, { selected }) => (
        <Fragment>
          <Checkbox
            icon={icon}
            checkedIcon={checkedIcon}
            style={{ marginRight: 8 }}
            checked={selected}
          />
          {option.title}
        </Fragment>
      )}
      style={{ width: 500 }}
      renderInput={(params) => (
        <TextField
          {...params}
          variant="outlined"
          label="Checkboxes"
          placeholder="Favorites"
        />
      )}
    />
  );
}

CategoryFieldArray.propTypes = {
  input: PropTypes.shape({
    value: PropTypes.arrayOf(PropTypes.string),
    onChange: PropTypes.func,
  }),
};

export default CategoryFieldArray;
