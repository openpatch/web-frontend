import { t, Trans } from '@lingui/macro';
import PropTypes from 'prop-types';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import DeleteIcon from '@material-ui/icons/Delete';
import { Field } from 'react-final-form';

import RichTextField from './RichTextField';

const RichTextFieldArray = ({
  fields,
  label,
  helperText,
  fullWidth,
  deactivateFromToolbar,
  extraButtons,
  locales,
  ...custom
}) => {
  return (
    <FormControl fullWidth={fullWidth} margin="dense" {...custom}>
      {label && <FormLabel>{label}</FormLabel>}
      {fields.map((field, index) => (
        <div key={field} style={{ display: 'flex', alignItems: 'center' }}>
          <Box width="100%">
            <Field
              label={locales.element(index)}
              name={field}
              fullWidth={fullWidth}
              component={RichTextField}
              deactivateFromToolbar={deactivateFromToolbar}
              extraButtons={extraButtons}
            />
          </Box>
          <Tooltip
            title={locales.remove(index)}
            aria-label={locales.remove(index)}
          >
            <IconButton onClick={() => fields.remove(index)}>
              <DeleteIcon />
            </IconButton>
          </Tooltip>
        </div>
      ))}
      <Button onClick={() => fields.push()}>{locales.add}</Button>
      {helperText && <FormHelperText>{helperText}</FormHelperText>}
    </FormControl>
  );
};

RichTextFieldArray.propTypes = {
  fields: PropTypes.object.isRequired,
  label: PropTypes.node,
  helperText: PropTypes.node,
  fullWidth: PropTypes.bool,
  deactivateFromToolbar: PropTypes.array,
  extraButtons: PropTypes.array,
  locales: PropTypes.shape({
    add: PropTypes.node,
    remove: PropTypes.func,
    element: PropTypes.func,
  }),
};

RichTextFieldArray.defaultProps = {
  locales: {
    add: <Trans>Add Element</Trans>,
    remove: (id) => t`Remove ${id}`,
    element: (id) => t`Element ${id}`,
  },
};

export default RichTextFieldArray;
