import PropTypes from 'prop-types';
import { Field, useField } from 'react-final-form';

import Checkbox from './Checkbox';
import FormSection from '../FormSection';
import FormSectionHeader from '../FormSectionHeader';
import FormSectionContent from '../FormSectionContent';
import { Evaluation } from '../../formats';

function EvaluationField({ field }) {
  const {
    input: { value },
  } = useField(field);
  const {
    input: { onChange },
  } = useField(`${field}.evaluation`);

  return (
    <FormSection>
      <FormSectionHeader>{value?.task}</FormSectionHeader>
      <FormSectionContent>
        <Field
          name={`${field}.evaluation.skip`}
          label="Skip Evaluation"
          type="checkbox"
          component={Checkbox}
        />
        {!value?.evaluation?.skip && (
          <Evaluation
            type={value?.formatType}
            field={`${field}.evaluation`}
            onChange={onChange}
            value={value?.evaluation}
            data={value?.data || {}}
          />
        )}
      </FormSectionContent>
    </FormSection>
  );
}

EvaluationField.propTypes = {
  field: PropTypes.string.isRequired,
};

export default EvaluationField;
