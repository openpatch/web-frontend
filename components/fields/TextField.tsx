import MuiTextField, {
  TextFieldProps as MuiTextFieldProps,
} from '@material-ui/core/TextField';
import { FieldRenderProps } from 'react-final-form';

export type TextFieldProps = FieldRenderProps<string, HTMLElement> &
  MuiTextFieldProps;

const TextField = ({
  input,
  label,
  meta: { touched, error },
  ...custom
}: TextFieldProps) => {
  return (
    <MuiTextField
      label={label}
      error={touched && Boolean(error)}
      helperText={error}
      variant="outlined"
      margin="dense"
      {...input}
      {...custom}
    ></MuiTextField>
  );
};

export default TextField;
