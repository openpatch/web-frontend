import { Trans } from '@lingui/macro';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MenuItem from '@material-ui/core/MenuItem';
import MuiTextField, { TextFieldProps } from '@material-ui/core/TextField';
import NotListedIcon from '@material-ui/icons/Link';
import PrivateIcon from '@material-ui/icons/Lock';
import PublicIcon from '@material-ui/icons/Public';
import { FieldRenderProps } from 'react-final-form';

export type PrivacySelectFieldProps = FieldRenderProps<string, HTMLElement> &
  TextFieldProps;

const PrivacySelectField = ({
  meta: { touched, error },
  input,
  ...custom
}: PrivacySelectFieldProps) => {
  return (
    <MuiTextField
      label={<Trans>Privacy</Trans>}
      error={touched && Boolean(error)}
      variant="outlined"
      margin="dense"
      select
      SelectProps={{
        renderValue: (value: any) => <Trans id={value} />, // eslint-disable-line react/display-name
      }}
      {...input}
      {...custom}
    >
      <MenuItem value="public">
        <ListItemIcon>
          <PublicIcon />
        </ListItemIcon>
        <ListItemText
          primary={<Trans id="public" />}
          secondary={<Trans>Every user can see this.</Trans>}
        />
      </MenuItem>
      <MenuItem value="notlisted">
        <ListItemIcon>
          <NotListedIcon />
        </ListItemIcon>
        <ListItemText
          primary={<Trans id="notlisted" />}
          secondary={<Trans>Every user, who has the link, can see this.</Trans>}
        />
      </MenuItem>
      <MenuItem value="private">
        <ListItemIcon>
          <PrivateIcon />
        </ListItemIcon>
        <ListItemText
          primary={<Trans id="private" />}
          secondary={<Trans>Only you can see this.</Trans>}
        />
      </MenuItem>
    </MuiTextField>
  );
};

export default PrivacySelectField;
