import { DateTimePicker, DateTimePickerProps } from '@material-ui/pickers';
import { FieldRenderProps } from 'react-final-form';

export type DateTimeField = FieldRenderProps<string, HTMLElement> &
  Omit<DateTimePickerProps, 'value' | 'onChange'>;

function DateTimeField({
  input,
  label,
  meta: { touched, error },
  ...custom
}: DateTimeField) {
  return (
    <DateTimePicker
      clearable
      label={label}
      error={touched && Boolean(error)}
      ampm={false}
      showTodayButton
      inputVariant="outlined"
      margin="dense"
      {...input}
      value={input.value == '' ? null : input.value}
      {...custom}
    ></DateTimePicker>
  );
}

export default DateTimeField;
