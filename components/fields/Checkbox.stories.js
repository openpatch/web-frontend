import { action } from '@storybook/addon-actions';
import Checkbox from './Checkbox';

export default {
  title: 'Field/Checkbox',
  component: Checkbox,
};

export const Default = () => <Checkbox label="Check me" />;

export const Checked = () => (
  <Checkbox
    label="Checked"
    input={{
      checked: true,
      onChange: action('change'),
    }}
  />
);

export const Error = () => (
  <Checkbox
    label="Checked"
    input={{
      checked: true,
      onChange: action('change'),
    }}
    meta={{
      touched: true,
      error: true,
    }}
  />
);
