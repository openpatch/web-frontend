import { action } from '@storybook/addon-actions';
import arrayMutators from 'final-form-arrays';

import DnDFieldArray from './DnDFieldArray';
import TextField from './TextField';
import RichTextField from './RichTextField';
import MForm from '../../components/Form';

export default {
  title: 'Field/DnDFieldArray',
  component: DnDFieldArray,
};

export const WithTextField = () => (
  <MForm
    onSubmit={action('submit')}
    mutators={{ ...arrayMutators }}
    initialValues={{
      form: ['aaa', 'bbb', 'ccc'],
    }}
    render={() => (
      <DnDFieldArray
        fullWidth
        elementComponent={TextField}
        elementComponentProps={{
          fullWidth: true,
        }}
        name="form"
      />
    )}
  />
);

export const WithRichTextField = () => (
  <MForm
    onSubmit={action('submit')}
    mutators={{ ...arrayMutators }}
    initialValues={{
      form: ['aaa', 'bbb', 'ccc'],
    }}
    render={() => (
      <DnDFieldArray
        fullWidth
        elementComponent={RichTextField}
        elementComponentProps={{
          fullWidth: true,
        }}
        name="form"
      />
    )}
  />
);
