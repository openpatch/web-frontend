import PropTypes from 'prop-types';
import { TimePicker } from '@material-ui/pickers';

function TimeField({ input, label, meta: { touched, error }, ...custom }) {
  function handleChange(value) {
    if (value) {
      let time = value.toISOString().split('T')[1];
      input.onChange(time);
    } else {
      input.onChange(value);
    }
  }

  let value = null;
  if (input.value != '') {
    const d = new Date().toISOString().split('T')[0];
    value = d + 'T' + input.value;
  }
  return (
    <TimePicker
      clearable
      label={label}
      error={touched && Boolean(error)}
      ampm={false}
      showTodayButton
      inputVariant="outlined"
      margin="dense"
      onChange={handleChange}
      value={value}
      {...custom}
    ></TimePicker>
  );
}

TimeField.propTypes = {
  input: PropTypes.object,
  label: PropTypes.string,
  meta: PropTypes.shape({
    touched: PropTypes.bool,
    error: PropTypes.string,
  }),
};

export default TimeField;
