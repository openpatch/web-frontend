import { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Field } from 'react-final-form';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import FormHelperText from '@material-ui/core/FormHelperText';

import CodeField from './CodeField';
import TextField from './TextField';
import { FieldArrayRenderProps } from 'react-final-form-arrays';

const useStyles = makeStyles((theme) => {
  const borderColor =
    theme.palette.type === 'light'
      ? 'rgba(0, 0, 0, 0.23)'
      : 'rgba(255, 255, 255, 0.23)';

  return {
    root: {
      flexGrow: 1,
      backgroundColor: theme.palette.background.paper,
      borderColor,
      borderTopLeftRadius: theme.shape.borderRadius,
      borderTopRightRadius: theme.shape.borderRadius,
      borderStyle: 'solid',
      borderWidth: 1,
    },
    placeholder: {
      backgroundColor: theme.palette.background.paper,
      display: 'flex',
      height: 100,
      padding: theme.spacing(1),
      alignItems: 'center',
      justifyContent: 'center',
    },
  };
});

export type CodeFieldArrayProps = {
  label: string;
  helperText?: string;
  fullWidth?: boolean;
  onGutterClick?: (index: number, row: number) => void;
} & FieldArrayRenderProps<
  {
    fileName: string;
    source: string;
  },
  HTMLElement
>;

const CodeFieldArray = ({
  fields,
  label,
  helperText,
  onGutterClick = () => null,
  fullWidth = true,
  ...props
}: CodeFieldArrayProps) => {
  const [tab, setTab] = useState(0);
  const classes = useStyles();

  function handleTabChange(_: any, value: number) {
    setTab(value);
  }
  function moveRight() {
    if (fields.length && tab < fields.length - 1) {
      fields.swap(tab, tab + 1);
      setTab((tab) => tab + 1);
    }
  }

  function moveLeft() {
    if (tab > 0) {
      fields.swap(tab - 1, tab);
      setTab((tab) => tab - 1);
    }
  }

  function deleteTab() {
    fields.remove(tab);
    setTab((tab) => (tab > 0 ? tab - 1 : 0));
  }

  function add() {
    fields.push({
      fileName: 'New File',
      source: '',
    });
    if (fields.length) {
      setTab(fields.length);
    }
  }

  return (
    <FormControl fullWidth={fullWidth} {...props}>
      {label && <FormLabel>{label}</FormLabel>}
      <div className={classes.root}>
        <Tabs
          value={tab}
          onChange={handleTabChange}
          variant="scrollable"
          scrollButtons="on"
          indicatorColor="primary"
          textColor="primary"
        >
          {fields.map((field, i) => (
            <Tab
              key={i}
              label={
                <Field
                  name={`${field}.fileName`}
                  label="Name"
                  component={TextField}
                />
              }
            />
          ))}
        </Tabs>
      </div>
      {fields.map((field, i) =>
        tab === i ? (
          <Field<{source: string, readOnlyRows: number[]}>
            key={i}
            fullWidth
            margin="none"
            name={`${field}`}
            component={props => <CodeField {...props} onGutterClick={(row: number) => onGutterClick(i, row)} />}
          />
        ) : null
      )}
      <div>
        <Button onClick={add}>Add</Button>
        <Button onClick={deleteTab}>Remove</Button>
        <Button onClick={moveLeft}>Move Left</Button>
        <Button onClick={moveRight}>Move Right</Button>
      </div>
      {helperText && <FormHelperText>{helperText}</FormHelperText>}
    </FormControl>
  );
};

export default CodeFieldArray;
