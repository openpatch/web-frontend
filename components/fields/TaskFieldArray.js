import PropTypes from 'prop-types';
import { Trans } from '@lingui/macro';
import Button from '@material-ui/core/Button';

import TaskField from './TaskField';
import FormSection from '../FormSection';
import FormSectionHeader from '../FormSectionHeader';
import FormSectionContent from '../FormSectionContent';

const TaskFieldArray = ({ fields }) => {
  function moveDown(index) {
    if (index < fields.length - 1) {
      fields.swap(index, index + 1);
    }
  }

  function moveUp(index) {
    if (index > 0) {
      fields.swap(index, index - 1);
    }
  }

  function deleteTask(index) {
    fields.remove(index);
  }

  function add() {
    fields.push({
      data: {},
      evaluation: {
        skip: true,
      },
    });
  }
  return (
    <>
      {fields.map((field, i) => (
        <TaskField
          key={i}
          field={field}
          deleteTask={() => deleteTask(i)}
          moveUp={() => moveUp(i)}
          moveDown={() => moveDown(i)}
        />
      ))}
      <FormSection>
        <FormSectionHeader></FormSectionHeader>
        <FormSectionContent>
          <Button fullWidth variant="outlined" onClick={add}>
            <Trans>Add Task</Trans>
          </Button>
        </FormSectionContent>
      </FormSection>
    </>
  );
};

TaskFieldArray.propTypes = {
  fields: PropTypes.shape({
    remove: PropTypes.func,
    push: PropTypes.func,
    swap: PropTypes.func,
    length: PropTypes.number,
    map: PropTypes.func,
  }),
};

export default TaskFieldArray;
