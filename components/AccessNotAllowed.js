import { Trans } from '@lingui/macro';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Security from 'mui-undraw/lib/Security';

function AccessNotAllowed() {
  return (
    <Box display="flex" flexDirection="column" alignItems="center">
      <Security style={{ maxWidth: 600, marginBottom: 8 }} />
      <Typography variant="subtitle1" align="center">
        <Trans>Access not allowed</Trans>
      </Typography>
    </Box>
  );
}

AccessNotAllowed.propTypes = {};

export default AccessNotAllowed;
