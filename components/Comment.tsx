import { Trans } from '@lingui/macro';
import Box from '@material-ui/core/Box';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import { useClaims } from '@services/authentification/provider';
import { Comment as CommentType } from '@services/comment/rooms';
import { useDateUtils } from '@utils/date';
import MemberAvatar from './MemberAvatar';

export type CommentProps = CommentType & { onDelete?: () => void };

const Comment = ({ member, message, createdOn, onDelete }: CommentProps) => {
  const claims = useClaims();
  const { formatDistance } = useDateUtils();
  let distance = null;
  if (createdOn) {
    distance = formatDistance(new Date(createdOn), new Date());
  }

  return (
    <Box display="flex" flexWrap="wrap" mt={2}>
      <MemberAvatar {...member} />
      <Box flex={1} ml={2} display="flex" flexDirection="column">
        <Box display="flex">
          <Typography
            style={{ marginRight: 4, fontWeight: 'bold' }}
            variant="caption"
          >
            {member?.username}
          </Typography>
          <Typography style={{ marginRight: 4 }} variant="caption">
            {distance}
          </Typography>
          {member?.id === claims?.id && (
            <Link onClick={onDelete}>
              <Trans>Delete</Trans>
            </Link>
          )}
        </Box>
        <Typography>{message}</Typography>
      </Box>
    </Box>
  );
};

export default Comment;
