import { action } from '@storybook/addon-actions';
import CodeEditor from './CodeEditor';

export default {
  title: 'CodeEditor',
  component: CodeEditor,
};

export const Empty = () => (
  <CodeEditor
    value={`class House:
    def get_number():
        pass`}
  />
);

export const WithReadOnly = () => (
  <CodeEditor
    readOnlyRows={[4, 5]}
    value={`class House:
    def get_number():
        pass
    
    def do_not_manipulate():
        pass`}
  />
);

export const WithGutterClick = () => (
  <CodeEditor
    onGutterClick={action('gutter click')}
    value={`class House:
    def get_number():
        pass
    
    def do_not_manipulate():
        pass`}
  />
);
