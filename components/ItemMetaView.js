import { Trans } from '@lingui/macro';
import Router from 'next/router';
import PropTypes from 'prop-types';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';

import PrivacyIndicator from './PrivacyIndicator';
import MemberHeader from './MemberHeader';
import LikeButton from './LikeButton';
import RemixButton from './RemixButton';
import ShareButton from './ShareButton';
import CollectionButton from './CollectionButton';
import TagsChips from './TagsChips';
import RichText from './RichText';
import Spacer from './Spacer';
import { convertFromRaw } from '../utils/editor';
import { useDateUtils } from '../utils/date';
import api from '../utils/api';
import { useNotifications } from '../utils/notification';

function ItemMetaView({
  id,
  name,
  member,
  tags,
  collections,
  createdOn,
  publicDescription,
  privacy,
  usedInTests,
  language,
  remixes,
  revalidate,
}) {
  const { formatDistance } = useDateUtils();
  const [add] = useNotifications();

  let distance = null;
  if (createdOn) {
    distance = formatDistance(new Date(createdOn), new Date());
  }

  function handleRemix() {
    api
      .post(`${process.env.NEXT_API_ITEMBANK_BROWSER}/v1/items/${id}/remix`)
      .then(({ data: { itemId } }) => {
        Router.push('/item/[id]/edit', `/item/${itemId}/edit`);
      })
      .catch(() => {
        add({
          message: <Trans>Something went wrong</Trans>,
          severity: 'error',
        });
      });
  }

  return (
    <Box>
      <Box display="flex" alignItems="center" flexWrap="wrap" mb={1}>
        <Box flex={1}>
          <Typography>{name}</Typography>
          <Box display="flex" minWidth={200} alignItems="center">
            <PrivacyIndicator privacy={privacy} />
            <Spacer />
            <Typography variant="caption">
              Is used {usedInTests} times
            </Typography>
            <Spacer />
            <Typography variant="caption">{distance}</Typography>
          </Box>
        </Box>
        <Box display="flex" justifyContent="center" alignItems="center">
          <CollectionButton
            collections={collections}
            id={id}
            type="items"
            onToggle={revalidate}
          />
          <ShareButton title={name} />
          <RemixButton remixes={remixes.length} onClick={handleRemix} />
          <LikeButton
            id={id}
            type="item"
            data={{ name, publicDescription, language }}
          />
        </Box>
      </Box>
      <Divider />
      <MemberHeader {...member} type="item" />
      <RichText editorState={convertFromRaw(publicDescription)} />
      <TagsChips tags={tags} />
    </Box>
  );
}

ItemMetaView.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
  member: PropTypes.object,
  tags: PropTypes.arrayOf(PropTypes.string),
  categories: PropTypes.arrayOf(PropTypes.string),
  collections: PropTypes.arrayOf(PropTypes.string),
  usedInTests: PropTypes.number,
  createdOn: PropTypes.string,
  publicDescription: PropTypes.object,
  privacy: PropTypes.oneOf(['private', 'public', 'notlisted']),
  language: PropTypes.string,
  remixes: PropTypes.array,
  revalidate: PropTypes.func,
};

ItemMetaView.defaultProps = {
  remixes: [],
  categories: [],
  tags: [],
  usedInTests: 0,
  collections: [],
  privacy: 'private',
  revalidate: () => null,
};

export default ItemMetaView;
