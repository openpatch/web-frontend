import { memo } from 'react';
import PropTypes from 'prop-types';
import { t } from '@lingui/macro';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import Box from '@material-ui/core/Box';
import IconButton from '@material-ui/core/IconButton';
import DragIndicatorIcon from '@material-ui/icons/DragIndicator';
import DeleteIcon from '@material-ui/icons/Delete';
import Tooltip from '@material-ui/core/Tooltip';

const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

export function Element({
  value,
  onDelete,
  Renderer,
  RendererProps,
  index,
  locales,
}) {
  return (
    <Draggable draggableId={value.id} index={index}>
      {(provided) => (
        <Box
          mb={1}
          display="flex"
          ref={provided.innerRef}
          {...provided.draggableProps}
        >
          <Box
            mr={1}
            display="flex"
            alignItems="center"
            justifyContent="center"
            {...provided.dragHandleProps}
          >
            <DragIndicatorIcon />
          </Box>
          <Renderer style={{ flex: 1 }} {...value} {...RendererProps} />
          {onDelete && (
            <Box
              ml={1}
              display="flex"
              alignItems="center"
              justifyContent="center"
              {...provided.dragHandleProps}
            >
              <Tooltip
                title={locales.remove(index)}
                aria-label={locales.remove(index)}
              >
                <IconButton onClick={() => onDelete(index)} size="small">
                  <DeleteIcon />
                </IconButton>
              </Tooltip>
            </Box>
          )}
        </Box>
      )}
    </Draggable>
  );
}

Element.propTypes = {
  value: PropTypes.shape({
    id: PropTypes.string.isRequired,
  }).isRequired,
  onDelete: PropTypes.func,
  Renderer: PropTypes.func.isRequired,
  RendererProps: PropTypes.object,
  index: PropTypes.number.isRequired,
  locales: PropTypes.shape({
    remove: PropTypes.func,
  }),
};

Element.defaultProps = {
  value: () => null,
  RendererProps: {},
  locales: {
    remove: (id) => t`Remove ${id}`,
  },
};

export const InnerList = memo(({ value, ...props }) => {
  return value.map((v, index) => (
    <Element value={v} index={index} key={v.id} {...props} />
  ));
});

InnerList.propTypes = {
  value: PropTypes.arrayOf(PropTypes.object),
};

InnerList.displayName = 'InnerList';

function DnDList({ value, onChange, Renderer, RendererProps, locales }) {
  function onDragEnd(result) {
    if (!result.destination) {
      return;
    }

    if (result.destination.index === result.source.index) {
      return;
    }

    const newValue = reorder(
      value,
      result.source.index,
      result.destination.index
    );

    onChange(newValue);
  }

  const handleDelete = (index) => {
    const newValue = [...value];
    newValue.splice(index, 1);
    onChange(newValue);
  };

  return (
    <DragDropContext onDragEnd={onDragEnd}>
      <Droppable droppableId="list">
        {(provided) => (
          <div ref={provided.innerRef} {...provided.droppableProps}>
            <InnerList
              value={value}
              locales={locales}
              onDelete={handleDelete}
              Renderer={Renderer}
              RendererProps={RendererProps}
            />
            {provided.placeholder}
          </div>
        )}
      </Droppable>
    </DragDropContext>
  );
}

DnDList.propTypes = {
  onChange: PropTypes.func.isRequired,
  Renderer: PropTypes.func.isRequired,
  RendererProps: PropTypes.object,
  locales: PropTypes.shape({
    remove: (id) => t`Remove ${id}`,
  }),
  value: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
    })
  ),
};

DnDList.defaultProps = {
  value: [],
  RendererProps: {},
};

export default DnDList;
