import Copyright from './Copyright';

export default {
  title: 'Copyright',
  component: Copyright
};

export const Default = () => <Copyright />;
