import PropTypes from 'prop-types';
import Fab from '@material-ui/core/Fab';
import EditIcon from '@material-ui/icons/Edit';
import Tooltip from '@material-ui/core/Tooltip';

function EditFab({ title, ...props }) {
  return (
    <Tooltip title={title}>
      <Fab color="primary" aria-label="edit" {...props}>
        <EditIcon />
      </Fab>
    </Tooltip>
  );
}

EditFab.propTypes = {
  title: PropTypes.string,
  onClick: PropTypes.func,
};

export default EditFab;
