import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Chip from '@material-ui/core/Chip';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    '& > *': {
      margin: theme.spacing(0.5),
    },
  },
}));

function TagsChips({ tags }) {
  const classes = useStyles();
  return (
    <Box className={classes.root}>
      {tags &&
        tags.map(({ name, color }) => (
          <Chip
            key={name}
            size="small"
            variant="outlined"
            label={name}
            style={{ borderColor: color, color: color }}
          />
        ))}
    </Box>
  );
}

TagsChips.propTypes = {
  tags: PropTypes.arrayOf(
    PropTypes.shape({
      color: PropTypes.string,
      name: PropTypes.string,
    })
  ),
};

TagsChips.defaultProps = {
  tags: [],
};

export default TagsChips;
