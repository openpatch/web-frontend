import PropTypes from 'prop-types';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';

import PrivacyIndicator from './PrivacyIndicator';
import Spacer from './Spacer';
import MemberHeader from './MemberHeader';
import LikeButton from './LikeButton';
import ShareButton from './ShareButton';
import RichText from './RichText';
import { convertFromRaw } from '../utils/editor';
import { useDateUtils } from '../utils/date';

function CollectionMetaView({
  id,
  name,
  member,
  createdOn,
  publicDescription,
  privacy,
}) {
  const { formatDistance } = useDateUtils();

  let distance = null;
  if (createdOn) {
    distance = formatDistance(new Date(createdOn), new Date());
  }

  return (
    <Box>
      <Box display="flex" alignItems="center">
        <Box flex={1}>
          <Typography>{name}</Typography>
          <Box display="flex" mb={2} alignItems="center">
            <PrivacyIndicator privacy={privacy} />
            <Spacer />
            <Typography variant="caption">{distance}</Typography>
          </Box>
        </Box>
        <ShareButton title={name} />
        <LikeButton
          id={id}
          type="collection"
          data={{ name, publicDescription }}
        />
      </Box>
      <Divider />
      <MemberHeader {...member} />
      <RichText editorState={convertFromRaw(publicDescription)} />
    </Box>
  );
}

CollectionMetaView.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
  member: PropTypes.object,
  createdOn: PropTypes.string,
  publicDescription: PropTypes.object,
  privacy: PropTypes.oneOf(['private', 'public', 'notlisted']),
};

CollectionMetaView.defaultProps = {
  privacy: 'private',
};

export default CollectionMetaView;
