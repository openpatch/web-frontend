import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    width: '100%',
    borderBottom: `1px solid ${theme.palette.grey[300]}`,
    padding: theme.spacing(3),
    [theme.breakpoints.down('sm')]: {
      flexDirection: 'column',
    },
  },
}));

const FormSection = (props) => {
  const classes = useStyles();
  return <div className={classes.container} {...props} />;
};

export default FormSection;
