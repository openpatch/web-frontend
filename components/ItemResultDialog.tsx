import { Trans } from '@lingui/macro';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import { ItemState } from '@services/assessment/session';
import { SessionItem } from '@services/assessment/session/items';
import { SessionProvider } from '@services/assessment/session/provider';
import ItemVersionRenderer from './ItemVersionRenderer';

export type ItemResultDialog = {
  open?: boolean;
  solution: ItemState['tasks'] | null;
  itemVersion: SessionItem | null;
  onClose: () => void;
};

function ItemResultDialog({
  open = false,
  solution,
  itemVersion,
  onClose,
}: ItemResultDialog) {
  return (
    <Dialog onClose={onClose} open={open} fullWidth maxWidth="lg">
      <DialogContent>
        <SessionProvider
          initialState={{
            currentItem: 0,
            items: {
              0: {
                tasks: solution !== null ? solution : {},
              },
            },
          }}
          readOnly
        >
          {itemVersion && (
            <ItemVersionRenderer
              tasks={itemVersion?.tasks}
              index={0}
              variant="result"
            />
          )}
        </SessionProvider>
      </DialogContent>
      <DialogActions>
        <Button color="primary" onClick={onClose}>
          <Trans>Close</Trans>
        </Button>
      </DialogActions>
    </Dialog>
  );
}

export default ItemResultDialog;
