import { Trans } from '@lingui/macro';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Following from 'mui-undraw/lib/Following';

function NoFollowing() {
  return (
    <Box display="flex" flexDirection="column" alignItems="center">
      <Following style={{ maxWidth: 600, marginBottom: 8 }} />
      <Typography variant="subtitle1" align="center">
        <Trans>Start following someone to see their activity.</Trans>
      </Typography>
    </Box>
  );
}

NoFollowing.propTypes = {};

export default NoFollowing;
