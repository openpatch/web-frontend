import PropTypes from 'prop-types';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import Tooltip from '@material-ui/core/Tooltip';

function CreateFab({ title, ...props }) {
  return (
    <Tooltip title={title}>
      <Fab color="primary" aria-label="add" {...props}>
        <AddIcon />
      </Fab>
    </Tooltip>
  );
}

CreateFab.propTypes = {
  title: PropTypes.string,
  onClick: PropTypes.func,
};

export default CreateFab;
