import { boolean } from '@storybook/addon-knobs';
import ProgressButton from './ProgressButton';

export default {
  title: 'ProgressButton',
  component: ProgressButton
};

export const Default = () => <ProgressButton>Progress</ProgressButton>;

export const Loading = () => (
  <ProgressButton fullWidth={boolean('fullWidth')} loading>
    Progress
  </ProgressButton>
);

export const Success = () => <ProgressButton success>Progress</ProgressButton>;
