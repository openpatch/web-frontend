import { Trans } from '@lingui/macro';
import PropTypes from 'prop-types';
import Link from 'next/link';
import { styled } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Typography from '@material-ui/core/Typography';
import RemoveRedEyeIcon from '@material-ui/icons/RemoveRedEye';
import IconButton from '@material-ui/core/IconButton';

import { useItem } from '@services/itembank/items';
import { useAssessmentItemReport } from '@services/assessment/report';
import { round } from '@utils/math';

const Container = styled(Box)({
  display: 'flex',
  flex: 1,
  overflowX: 'auto',
});

const InnerBox = styled(Box)({
  minWidth: 'min-content',
  display: 'flex',
});

const NodeContainer = styled(Card)(({ theme }) => ({
  width: 240,
  margin: theme.spacing(1),
  display: 'flex',
  flexDirection: 'column',
  overflowX: 'auto',
}));

const DragHeader = styled(CardHeader)(({ theme, isDragging }) => ({
  backgroundColor: isDragging ? 'green' : null,
  color: isDragging ? theme.palette.primary.contrastText : null,
}));

function TestItem({ assessmentId, id, itemVersion: { itemId, version } }) {
  const { item } = useItem(itemId);
  const itemReport = useAssessmentItemReport(assessmentId, id);

  return (
    <Box pb={2}>
      <Card variant="outlined">
        {item && (
          <DragHeader
            title={item?.name}
            subheader={<Trans>Version {version}</Trans>}
            titleTypographyProps={{
              style: { fontSize: '1.1rem' },
            }}
          />
        )}
        <CardContent>
          <Box display="flex" flexWrap="wrap">
            <Box textAlign="center" p={2}>
              <Typography>
                {round(itemReport?.statistic?.avgScore || 0)}
              </Typography>
              <Typography variant="caption">
                <Trans>Ø Score</Trans>
              </Typography>
            </Box>
            <Box textAlign="center" p={2}>
              <Typography>
                {itemReport?.statistic?.avgTime
                  ? new Date(itemReport?.statistic?.avgTime)
                      .toISOString()
                      .substr(11, 8)
                  : 0}
              </Typography>
              <Typography variant="caption">
                <Trans>Ø Time</Trans>
              </Typography>
            </Box>
          </Box>
          <Box textAlign="center" p={2}>
            <Typography>{itemReport?.statistic?.count || 0}</Typography>
            <Typography variant="caption">
              <Trans>Results</Trans>
            </Typography>
          </Box>
        </CardContent>
        <CardActions>
          <Link
            href={`/assessment/[id]/item/[iid]/report`}
            as={`/assessment/${assessmentId}/item/${id}/report`}
          >
            <IconButton>
              <RemoveRedEyeIcon />
            </IconButton>
          </Link>
        </CardActions>
      </Card>
    </Box>
  );
}

TestItem.propTypes = {
  assessmentId: PropTypes.string,
  id: PropTypes.string,
  itemVersion: PropTypes.shape({
    itemId: PropTypes.string,
    version: PropTypes.number,
  }),
};

function TestNode({ name, flow, start, items, assessmentId }) {
  return (
    <NodeContainer>
      <DragHeader isDragging={start} title={name} subtitle={flow} />
      <CardContent>
        {items.map((item) => (
          <TestItem key={item.id} {...item} assessmentId={assessmentId} />
        ))}
      </CardContent>
    </NodeContainer>
  );
}

TestNode.propTypes = {
  name: PropTypes.string,
  flow: PropTypes.string,
  start: PropTypes.bool,
  assessmentId: PropTypes.string,
  items: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
    })
  ),
};

function TestVersionStatistic({ nodes, assessmentId }) {
  nodes = Object.values(nodes);
  return (
    <Container>
      <InnerBox>
        {nodes.map((node) => (
          <TestNode key={node.id} {...node} assessmentId={assessmentId} />
        ))}
      </InnerBox>
    </Container>
  );
}

TestVersionStatistic.propTypes = {
  assessmentId: PropTypes.string,
  nodes: PropTypes.object,
};

TestVersionStatistic.defaultProps = {
  nodes: {},
};

export default TestVersionStatistic;
