import { t } from '@lingui/macro';
import { TextField } from '@material-ui/core';
import { Collection } from '@services/itembank/collections';
import { Item } from '@services/itembank/items';
import React from 'react';
import { Field } from 'react-final-form';
import PrivacySelectField from './fields/PrivacySelectField';
import RichTextField from './fields/RichTextField';
import Form, { FormProps } from './Form';
import FormSection from './FormSection';
import FormSectionContent from './FormSectionContent';
import FormSectionHeader from './FormSectionHeader';
import Joyride from './Joyride';

const required = (value: any) => (value ? undefined : 'Required');

export type CollectionMetaFormProps = {
  initialValues?: Collection;
} & Pick<
  FormProps<Pick<Collection, 'id' | 'name' | 'privacy' | 'publicDescription'>>,
  'onSubmit' | 'submitVariant'
>;

const steps = [
  {
    target: '#name',
    content: t`Here you can give your item a name`,
  },
  {
    target: '#privacy',
    content: t`Determine if you want to share your item with the public`,
  },
  {
    target: '#description',
    content: t`Describe what one can except of your item`,
  },
];

function CollectionMetaForm({
  onSubmit,
  initialValues,
  submitVariant = 'fab',
  ...props
}: CollectionMetaFormProps) {
  return (
    <>
      <Joyride steps={steps} name="ItemMetaForm" />
      <Form
        onSubmit={onSubmit}
        submitVariant={submitVariant}
        initialValues={initialValues}
        render={() => (
          <>
            <FormSection>
              <FormSectionHeader>{t`Meta`}</FormSectionHeader>
              <FormSectionContent>
                <Field<string>
                  name="name"
                  id="name"
                  label={t`Name`}
                  validate={required}
                  fullWidth
                  component={TextField}
                />
                <Field<Item['privacy']>
                  name="privacy"
                  id="privacy"
                  defaultValue="private"
                  fullWidth
                  component={PrivacySelectField}
                />
                <Field<Item['publicDescription']>
                  name="publicDescription"
                  id="description"
                  label={t`Description`}
                  fullWidth
                  deactivateFromToolbar={[
                    'link',
                    'image',
                    'embed',
                    'font-size',
                    'format-color-text',
                    'format-color-fill',
                    'code-block',
                    'ordered-list',
                    'unordered-list',
                    'blockquote',
                  ]}
                  component={RichTextField}
                />
              </FormSectionContent>
            </FormSection>
          </>
        )}
        {...props}
      ></Form>
    </>
  );
}

export default CollectionMetaForm;
