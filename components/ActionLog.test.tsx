import { render } from '@testing-library/react';
import ActionLog from './ActionLog';

describe('<ActionLog />', () => {
  it('should render', () => {
    const { getByText } = render(<ActionLog />);
    expect(getByText('Time')).toBeInTheDocument();
  });
});
