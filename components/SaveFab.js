import PropTypes from 'prop-types';
import Fab from '@material-ui/core/Fab';
import SaveIcon from '@material-ui/icons/Save';
import Tooltip from '@material-ui/core/Tooltip';

function SaveFab({ title, loading, ...props }) {
  return (
    <Tooltip title={title}>
      <Fab color="primary" disabled={loading} aria-label="edit" {...props}>
        <SaveIcon />
      </Fab>
    </Tooltip>
  );
}

SaveFab.propTypes = {
  title: PropTypes.string,
  loading: PropTypes.bool,
  onClick: PropTypes.func,
};

export default SaveFab;
