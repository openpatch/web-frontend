import { styled } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';

const BlankCenteredLayout = styled(Box)(({ theme }) => ({
  position: 'absolute',
  top: 0,
  bottom: 0,
  left: 0,
  right: 0,
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  justifyContent: 'center',
  height: '100vh',
  width: '100vw',
  overflowX: 'scroll',
  zIndex: 1260,
  backgroundColor: theme.palette.primary.main
}));

export default BlankCenteredLayout;
