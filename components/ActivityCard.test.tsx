import { render, act } from '@testing-library/react';
import ActivityCard from './ActivityCard';
import { TestingProvider } from '../test-utils';
import { dynamicActivate } from '../utils/i18n';

describe('<ActivityCard />', () => {
  it('should render', () => {
    act(() => {
      dynamicActivate('en');
    });
    const { getByText } = render(
      <ActivityCard
        id="bal"
        type="create"
        resource={{
          id: 'bal',
          type: 'item',
          data: {
            name: 'Hi',
          },
        }}
        createdOn=""
        member={{
          id: '1234',
          fullName: null,
          username: 'test-user',
        }}
      />,
      { wrapper: TestingProvider }
    );
    expect(getByText('test-user')).toBeInTheDocument();
  });
});
