import RichTextEditor, { RichTextEditorProps } from './RichTextEditor';

export type RichTextProps = Omit<RichTextEditorProps, 'onChange'>;

function RichText({ readOnly = true, ...props }: RichTextProps) {
  return (
    <RichTextEditor {...props} readOnly={readOnly} onChange={() => null} />
  );
}

export default RichText;
