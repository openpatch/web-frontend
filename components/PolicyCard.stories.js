import PolicyCard from './PolicyCard';

export default {
  title: 'PolicyCard',
  component: PolicyCard
};

export const Default = () => (
  <PolicyCard
    id="some-uuid"
    type="privacy"
    draft={false}
    language="en"
    createdOn={new Date().toISOString()}
  />
);

export const Draft = () => (
  <PolicyCard
    id="some-uuid"
    type="privacy"
    draft={true}
    language="de"
    createdOn={new Date().toISOString()}
  />
);
