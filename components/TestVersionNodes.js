import PropTypes from 'prop-types';
import { styled } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Tooltip from '@material-ui/core/Tooltip';
import RemoveRedEyeIcon from '@material-ui/icons/RemoveRedEye';
import IconButton from '@material-ui/core/IconButton';
import { useItem } from '@services/itembank/items';

import Link from './Link';

const Container = styled(Box)({
  display: 'inline-flex',
});

const NodeContainer = styled(Card)(({ theme }) => ({
  width: 240,
  margin: theme.spacing(1),
  display: 'flex',
  flexDirection: 'column',
}));

const DragHeader = styled(CardHeader)(({ theme, isDragging }) => ({
  backgroundColor: isDragging ? 'green' : null,
  color: isDragging ? theme.palette.primary.contrastText : null,
  '& span': {
    color: isDragging ? theme.palette.primary.contrastText : null,
  },
}));

function TestItem({ itemVersion: { itemId, version } }) {
  const { item } = useItem(itemId);

  return (
    <Box pb={2}>
      <Card variant="outlined">
        {item && (
          <DragHeader
            title={item?.name}
            subheader={`Version ${version}`}
            titleTypographyProps={{
              style: { fontSize: '0.9rem' },
            }}
            subheaderTypographyProps={{
              style: { fontSize: '0.7rem' },
            }}
            action={
              <Tooltip title="View Item">
                <Link
                  href={`/item/[id]?version=${version}`}
                  as={`/item/${itemId}?version=${version}`}
                >
                  <IconButton>
                    <RemoveRedEyeIcon />
                  </IconButton>
                </Link>
              </Tooltip>
            }
          />
        )}
      </Card>
    </Box>
  );
}

TestItem.propTypes = {
  id: PropTypes.string,
  itemVersion: PropTypes.shape({
    itemId: PropTypes.string,
    version: PropTypes.number,
  }),
};

function TestNode({ name, flow, start, items }) {
  return (
    <NodeContainer>
      <DragHeader
        isDragging={start}
        title={name}
        subheader={flow}
        titleTypographyProps={{
          style: { fontSize: '0.9rem' },
        }}
        subheaderTypographyProps={{
          style: { fontSize: '0.7rem' },
        }}
      />
      <CardContent>
        {items.map((item) => (
          <TestItem key={item.id} {...item} />
        ))}
      </CardContent>
    </NodeContainer>
  );
}

TestNode.propTypes = {
  name: PropTypes.string,
  flow: PropTypes.string,
  start: PropTypes.bool,
  items: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
    })
  ),
};

function TestVersionNodes({ nodes }) {
  return (
    <Container>
      {nodes.map((node) => (
        <TestNode key={node.id} {...node} />
      ))}
    </Container>
  );
}

TestVersionNodes.propTypes = {
  nodes: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
    })
  ),
};

TestVersionNodes.defaultProps = {
  nodes: [],
};

export default TestVersionNodes;
