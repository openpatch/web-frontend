import { action } from '@storybook/addon-actions';
import { useState } from 'react';
import DnDList from './DnDList';
import ItemCard from './ItemCard';
import faker from '../utils/mock/faker';

export default {
  title: 'DnDList',
  component: DnDList,
};

export const WithState = () => {
  const [value, setValue] = useState(faker.genList.item(20));
  return (
    <DnDList
      value={value}
      onDelete={action('delete')}
      onChange={setValue}
      Renderer={ItemCard}
    />
  );
};
