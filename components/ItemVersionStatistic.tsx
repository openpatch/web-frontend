import { Statistic } from '@formats/index';
import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import { TestItemStatistic } from '@services/assessment/report';
import { convertFromRaw } from '@utils/editor';
import { memo } from 'react';
import RichText from './RichText';

const useStyles = makeStyles((theme) => ({
  container: {
    marginBottom: theme.spacing(3),
  },
  header: {
    padding: theme.spacing(3),
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.primary.contrastText,
    textAlign: 'center',
  },
  body: {
    padding: theme.spacing(3),
  },
}));

export type ItemVersionStatisticProps = {
  statistic?: TestItemStatistic['statistic'];
  tasks?: TestItemStatistic['tasks'];
};

function ItemVersionStatistic({ statistic, tasks }: ItemVersionStatisticProps) {
  const classes = useStyles();
  return (
    <>
      {tasks?.map((task, index) => (
        <Paper className={classes.container} key={task.id}>
          {task.task && <Box className={classes.header}>{task.task}</Box>}
          <Box className={classes.body}>
            {task.text && (
              <Box mb={2}>
                <RichText editorState={convertFromRaw(task.text)} />
              </Box>
            )}
            <Statistic
              key={task.id}
              type={task.formatType} // TODO check format
              statistic={{ ...(statistic?.tasks?.[index]?.format || {}) }}
              data={task.data}
              evaluation={task.evaluation}
            />
          </Box>
        </Paper>
      ))}
    </>
  );
}

export default memo<ItemVersionStatisticProps>(ItemVersionStatistic);
