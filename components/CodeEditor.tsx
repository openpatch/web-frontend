import { useRef, useEffect } from 'react';
import { useTheme, makeStyles } from '@material-ui/core/styles';
import { AceEditor } from './AceEditor';
import { IAceEditorProps, IMarker } from 'react-ace';
import { IAceEditor } from 'react-ace/lib/types';

const useStyles = makeStyles((theme) => ({
  ace: {
    '& .read-only': {
      position: 'absolute',
      background: theme.palette.primary.main,
      opacity: 0.3,
      width: '100%!important',
    },
  },
}));

export type CodeEditorProps = {
  markers?: IMarker[];
  onGutterClick?: (row: number) => void;
  readOnlyRows?: number[];
} & Omit<IAceEditorProps, "markers">

const CodeEditor = ({
  markers = [],
  onGutterClick,
  readOnlyRows = [],
  onChange,
  value,
  ...props
}: CodeEditorProps) => {
  const theme = useTheme();
  const editorRef = useRef<IAceEditor | null>(null);
  const classes = useStyles();

  useEffect(() => {
    const editor = editorRef.current?.editor;
    if (editor) {
      editor.on('guttermousedown', (e: any) => {
        const target = e.domEvent.target;
        if (target.className.indexOf('ace_gutter-cell') == -1) return;
        if (!editor.isFocused()) return;
        if (e.clientX > 25 + target.getBoundingClientRect().left) return;

        const row = e.getDocumentPosition().row;
        if (row !== null && onGutterClick) {
          onGutterClick(row);
        }
      });
    }
  }, []);

  const handleChange: IAceEditorProps["onChange"] = (value, e) => {
    const editor = editorRef.current?.editor;
    const { start, end } = e;

    if (!readOnlyRows.includes(start.row) && !readOnlyRows.includes(end.row) && onChange) {
      onChange(value, e);
    } else {
      const undoManager = editor.session.$undoManager;
      const { $lastDelta } = undoManager;
      if (
        readOnlyRows.includes($lastDelta?.end?.row) ||
        readOnlyRows.includes($lastDelta?.start?.row)
      ) {
        editor.session.$undoManager.undo();
      }
    }
  }

  return (
    <AceEditor
      // AceEditor does not ship types for ref
      // @ts-ignore
      ref={editorRef}
      className={classes.ace}
      fontSize={14}
      theme={theme.palette.type === 'light' ? 'github' : 'gruvbox'}
      mode="java"
      width="100%"
      highlightActiveLine={false}
      showPrintMargin={false}
      markers={[
        ...readOnlyRows.map((row) => ({
          startRow: row,
          startCol: 0,
          endRow: row,
          endCol: 1,
          className: 'read-only',
          type: 'fullLine',
        })),
        ...markers,
      ] as IMarker[]}
      editorProps={{ $blockScrolling: Infinity }}
      onChange={handleChange}
      value={value}
      {...props}
    />
  );
};

export default CodeEditor;
