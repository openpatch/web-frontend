import { Trans } from '@lingui/macro';
import Button, { ButtonProps } from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import { useLikes } from '@services/activity/resources';
import { Collection } from '@services/itembank/collections';
import { Item } from '@services/itembank/items';
import { Test } from '@services/itembank/tests';
import PropTypes from 'prop-types';

export type LikeButtonViewProps = {
  likes: number;
  isLiked: boolean;
} & ButtonProps;

export function LikeButtonView({
  likes,
  isLiked,
  ...props
}: LikeButtonViewProps) {
  return (
    <Tooltip title={<Trans>Like</Trans>}>
      <Button
        color={isLiked ? 'primary' : 'default'}
        startIcon={<ThumbUpIcon />}
        {...props}
      >
        {likes}
      </Button>
    </Tooltip>
  );
}

export type LikeButtonProps = {
  id: string;
} & (
  | {
      type: 'test';
      data: Test;
    }
  | {
      type: 'item';
      data: Item;
    }
  | {
      type: 'collection';
      data: Collection;
    }
);

function LikeButton({ id, type, data }: LikeButtonProps) {
  const { like, postLike, deleteLike, revalidate } = useLikes(id, type);

  function handleLike() {
    if (like?.isLiked) {
      deleteLike().then(() => revalidate({}));
    } else {
      postLike(data).then(() => revalidate({}));
    }
  }

  return (
    <LikeButtonView
      likes={like?.likesCount || 0}
      isLiked={like?.isLiked || false}
      onClick={handleLike}
    />
  );
}

LikeButton.propTypes = {
  id: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  data: PropTypes.object,
};

export default LikeButton;
