import { action } from '@storybook/addon-actions';
import ErrorPage from './ErrorPage';

export default {
  title: 'ErrorPage',
  component: ErrorPage
};

export const Default = () => <ErrorPage onReport={action('report')} />;
