import PropTypes from 'prop-types';
import { Trans } from '@lingui/macro';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';

import RichText from './RichText';
import ProgressButton from './ProgressButton';
import { convertFromRaw } from '../utils/editor';

function PolicyDialog({ content, open, onClose, onAgree, loading }) {
  return (
    <Dialog open={open}>
      <DialogContent dividers>
        <DialogContentText>
          <RichText editorState={convertFromRaw(content)} />
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose}>
          <Trans>Disagree</Trans>
        </Button>
        <ProgressButton onClick={onAgree} color="primary" loading={loading}>
          <Trans>Agree</Trans>
        </ProgressButton>
      </DialogActions>
    </Dialog>
  );
}

PolicyDialog.propTypes = {
  loading: PropTypes.bool,
  content: PropTypes.object,
  type: PropTypes.string,
  onAgree: PropTypes.func,
  onClose: PropTypes.func,
  open: PropTypes.bool,
};

export default PolicyDialog;
