import { action } from '@storybook/addon-actions';
import Comment from './Comment';

export default {
  title: 'Comment',
  component: Comment,
};

export const FromOther = () => (
  <Comment
    member={{
      id: 'some-uuid',
      username: 'a username',
    }}
    user={{
      id: 'another-uuid',
    }}
    message="This is a comment"
    createdOn={new Date().toString()}
    onDelete={action('delete')}
  />
);

export const My = () => (
  <Comment
    member={{
      id: 'some-uuid',
      username: 'a username',
    }}
    user={{
      id: 'some-uuid',
    }}
    message="This is a comment"
    createdOn={new Date().toString()}
    onDelete={action('delete')}
  />
);
