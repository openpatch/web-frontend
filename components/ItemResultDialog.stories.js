import { boolean } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import ItemResultDialog from './ItemResultDialog';

export default {
  title: 'ItemResultDialog',
  component: ItemResultDialog
};

export const Default = () => (
  <ItemResultDialog
    open={boolean('open', true)}
    onClose={action('close')}
    itemVersion={{
      createdOn: '2020-03-21T07:42:06',
      item: 'e2e833ff-ddab-40ff-98f8-da84948c3830',
      status: 'pilot',
      tasks: [
        {
          data: { choices: ['1', '2', '3'] },
          evaluation: { choices: { '2': true } },
          formatType: 'choice',
          formatVersion: 1,
          task: 'Number Two'
        }
      ],
      updatedOn: '2020-03-21T07:42:06',
      version: 1
    }}
    solution={{
      '0': {
        choices: { '0': true }
      }
    }}
  />
);

export const NoSolution = () => (
  <ItemResultDialog
    open={boolean('open', true)}
    onClose={action('close')}
    itemVersion={{
      createdOn: '2020-03-21T07:42:06',
      item: 'e2e833ff-ddab-40ff-98f8-da84948c3830',
      status: 'pilot',
      tasks: [
        {
          data: { choices: ['1', '2', '3'] },
          evaluation: { choices: { '2': true } },
          formatType: 'choice',
          formatVersion: 1,
          task: 'Number Two'
        }
      ],
      updatedOn: '2020-03-21T07:42:06',
      version: 1
    }}
    solution={null}
  />
);
