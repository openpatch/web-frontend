import { t } from '@lingui/macro';
import AppBar from '@material-ui/core/AppBar';
import Box from '@material-ui/core/Box';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Container from '@material-ui/core/Container';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import IconButton from '@material-ui/core/IconButton';
import MUILink from '@material-ui/core/Link';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import Toolbar from '@material-ui/core/Toolbar';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';
import AssessmentIcon from '@material-ui/icons/Assessment';
import AssignmentIcon from '@material-ui/icons/Assignment';
import BookIcon from '@material-ui/icons/Book';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import DashboardIcon from '@material-ui/icons/Dashboard';
import DeveloperModeIcon from '@material-ui/icons/DeveloperMode';
import FeedbackIcon from '@material-ui/icons/Feedback';
import HelpIcon from '@material-ui/icons/Help';
import MenuIcon from '@material-ui/icons/Menu';
import PlaylistPlayIcon from '@material-ui/icons/PlaylistPlay';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import WhatshotIcon from '@material-ui/icons/Whatshot';
import { MemberClaims } from '@services/authentification/members';
import { useClaims } from '@services/authentification/provider';
import clsx from 'clsx';
import Link from 'next/link';
import Router from 'next/router';
import { ReactNode, useState } from 'react';
import ReactJoyride from 'react-joyride';
import AvatarMenu from './AvatarMenu';
import Copyright from './Copyright';
import { useStyles } from './DrawerLayout.styles';
import Joyride from './Joyride';
import Logo from './Logo';
import Search from './Search';

export type DrawerBreadcrumbProps = {
  href: string;
  title: string;
  as?: string;
  active?: boolean;
};

export type DrawerSecondaryLinks = {
  id: string;
  text: string;
  links: {
    icon?: ReactNode;
    text: string;
    id: string;
    onClick?: () => void;
  }[];
}[];

export type DrawerMainLinks = {
  id: string;
  icon?: ReactNode;
  text: string;
  onClick?: () => void;
}[];

export type DrawerBottomLinks = {
  id: string;
  icon?: ReactNode;
  text: string;
  onClick?: () => void;
}[];

export type DrawerSettingLinks = {
  id: string;
  icon?: ReactNode;
  text: string;
  onClick?: () => void;
}[];

export type DrawerLayoutViewProps = {
  settingLinks?: DrawerSettingLinks;
  secondaryLinks?: DrawerSecondaryLinks;
  mainLinks?: DrawerMainLinks;
  bottomLinks?: DrawerBottomLinks;
  breadcrumbs?: DrawerBreadcrumbProps[];
  onDrawerToggle?: () => void;
  open?: boolean;
  children?: ReactNode;
  fab?: ReactNode;
  claims: MemberClaims | null;
};

export function DrawerLayoutView({
  settingLinks = [],
  secondaryLinks = [],
  mainLinks = [],
  bottomLinks = [],
  claims,
  open = false,
  onDrawerToggle,
  breadcrumbs = [],
  children,
  fab,
}: DrawerLayoutViewProps) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="absolute" className={classes.appBar}>
        <Toolbar className={classes.toolbar}>
          {!open ? (
            <IconButton
              edge="start"
              color="inherit"
              aria-label="open drawer"
              onClick={onDrawerToggle}
              className={classes.menuButton}
            >
              <MenuIcon />
            </IconButton>
          ) : (
            <IconButton
              edge="start"
              color="inherit"
              aria-label="close drawer"
              onClick={onDrawerToggle}
              className={classes.menuButton}
            >
              <ChevronLeftIcon />
            </IconButton>
          )}
          <Box className={classes.title}>
            <Link href="/">
              <a href="/">
                <Logo />
              </a>
            </Link>
          </Box>
          <Search />
          <Box flexGrow={1} />
          <AvatarMenu />
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        classes={{
          paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
        }}
        open={open}
      >
        <div className={classes.toolbarIcon}></div>
        <List>
          {mainLinks.map(({ icon, onClick, text, id }) => (
            <Tooltip title={text} key={id}>
              <ListItem id={id} key={id} button onClick={onClick}>
                <ListItemIcon>{icon}</ListItemIcon>
                <ListItemText style={{ overflow: 'hidden' }} primary={text} />
              </ListItem>
            </Tooltip>
          ))}
        </List>
        {claims?.username && (
          <>
            <Divider />
            <List>
              {secondaryLinks.map(({ text, id, links }, i) => (
                <div key={text || i}>
                  {text && open && (
                    <ListSubheader style={{ overflow: 'hidden' }} id={id} inset>
                      {text}
                    </ListSubheader>
                  )}
                  {links.map(({ id, text, onClick, icon }) => (
                    <Tooltip title={text} key={id}>
                      <ListItem
                        id={id}
                        style={{ overflow: 'hidden' }}
                        key={id}
                        button
                        onClick={onClick}
                      >
                        <ListItemIcon>{icon}</ListItemIcon>
                        <ListItemText primary={text} />
                      </ListItem>
                    </Tooltip>
                  ))}
                </div>
              ))}
            </List>
          </>
        )}
        <Divider />
        <List>
          {settingLinks.map(({ id, icon, onClick, text }) => (
            <Tooltip title={text} key={id}>
              <ListItem
                id={id}
                key={id}
                style={{ overflow: 'hidden' }}
                button
                onClick={onClick}
              >
                <ListItemIcon>{icon}</ListItemIcon>
                <ListItemText primary={text} />
              </ListItem>
            </Tooltip>
          ))}
        </List>
        <Box flex={1} />
        <div className={classes.bottomLinks}>
          {open &&
            bottomLinks.map(({ id, onClick, text }) => (
              <Typography
                id={id}
                key={id}
                onClick={onClick}
                variant="caption"
                className={classes.bottomLink}
              >
                {text}
              </Typography>
            ))}
        </div>
      </Drawer>
      <main className={clsx(classes.content, !open && classes.contentWide)}>
        <div className={classes.appBarSpacer} />
        <Box p={4}>
          <Box pb={2}>
            <Breadcrumbs maxItems={5} aria-label="breadcrumb">
              {breadcrumbs.map(({ href, as, title = '', active }) => (
                <Link href={href} as={as || href} key={href} passHref>
                  {active ? (
                    <Typography color="textPrimary">{title}</Typography>
                  ) : (
                    <MUILink color="inherit">{title}</MUILink>
                  )}
                </Link>
              ))}
            </Breadcrumbs>
          </Box>
          <Container>{children || ''}</Container>
          <Box pt={4}>
            <Copyright />
          </Box>
        </Box>
        <Box style={{ position: 'fixed', right: 16, bottom: 16 }}>{fab}</Box>
      </main>
    </div>
  );
}

export type DrawerLayoutProps = {
  children?: ReactNode;
  fab?: ReactNode;
  breadcrumbs?: DrawerLayoutViewProps['breadcrumbs'];
  joyride?: boolean;
};

export default function DrawerLayout({
  children = null,
  fab = null,
  breadcrumbs = [],
  joyride = false,
}: DrawerLayoutProps) {
  const claims = useClaims();
  const [open, setOpen] = useState(false);

  const steps: ReactJoyride['props']['steps'] = [
    {
      placement: 'left',
      target: '#drawer-dashboard',
      content: t`Here you will find the recent activity of members you follow`,
    },
    {
      placement: 'left',
      target: '#drawer-discover',
      content: t`Here you will find new items and tests from other members`,
    },
    {
      placement: 'left',
      target: '#drawer-likes',
      content: t`Here you will find items and tests you have liked`,
    },
    {
      placement: 'left',
      target: '#drawer-my-library',
      content: t`Here you will find everything you have created`,
    },
    {
      placement: 'left',
      target: '#drawer-items',
      content: t`Your items. Items are the foundation of OpenPatch. Here you can create yours.`,
    },
    {
      target: '#drawer-tests',
      content: t`Your tests. Test consists of items. They can be yours or they can be public ones.`,
    },
    {
      target: '#drawer-assessments',
      content: t`Your assessments. If you want to conduct a test, you need to create an assessment for it.`,
    },
  ];

  const mainLinks = [
    {
      icon: <WhatshotIcon />,
      text: t`Discover`,
      id: 'drawer-discover',
      onClick: () => {
        Router.push('/discover');
      },
    },
  ];

  const secondaryLinks = [
    {
      text: t`My Library`,
      id: 'drawer-my-library',
      links: [
        {
          icon: <DashboardIcon />,
          text: t`Dashboard`,
          id: 'drawer-dashboard',
          onClick: () => {
            Router.push('/dashboard');
          },
        },
        {
          icon: <ThumbUpIcon />,
          text: t`Likes`,
          id: 'drawer-likes',
          onClick: () => {
            Router.push('/likes');
          },
        },
        {
          id: 'drawer-items',
          icon: <AssignmentIcon />,
          text: t`Items`,
          onClick: () => {
            Router.push('/items?my=true');
          },
        },
        {
          id: 'drawer-tests',
          icon: <BookIcon />,
          text: t`Tests`,
          onClick: () => {
            Router.push('/tests?my=true');
          },
        },
        {
          id: 'drawer-collections',
          icon: <PlaylistPlayIcon />,
          text: t`Collections`,
          onClick: () => {
            Router.push('/collections?my=true');
          },
        },
        {
          id: 'drawer-assessments',
          icon: <AssessmentIcon />,
          text: t`Assessments`,
          onClick: () => {
            Router.push('/assessments?my=true');
          },
        },
      ],
    },
  ];

  const settingLinks = [
    {
      id: 'drawer-help',
      text: t`Help`,
      icon: <HelpIcon />,
      onClick: () => {
        Router.push('/help');
      },
    },
    {
      id: 'drawer-feedback',
      text: t`Send Feedback`,
      icon: <FeedbackIcon />,
      onClick: () => {
        window.location.href = 'mailto:feedback@openpatch.org';
      },
    },
    {
      id: 'drawer-source-code',
      text: t`Source Code`,
      icon: <DeveloperModeIcon />,
      onClick: () => {
        window.location.href = 'https://gitlab.com/openpatch';
      },
    },
  ];

  const bottomLinks = [
    {
      id: 'drawer-twitter',
      text: t`Twitter`,
      onClick: () => {
        window.location.href = 'https://twitter.com/OpenPatchOrg';
      },
    },
    {
      id: 'drawer-contact',
      text: t`Contact`,
      onClick: () => {
        Router.push('/contact');
      },
    },
    {
      id: 'drawer-policy',
      text: t`Privacy Policy`,
      onClick: () => {
        Router.push('/policy/privacy');
      },
    },
  ];

  const handleDrawerToggle = () => {
    setOpen(!open);
  };

  return (
    <>
      {joyride && (
        <Joyride steps={steps} name="DrawerLayout" callback={() => null} />
      )}
      <DrawerLayoutView
        mainLinks={mainLinks}
        secondaryLinks={secondaryLinks}
        settingLinks={settingLinks}
        bottomLinks={bottomLinks}
        onDrawerToggle={handleDrawerToggle}
        fab={fab}
        breadcrumbs={breadcrumbs}
        open={Boolean(open)}
        claims={claims}
      >
        {children}
      </DrawerLayoutView>
    </>
  );
}
