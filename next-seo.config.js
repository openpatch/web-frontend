export default {
  titleTemplate: 'OpenPatch | %s',
  title: 'Assessment and Training',
  description:
    'OpenPatch is an open source platform for assessment and training of competencies.',
  openGraph: {
    type: 'website',
    locale: 'en_US',
    url: 'https://www.openpatch.app/',
    title: 'OpenPatch',
    description:
      'OpenPatch is an open source platform for assessment and training of competencies.',
    site_name: 'OpenPatch',
    images: [
      {
        url:
          'https://og.openpatch.app/OpenPatch.png?theme=light&username=Join&type=+',
        alt: 'OpenPatch',
      },
    ],
  },
  twitter: {
    site: '@openpatchapp',
    cardType: 'summary_large_image',
  },
};
