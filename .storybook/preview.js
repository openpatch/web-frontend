import React from 'react';
import { addParameters } from '@storybook/react';
import { withI18n } from 'storybook-addon-i18n';
import { useDarkMode } from 'storybook-dark-mode';
import { action } from '@storybook/addon-actions';
import { addDecorator } from '@storybook/react';
import { ThemeProviderView } from '../theme';
import CssBaseline from '@material-ui/core/CssBaseline';
import 'vis-network/styles/vis-network.css';
import { RouterContext } from 'next/dist/next-server/lib/router-context';

import I18nProvider from '../components/I18nProvider';
import { NotificationProvider } from '../utils/notification';
import { mockApi, mockRouter } from '../utils/mock';
import { Router } from 'next/router';

mockApi();
Router.router = mockRouter({ callback: action('link clicked') });

console.log(Router)
console.log("HALLO")

const LocaleProvider = ({ locale, children }) => (
  <RouterContext.Provider
    value={{
      ...Router.router,
      locale,
    }}
  >
    <I18nProvider locale={locale}>{children}</I18nProvider>
  </RouterContext.Provider>
);

addParameters({
  i18n: {
    provider: LocaleProvider,
    supportedLocales: ['en', 'de'],
  },
});

addDecorator(withI18n);
addDecorator((storyFn) => (
  <ThemeProviderView darkMode={useDarkMode()}>
    <NotificationProvider>
      <CssBaseline />
      <div style={{ padding: 8 }}>{storyFn()}</div>
    </NotificationProvider>
  </ThemeProviderView>
));
