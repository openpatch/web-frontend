const webpack = require('webpack');
const path = require('path');

module.exports = {
  reactOptions: {
    fastRefresh: true,
    strictMode: true,
  },
  stories: ['../**/*.stories.@(js|mdx|ts|tsx)'],
  addons: [
    '@storybook/addon-actions',
    '@storybook/addon-knobs',
    '@storybook/addon-links',
    '@storybook/addon-docs',
    '@storybook/addon-viewport',
    '@storybook/addon-a11y',
    'storybook-dark-mode/register',
    'storybook-addon-i18n/register',
  ],
  webpackFinal: async (baseConfig) => {
    (baseConfig.resolve.alias = {
      '@services': path.resolve(__dirname, '../services/'),
      '@utils': path.resolve(__dirname, '../utils/'),
      '@components': path.resolve(__dirname, '../components/'),
      '@formats': path.resolve(__dirname, '../formats/'),
    }),
      baseConfig.module.rules.push({
        test: /\.po/,
        use: [
          {
            loader: '@lingui/loader',
          },
        ],
      });

    // merge whatever from nextConfig into the webpack config storybook will use
    return baseConfig;
  },
};
