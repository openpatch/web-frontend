import { DraftStyleMap } from 'draft-js';

export type Color = {
  label: string;
  value: string;
};

export const colors: Color[] = [
  { label: 'blue', value: '#4e79a7' },
  { label: 'green', value: '#59a14f' },
  { label: 'orange', value: '#f28e2b' },
  { label: 'red', value: '#e15759' },
  { label: 'pink', value: '#ff9da7' },
  { label: 'brown', value: '#9c755f' },
].sort((c1, c2) => (c1.label > c2.label ? 1 : -1));

const customStyleMap: DraftStyleMap = {};
colors.forEach((color) => {
  customStyleMap[`HIGHLIGHT-${color.label}`] = {
    backgroundColor: color.value,
  };
  customStyleMap[`TEXT-COLOR-${color.label}`] = {
    color: color.value,
  };
});

export default {
  customStyleMap,
  colors,
};
