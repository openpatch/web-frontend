import createEmojiPlugin from '@draft-js-plugins/emoji';

export default createEmojiPlugin();
