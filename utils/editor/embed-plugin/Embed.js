import PropTypes from 'prop-types';

const Embed = ({ block, contentState }) => {
  // leveraging destructuring to omit certain properties from props
  const { src, title, width, height } = contentState
    .getEntity(block.getEntityAt(0))
    .getData();
  return (
    <iframe
      title={title}
      width={width}
      height={height}
      src={src}
      frameBorder="0"
      allowFullScreen
    />
  );
};

Embed.propTypes = {
  block: PropTypes.shape({
    getEntityAt: PropTypes.func,
  }),
  contentState: PropTypes.shape({
    getEntity: PropTypes.func,
  }),
};

export default Embed;
