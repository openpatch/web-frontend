import add from './add';
import EmbedComponent from './Embed';

export default {
  blockRendererFn: (block, { getEditorState }) => {
    if (block.getType() === 'atomic') {
      const contentState = getEditorState().getCurrentContent();
      const entity = block.getEntityAt(0);
      if (!entity) return null;
      const type = contentState.getEntity(entity).getType();
      if (type === 'EMBED' || type === 'embed') {
        return {
          component: EmbedComponent,
          editable: false,
        };
      }
      return null;
    }

    return null;
  },
  add,
};
