const customStyleMap = {
  TITLE: {
    fontSize: '1.5em',
  },
  MONOSPACE: {
    fontFamily: 'monospace',
  },
};

export default {
  customStyleMap,
};
