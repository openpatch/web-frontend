import PropTypes from 'prop-types';
import { EditorState, RichUtils } from 'draft-js';

function add(editorState, url) {
  const contentState = editorState.getCurrentContent();
  const contentStateWithEntity = contentState.createEntity('LINK', 'MUTABLE', {
    url,
  });
  const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
  const newEditorState = EditorState.set(editorState, {
    currentContent: contentStateWithEntity,
  });
  return RichUtils.toggleLink(
    newEditorState,
    newEditorState.getSelection(),
    entityKey
  );
}

function remove(editorState) {
  const selection = editorState.getSelection();

  return RichUtils.toggleLink(editorState, selection, null);
}

function linkStrategy(contentBlock, callback, contentState) {
  contentBlock.findEntityRanges((character) => {
    const entityKey = character.getEntity();
    return (
      entityKey !== null &&
      contentState.getEntity(entityKey).getType() === 'LINK'
    );
  }, callback);
}

const Link = ({ contentState, entityKey, children }) => {
  const { url } = contentState.getEntity(entityKey).getData();
  return <a href={url}>{children}</a>;
};

Link.propTypes = {
  contentState: PropTypes.shape({
    getEntity: PropTypes.func,
  }),
  entityKey: PropTypes.string,
  children: PropTypes.any,
};

export default {
  decorators: [
    {
      strategy: linkStrategy,
      component: Link,
    },
  ],
  add,
  remove,
};
