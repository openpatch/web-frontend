import PropTypes from 'prop-types';

const Image = ({ block, contentState }) => {
  // leveraging destructuring to omit certain properties from props
  const { src, alt, width, height } = contentState
    .getEntity(block.getEntityAt(0))
    .getData();
  return (
    <img
      alt={alt}
      width={width || '100%'}
      height={height || 'auto'}
      src={src}
      role="presentation"
    />
  );
};

Image.propTypes = {
  block: PropTypes.shape({
    getEntityAt: PropTypes.func,
  }),
  contentState: PropTypes.shape({
    getEntity: PropTypes.func,
  }),
};

export default Image;
