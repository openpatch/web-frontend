import add from './add';
import ImageComponent from './Image';

export default {
  blockRendererFn: (block, { getEditorState }) => {
    if (block.getType() === 'atomic') {
      const contentState = getEditorState().getCurrentContent();
      const entity = block.getEntityAt(0);
      if (!entity) return null;
      const type = contentState.getEntity(entity).getType();
      if (type === 'IMAGE' || type === 'image') {
        return {
          component: ImageComponent,
          editable: false,
        };
      }
      return null;
    }

    return null;
  },
  add,
};
