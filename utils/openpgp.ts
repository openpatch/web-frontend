declare global {
  interface OpenPGPKey {
    publicKeyArmored: string;
    privateKeyArmored: string;
  }
  interface OpenPGP {
    generateKey: (options: any) => Promise<OpenPGPKey>;
    decrypt: (
      options: any
    ) => Promise<{
      data: string;
    }>;
    encrypt: (
      options: any
    ) => Promise<{
      data: string; // The cipher text
    }>;
    message: any;
    key: any;
  }
  interface Window {
    openpgp: OpenPGP;
    passphrases: Record<string, string>;
  }
}
export interface UserID {
  name?: string;
  email?: string;
}

let openpgp: OpenPGP;
if (typeof window !== 'undefined') {
  openpgp = window.openpgp;
  window.passphrases = {};
}

export async function initWorker() {
  if (!openpgp) {
    return;
  }
  // return openpgp.initWorker({ path: '/openpgp/openpgp.worker.js' });
}

export async function destroyWorker() {
  if (!openpgp) {
    return;
  }
  // return openpgp.destroyWorker();
}

export function generateKeyPair(passphrase: string) {
  const options = {
    userIds: [{ name: 'OpenPatch', email: 'encrypt@openpatch.app' }],
    numBits: 2048,
    passphrase,
  };

  return openpgp.generateKey(options);
}

export async function encrypt(
  publicKeyArmored: string,
  message: string
): Promise<{
  data: string; // The cipher text
}> {
  const options = {
    message: openpgp.message.fromText(message),
    publicKeys: (await openpgp.key.readArmored(publicKeyArmored)).keys,
  };

  return openpgp.encrypt(options);
}

export async function decrypt(
  privateKeyArmored: string,
  passphrase: string,
  message: string
): Promise<{
  data: string;
}> {
  const privateKey = (await openpgp.key.readArmored([privateKeyArmored]))
    .keys[0];
  await privateKey.decrypt(passphrase);

  const options = {
    message: await openpgp.message.readArmored(message),
    privateKeys: [privateKey],
  };

  return openpgp.decrypt(options);
}
