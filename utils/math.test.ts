import { round, updateAvg, random } from './math';

describe('math', () => {
  test('round default', () => {
    const number = 2.2222222222;

    expect(round(number)).toBe(2.22);
  });

  test('round with 5 decimals', () => {
    const number = 3.333333;
    expect(round(number, 5)).toBe(3.33333);
  });

  test('round with 0 decimals', () => {
    const number = 4.444444;
    expect(round(number, 0)).toBe(4);
  });

  test('updateAvg', () => {
    const old = 3.5;
    const add = 4;
    const count = 100;

    expect(updateAvg(old, add, count)).toBe(3.504950495049505);
  });

  test('updateAvg with no count', () => {
    const old = 3.5;
    const add = 4;

    expect(updateAvg(old, add)).toBe(add);
  });

  test('random', () => {
    const x = random.next();
    const y = random.next();

    expect(x === y).toBeFalsy();
  });

  test('random with seed', () => {
    random.setSeed(1);
    const x = random.next();

    random.setSeed(1);
    const y = random.next();

    expect(x === y).toBeTruthy();
  });
});
