import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert, { AlertProps } from '@material-ui/lab/Alert';
import {
  createContext,
  ReactNode,
  SyntheticEvent,
  useContext,
  useRef,
  useState,
} from 'react';
import uuid4 from './uuid';

type Notification = {
  id?: string;
  message: string;
  severity: 'error' | 'success' | 'info' | 'warning';
  action?: ReactNode;
  date?: Date;
  key?: string;
};

type NotificationQueue = Array<Notification>;

type NotifcationContextProps = {
  add: (notification: Notification) => void;
  remove: (key: string) => void;
  removeAll: () => void;
  queue: NotificationQueue;
};

const NotificationContext = createContext<NotifcationContextProps>({
  add: () => null,
  remove: () => null,
  removeAll: () => null,
  queue: [],
});

/*
 * Notification Obejct {
 *  message: String,
 *  action: React Element,
 *  severity: One of ["error", "warning", "info", "success"]
 * }
 */
export function useNotifications(): [
  NotifcationContextProps['add'],
  NotifcationContextProps['remove'],
  NotifcationContextProps['removeAll'],
  Notification[]
] {
  const ctx = useContext(NotificationContext);
  if (!ctx) {
    throw new Error(
      'useNotifications can only be used in a NotiviationProvider.'
    );
  }
  return [ctx.add, ctx.remove, ctx.removeAll, ctx.queue];
}

function Alert(props: AlertProps) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export type NotificaitonProviderProps = {
  children: ReactNode;
};
export function NotificationProvider({ children }: NotificaitonProviderProps) {
  const [open, setOpen] = useState(false);
  const [notification, setNotification] = useState<Notification | undefined>();
  const queue = useRef<NotificationQueue>([]);

  function add(notification: Notification) {
    queue.current.push({
      ...notification,
      date: new Date(),
      key: uuid4(),
    });

    if (open) {
      setOpen(false);
    } else {
      processQueue();
    }
  }

  function remove(key: string) {
    queue.current = queue.current.filter((n) => n.key === key);
    // remove notification with string
  }

  function removeAll() {
    queue.current = [];
  }

  function processQueue() {
    if (queue.current.length > 0) {
      const notification = queue.current.shift();
      setNotification(notification);
      setOpen(true);
    }
  }

  function handleClose(_: SyntheticEvent, reason?: string) {
    if (reason === 'clickaway') {
      return;
    }

    setOpen(false);
  }

  function handleExited() {
    processQueue();
  }

  return (
    <NotificationContext.Provider
      value={{ add, remove, removeAll, queue: queue.current }}
    >
      {children}
      <Snackbar
        key={notification?.id}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        open={open}
        autoHideDuration={5000}
        onExited={handleExited}
        onClose={handleClose}
      >
        <Alert
          severity={notification?.severity || 'info'}
          onClose={handleClose}
          action={notification?.action}
        >
          {notification?.message}
        </Alert>
      </Snackbar>
    </NotificationContext.Provider>
  );
}
