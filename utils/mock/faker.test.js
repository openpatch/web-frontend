import faker from './faker';

describe('faker', () => {
  test('gen', () => {
    const username = faker.gen.username;
    expect(username).toBeTruthy();
  });

  test('genList', () => {
    const usernames = faker.genList.username(20);
    expect(usernames.length).toBe(20);
  });

  test('define', () => {
    faker.define('test', () => faker.gen.username());
    expect(faker.gen.test()).toBeTruthy();
  });

  test('random', () => {
    const x = faker.helper.random();
    const y = faker.helper.random();

    expect(x === y).toBeFalsy();
  });

  test('with seed', () => {
    faker.seed = 1;
    const x = faker.helper.random();

    faker.seed = 1;
    const y = faker.helper.random();

    expect(x === y).toBeTruthy();
  });
});
