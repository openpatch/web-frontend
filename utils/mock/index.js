import MockAdapter from 'axios-mock-adapter';

import api from '../api';
import faker from './faker';
import { convertFromRaw } from '../editor';

faker.define('draft', (words) => convertFromRaw(faker.gen.text(words)));

faker.define('member', () => ({
  id: faker.gen.uuid(),
  username: faker.gen.username(),
  fullName: faker.gen.fullName(),
}));

faker.define('status', () =>
  faker.helper.randomItem(['draft', 'pilot', 'faulty'])
);

faker.define('itemVersion', () => ({
  version: 1,
  status: faker.gen.status(),
}));

faker.define('item', () => ({
  id: faker.gen.uuid(),
  name: faker.gen.name(),
  versions: faker.genList.itemVersion(1),
  member: faker.gen.member(),
  createdOn: faker.gen.date(),
  publicDescription: faker.gen.text(20),
}));

const mockApi = () => {
  const mockApi = new MockAdapter(api);

  /** Mock Authentification - Member **/
  const membersBaseURL = `${process.env.NEXT_API_AUTHENTIFICATION_BROWSER}/v1/members`;
  mockApi.onGet(membersBaseURL).reply(200, {
    members: faker.genList.member(20),
  });
  const memberByIdReg = new RegExp(`${membersBaseURL}/*`);
  mockApi.onGet(memberByIdReg).reply(() => [
    200,
    {
      member: faker.gen.member(),
    },
  ]);

  /** Mock Itembank - Item **/
  const itemsBaseURL = `${process.env.NEXT_API_ITEMBANK_BROWSER}/v1/items`;
  mockApi.onGet(itemsBaseURL).reply(() => [
    200,
    {
      items: faker.genList.item(20),
    },
  ]);
  const itemByIdReg = new RegExp(`${itemsBaseURL}/[A-za-z0-9-]+`);
  mockApi.onGet(itemByIdReg).reply(() => [
    200,
    {
      item: faker.gen.item(),
    },
  ]);
  const itemVersionByIdReg = new RegExp(
    `${itemsBaseURL}/[A-za-z0-9-]+/version/[A-za-z0-9-]`
  );
  mockApi.onGet(itemVersionByIdReg).reply(() => [
    200,
    {
      itemVersion: faker.gen.itemVersion(),
    },
  ]);
};

const mockRouter = ({ callback }) => {
  const actionWithPromiseAndCallback = (e) => {
    callback(e);
    return actionWithPromise(e);
  };
  const actionWithPromise = () => {
    // we need to return promise because it is needed by Link.linkClicked
    return new Promise((resolve) => resolve());
  };
  return {
    push: actionWithPromiseAndCallback,
    replace: actionWithPromiseAndCallback,
    prefetch: actionWithPromise,
    locale: 'en',
  };
};

export { faker, mockApi, mockRouter };
