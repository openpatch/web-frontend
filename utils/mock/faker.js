import { uuidv4 } from '../uuid';
import { getLoremIpsum } from '../editor';
import { usernames, names, fullNames } from './fakerData';

const faker = (() => {
  const seed = Math.random();
  const generators = {};
  const listGenerators = {};
  const helper = {};
  const define = (name, generator) => {
    generators[name] = generator;
    listGenerators[name] = (times) => {
      const fakeData = [];
      while (times > 0) {
        fakeData.push(generator());
        times--;
      }
      return fakeData;
    };
  };

  return {
    seed,
    gen: generators,
    genList: listGenerators,
    define,
    helper,
  };
})();

faker.helper.random = () => {
  const x = Math.sin(faker.seed++) * 10000;
  return x - Math.floor(x);
};

faker.helper.randomItem = (items) =>
  items[Math.floor(faker.helper.random() * items.length)];

faker.define(
  'rgb',
  () => '#' + ('000000' + this.integer(0, 16777216).toString(16)).slice(-6)
);
faker.define('float', (min, max) => min + faker.helper.random() * (max - min));
faker.define('integer', (min, max) => Math.round(faker.gen.float(min, max)));
faker.define('uuid', () => uuidv4());
faker.define('username', () => faker.helper.randomItem(usernames));
faker.define('draft', () => getLoremIpsum());
faker.define('name', () => faker.helper.randomItem(names));
faker.define('fullName', () => faker.helper.randomItem(fullNames));
faker.define('boolean', () => Math.random() > 0.5);
faker.define(
  'date',
  () => new Date(+new Date() - Math.floor(faker.helper.random() * 10000000000))
);
faker.define('text', (words) => {
  let text = '';
  let alphabet = 'A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,r,s,t,u,v,w,x,y,z'.split(
    ','
  );
  let wordCount = words;
  for (let i = 0; i < wordCount; i++) {
    let rand = null;
    for (let x = 0; x < 7; x++) {
      rand = Math.floor(Math.random() * alphabet.length);
      text += alphabet[rand];
    }
    if (i < wordCount - 1) text += ' ';
    else text += '.';
  }
  return text;
});

export default faker;
