import axios, {
  AxiosError,
  AxiosInstance,
  AxiosTransformer,
  CancelTokenStatic,
} from 'axios';
import createAuthRefreshInterceptor from 'axios-auth-refresh';
import humps from 'humps';
import { GetServerSidePropsContext } from 'next';
import Router from 'next/router';
import { getCookie, setCookie } from './cookies';

export type QuerySortDirection = 'asc' | 'desc';
export type QueryFilterKey =
  | 'isNull'
  | 'isNotNull'
  | { like: string }
  | { equals: string }
  | { in: string[] | number[] }
  | { notIn: string[] | number[] }
  | { notEqualIn: string[] | number[] }
  | { gt: number }
  | { gte: number }
  | { lt: number }
  | { lte: number };

export type QuerySingleFilter = Record<string, QueryFilterKey>;
export type QueryOrFilter = Record<'or', QuerySingleFilter>;
export type QueryAndFilter = Record<'and', QuerySingleFilter>;

export type QueryFilter = QuerySingleFilter | QueryOrFilter | QueryAndFilter;
export type QuerySort = Record<string, QuerySortDirection>;
export type QueryLimit = number;
export type QueryOffset = number;

export type Query =
  | {
      filter: QueryFilter;
    }
  | {
      sort: QuerySort;
    }
  | {
      limit: QueryLimit;
      offset: QueryOffset;
    }
  | {
      filter: QueryFilter;
      limit: QueryLimit;
      offset: QueryOffset;
    }
  | {
      sort: QuerySort;
      limit: QueryLimit;
      offset: QueryOffset;
    }
  | {
      filter: QueryFilter;
      sort: QuerySort;
      limit: QueryLimit;
      offset: QueryOffset;
    }
  | Record<string, never>;

export function makeFilter(query: string, keys: string[]): QueryFilter {
  const filter: QueryOrFilter = {
    or: {},
  };
  const like = `%${query}%`;
  keys.forEach((key) => {
    filter.or[key] = { like: like };
  });
  return filter;
}

function getAccessToken(ctx: GetServerSidePropsContext | null) {
  return getCookie(ctx, 'accessToken');
}

function setAccessToken(
  ctx: GetServerSidePropsContext | null = null,
  token: string
) {
  setCookie(ctx, 'accessToken', token);
}

function getRefreshToken(ctx: GetServerSidePropsContext | null = null) {
  return getCookie(ctx, 'refreshToken');
}

export const makeApi = (
  ctx: GetServerSidePropsContext | null = null
): AxiosInstance & { CancelToken?: CancelTokenStatic } => {
  const api: AxiosInstance & { CancelToken?: CancelTokenStatic } = axios.create(
    {
      timeout: 50000,
      transformResponse: [
        ...(axios.defaults.transformResponse as AxiosTransformer[]),
        (data) => humps.camelizeKeys(data),
      ],
      transformRequest: [
        (data) => {
          if (typeof window !== 'undefined' && data instanceof FormData) {
            return data;
          }
          return humps.decamelizeKeys(data);
        },
        ...(axios.defaults.transformRequest as AxiosTransformer[]),
      ],
    }
  );

  api.interceptors.response.use(
    (response) => {
      return response;
    },
    (error) => {
      if (error?.response?.status === 403) {
        if (typeof window === 'undefined' && ctx !== null) {
          ctx.res?.writeHead(302, { Location: '/access-not-allowed' });
          ctx.res?.end();
        } else {
          Router.push('/access-not-allowed');
        }
      }
      return Promise.reject(error);
    }
  );

  api.interceptors.request.use((request) => {
    // because of hooks sometime the request is done to early,
    // therefore undefined will be in the url, which will lead
    // to an invalid request. So we will just dismis the request.
    if (!request || !request.url) {
      return Promise.reject();
    }
    if (
      request.url.search('/undefined') !== -1 ||
      request.url.search('/null') !== -1
    ) {
      return Promise.reject();
    }

    request.headers['Authorization'] = `Bearer ${getAccessToken(ctx)}`;
    request.headers['Access-Control-Allow-Origin'] = '*';
    return request;
  });

  async function refreshAuthLogic(failedRequest: any) {
    if (!failedRequest || !failedRequest.url) {
      return Promise.reject();
    }
    return axios
      .get(`${process.env.NEXT_API_AUTHENTIFICATION_BROWSER}/v1/refresh`, {
        timeout: 10000,
        headers: {
          Authorization: `Bearer ${getRefreshToken(ctx)}`,
        },
      })
      .then((response) => {
        const accessToken = response.data.access_token;
        setAccessToken(ctx, accessToken);
        failedRequest.response.config.headers[
          'Authorization'
        ] = `Bearer ${accessToken}`;
        return Promise.resolve();
      })
      .catch((e) => {
        if (typeof window === 'undefined' && ctx) {
          if (ctx.resolvedUrl !== '/login') {
            ctx.res?.writeHead(302, {
              Location: `/login?redirect=${ctx.resolvedUrl}`,
            });
          } else {
            ctx.res?.writeHead(302, { Location: '/login' });
          }
          ctx.res?.end();
        } else {
          if (window.location.pathname !== '/login') {
            Router.push(
              `/login?redirect=${window.location.pathname}${window.location.search}`
            );
          } else {
            Router.push('/login');
          }
        }
        return Promise.reject(e);
      });
  }

  createAuthRefreshInterceptor(api, refreshAuthLogic);

  api.CancelToken = axios.CancelToken;
  return api;
};

export type ApiError<T> = AxiosError<T>;

export function isApiError<T>(error: any): error is ApiError<T> {
  return (error as AxiosError).isAxiosError !== undefined;
}

export default makeApi();
