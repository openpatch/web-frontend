import { useUtils } from '@material-ui/pickers';
import {
  format as f,
  formatDistance as fD,
  formatDistanceStrict as fDS,
  formatRelative as fR,
} from 'date-fns';
import { de, enGB } from 'date-fns/locale';

export const locales = {
  en: enGB,
  de,
};

export const useDateUtils = () => {
  const { locale } = useUtils();

  const formatDistance: typeof fD = (d1, d2, options) =>
    fD(d1, d2, { locale, ...options });
  const format: typeof f = (d, format, options) =>
    f(d, format, { locale, ...options });
  const formatDistanceStrict: typeof fDS = (d1, d2, options) =>
    fDS(d1, d2, { locale, ...options });
  const formatRelative: typeof fR = (d1, d2, options) =>
    fR(d1, d2, { locale, ...options });
  const utcDate = () => {
    const now = new Date();
    const utc = new Date(now.getTime() + now.getTimezoneOffset() * 60000);
    return utc;
  };

  return {
    locale,
    formatDistance,
    format,
    formatDistanceStrict,
    formatRelative,
    utcDate,
  };
};
