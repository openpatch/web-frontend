import { AxiosRequestConfig, AxiosResponse } from 'axios';
import humps from 'humps';
import Router from 'next/router';
import useSWR, {
  ConfigInterface,
  SWRInfiniteConfigInterface,
  SWRInfiniteResponseInterface,
  useSWRInfinite,
} from 'swr';
import { mutateCallback, revalidateType } from 'swr/dist/types';
import api from './api';

export type RequestOptions<T> = ConfigInterface<AxiosResponse<T>> & {
  axios?: AxiosRequestConfig;
};

export type RequestReturn<T> = {
  data?: T;
  response?: AxiosResponse<T>;
  error: any;
  isValidating: boolean;
  revalidate: revalidateType;
  mutate: mutateCallback;
};

export type RequestInfiniteOptions<R> = SWRInfiniteConfigInterface<
  AxiosResponse<R>,
  any
>;

export const useRequest = <T>(
  request: string | null | { url: string; params?: any },
  { initialData, axios = {}, ...config }: RequestOptions<T> = {}
): RequestReturn<T> => {
  let f;
  if (typeof request === 'string') {
    f = () => api({ url: request, ...axios });
  } else if (request === null) {
    f = null;
  } else {
    if (request?.params) {
      request.params = humps.decamelizeKeys(request.params);
    }
    f = () => api({ ...request, ...axios });
  }
  const { data: response, error, isValidating, mutate, revalidate } = useSWR<
    AxiosResponse<T>,
    any
  >(request && JSON.stringify(request), f, {
    ...config,
    initialData: initialData,
  });

  if (error?.code === 401) {
    Router.push('/login');
  }

  return {
    data: response && response.data,
    response,
    error,
    isValidating,
    revalidate,
    mutate,
  };
};

export type Selector<R, T> = (d: R) => T[];
const getKey = <R, T>(
  pageSize: number,
  url: string,
  params: any,
  selector: Selector<R, T>
) => (pageIndex: number, previousPageData: AxiosResponse<R> | null) => {
  const offset = pageIndex * pageSize;
  const limit = pageSize;

  if (!params || !params?.query) {
    params = {
      query: {
        offset,
        limit,
      },
    };
  } else {
    params.query.offset = offset;
    params.query.limit = limit;
  }
  params = humps.decamelizeKeys(params);

  if (
    previousPageData &&
    previousPageData.data &&
    selector(previousPageData.data) &&
    !selector(previousPageData.data)?.length
  ) {
    return null;
  }

  return url + new URLSearchParams(params).toString();
};

export type RequestInfiniteReturn<R, T> = Omit<
  RequestReturn<T>,
  'data' | 'response'
> & {
  data: T[] | null;
  responses?: AxiosResponse<R>[];
  size: number;
  setSize: SWRInfiniteResponseInterface['setSize'];
  isLoadingMore?: boolean;
  isReachingEnd?: boolean;
  isEmpty?: boolean;
};

export const useRequestInfinite = <R, T>(
  url: string,
  params: any,
  pageSize: number,
  selector: Selector<R, T>,
  config: RequestInfiniteOptions<R> = {}
): RequestInfiniteReturn<R, T> => {
  const key = getKey(pageSize, url, params, selector);
  const {
    data: responses,
    error,
    isValidating,
    mutate,
    size,
    setSize,
    revalidate,
  } = useSWRInfinite<AxiosResponse<R>, any>(key, api, config);

  if (error?.code === 401) {
    Router.push('/login');
  }

  const data = responses && responses.map((r) => r.data);

  const selectedData: T[] | null = data
    ? ([] as T[]).concat(...data.map((d) => selector(d)))
    : null;
  const isLoadingInitialData = !data && !error;
  const isLoadingMore =
    isLoadingInitialData ||
    (size > 0 && data && typeof data[size - 1] === 'undefined');
  const isEmpty = !selectedData || selectedData?.length === 0;
  const isReachingEnd =
    isEmpty || (data && selector(data[data.length - 1])?.length < pageSize);

  return {
    data: selectedData,
    responses,
    error,
    isValidating,
    mutate,
    size,
    setSize,
    revalidate,
    isLoadingMore,
    isReachingEnd,
    isEmpty,
  };
};
