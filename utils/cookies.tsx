import { GetServerSidePropsContext } from 'next';
import nookies from 'nookies';

export const setCookie = (
  ctx: GetServerSidePropsContext | null = null,
  key: string,
  value: string,
  options: any = {}
): void => {
  nookies.set(ctx, key, value, {
    maxAge: 30 * 24 * 60 * 60,
    path: '/',
    domain:
      process.env.NODE_ENV === 'production'
        ? process.env.NEXT_COOKIE_DOMAIN
        : undefined,
    secure: process.env.NODE_ENV === 'production' ? true : undefined,
    ...options,
  });
};

export const removeCookie = (
  ctx: GetServerSidePropsContext | null = null,
  key: string,
  options: any = {}
): void => {
  nookies.destroy(ctx, key, {
    path: '/',
    domain:
      process.env.NODE_ENV === 'production'
        ? process.env.NEXT_COOKIE_DOMAIN
        : undefined,
    ...options,
  });
};
export const getCookie = (
  ctx: GetServerSidePropsContext | null = null,
  key: string
): string => {
  return nookies.get(ctx)[key];
};
