export function updateAvg(old: number, add: number, count?: number) {
  if (!count) {
    return add;
  }
  return (old * count + add) / (count + 1);
}

export function round(number: number, decimals = 2) {
  const factor = Math.pow(10, decimals);
  return Math.round(number * factor) / factor;
}

function RandomGenerator(seed = Math.random()) {
  function setSeed(newSeed: number) {
    seed = newSeed;
  }

  function getSeed() {
    return seed;
  }

  function next() {
    const x = Math.sin(seed++) * 10000;
    return x - Math.floor(x);
  }

  return {
    getSeed,
    setSeed,
    next,
  };
}

export const random = RandomGenerator();
