import { useInterval } from './interval';
import { renderHook } from '@testing-library/react-hooks';

describe('interval', () => {
  test('useInterval callback', () => {
    jest.useFakeTimers();
    const callback = jest.fn();

    renderHook(() => useInterval(callback, 1000));

    expect(callback).toHaveBeenCalledTimes(0);

    jest.advanceTimersByTime(1000);
    expect(callback).toHaveBeenCalledTimes(1);

    jest.advanceTimersByTime(2000);
    expect(callback).toHaveBeenCalledTimes(3);
  });
});
