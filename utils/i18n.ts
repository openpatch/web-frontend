import { i18n } from '@lingui/core';
import { de, en } from 'make-plural/plurals';

export type Locale = 'de' | 'en';

export const languages: Record<string, string> = {
  de: `Deutsch`,
  en: `English`,
};

export { i18n };

export const defaultLocale: Locale = 'en';

i18n.loadLocaleData({
  de: { plurals: de },
  en: { plurals: en },
});

if (process.env.NODE_ENV !== 'test') {
  dynamicActivate('en');
}

export async function dynamicActivate(locale: Locale) {
  if (process.env.NODE_ENV === 'test') {
    const messages = {};
    i18n.load(locale, messages);
    i18n.activate(locale);
  } else {
    const { messages } = await import(`../locale/${locale}/messages`);
    i18n.load(locale, messages);
    i18n.activate(locale);
  }
}
