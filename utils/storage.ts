import { useCallback, useEffect, useState } from 'react';

type StorageEventPayload<T> = {
  key: string;
  value: T | null;
};

let SafeCustomEvent: typeof CustomEvent;
if (typeof window === 'undefined') {
  // fake it until you make it!
  // aka fake window on the server
  class CustomEvent {}
  SafeCustomEvent = (CustomEvent as unknown) as any;
} else {
  SafeCustomEvent = window.CustomEvent;
}

class StorageEvent<T> extends SafeCustomEvent<StorageEventPayload<T>> {
  bubbles = false;
  cancelable = false;
  constructor(
    event: string,
    params: CustomEventInit<StorageEventPayload<T>> = {}
  ) {
    super(event, {
      ...params,
    });
  }
}

class LocalStorageChanged<T> extends StorageEvent<T> {
  constructor(payload: StorageEventPayload<T>) {
    super('onLocalStorageChange', { detail: payload });
  }
}

class SessionStorageChanged<T> extends StorageEvent<T> {
  constructor(payload: StorageEventPayload<T>) {
    super('onSessionStorageChange', { detail: payload });
  }
}

function tryParse<T>(value: string | null): T | null {
  if (!value) {
    return null;
  }
  try {
    return JSON.parse(value);
  } catch {
    return null;
  }
}

function writeLocalStorage<T>(key: string, value: T) {
  try {
    localStorage.setItem(
      key,
      typeof value === 'object' ? JSON.stringify(value) : `${value}`
    );
    window.dispatchEvent(
      new LocalStorageChanged<T>({ key, value })
    );
  } catch (error) {
    console.log(error);
  }
}

function deleteLocalStorage(key: string) {
  localStorage.removeItem(key);
  window.dispatchEvent(new LocalStorageChanged({ key, value: null }));
}

export function useLocalStorage<T>(
  key: string,
  initialValue?: T
): [T | null, (value: T) => void, () => void] {
  const [storedValue, setStoredValue] = useState<T | null>(
    initialValue || null
  );

  const onLocalStorageChange = useCallback(
    (event) => {
      if (event.type === 'onLocalStorageChange') {
        if (event.detail.key === key) {
          setStoredValue(event.detail.value);
        }
      } else if (event.key === key) {
        setStoredValue(event.newValue);
      }
    },
    [key]
  );

  useEffect(() => {
    const item = localStorage.getItem(key);
    setStoredValue(tryParse<T>(item) || initialValue || null);
  }, []);

  useEffect(() => {
    window.addEventListener('onLocalStorageChange', onLocalStorageChange);
    window.addEventListener('storage', onLocalStorageChange);

    return () => {
      window.removeEventListener('onLocalStorageChange', onLocalStorageChange);
      window.removeEventListener('storage', onLocalStorageChange);
    };
  }, [onLocalStorageChange]);

  return [
    storedValue,
    (value: T) => writeLocalStorage<T>(key, value),
    () => deleteLocalStorage(key),
  ];
}

function writeSessionStorage<T>(key: string, value: T) {
  try {
    sessionStorage.setItem(
      key,
      typeof value === 'object' ? JSON.stringify(value) : `${value}`
    );
    window.dispatchEvent(new SessionStorageChanged({ key, value }));
  } catch (error) {
    console.log(error);
  }
}

function deleteSessionStorage(key: string) {
  sessionStorage.removeItem(key);
  window.dispatchEvent(new SessionStorageChanged({ key, value: null }));
}

export function useSessionStorage<T>(
  key: string,
  initialValue?: T
): [T | null, (value: T) => void, () => void] {
  const [storedValue, setStoredValue] = useState<T | null>(
    initialValue || null
  );

  const onSessionStorageChange = useCallback(
    (event) => {
      if (event.type === 'onSessionStorageChange') {
        if (event.detail.key === key) {
          setStoredValue(event.detail.value);
        }
      } else if (event.key === key) {
        setStoredValue(event.newValue);
      }
    },
    [key]
  );

  useEffect(() => {
    const item = sessionStorage.getItem(key);
    setStoredValue(tryParse(item) || initialValue || null);
  }, []);

  useEffect(() => {
    window.addEventListener('onSessionStorageChange', onSessionStorageChange);
    window.addEventListener('storage', onSessionStorageChange);

    return () => {
      window.removeEventListener(
        'onSessionStorageChange',
        onSessionStorageChange
      );
      window.removeEventListener('storage', onSessionStorageChange);
    };
  }, [onSessionStorageChange]);

  return [
    storedValue,
    (value: T) => writeSessionStorage<T>(key, value),
    () => deleteSessionStorage(key),
  ];
}
