const { default: I18nProvider } = require('./components/I18nProvider');

export const TestingProvider: React.ComponentType = ({ children }) => (
  <I18nProvider>{children}</I18nProvider>
);
