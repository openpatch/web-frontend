module.exports = {
  watchPlugins: ['jest-runner-eslint/watch-fix'],
  collectCoverageFrom: [
    '(components|pages|utils|formats|services)/**/*.{js,jsx,ts,tsx}',
    '!**/*.d.ts',
    '!**/node_modules/**',
    '!**/*.test.{js,ts,tsx}',
    '!**/*.stories.js',
    '!**/public/**',
    '!**/storybook-static/**',
  ],
  projects: [
    {
      displayName: 'test',
      setupFilesAfterEnv: ['<rootDir>/setupTests.js'],
      testPathIgnorePatterns: [
        '/node_modules/',
        '/.next/',
        '/utils/test.js',
        'next.config.js',
      ],
      testMatch: ['<rootDir>/**/*.test.{js,ts,tsx}'],
      transform: {
        '^.+\\.(js|jsx|ts|tsx)$': '<rootDir>/node_modules/babel-jest',
        '^.+\\.css$': '<rootDir>/config/jest/cssTransform.js',
      },
      transformIgnorePatterns: [
        '/node_modules/',
        '^.+\\.module\\.(css|sass|scss)$',
      ],
      moduleNameMapper: {
        '^.+\\.module\\.(css|sass|scss)$': 'identity-obj-proxy',
        '@services/(.*)': '<rootDir>/services/$1',
        '@utils/(.*)': '<rootDir>/utils/$1',
        '@components/(.*)': '<rootDir>/components/$1',
        '@formats/(.*)': '<rootDir>/formats/$1',
      },
      moduleDirectories: ['node_modules'],
    },
    {
      runner: 'jest-runner-eslint',
      displayName: 'lint',
      testMatch: [
        '<rootDir>/(components|pages|formats|utils|services)/**/*.{js,ts,tsx}',
      ],
      testPathIgnorePatterns: [
        'sentry.js',
        '/pages/playground/',
        '/public/',
        '/config/',
        '/node_modules/',
        'next.config.js',
        '/.next/',
        '/utils/test.js',
        'jest.config.js',
      ],
    },
  ],
};
